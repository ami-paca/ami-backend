import sbt._

object Version {
  lazy val amibackend = "0.1"
  lazy val scalaTest = "3.1.1"
  lazy val scalaVersion = "2.12.10"
  lazy val synaptixVersion = "0.1.11-SNAPSHOT"
  lazy val stanfordNLP = "4.0.0"
  lazy val spark = "3.0.1"
  lazy val alpakkaXml = "1.0.2"
  lazy val scalaLangMod = "1.1.1"
  lazy val dl4j = "1.0.0-beta7"
  lazy val alpakkaCsv = "2.0.2"
  lazy val hnswlib = "0.0.46"

  // "0.1.9-SNAPSHOT"
}

object Library {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val synaptixCore = "com.mnemotix" %% "synaptix-core" % Version.synaptixVersion
  lazy val synaptixHTTP = "com.mnemotix" %% "synaptix-http-toolkit" % Version.synaptixVersion
  lazy val synaptixCache = "com.mnemotix" %% "synaptix-cache-toolkit" % Version.synaptixVersion
  lazy val synaptixIndexing = "com.mnemotix" %% "synaptix-indexing-toolkit" % Version.synaptixVersion
  lazy val lifty = "com.mnemotix" %% "analytix-lifty" % Version.synaptixVersion
  lazy val synaptixRdfToolKit = "com.mnemotix" %% "synaptix-rdf-toolkit" % Version.synaptixVersion
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % "1.2.3"
  lazy val quartz = "org.quartz-scheduler" % "quartz" % "2.3.2"
  lazy val play = "com.typesafe.play" %% "play" % "2.8.2"
  lazy val analytixCommons = "com.mnemotix" %% "analytix-commons" % Version.synaptixVersion
  lazy val analytixPercolation = "com.mnemotix" %% "analytix-percolation" % Version.synaptixVersion
  lazy val analytixNlp = "com.mnemotix" %% "analytix-nlp" % Version.synaptixVersion
  lazy val akkaHttpPlayJson = "de.heikoseeberger" %% "akka-http-play-json" % "1.29.1"
  lazy val locationtechJts = "org.locationtech.jts" % "jts" % "1.17.1" pomOnly()
  lazy val alpakkaCsv = "com.lightbend.akka" %% "akka-stream-alpakka-csv" % "2.0.2"
  lazy val stanfordNLP = "edu.stanford.nlp" % "stanford-corenlp" % Version.stanfordNLP
  lazy val stanfordNLPFrench = "edu.stanford.nlp" % "stanford-corenlp" % Version.stanfordNLP classifier "models-french"
  lazy val amqpLib = "com.mnemotix" %% "synaptix-amqp-toolkit" % Version.synaptixVersion
  lazy val breeze = "org.scalanlp" %% "breeze" % "1.1"
  lazy val breezeNative = "org.scalanlp" %% "breeze-natives" % "1.1"
  lazy val alpakkaXml = "com.lightbend.akka" %% "akka-stream-alpakka-xml" % Version.alpakkaXml
  lazy val scalaLangMod = "org.scala-lang.modules" %% "scala-xml" % Version.scalaLangMod
  lazy val jsoup = "org.jsoup" % "jsoup" % "1.13.1"
  lazy val dl4j = "org.deeplearning4j" % "deeplearning4j-core" % Version.dl4j
  lazy val dl4jnn = "org.deeplearning4j" % "deeplearning4j-nn" % Version.dl4j
  lazy val dl4jmodel = "org.deeplearning4j" % "deeplearning4j-modelimport" % Version.dl4j
  lazy val dl4jNlp = "org.deeplearning4j" % "deeplearning4j-nlp" % Version.dl4j
  lazy val nd4fj = "org.nd4j" % "nd4j-native" % Version.dl4j
  lazy val hnswlib = "com.github.jelmerk" % "hnswlib-core" % Version.hnswlib
  lazy val hnswlibScala = "com.github.jelmerk" %% "hnswlib-scala" % Version.hnswlib

}





