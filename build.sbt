import Library.{akkaHttpPlayJson, lifty, _}
import sbt.Keys.{libraryDependencies, resolvers}

val meta = """META.INF(.)*""".r

lazy val commons = Seq(
  scalaVersion := Version.scalaVersion,
  version := Version.amibackend,
  developers := List(
    Developer(
      id = "prlherisson",
      name = "Pierre-René Lhérisson",
      email = "pr.lherisson@gmail.com",
      url = url("http://www.mnemotix.com")
    ),
    Developer(
      id = "ndelaforge",
      name = "Nicolas Delaforge",
      email = "nicolas.delaforge@mnemotix.com",
      url = url("http://mnemotix.com")
    ),
  ),
  credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),
  pomIncludeRepository := { _ => false },
  publishArtifact := true,
  publishMavenStyle := true,
  publishTo in ThisBuild := {
    val nexus = "https://nexus.mnemotix.com/repository"
    if (isSnapshot.value) Some("MNX Nexus" at nexus + "/maven-snapshots/")
    else Some("MNX Nexus" at nexus + "/maven-releases/")
  },
  resolvers ++= Seq(
    Resolver.mavenLocal,
    Resolver.typesafeRepo("releases"),
    Resolver.typesafeIvyRepo("releases"),
    Resolver.sbtPluginRepo("releases"),
    Resolver.bintrayRepo("owner", "repo"),
    "MNX Nexus (releases)" at "https://nexus.mnemotix.com/repository/maven-releases/",
    "MNX Nexus (snapshots)" at "https://nexus.mnemotix.com/repository/maven-snapshots/"
  ),
  test in assembly := {},
  assemblyMergeStrategy in assembly := {
    case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
    case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
    //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
    case n if n.endsWith(".conf") => MergeStrategy.concat
    case n if n.endsWith(".properties") => MergeStrategy.concat
    case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
    case PathList("META-INF", xs@_*) => MergeStrategy.discard
    case meta(_) => MergeStrategy.discard
    case x => MergeStrategy.first
  }
)

lazy val commonsModule = (project in file("modules/commons")).settings(
  commons,
  name := "ami-commons",
  description := "commons",
    libraryDependencies ++= Seq(
      analytixCommons,
      synaptixRdfToolKit,
      lifty
  )
)

lazy val connectors = (project in file("modules/connectors")).settings(
  commons,
  name := "ami-connectors",
  description := "connectors to api",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixCore,
    synaptixHTTP,
    synaptixIndexing,
    akkaHttpPlayJson,
    synaptixCache,
    logbackClassic,
    synaptixRdfToolKit,
    quartz,
    locationtechJts
  ),
  mainClass in(Compile, run) := Some("com.mnemotix.ami.connectors.services.JobRunner"),
  mainClass in assembly := Some("com.mnemotix.ami.connectors.services.JobRunner"),
  assemblyJarName in assembly := "ami-connectors.jar",
  imageNames in docker := Seq(
    ImageName(
      namespace = Some("registry.gitlab.com/ami-paca/ami-backend"),
      repository = name.value,
      tag = Some(version.value)
    )
  ),
  buildOptions in docker := BuildOptions(cache = false),
  dockerfile in docker := {
    val artifact: File = assembly.value
    val artifactTargetPath = s"/app/${artifact.name}"

    new Dockerfile {
      from("adoptopenjdk/openjdk11:alpine-slim")
      add(artifact, artifactTargetPath)
      entryPoint("java", "-jar", artifactTargetPath)
    }
  }
).dependsOn(commonsModule).aggregate(commonsModule).enablePlugins(DockerPlugin)

lazy val datasudAnnotator = (project in file("modules/datasud-annotator")).settings(
  commons,
  name := "ami-datasud-annotator",
  description := "ami datasud annotator",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixCore,
    synaptixRdfToolKit,
    play,
    quartz,
    lifty,
    alpakkaCsv,
    synaptixIndexing,
    analytixCommons,
    analytixPercolation,
    synaptixCache,
    "au.com.bytecode" % "opencsv" % "2.4"
  )
).dependsOn(offrePercolator).aggregate(offrePercolator)

lazy val romeAnnotator = (project in file("modules/rome-annotator")).settings(
  commons,
  name := "ami-rome-annotator",
  description := "ami rome annotator",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixCore,
    synaptixHTTP,
    akkaHttpPlayJson,
    synaptixIndexing,
    synaptixRdfToolKit,
    logbackClassic,
    quartz,
    play,
    lifty,
    logbackClassic,
    stanfordNLPFrench,
    stanfordNLP,
    alpakkaCsv
  )
).dependsOn(commonsModule).aggregate(commonsModule)

lazy val escoAnnotator = (project in file("modules/esco-annotator")).settings(
  commons,
  name := "ami-esco-annotator",
  description := "ami esco annotator",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixCore,
    synaptixHTTP,
    akkaHttpPlayJson,
    synaptixIndexing,
    synaptixRdfToolKit,
    logbackClassic,
    quartz,
    play,
    logbackClassic,
    stanfordNLPFrench,
    stanfordNLP,
    alpakkaCsv
  )
).dependsOn(commonsModule).aggregate(commonsModule)

lazy val rncpAnnotator = (project in file("modules/rncp-annotator")).settings(
  commons,
  name := "ami-rncp-annotator",
  description := "ami rncp annotator",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixCore,
    synaptixHTTP,
    akkaHttpPlayJson,
    synaptixIndexing,
    logbackClassic,
    quartz,
    play,
    lifty,
    logbackClassic,
    stanfordNLPFrench,
    stanfordNLP,
    alpakkaCsv,
    alpakkaXml,
    scalaLangMod,
    jsoup
  )
).dependsOn(commonsModule).aggregate(commonsModule)

lazy val amiAlignement = (project in file("modules/alignement")).settings(
  commons,
  name := "ami-alignement",
  description := "ami concepts alignement",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixCore,
    synaptixHTTP,
    akkaHttpPlayJson,
    synaptixIndexing,
    synaptixRdfToolKit,
    synaptixCache,
    logbackClassic,
    quartz,
    play,
    lifty,
    logbackClassic,
    stanfordNLPFrench,
    stanfordNLP,
    alpakkaCsv,
    alpakkaXml,
    scalaLangMod,
    jsoup,
    analytixNlp,
    dl4j,
    dl4jnn,
    dl4jmodel,
    dl4jNlp,
    nd4fj,
    hnswlib,
    hnswlibScala
  )
).dependsOn(rncpAnnotator, romeAnnotator).aggregate(rncpAnnotator, romeAnnotator)

lazy val zonegeographiques = (project in file("modules/zones-geographiques")).settings(
  commons,
  name := "ami-zones-geographiques",
  description := "zones geographiques pour le projet ami",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixCore,
    synaptixRdfToolKit,
    play,
    quartz,
    lifty,
    alpakkaCsv
  ),
  resolvers += Resolver.bintrayRepo("jroper", "maven")
)

lazy val apecconceptannotator = (project in file("modules/apec-concept-annotator")).settings(
  commons,
  name := "apec-concept-annotator",
  description := "données concept metiers apec",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixCore,
    synaptixHTTP,
    akkaHttpPlayJson,
    synaptixIndexing,
    synaptixRdfToolKit,
    logbackClassic,
    quartz,
    play,
    lifty,
    logbackClassic,
    alpakkaCsv,
    stanfordNLPFrench,
    stanfordNLP
  )
)

lazy val apecdatauploader = (project in file("modules/apec-data-uploader")).settings(
  commons,
  name := "apec-data-uploader",
  description := "données emplois apec",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixRdfToolKit
  )
).dependsOn(connectors, apecconceptannotator).aggregate(connectors, apecconceptannotator)

lazy val offrePercolator = (project in file("modules/offer-percolator")).settings(
  commons,
  name := "offer-percolator",
  description := "offer percolator",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixCore,
    synaptixIndexing,
    logbackClassic,
    quartz,
    analytixCommons,
    analytixPercolation,
    synaptixCache,
    alpakkaCsv,
    hnswlib,
    hnswlibScala,
    dl4j,
    dl4jNlp,
    nd4fj
  )
).dependsOn(connectors, commonsModule).aggregate(connectors, commonsModule)

lazy val amiAnalyses = (project in file("modules/analyses")).settings(
  commons,
  name := "ami-analyzer",
  description := "modules d'analyse sur les données ami",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixCore,
    synaptixIndexing,
    quartz,
    analytixCommons,
    amqpLib,
    breeze,
    breezeNative
  ),
  mainClass in(Compile, run) := Some("com.mnemotix.ami.services.jc.AmiAnalyzer"),
  mainClass in assembly := Some("com.mnemotix.ami.services.jc.AmiAnalyzer"),
  assemblyJarName in assembly := "ami-analyzer.jar",
  imageNames in docker := Seq(
    ImageName(
      namespace = Some("registry.gitlab.com/ami-paca/ami-backend"),
      repository = name.value,
      tag = Some(version.value)
    )
  ),
  buildOptions in docker := BuildOptions(cache = false),
  dockerfile in docker := {
    val artifact: File = assembly.value
    val artifactTargetPath = s"/app/${artifact.name}"

    new Dockerfile {
      from("adoptopenjdk/openjdk10:alpine-slim")
      add(artifact, artifactTargetPath)
      entryPoint("java", "-jar", artifactTargetPath)
    }
  }
).enablePlugins(DockerPlugin)