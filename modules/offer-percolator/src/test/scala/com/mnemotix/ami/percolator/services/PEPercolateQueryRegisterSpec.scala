/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.percolator.services

import akka.stream.scaladsl.Sink
import com.mnemotix.ami.percolator.AmiPercolationSpec

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class PEPercolateQueryRegisterSpec extends AmiPercolationSpec {

  it should "extract offer from index" in  {
    val start = System.currentTimeMillis()
    val peOfferResourceExtractor  = new PEPercolateQueryMatcher()
    val fut = peOfferResourceExtractor.esOfferExtractor("dev-openemploi-offer-pe-copy", Seq("description"),  Some("dateCreation")).runWith(Sink.seq)
    val res = Await.result(fut, Duration.Inf)
    val elapsedTimeMillis = System.currentTimeMillis()-start
    println(elapsedTimeMillis/(60*1000F))
  }

  it should "percolate" ignore {
    val peOfferResourceExtractor  = new PEPercolateQueryMatcher()
    val fut = peOfferResourceExtractor.esOfferExtractor("dev-openemploi-offer-pe-copy", Seq("description"),  Some("dateCreation")).runWith(Sink.seq)
    val size = Await.result(fut, Duration.Inf).flatten.size
    val perco = peOfferResourceExtractor.percolate("dev-openemploi-offer-pe-copy", Seq("description"),  Some("dateCreation"), "dev-openemploi-skill",
      50, Some(0), Some(250), size)



    //percoIdxName: String,percolationGroupBy: Int,from: Option[Int],size: Option[Int],total: Int
  }
}
