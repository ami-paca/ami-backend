package com.mnemotix.ami.percolator.helpers

import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class PercolatorConfigSpec extends FlatSpec with Matchers {
  it should "read the config file" in {
    PercolatorConfig.peOfferIdx shouldEqual "dev-openemploi-offer-pe"
    PercolatorConfig.apecOfferIdx shouldEqual "dev-openemploi-offer-apec"
    PercolatorConfig.conceptsRomeIdx shouldEqual "dev-openemploi-concept"
    PercolatorConfig.mmURI shouldEqual "https://mindmatcher.org/ontology/"
    PercolatorConfig.mnxURI shouldEqual "http://ns.mnemotix.com/ontologies/2019/8/generic-model/"
    PercolatorConfig.amiURI shouldEqual "http://openemploi.datasud.fr/ontology/"
  }
}