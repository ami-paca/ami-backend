package com.mnemotix.ami.percolator.services

import akka.stream.scaladsl.Sink
import com.mnemotix.ami.percolator.AmiPercolationSpec
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.synaptix.index.IndexClient

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class PEOfferResourceExtractorSpec extends AmiPercolationSpec {

  it should "retrieve an Apec concpet" in {
    IndexClient.init()
    val resp = Await.result(IndexClient.matchQuery(Seq(PercolatorConfig.conceptsRomeIdx), "prefLabel", "Assistant administration des ventes"), Duration.Inf)
    println(resp.result.hits.size)
    resp.result.hits.hits.foreach(println(_))
  }

  it should "percolate" in {

  }
}