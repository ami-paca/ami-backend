/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.percolator.services

import akka.NotUsed
import akka.stream.scaladsl.{Sink, Source}
import com.mnemotix.ami.percolator.AmiPercolationSpec
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.analytix.percolation.model.PercolatorText
import com.mnemotix.analytix.percolation.services.{ESClient, ESPercolatorTextExtractor}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class CSVDumperSpec extends AmiPercolationSpec {

  it should "dump content in a csv" in {
    def offer: Seq[PercolatorText] = {
      val occupations = Seq("http://ontology.datasud.fr/openemploi/data/occupation/1940156419009be4516d6b6f978780d5",
        "http://ontology.datasud.fr/openemploi/data/occupation/511fff073ab20bab31a3eaa713e89048",
        "http://ontology.datasud.fr/openemploi/data/occupation/b0c4fb9b1ebba5a9c21ced19553a0be8",
        "http://ontology.datasud.fr/openemploi/data/occupation/9995e6bbfcc2e4af21970e248c458b41",
        "http://ontology.datasud.fr/openemploi/data/occupation/4d337978e3bdc25a6fc2d0fa54609cea",
        "http://ontology.datasud.fr/openemploi/data/occupation/42c55eeff8c8d95efc54880522c06d0b",
        "http://ontology.datasud.fr/openemploi/data/occupation/6e704ea60ee7bdccbc0325fbf1c11095",
        "http://ontology.datasud.fr/openemploi/data/occupation/2da1d4af5fc3936bf062ca95e20cdd0",
        "http://ontology.datasud.fr/openemploi/data/occupation/ec79c609392258bcacfe9f2c5ebdd1d5",
        "http://ontology.datasud.fr/openemploi/data/occupation/35bc15078fefc0ab768f425c8e6aef2f")

      val extractor = new PEOfferResourceExtractor
      extractor.init
      extractor.extractAsync.filter(r => r.eoOffer.occupation.isDefined).
        filter(r => occupations.contains(r.eoOffer.occupation.get)).map(r => PercolatorText(r.esId, r.eoOffer.description.toSeq)).toSeq
    }

    val csvDumper = new CSVDumper("modules/offer-percolator/src/test/resources/csv/micro_electronique/embedding_offer.csv", PercolatorConfig.cacheRecogsEmbeddingOfferDir)
    csvDumper.init
    val uris = offer.map(_.docId)
    csvDumper.dump(uris)
  }

  it should "dump training contents in a csv" ignore {

    implicit val esClient = new ESClient()

    def esTrainingExtractor(idxName: String, fields: Seq[String], sortField: Option[String]): Source[Seq[PercolatorText], NotUsed] = {
      lazy val espercolatorTextExtractor = new ESPercolatorTextExtractor(idxName, fields, sortField)
      espercolatorTextExtractor.init
      espercolatorTextExtractor.batchExtract
    }

    val trainingsFut = esTrainingExtractor("dev-openemploi-training", Seq("description"), Some("entityId")).runWith(Sink.seq)
    val ids = Await.result(trainingsFut, Duration.Inf).flatten.map(_.docId)
    val csvDumper = new CSVDumper("modules/offer-percolator/src/test/resources/csv/micro_electronique/embedding_trainings.csv", "")
    csvDumper.init
    csvDumper.dump(ids)
  }

  it should "dump elastic search by one in a csv" in {
    def offer: Seq[PercolatorText] = {
      val occupations = Seq("http://ontology.datasud.fr/openemploi/data/occupation/1940156419009be4516d6b6f978780d5",
        "http://ontology.datasud.fr/openemploi/data/occupation/511fff073ab20bab31a3eaa713e89048",
        "http://ontology.datasud.fr/openemploi/data/occupation/b0c4fb9b1ebba5a9c21ced19553a0be8",
        "http://ontology.datasud.fr/openemploi/data/occupation/9995e6bbfcc2e4af21970e248c458b41",
        "http://ontology.datasud.fr/openemploi/data/occupation/4d337978e3bdc25a6fc2d0fa54609cea",
        "http://ontology.datasud.fr/openemploi/data/occupation/42c55eeff8c8d95efc54880522c06d0b",
        "http://ontology.datasud.fr/openemploi/data/occupation/6e704ea60ee7bdccbc0325fbf1c11095",
        "http://ontology.datasud.fr/openemploi/data/occupation/2da1d4af5fc3936bf062ca95e20cdd0",
        "http://ontology.datasud.fr/openemploi/data/occupation/ec79c609392258bcacfe9f2c5ebdd1d5",
        "http://ontology.datasud.fr/openemploi/data/occupation/35bc15078fefc0ab768f425c8e6aef2f")

      val extractor = new PEOfferResourceExtractor
      extractor.init
      extractor.extractAsync.filter(r => r.eoOffer.occupation.isDefined).
        filter(r => occupations.contains(r.eoOffer.occupation.get)).map(r => PercolatorText(r.esId, r.eoOffer.description.toSeq)).toSeq
    }

    val csvDumper = new CSVDumper("modules/offer-percolator/src/test/resources/csv/micro_electronique/es_offer.csv", PercolatorConfig.cacheRecogsESOneOfferDir)
    csvDumper.init
    val uris = offer.map(_.docId)
    csvDumper.dump(uris)
  }

  it should "dump elastic search group in a csv" in {
    def offer: Seq[PercolatorText] = {
      val occupations = Seq("http://ontology.datasud.fr/openemploi/data/occupation/1940156419009be4516d6b6f978780d5",
        "http://ontology.datasud.fr/openemploi/data/occupation/511fff073ab20bab31a3eaa713e89048",
        "http://ontology.datasud.fr/openemploi/data/occupation/b0c4fb9b1ebba5a9c21ced19553a0be8",
        "http://ontology.datasud.fr/openemploi/data/occupation/9995e6bbfcc2e4af21970e248c458b41",
        "http://ontology.datasud.fr/openemploi/data/occupation/4d337978e3bdc25a6fc2d0fa54609cea",
        "http://ontology.datasud.fr/openemploi/data/occupation/42c55eeff8c8d95efc54880522c06d0b",
        "http://ontology.datasud.fr/openemploi/data/occupation/6e704ea60ee7bdccbc0325fbf1c11095",
        "http://ontology.datasud.fr/openemploi/data/occupation/2da1d4af5fc3936bf062ca95e20cdd0",
        "http://ontology.datasud.fr/openemploi/data/occupation/ec79c609392258bcacfe9f2c5ebdd1d5",
        "http://ontology.datasud.fr/openemploi/data/occupation/35bc15078fefc0ab768f425c8e6aef2f")

      val extractor = new PEOfferResourceExtractor
      extractor.init
      extractor.extractAsync.filter(r => r.eoOffer.occupation.isDefined).
        filter(r => occupations.contains(r.eoOffer.occupation.get)).map(r => PercolatorText(r.esId, r.eoOffer.description.toSeq)).toSeq
    }

    val csvDumper = new CSVDumper("modules/offer-percolator/src/test/resources/csv/micro_electronique/es_bulk_offer.csv", PercolatorConfig.cacheRecogsESBulkOfferDir)
    csvDumper.init
    val uris = offer.map(_.docId)
    csvDumper.dump(uris)
  }
}