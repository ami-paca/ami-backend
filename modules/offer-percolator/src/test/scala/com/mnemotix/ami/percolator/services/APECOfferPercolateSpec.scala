package com.mnemotix.ami.percolator.services

import akka.Done
import com.mnemotix.ami.percolator.AmiPercolationSpec
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class APECOfferPercolateSpec extends AmiPercolationSpec {

 it should "request skill index" in  {
  IndexClient.init()
  val qry =
   s"""
      |{
      | "from" :0, "size" : 5 ,
      | "query": {
      |   "match_all": {}
      | }
      |}
      |""".stripMargin
  val rs = Await.result(IndexClient.rawQuery(RawQuery(Seq("dev-openemploi-skill"), Json.parse(qry).as[JsObject])), Duration.Inf)
  rs.result.hits.hits.foreach { hit =>
    println(hit.sourceAsString)
  }
 }

 it should "percolate apec offer" ignore  {
  val perco = new APECOfferPercolate()
  val documents: Done = perco.requester
  println(documents)
 }



 it should "request apec offer after percolator" in {

 }

}