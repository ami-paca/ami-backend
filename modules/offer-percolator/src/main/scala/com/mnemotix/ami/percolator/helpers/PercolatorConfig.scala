package com.mnemotix.ami.percolator.helpers

import com.typesafe.config.ConfigFactory

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object PercolatorConfig {
  lazy val conf = Option(ConfigFactory.load().getConfig("percolator")).getOrElse(ConfigFactory.empty())
  lazy val peOfferIdx = conf.getString("index.peoffer.name")
  lazy val apecOfferIdx = conf.getString("index.apecoffer.name")
  lazy val conceptsRomeIdx = conf.getString("index.concepts.rome")

  lazy val mmURI = conf.getString("ontology.mm.uri")
  lazy val mnxURI = conf.getString("ontology.mnx.uri")
  lazy val amiURI = conf.getString("ontology.ami.uri")

  lazy val cacheRecogsESBulkOfferDir = conf.getString("cache.recogs.es.bulk.offer.dir")
  lazy val cacheRecogsESBulkTrainingDir = conf.getString("cache.recogs.es.bulk.training.dir")

  lazy val cacheRecogsESOneOfferDir = conf.getString("cache.recogs.es.one.offer.dir")
  lazy val cacheRecogsESOneTrainingDir = conf.getString("cache.recogs.es.one.training.dir")

  lazy val cacheRecogsEmbeddingOfferDir = conf.getString("cache.recogs.embedding.offer.dir")
  lazy val cacheRecogsEmbeddingTrainingDir = conf.getString("cache.recogs.embedding.training.dir")
}
