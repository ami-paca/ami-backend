package com.mnemotix.ami.percolator.services

import java.util.Date

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.mnemotix.ami.connectors.model.EOffer
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.ami.percolator.model.ESEoffer
import com.mnemotix.analytix.percolation.api.AbstractResourceExtractor
import com.mnemotix.analytix.percolation.exceptions.PercolationExtractException
import com.mnemotix.lifty.models.LiftyResource
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsObject, Json}

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class PEOfferResourceExtractor(implicit ec: ExecutionContext) extends LazyLogging {

  lazy val microElecQry = s"""{
                             | "_source": "description",
                             | "from" : 0, "size" : 5000,
                             | "query": {
                             |   "bool" : {
                             |    "filter" : [
                             |    {
                             |       "terms": {
                             |         "occupation":["http://ontology.datasud.fr/openemploi/data/occupation/1940156419009be4516d6b6f978780d5", "http://ontology.datasud.fr/openemploi/data/occupation/511fff073ab20bab31a3eaa713e89048","http://ontology.datasud.fr/openemploi/data/occupation/b0c4fb9b1ebba5a9c21ced19553a0be8", "http://ontology.datasud.fr/openemploi/data/occupation/9995e6bbfcc2e4af21970e248c458b41", "http://ontology.datasud.fr/openemploi/data/occupation/4d337978e3bdc25a6fc2d0fa54609cea","http://ontology.datasud.fr/openemploi/data/occupation/42c55eeff8c8d95efc54880522c06d0b", "http://ontology.datasud.fr/openemploi/data/occupation/6e704ea60ee7bdccbc0325fbf1c11095", "http://ontology.datasud.fr/openemploi/data/occupation/2da1d4af5fc3936bf062ca95e20cdd0", "http://ontology.datasud.fr/openemploi/data/occupation/ec79c609392258bcacfe9f2c5ebdd1d5", "http://ontology.datasud.fr/openemploi/data/occupation/35bc15078fefc0ab768f425c8e6aef2f"
                             |         ]
                             |       }
                             |     }
                             |     ]
                             |   }
                             | }
                             |}""".stripMargin

  lazy val idxName = PercolatorConfig.peOfferIdx

  def init = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
    IndexClient.init()
  }

  def extractAsync = {
    try {

      //IndexClient.rawQuery(RawQuery(Seq(indexName), Json.parse(microElecQry).as[JsObject]))
      requester
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the PEOffer extraction process", t)
        throw new PercolationExtractException("An error occured during the PEOffer extraction process", Some(t))
    }
  }


  def requester = {
    var eoffers = new ListBuffer[ESEoffer]()

    val size = Await.result(IndexClient.count(idxName), Duration.Inf)
    size.result.count

    val responseFirstRequester = firstRequester
    if (responseFirstRequester.isSuccess) {
      val scrollID = responseFirstRequester.result.scrollId
      responseFirstRequester.result.hits.hits.map { hit =>
        val json = Json.parse(hit.sourceAsString)
        val eoffer = json.as[EOffer]
        val esoffer = ESEoffer(hit.id, eoffer)
        eoffers.append(esoffer)
      }
      requesterHelper(scrollID, eoffers, size.result.count)
    }
    else {
      logger.error(s"Unable to execute the scroll query on index [${idxName}]")
      throw new Exception(s"Unable to execute the scroll query on index [${idxName}]")
    }
  }

  def firstRequester = {
    Await.result(IndexClient.scrollQuery(Seq(idxName), Some("3m"), 500, "dateCreation"), Duration.Inf)
  }

  def requesterHelper(scrollID: Option[String], eoffers: ListBuffer[ESEoffer], size: Long): ListBuffer[ESEoffer] = {
    if (scrollID.isDefined & eoffers.size < size) {

      val scrollResponse = Await.result(IndexClient.searchScroll(scrollID.get, None), Duration.Inf)
      if (scrollResponse.isSuccess) {
        val newScrollID: Option[String] = scrollResponse.result.scrollId
        scrollResponse.result.hits.hits.map { hit =>
          val json = Json.parse(hit.sourceAsString)
          val eoffer = json.as[EOffer]
          val esoffer = ESEoffer(hit.id, eoffer)
          eoffers.append(esoffer)
        }
        requesterHelper(newScrollID, eoffers, size)
      }
      else {
        eoffers
      }
    }
    else {
      eoffers
    }
  }


}