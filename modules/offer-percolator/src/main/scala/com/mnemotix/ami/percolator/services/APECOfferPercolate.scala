package com.mnemotix.ami.percolator.services

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{GraphDSL, RunnableGraph, Sink}
import com.mnemotix.ami.connectors.model.EOffer
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.ami.percolator.model.ESEoffer
import com.mnemotix.analytix.commons.exception.AnalytixException
import com.mnemotix.analytix.percolation.services.{ESClient, ESOutput, ESRecognizer}
import com.mnemotix.synaptix.index.IndexClient
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.{SearchHit, SearchResponse}
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class APECOfferPercolate extends LazyLogging{
  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
  implicit val executionContext = ExecutionContext.global
  implicit val esClient = new ESClient()

  lazy val apecOfferIndex = PercolatorConfig.apecOfferIdx
  lazy val recognizer = new ESRecognizer("dev-openemploi-skill")
  recognizer.init
  lazy val output = new ESOutput(apecOfferIndex)
  output.init
  lazy val limit = 500

  def requester = {
    val responseFirstRequester = firstRequester
    if (responseFirstRequester.isSuccess) {
      val scrollID = responseFirstRequester.result.scrollId
      val offers= hits(responseFirstRequester.result.hits.hits)
      val perco = Await.result(percolateAsync(offers), Duration.Inf)
      println(perco)
      val total = Await.result(
        esClient.count(apecOfferIndex), Duration.Inf
      )
      requesterHelper(scrollID, total)
    }
    else {
      logger.error(s"Unable to execute the scroll query on index [${apecOfferIndex}]")
      throw AnalytixException(s"Unable to execute the scroll query on index [${apecOfferIndex}]", None)
    }
  }

  def firstRequester = {
    Await.result(IndexClient.scrollQuery(Seq(apecOfferIndex), Some("3m"), limit, "dateCreation"), Duration.Inf)
  }

  def hits(eshits: Array[SearchHit]) = {
    eshits.map { hit =>
      val json = Json.parse(hit.sourceAsString)
      val eoffer = json.as[EOffer]
      ESEoffer(hit.id, eoffer)
    }
  }

  @tailrec
  private def requesterHelper(scrollID: Option[String], total: Long): Done = {
    if (scrollID.isDefined && total > limit) {
      //println(total)
      val scrollResponse = Await.result(IndexClient.searchScroll(scrollID.get, None), Duration.Inf)
      val returned = scrollResponse.result.hits.size
      if (scrollResponse.isSuccess && returned > 0) {
        //println(scrollResponse.result.hits.size)
        val newScrollID: Option[String] = scrollResponse.result.scrollId
        val esoffers: Array[ESEoffer] = hits(scrollResponse.result.hits.hits)
        Await.result(percolateAsync(esoffers), Duration.Inf)
        requesterHelper(newScrollID, total - returned)
      }
      else {
        logger.error(s"Problem with the scroll id in the scroll query on index [${apecOfferIndex}]")
        throw AnalytixException(s"Problem with the scroll id in the scroll query on index [${apecOfferIndex}]", None)
      }
    }
    else {
      val scrollResponse = Await.result(IndexClient.searchScroll(scrollID.get, None), Duration.Inf)
      if (scrollResponse.isSuccess) {
        val esoffers: Array[ESEoffer] = hits(scrollResponse.result.hits.hits)
        Await.result(percolateAsync(esoffers), Duration.Inf)
      }
      else {
        logger.error(s"Problem with the scroll id in the scroll query on index [${apecOfferIndex}]")
        throw AnalytixException(s"Problem with the scroll id in the scroll query on index [${apecOfferIndex}]", None)
      }
    }
  }

  def percolateAsync(esEoffer: Seq[ESEoffer]): Future[Done] = {
    val topHeadSink = Sink.ignore
    lazy val extractor = new APECOfferResourceExtractor(esEoffer)
    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink){ implicit builder =>
      (topHS) =>
        import GraphDSL.Implicits._
        extractor.extractAsync ~> recognizer.recognizeAsync() ~> output.bulkWrite ~> topHS
        ClosedShape
    }).run()
  }
}
