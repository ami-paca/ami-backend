/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.percolator.services

import com.mnemotix.ami.percolator.model.{MiniConcept, Skill, SkillInfo, ZoneGeoWkt}
import com.mnemotix.synaptix.rdf.client.RDFClient

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class RDFExtracter(implicit ec: ExecutionContext) {
  def init: Boolean = {
    RDFClient.init()
  }

  implicit lazy val conn = {
    RDFClient.getReadConnection(("dev-openemploi"))
  }

  def wkt: Future[Seq[ZoneGeoWkt]] = {
    val spqrlr = """PREFIX ope: <http://openemploi.datasud.fr/ontology/>
                   |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                   |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
                   |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                   |
                   |SELECT ?uri ?wkt ?title WHERE {
                   |    ?uri rdf:type <http://ns.mnemotix.com/ontologies/2019/8/generic-model/Polygon> .
                   |    ?uri mnx:asWKT ?wkt .
                   |    ?uri <http://purl.org/dc/terms/title> ?title
                   |}""".stripMargin

    var applicationKindTypeVocabularies = new ListBuffer[ZoneGeoWkt]()
    val futureApplicationKindIndividualsAsync = RDFClient.select(spqrlr).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        applicationKindTypeVocabularies += ZoneGeoWkt(bs.getValue("uri").stringValue(), bs.getValue("wkt").stringValue(), bs.getValue("title").stringValue())
      }
      applicationKindTypeVocabularies.toSeq
    }

    futureApplicationKindIndividualsAsync onComplete {
      case Success(i) => println(s"The application-kind request succeed. The number of applicationKindType : ${i.size}")
      case Failure(t) => println("There is a failure here", t)
    }
    futureApplicationKindIndividualsAsync
  }

  def skills = {
    val sprl = s"""PREFIX ope: <http://openemploi.datasud.fr/ontology/>
                  |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                  |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
                  |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                  |
                  |SELECT ?uri ?label ?notation
                  |FROM NAMED <http://ontology.datasud.fr/openemploi/graph/rome/>
                  |WHERE {
                  |    GRAPH ?g
                  |    {
                  |    ?uri rdf:type <https://ontologies.mindmatcher.org/carto/Skill> .
                  |	?uri skos:prefLabel ?label .
                  |    ?uri skos:notation ?notation
                  |    }
                  |}""".stripMargin

    var applicationKindTypeVocabularies = new ListBuffer[SkillInfo]()
    val futureApplicationKindIndividualsAsync = RDFClient.select(sprl).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        applicationKindTypeVocabularies += SkillInfo(bs.getValue("uri").stringValue(), bs.getValue("label").stringValue(), bs.getValue("notation").stringValue())
      }
      applicationKindTypeVocabularies.toSeq
    }

    futureApplicationKindIndividualsAsync onComplete {
      case Success(i) => println(s"The application-kind request succeed. The number of applicationKindType : ${i.size}")
      case Failure(t) => println("There is a failure here", t)
    }
    futureApplicationKindIndividualsAsync
  }

  lazy val sparqlSkillGroup = """PREFIX ope: <http://openemploi.datasud.fr/ontology/>
                                |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                                |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
                                |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                                |
                                |SELECT ?uri ?sGroup ?label ?notation
                                |FROM NAMED <http://ontology.datasud.fr/openemploi/graph/rome/>
                                |WHERE {
                                |    GRAPH ?g
                                |    {
                                |    ?uri rdf:type <https://ontologies.mindmatcher.org/carto/Skill> .
                                |    ?uri ope:memberOf ?sGroup .
                                |    ?uri skos:prefLabel ?label .
                                |    ?uri skos:notation ?notation
                                |    }
                                |}""".stripMargin

  def skillGroup: Future[Seq[Skill]] = {
    val skillGroups = new ListBuffer[(String, String)]()
    val fut = RDFClient.select(sparqlSkillGroup).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        skillGroups += ((bs.getValue("uri").stringValue(), bs.getValue("sGroup").stringValue()))
      }
      skillGroups
    }

    fut.map { skillGs =>
      skillGs.groupBy(_._1).collect {
        case e => e._1 -> e._2.map(_._2).distinct.toSeq
      }.map(couple => Skill(couple._1, None, None, None, None, Some(couple._2))).toSeq
    }
  }

  def metierRome = {
    val sprql = """PREFIX ope: <http://openemploi.datasud.fr/ontology/>
                  |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                  |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
                  |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                  |
                  |SELECT DISTINCT ?uri ?label ?notation ?uriSkill
                  |FROM NAMED <http://ontology.datasud.fr/openemploi/graph/rome/>
                  |WHERE {
                  |    GRAPH ?g
                  |    {
                  |    ?uri rdf:type <https://ontologies.mindmatcher.org/carto/Occupation> .
                  |    ?uri skos:inScheme <http://ontology.datasud.fr/openemploi/data/scheme/1> .
                  |	?uri skos:prefLabel ?label .
                  |    ?uri skos:notation ?notation .
                  |    ?topConcept rdf:type skos:Concept .
                  |    <http://ontology.datasud.fr/openemploi/data/scheme/1>  skos:hasTopConcept ?topConcept .
                  |    ?uri (skos:broader|^skos:narrower)/(skos:broader|^skos:narrower) ?topConcept .
                  |    ?uriSkill <https://ontologies.mindmatcher.org/carto/hasOccupation> ?uri
                  |    }
                  |}""".stripMargin


    var applicationKindTypeVocabularies = new ListBuffer[MiniConcept]()
    val futureApplicationKindIndividualsAsync = RDFClient.select(sprql).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        applicationKindTypeVocabularies += MiniConcept(bs.getValue("uri").stringValue(), bs.getValue("label").stringValue(), bs.getValue("notation").stringValue(), bs.getValue("uriSkill").stringValue())
      }
      applicationKindTypeVocabularies.toSeq
    }

    futureApplicationKindIndividualsAsync onComplete {
      case Success(i) => println(s"The application-kind request succeed. The number of applicationKindType : ${i.size}")
      case Failure(t) => println("There is a failure here", t)
    }
    futureApplicationKindIndividualsAsync
  }
}
