package com.mnemotix.ami.percolator.services

import akka.Done
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.analytix.percolation.services.es.PercolateQueryRegister
import com.mnemotix.analytix.percolation.services.{ESClient, ESConceptExtractor}

import scala.concurrent.{ExecutionContext, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RomePercolateQueryRegister {

  implicit val executionContext = ExecutionContext.global
  implicit val esPercolatorClient = new ESClient()
  implicit val extractor = new ESConceptExtractor(PercolatorConfig.conceptsRomeIdx)

  //val percolatorRegister = new PercolateQueryRegister(PercolatorConfig.conceptsRomeIdx)

  /*def register: Future[Done] = {
    percolatorRegister.init
    percolatorRegister.registerAsync()
  }*/
}

/*
implicit val executionContext = ExecutionContext.global
  implicit val esPercolatorClient = new ESClient()
  implicit val esConceptExtractor = new ESConceptExtractor(PercolatorConfig.conceptsRomeIdx)

  val queryRegister = new PercolateQueryRegister(PercolatorConfig.conceptsRomeIdx)
  queryRegister.init

  val res = queryRegister.registerAsync
  Await.result(res, Duration.Inf)
 */