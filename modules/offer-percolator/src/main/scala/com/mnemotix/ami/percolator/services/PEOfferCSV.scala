/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.percolator.services

import java.io.{BufferedWriter, File, FileWriter}
import java.text.SimpleDateFormat

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink}
import au.com.bytecode.opencsv.CSVWriter
import com.mnemotix.ami.connectors.model.EOffer
import com.mnemotix.ami.percolator.model.{CompetenceNewOld, ESEoffer, Rdb, Skill, SkillInfo}
import com.mnemotix.lifty.services.CSVExtractor
import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.index.IndexClient
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

import collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor}

object PEOfferCSV extends App with LazyLogging {
  implicit val system = ActorSystem("peofferUpdater")
  implicit val executionContext: ExecutionContextExecutor = ExecutionContext.global
  implicit val materializer = ActorMaterializer()
  lazy val liftyCSVExtractor: CSVExtractor = new CSVExtractor("modules/offer-percolator/src/test/resources/competence_uris.csv", None)
  lazy val source = liftyCSVExtractor.extractAsync
  lazy val flowSerializerNOCompetence: Flow[Map[String, String], CompetenceNewOld, NotUsed] = Flow[Map[String,String]].map {
    aMap => competenceNewOldserializer(aMap)
  }

  lazy val idxName = "dev-openemploi-offer-pe-dev"
  lazy val rdfExtracter = new RDFExtracter()

  rdfExtracter.init
  val groupSkills: Seq[Skill] = Await.result(rdfExtracter.skillGroup, Duration.Inf)
  val skillsInfo: Seq[SkillInfo]  = Await.result(rdfExtracter.skills, Duration.Inf)
  val occupations = Await.result(rdfExtracter.metierRome, Duration.Inf)
  val wkt = Await.result(rdfExtracter.wkt, Duration.Inf)

  val oldNewUris = Await.result(source.via(flowSerializerNOCompetence).runWith(Sink.seq), Duration.Inf)

  val eoffers = requester
  println(eoffers.size)
  println(eoffers.filter(_.eoOffer.competences.isDefined).size)
  offerDumperBC(eoffers)

  def requester = {
    var eoffers = new ListBuffer[ESEoffer]()

    val size = Await.result(IndexClient.count(idxName), Duration.Inf)
    size.result.count

    val responseFirstRequester = firstRequester
    if (responseFirstRequester.isSuccess) {
      val scrollID = responseFirstRequester.result.scrollId
      responseFirstRequester.result.hits.hits.map { hit =>
        val json = Json.parse(hit.sourceAsString)
        val eoffer = json.as[EOffer]
        val esoffer = ESEoffer(hit.id, eoffer)
        eoffers.append(esoffer)
      }
      requesterHelper(scrollID, eoffers, size.result.count)
    }
    else {
      logger.error(s"Unable to execute the scroll query on index [${idxName}]")
      throw new Exception(s"Unable to execute the scroll query on index [${idxName}]")
    }
  }

  def firstRequester = {
    Await.result(IndexClient.scrollQuery(Seq(idxName), Some("3m"), 500, "dateCreation"), Duration.Inf)
  }

  def requesterHelper(scrollID: Option[String], eoffers: ListBuffer[ESEoffer], size: Long): ListBuffer[ESEoffer] = {
    if (scrollID.isDefined & eoffers.size < size) {

      val scrollResponse = Await.result(IndexClient.searchScroll(scrollID.get, None), Duration.Inf)
      if (scrollResponse.isSuccess) {
        val newScrollID: Option[String] = scrollResponse.result.scrollId
        scrollResponse.result.hits.hits.map { hit =>
          val json = Json.parse(hit.sourceAsString)
          val eoffer = json.as[EOffer]
          val esoffer = ESEoffer(hit.id, eoffer)
          eoffers.append(esoffer)
        }
        requesterHelper(newScrollID, eoffers, size)
      }
      else {
        eoffers
      }
    }
    else {
      eoffers
    }
  }

  def offerDumperBC(eoffers: Seq[ESEoffer]): Unit = {
    var i = 0
    val rocksDBStore = new RocksDBStore("modules/offer-percolator/src/test/resources/cache/competences.db")
    rocksDBStore.init
    val fileWriter = new FileWriter(new File("modules/offer-percolator/src/test/resources/csv/offreEmploi.csv"), true)

    val outputFile = new BufferedWriter(fileWriter)
    val csvWriter = new CSVWriter(outputFile)

    val csvSchema = Array("id","dateActualisation","description","entrepriseNom", "experienceLibelle", "alternance","formations", "intitule", "contrat", "salaire", "secteurActivite", "provenance", "zoneEmploi", "metier","competences","competencesDestription", "cblocksTagges", "cblockDescription")

    var listOfRecords = new ListBuffer[Array[String]]()
    listOfRecords += csvSchema

    eoffers.foreach { eoffer =>
      val id = eoffer.esId
      val competences = eoffer.eoOffer.competences

      val occupation = eoffer.eoOffer.occupation
      val zoneEmploi = eoffer.eoOffer.zoneEmploi
      val dateActualisation = dateActualisationString(eoffer)
      val desc = eoffer.eoOffer.description.getOrElse(" ")
      val entrepriseName = if (eoffer.eoOffer.entreprise.isDefined) eoffer.eoOffer.entreprise.get.nom.getOrElse(" ") else " "
      val experienceLibelle	= eoffer.eoOffer.experienceLibelle.getOrElse("").trim
      val alternance = if (eoffer.eoOffer.alternance.isDefined) eoffer.eoOffer.alternance.get.toString else " "
      val formations = if (eoffer.eoOffer.formations.isDefined) eoffer.eoOffer.formations.get.mkString(";").trim else " ".trim
      val intitule= eoffer.eoOffer.intitule.getOrElse(" ")
      val contrat = eoffer.eoOffer.natureContrat.getOrElse(" ")
      val salaire = if (eoffer.eoOffer.salaire.isDefined) eoffer.eoOffer.salaire.get.toString.trim else " "
      val secteurActivite = eoffer.eoOffer.secteurActiviteLibelle.getOrElse(" ")
      val provenanceString: String = {
        if (eoffer.eoOffer.offreInfo.isDefined) {
          if (eoffer.eoOffer.offreInfo.get.partenaires.isDefined) {
            eoffer.eoOffer.offreInfo.get.partenaires.get.mkString(";")
          }
          else " "

        } else " "
      }
      val occupationsString: String = {
        if (occupation.isDefined) {
          occupationToString(occupation.get).notation
        }
        else " "
      }
      val zoneEmploiString: String = {
        if (zoneEmploi.isDefined) {
          wktLibelle(zoneEmploi.get).title.trim
        }
        else " "
      }
      val competencesString: String = {
        if (competences.isDefined) {
          val newUris = oldNewUris.filter { obj =>
            competences.get.contains(obj.old_uri)
          }.map(_.new_uri)
          competenceToString(newUris).map(_.notation).mkString(";")
        }
        else " "
      }

      val futcompetencesDescription = rocksDBStore.get(id)
      val res = Await.result(futcompetencesDescription, Duration.Inf)
      val competencesDescription = if (res.isDefined) {
        val json  = Json.parse(res.get.utf8String)
        val recogs = json.as[Rdb]
        Some(recogs.recogs.get.toSeq.map(_.competences.toSeq.flatten).flatten.seq)
      } else None

      val competencesDescriptionString: String = {
        if (competencesDescription.isDefined) {
          competenceToString(competencesDescription.get).map(_.notation).mkString(";")
        }
        else " "
      }

      val cblocksTagges = {
        if (competences.isDefined) {
          val newUris = oldNewUris.filter { obj =>
            competences.get.contains(obj.old_uri)
          }.map(_.new_uri)

          blocComp(newUris).mkString(";")
        }
        else " "
      }

      val cblockDescription = {
        if (competencesDescription.isDefined) {
          blocComp(competencesDescription.toSeq.flatten).mkString(";")
        }
        else " "
      }
      i = i + 1

      logger.info(s"${i} / ${eoffers.size}")
      //    val csvSchema = Array("id","dateActualisation","description","entrepriseNom", "experienceLibelle", "alternance","formations", "intitule",
      //    "contrat", "salaire", "secteurActivite", "provenance", "zoneEmploi", "metier","competences","competencesDestription", "cblocksTagges", "cblockDescription")
      val toWrite = Array(id,dateActualisation,desc,entrepriseName,experienceLibelle,alternance,formations,intitule,contrat,salaire, secteurActivite, provenanceString, zoneEmploiString,occupationsString,competencesString, competencesDescriptionString, cblocksTagges, cblockDescription)
      listOfRecords += toWrite
    }

    csvWriter.writeAll(listOfRecords.toList.asJava)
    csvWriter.close()
    fileWriter.close()
    outputFile.close()
    System.exit(0)
  }

  def occupationToString(uri: String) = {
    occupations.filter(_.uri == uri).last
  }


  def competenceNewOldserializer(aMap : Map[String, String]): CompetenceNewOld = {
    CompetenceNewOld(
      aMap.get("competence_id").get.trim,
      aMap.get("new_uri").get.trim,
      aMap.get("old_uri").get.trim
    )
  }

  def blocComp(comps: Seq[String]): Seq[String] = {
    groupSkills.filter { groupSkill =>
      comps.contains(groupSkill.uri)
    }.map { groupSkill =>
      groupSkill.groupUri
    }.flatten.flatten.distinct
  }
  def competenceToString(uris: Seq[String]) = {
    skillsInfo.filter { skill =>
      uris.contains(skill.uri)
    }.distinct
  }

  def dateActualisationString(eoffer: ESEoffer): String = {
    val DATE_FORMAT = "dd/MM/YYYY"
    val dateFormat = new SimpleDateFormat(DATE_FORMAT)
    dateFormat.format(eoffer.eoOffer.dateCollecte.get)
  }

  def wktLibelle(uri: String) = {
    wkt.filter(_.uri == uri).last
  }
}
