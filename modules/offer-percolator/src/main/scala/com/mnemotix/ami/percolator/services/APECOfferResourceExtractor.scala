package com.mnemotix.ami.percolator.services

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.ami.percolator.model.ESEoffer
import com.mnemotix.analytix.percolation.api.AbstractResourceExtractor
import com.mnemotix.lifty.models.LiftyResource
import com.mnemotix.synaptix.index.IndexClient
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.SearchResponse

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class APECOfferResourceExtractor(esEoffer: Seq[ESEoffer])(implicit ec: ExecutionContext) extends AbstractResourceExtractor {

  lazy val indexName = PercolatorConfig.apecOfferIdx

  override def init = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
    IndexClient.init()
  }

  override def extractAsync: Source[Seq[LiftyResource], NotUsed] = {
    Source.single(esEoffer.map(esOffer => LiftyResource(esOffer.esId, "https://mindmatcher.org/ontology/","Offer", s"${esOffer.eoOffer.description.getOrElse("")}")))
  }
}