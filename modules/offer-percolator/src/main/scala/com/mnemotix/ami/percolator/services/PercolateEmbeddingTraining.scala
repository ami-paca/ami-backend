/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.percolator.services

import java.nio.charset.StandardCharsets

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import com.github.jelmerk.knn.scalalike.hnsw.HnswIndex
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.ami.percolator.model.{RecogsCompetenceTraining, Skill, SkillInfo}
import com.mnemotix.analytix.percolation.model.{PercoVector, PercolatorText}
import com.mnemotix.analytix.percolation.services.embedding.HsnwPercolationModel
import com.mnemotix.analytix.percolation.services.{ConceptEmbeddingSummarizer, ESClient, ESConceptExtractor, ESPercolatorTextExtractor}
import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.index.IndexClient
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object PercolateEmbeddingTraining extends App with LazyLogging  {
  implicit val system = ActorSystem()
  implicit val executionContext = system.dispatcher
  implicit val esClient = new ESClient()
  implicit val esConceptExtract = new ESConceptExtractor("dev-openemploi-skill")
  implicit val conceptEmbedding = new ConceptEmbeddingSummarizer("french")

  lazy val rdfExtracter = new RDFExtracter()
  IndexClient.init()
  rdfExtracter.init
  val groupSkills: Seq[Skill] = Await.result(rdfExtracter.skillGroup, Duration.Inf)
  val skillsInfo: Seq[SkillInfo]  = Await.result(rdfExtracter.skills, Duration.Inf)

  val UTF8: String = StandardCharsets.UTF_8.name
  val store = new RocksDBStore(PercolatorConfig.cacheRecogsEmbeddingTrainingDir)
  store.init

  val rome = Seq("H2502", "H1206", "H2602", "I1305", "I1102", "H1504", "H2605", "H1209", "H2603", "H2604")

  logger.info(s"computing embeddings...")
  val model = learnEmbedding
  logger.info("computing embeddings done")


  logger.info(s"requesting training...")
  val stratOffersTime = System.currentTimeMillis()
  val trainings = esExtractor.toArray
  logger.info(s">> Time took to request ${trainings.size} offers description => {} s", (System.currentTimeMillis() - stratOffersTime)/ 100)

  logger.info(s"percolate trainings...")
  val stratPercoTime = System.currentTimeMillis()
  val percosFut = perco(model, trainings)
  Await.result(percosFut, Duration.Inf)
  logger.info(s">> Time took to compute all percolate => {} s", (System.currentTimeMillis() - stratPercoTime)/ 100)

  sys addShutdownHook {
    Await.result(system.terminate(), Duration.Inf)
  }

  def perco(model: HnswIndex[String, Array[Float], PercoVector, Float], percolatorTxts: Array[PercolatorText]) = {
    val hsnwPercolationModel = new HsnwPercolationModel()

    logger.info(s"we will percolate ${percolatorTxts.length} documents on an index of ${model.size} of concept")

    val source = Source.fromIterator(() => (percolatorTxts).iterator).grouped(100).map { texts =>
      val knnStartTime = System.currentTimeMillis()
      val lines = texts.map { percolatorTxt =>
        val description = percolatorTxt.texts.mkString(" ")
        val descriptionEmb = conceptEmbedding.embeddingHelper(description)
        val percoVector = new PercoVector(percolatorTxt.docId, descriptionEmb.map(_.toFloat))
        val results = hsnwPercolationModel.predict(model, percoVector, 20)
        val compUris = results.map(_.item().id)
        val notations = competenceToString(compUris)
        val bloc = blocComp(compUris).mkString(";")
        Array(percolatorTxt.docId,notations,bloc)
      }
      logger.info(s">> Time took to compute similarity for ${texts.size} text => {} s", (System.currentTimeMillis() - knnStartTime)/ 100)
      memoireTampon(lines)
    }

    source.runWith(Sink.ignore)
  }

  def learnEmbedding: HnswIndex[String, Array[Float], PercoVector, Float] = {
    val startTime = System.currentTimeMillis()
    val hsnwPercolationModel = new HsnwPercolationModel()
    val modelFut = hsnwPercolationModel.fit
    val model: HnswIndex[String, Array[Float], PercoVector, Float] = Await.result(modelFut, Duration.Inf)
    logger.info(s">> Time took to compute ${model.size} embeddings => {} s", (System.currentTimeMillis() - startTime)/ 100)
    model
  }

  def esExtractor = {
    val trainingsFut = esTrainingExtractor("dev-openemploi-training", Seq("description"), Some("entityId")).runWith(Sink.seq)
    Await.result(trainingsFut, Duration.Inf).flatten
  }

  def esTrainingExtractor(idxName: String, fields: Seq[String], sortField: Option[String]): Source[Seq[PercolatorText], NotUsed] = {
    lazy val espercolatorTextExtractor = new ESPercolatorTextExtractor(idxName, fields, sortField)
    espercolatorTextExtractor.init
    espercolatorTextExtractor.batchExtract
  }

  def competenceToString(competences: Seq[String]) = {
    competencesInfo(competences).map(_.notation).mkString(";")
  }

  def competencesInfo(uris: Seq[String]) = {
    skillsInfo.filter { skill =>
      uris.contains(skill.uri)
    }.distinct
  }

  def blocComp(comps: Seq[String]): Seq[String] = {
    groupSkills.filter { groupSkill =>
      comps.contains(groupSkill.uri)
    }.map { groupSkill =>
      groupSkill.groupUri
    }.flatten.flatten.distinct
  }

  def memoireTampon(newLines: Iterable[Array[String]]) = {
    newLines.map { line =>
      val uri = line(0).toString.getBytes(UTF8)
      val json = Json.toJson(RecogsCompetenceTraining(line(0), line(1), line(2)))
      val value = ByteString(Json.prettyPrint(json).getBytes(UTF8))
      store.put(uri, value)
    }
  }
}
