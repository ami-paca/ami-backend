/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.percolator.services

import java.io.{BufferedWriter, File, FileWriter}
import java.nio.charset.StandardCharsets

import au.com.bytecode.opencsv.CSVWriter
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.ami.percolator.model.RecogsCompetenceTraining
import com.mnemotix.synaptix.cache.RocksDBStore
import play.api.libs.json.Json

import scala.collection.mutable.ListBuffer
import collection.JavaConverters._
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}


class CSVDumper(fileOutputPath: String, cacheDir: String)(implicit val ec: ExecutionContext) {

  val UTF8: String = StandardCharsets.UTF_8.name
  val store = new RocksDBStore(cacheDir)

  def init = {
    store.init
  }

  def dump(uris: Seq[String]): Unit = {
    val line = recogs(uris)
    dumper(fileOutputPath, content = line)
  }

  def recogs(uris: Seq[String]):Seq[Array[String]] = {
    uris.map { uri =>
      val futContent = store.get(uri)
      val content = Await.result(futContent, Duration.Inf).toSeq.map(_.utf8String).map(content => Json.parse(content).as[RecogsCompetenceTraining])
      content.map(recog => Seq(recog.uri, recog.competence, recog.bloc).toArray).flatten.toArray
    }
  }

  def dumper(pathname: String, content: Seq[Array[String]]) = {
    val fileWriter = new FileWriter(new File(pathname), true)
    val outputFile = new BufferedWriter(fileWriter)
    val csvWriter = new CSVWriter(outputFile)
    val csvSchema = Array("id","competencesDestription", "cblockDescription")
    val listOfRecords = new ListBuffer[Array[String]]()
    listOfRecords += csvSchema
    val allContent =  listOfRecords ++ content
    csvWriter.writeAll(allContent.toList.asJava)
    csvWriter.close()
    fileWriter.close()
    outputFile.close()
  }

}
