package com.mnemotix.ami.percolator.services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink, Source}
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.ami.percolator.model.{PercoResultSelection, Recogs}
import com.mnemotix.analytix.percolation.model.PercolatorText
import com.mnemotix.analytix.percolation.services.{ESClient, ESConceptExtractor, ESPercolatorTextExtractor}
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.ESIndexable
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.bulk.BulkResponse
import com.sksamuel.elastic4s.requests.searches.SearchResponse
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsObject, Json}

import scala.collection.immutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * todo update synaptix-toolkit
 */

class PEPercolateQueryMatcher() extends LazyLogging {
  implicit val system = ActorSystem()
  implicit val executionContext: ExecutionContext = system.dispatcher
  implicit val esClient = new ESClient()

  IndexClient.init()

  var i = 0

  lazy val topHeadSink = Sink.ignore

  def percolate(idxName: String, fields: Seq[String], sortField: Option[String],
                percoIdxName: String, percolationGroupBy: Int, from: Option[Int], size: Option[Int], total: Int) = {


    val offersFut = esOfferExtractor(idxName, fields, sortField).runWith(Sink.seq)
    val offers = Await.result(offersFut, Duration.Inf).flatten


    val documents = new ListBuffer[Map[String, String]]()
    val parsed = new ListBuffer[PercolatorText]()
    offers.foreach { offer =>
      val aMap =  Map("percoLabel" -> offer.texts.mkString(" ").trim)
      if (documents.size == 100) {
        val multiRes = IndexClient.multiPercolate(Seq(percoIdxName), documents, from, size)
        val oldMap = esResultatSelection(multiRes, parsed)
        val indexables = esIndexable(oldMap)
        val updateResponse = indexUpate(idxName, indexables, total)
        Await.result(updateResponse, Duration.Inf)
        documents.clear()
        parsed.clear()
      }
      else {
        parsed += offer
        documents += aMap
      }
    }
  }



  def indexUpate(idxName:String, indexables: Iterable[ESIndexable], total: Int) = {
    def processed(nbr:Int): Unit = {
      if (nbr % 500 == 0) {
        logger.info(s"we processed $nbr / $total in $idxName")
      }
      else (logger.info(s"$nbr"))
    }

    val fut = IndexClient.bulkUpdate(idxName, indexables.toSeq: _*)
    fut.onComplete {
      case Success(res) => {
        i = i + res.result.items.size
        processed(i)
        //logger.debug(s"failures ${res.result.failures.size}")
        //logger.debug(s"successes ${res.result.successes.size}")

        //logger.debug(s"Bulk update into index[${idxName}] was completed successfully.")
      }
      case Failure(exception) => logger.error(s"Unable to bulk insert documents into index [${idxName}]", exception)
    }
    fut
  }

  def esOfferExtractor(idxName: String, fields: Seq[String], sortField: Option[String]): Source[Seq[PercolatorText], NotUsed] = {
    lazy val espercolatorTextExtractor = new ESPercolatorTextExtractor(idxName, fields, sortField)
    espercolatorTextExtractor.init
    espercolatorTextExtractor.batchExtract
  }



  def esResultatSelection(resultat: Future[Response[SearchResponse]], allTexts: Seq[PercolatorText])  = {
    val response = Await.result(resultat, Duration.Inf)
    response.result.hits.hits.map { hit =>
      val json = Json.parse(hit.sourceAsString)
      val entityId = (json \ "entityId").as[String]
      val docs = hit.fields("_percolator_document_slot").asInstanceOf[Seq[Int]]
      (entityId, docs.map(num => allTexts(num).docId))
    }.toMap

  }

  def esIndexable(oldMap: Map[String, Seq[String]]) = {
    oldMap.foldLeft(Map[String, Seq[String]]().withDefaultValue(Seq())) {
          case (m, (a, bs)) => bs.foldLeft(m)((map, b) => map.updated(b, m(b) :+ a))
    }.map(couple => ESIndexable(couple._1, jsonCompetence(couple._2)))
  }

  def jsonCompetence(competences: Seq[String]) = {
    val comp = {
      if (competences.size > 10) {
        competences.slice(0,10)
      }
      else competences
    }

    val recogs = Recogs(Some(comp), None, None)
    Some(Json.toJson(recogs).as[JsObject])
  }

  def termsBuilder(terms: Seq[String]) = {
    if (terms.size > 1) {
      terms.map(term => s""""$term"""").mkString(",")
    }
    else {
      s""""${terms(0)}""""
    }
  }

}

/*
     val indexables = Seq(ESIndexable(PercolationTestUtils.concept.entityId.get, Some(Json.parse("""{"broader":"www.example.com/data#b5"}""").as[JsObject])),
        ESIndexable(PercolationTestUtils.concept2.entityId.get, Some(Json.parse("""{"broader":"www.example.com/data#4broader"}""").as[JsObject]))
      )
      IndexClient.bulkUpdate(percolatorIdxName,indexables:_*).futureValue.isSuccess shouldBe true
 */