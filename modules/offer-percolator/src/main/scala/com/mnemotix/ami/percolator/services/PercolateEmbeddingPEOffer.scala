/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.percolator.services

import java.nio.charset.StandardCharsets
import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import com.github.jelmerk.knn.scalalike.hnsw.HnswIndex
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.ami.percolator.model.{RecogsCompetenceTraining, Skill, SkillInfo}
import com.mnemotix.analytix.percolation.model.{PercoVector, PercolatorText}
import com.mnemotix.analytix.percolation.services.embedding.HsnwPercolationModel
import com.mnemotix.analytix.percolation.services.{ConceptEmbeddingSummarizer, ESClient, ESConceptExtractor}
import com.mnemotix.synaptix.cache.RocksDBStore
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

object PercolateEmbeddingPEOffer extends App with LazyLogging {
  implicit val system = ActorSystem()
  implicit val executionContext = system.dispatcher
  implicit val esPercolatorClient = new ESClient()
  implicit val esConceptExtract = new ESConceptExtractor("dev-openemploi-skill")
  implicit val conceptEmbedding = new ConceptEmbeddingSummarizer("french")
  lazy val rdfExtracter = new RDFExtracter()
  rdfExtracter.init

  val rdfRequests = System.currentTimeMillis()
  logger.info(s"Skill information requests...")
  val groupSkills: Seq[Skill] = Await.result(rdfExtracter.skillGroup, Duration.Inf)
  val skillsInfo: Seq[SkillInfo]  = Await.result(rdfExtracter.skills, Duration.Inf)
  logger.info(s">> Skill information requests done in => {} s", (System.currentTimeMillis() - rdfRequests) / 100)


  val UTF8: String = StandardCharsets.UTF_8.name
  val store = new RocksDBStore(PercolatorConfig.cacheRecogsEmbeddingOfferDir)
  store.init

  logger.info(s"computing embeddings...")
  val model = learnEmbedding
  logger.info("computing embeddings done")

  logger.info(s"requesting offers...")
  val stratOffersTime = System.currentTimeMillis()
  val offers = offer
  logger.info(s">> Time took to request ${offers.size} offers description => {} s", (System.currentTimeMillis() - stratOffersTime)/ 100)

  logger.info(s"percolate offers...")
  val stratPercoTime = System.currentTimeMillis()
  val percosFut: Future[Done] = perco(model, offers.toArray)
  Await.result(percosFut, Duration.Inf)
  logger.info(s">> Time took to compute all percolate => {} s", (System.currentTimeMillis() - stratPercoTime)/ 100)

  sys addShutdownHook {
    Await.result(system.terminate(), Duration.Inf)
  }

  def learnEmbedding: HnswIndex[String, Array[Float], PercoVector, Float] = {
    val startTime = System.currentTimeMillis()
    val hsnwPercolationModel = new HsnwPercolationModel()
    val modelFut = hsnwPercolationModel.fit
    val model: HnswIndex[String, Array[Float], PercoVector, Float] = Await.result(modelFut, Duration.Inf)
    logger.info(s">> Time took to compute ${model.size} embeddings => {} s", (System.currentTimeMillis() - startTime)/ 100)
    model
  }

  def offer = {
    val occupations = Seq("http://ontology.datasud.fr/openemploi/data/occupation/1940156419009be4516d6b6f978780d5",
      "http://ontology.datasud.fr/openemploi/data/occupation/511fff073ab20bab31a3eaa713e89048",
      "http://ontology.datasud.fr/openemploi/data/occupation/b0c4fb9b1ebba5a9c21ced19553a0be8",
      "http://ontology.datasud.fr/openemploi/data/occupation/9995e6bbfcc2e4af21970e248c458b41",
      "http://ontology.datasud.fr/openemploi/data/occupation/4d337978e3bdc25a6fc2d0fa54609cea",
      "http://ontology.datasud.fr/openemploi/data/occupation/42c55eeff8c8d95efc54880522c06d0b",
      "http://ontology.datasud.fr/openemploi/data/occupation/6e704ea60ee7bdccbc0325fbf1c11095",
      "http://ontology.datasud.fr/openemploi/data/occupation/2da1d4af5fc3936bf062ca95e20cdd0",
      "http://ontology.datasud.fr/openemploi/data/occupation/ec79c609392258bcacfe9f2c5ebdd1d5",
      "http://ontology.datasud.fr/openemploi/data/occupation/35bc15078fefc0ab768f425c8e6aef2f")

    val extractor = new PEOfferResourceExtractor
    extractor.init
    extractor.extractAsync.filter(r => r.eoOffer.occupation.isDefined).
      filter(r => occupations.contains(r.eoOffer.occupation.get)).map(r => PercolatorText(r.esId, r.eoOffer.description.toSeq)).toSeq
  }

  def perco(model: HnswIndex[String, Array[Float], PercoVector, Float], percolatorTxts: Array[PercolatorText]) = {
    val hsnwPercolationModel = new HsnwPercolationModel()

    logger.info(s"we will percolate ${percolatorTxts.length} documents on an index of ${model.size} of concept")

    val source: Source[Iterable[Future[Unit]], NotUsed] = Source.fromIterator(() => (percolatorTxts).iterator).grouped(100).map { texts =>
      val knnStartTime = System.currentTimeMillis()
      val lines = texts.map { percolatorTxt =>
        val description = percolatorTxt.texts.mkString(" ")
        val descriptionEmb = conceptEmbedding.embeddingHelper(description)
        val percoVector = new PercoVector(percolatorTxt.docId, descriptionEmb.map(_.toFloat))
        val results = hsnwPercolationModel.predict(model, percoVector, 20)
        val compUris = results.map(_.item().id)
        val notations = competenceToString(compUris)
        val bloc = blocComp(compUris).mkString(";")
        Array(percolatorTxt.docId,notations,bloc)
      }
      logger.info(s">> Time took to compute similarity for ${texts.size} text => {} s", (System.currentTimeMillis() - knnStartTime)/ 100)
      memoireTampon(lines)
    }

    source.runWith(Sink.ignore)
  }

  def competenceToString(competences: Seq[String]) = {
    competencesInfo(competences).map(_.notation).mkString(";")
  }

  def competencesInfo(uris: Seq[String]) = {
    skillsInfo.filter { skill =>
      uris.contains(skill.uri)
    }.distinct
  }

  def blocComp(comps: Seq[String]): Seq[String] = {
    groupSkills.filter { groupSkill =>
      comps.contains(groupSkill.uri)
    }.map { groupSkill =>
      groupSkill.groupUri
    }.flatten.flatten.distinct
  }

  //case class RecogsCompetenceTraining(uri: String, competence: String, bloc: String)
  def memoireTampon(newLines: Iterable[Array[String]]): Iterable[Future[Unit]] = {
    newLines.map { line =>
      val uri = line(0).toString.getBytes(UTF8)
      val json = Json.toJson(RecogsCompetenceTraining(line(0), line(1), line(2)))
      val value = ByteString(Json.prettyPrint(json).getBytes(UTF8))
      store.put(uri, value)
    }
  }
}