/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services.tasks

import com.mnemotix.ami.AnalysesTestSpec
import com.mnemotix.ami.model.{TrainingPerMonth}
import com.mnemotix.amqp.api.{AmqpClientConfiguration, AmqpMessage}
import play.api.libs.json.Json

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class TrainingPerMonthTaskSpec extends AnalysesTestSpec  {

  val task = new TrainingPerMonthTask("ami.analyze.training.count.month", AmqpClientConfiguration.exchangeName)
//formationIndex: Seq[String], zoneEmploiUri: Seq[String], occupationUri: Seq[String], dategte: String, datelte: String
  it should "analyze offer by count" in {
    val trainingParam =  new TrainingPerMonth(Seq("dev-openemploi-training"), Seq("http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3"), Seq("http://ontology.datasud.fr/openemploi/data/occupation/c1806e90b273f350b2cbd4909255950e"),"2019-11-06T08:56:22.961Z","2021-05-06T07:56:22.961Z")
    println(Json.prettyPrint(Json.toJson(trainingParam)))
    val message = AmqpMessage(Map.empty, Json.toJson(trainingParam))
    val resultMessage = Await.result(task.onMessage(message.toReadResult()), Duration.Inf)
    val msg = Json.parse(resultMessage.bytes.utf8String)
    println(Json.prettyPrint(msg))
    val parsing = msg.validate[AmqpMessage]
    parsing.isSuccess shouldBe true
    println(parsing.get.body)
  }

}
