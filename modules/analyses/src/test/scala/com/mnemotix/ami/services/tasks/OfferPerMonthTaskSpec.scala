/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services.tasks

import com.mnemotix.ami.AnalysesTestSpec
import com.mnemotix.ami.model.OfferPerMonth
import com.mnemotix.amqp.api.{AmqpClientConfiguration, AmqpMessage}
import play.api.libs.json.Json

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class OfferPerMonthTaskSpec extends AnalysesTestSpec {

  val task = new OfferPerMonthTask("ami.analyze.offer.count.month", AmqpClientConfiguration.exchangeName)

  it should "analyze offer by count" in {
    val offersParam =  new OfferPerMonth(Seq("dev-openemploi-offer-pe"),
      Seq("http://openemploi.datasud.fr/data/93b546ac99af1db880e741d1fc948849","http://openemploi.datasud.fr/data/e839293f405a7f637f0680cd5412f53","http://openemploi.datasud.fr/data/e8df051ea51fe8332cbc89cee8bcef10"),
      Seq("http://ontology.datasud.fr/openemploi/data/occupation/44b81c43de028852800314c5b6698173","http://ontology.datasud.fr/openemploi/data/occupation/4c18e015523e4705ee0241d513c22c48","http://ontology.datasud.fr/openemploi/data/occupation/c1806e90b273f350b2cbd4909255950e"),
      "2020-06-20T13:24:12.349Z", "2020-11-20T14:24:12.349Z")
    println(Json.prettyPrint(Json.toJson(offersParam)))
    val message = AmqpMessage(Map.empty, Json.toJson(offersParam))
    val resultMessage = Await.result(task.onMessage(message.toReadResult()), Duration.Inf)
    val msg = Json.parse(resultMessage.bytes.utf8String)
    println(Json.prettyPrint(msg))
    val parsing = msg.validate[AmqpMessage]
    parsing.isSuccess shouldBe true
    println(parsing.get.body)
  }

}


