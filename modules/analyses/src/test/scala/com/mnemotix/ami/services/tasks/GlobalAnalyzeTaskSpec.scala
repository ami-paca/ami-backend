/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services.tasks

import com.mnemotix.ami.AnalysesTestSpec
import com.mnemotix.ami.model.Analyses
import com.mnemotix.amqp.api.{AmqpClientConfiguration, AmqpMessage}
import play.api.libs.json.Json

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class GlobalAnalyzeTaskSpec extends AnalysesTestSpec {

  val task = new GlobalAnalyzeTask("ami.analyze.global", AmqpClientConfiguration.exchangeName)

  it should "find best combination of occupation area" in {
    val params = new Analyses(
      Seq("dev-openemploi-offer-pe"),
      Seq("dev-openemploi-training"),
      Seq("dev-openemploi-skill"),
      Seq("http://openemploi.datasud.fr/data/93b546ac99af1db880e741d1fc948849","http://openemploi.datasud.fr/data/e839293f405a7f637f0680cd5412f53","http://openemploi.datasud.fr/data/e8df051ea51fe8332cbc89cee8bcef10"),
      Seq("http://ontology.datasud.fr/openemploi/data/occupation/44b81c43de028852800314c5b6698173","http://ontology.datasud.fr/openemploi/data/occupation/4c18e015523e4705ee0241d513c22c48","http://ontology.datasud.fr/openemploi/data/occupation/c1806e90b273f350b2cbd4909255950e"),
      Seq("http://ontology.datasud.fr/openemploi/data/skill/7c61baef079a3ec2c897efa2ae455302","http://ontology.datasud.fr/openemploi/data/skill/ff963050455e47882b08eb044dfb8f84","http://ontology.datasud.fr/openemploi/data/skill/3f3468f0a5ee881c101f7d90cd80db5","http://ontology.datasud.fr/openemploi/data/skill/19be85660e980f07ee0b6935f629e200","http://ontology.datasud.fr/openemploi/data/skill/7ff1b3a44c5240968c8e79407a934ad7","http://ontology.datasud.fr/openemploi/data/skill/166eb3175e5d7fabcc63a8b4222625f"),
      "2020-06-20T13:24:12.349Z", "2020-11-20T14:24:12.349Z"
    )

    println(Json.prettyPrint(Json.toJson(params)))
    val message = AmqpMessage(Map.empty, Json.toJson(params))
    val resultMessage = Await.result(task.onMessage(message.toReadResult()), Duration.Inf)
    val msg = Json.parse(resultMessage.bytes.utf8String)
    println(Json.prettyPrint(msg))
    val parsing = msg.validate[AmqpMessage]
    parsing.isSuccess shouldBe true
    println(parsing.get.body)
  }
}