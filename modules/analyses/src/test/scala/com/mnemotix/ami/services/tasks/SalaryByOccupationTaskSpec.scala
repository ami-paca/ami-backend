/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services.tasks

import com.mnemotix.ami.AnalysesTestSpec
import com.mnemotix.ami.model.{OfferPerMonth, SalaryPerMonth}
import com.mnemotix.amqp.api.{AmqpClientConfiguration, AmqpMessage}
import play.api.libs.json.Json

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class SalaryByOccupationTaskSpec extends AnalysesTestSpec {

  val task = new SalaryByOccupationTask("ami.analyze.offer.salary.mean.month", AmqpClientConfiguration.exchangeName)

  it should "analyze offer by count" in {
    val salaryParam =  new SalaryPerMonth(Seq("dev-openemploi-offer-pe"), Seq("http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3"), Seq("http://ontology.datasud.fr/openemploi/data/occupation/44b81c43de028852800314c5b6698173"), "2020-06-02T10:10:10.967Z", "2020-10-02T10:10:10.967Z")
    println(Json.prettyPrint(Json.toJson(salaryParam)))
    val message = AmqpMessage(Map.empty, Json.toJson(salaryParam))
    val resultMessage = Await.result(task.onMessage(message.toReadResult()), Duration.Inf)
    val msg = Json.parse(resultMessage.bytes.utf8String)
    println(Json.prettyPrint(msg))
    val parsing = msg.validate[AmqpMessage]
    parsing.isSuccess shouldBe true
    println(parsing.get.body)
  }
}
