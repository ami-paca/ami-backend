/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services

import akka.actor.ActorSystem
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

class AnalyzerIndexSpec extends AnyFlatSpec with Matchers {

  implicit val system = ActorSystem(this.getClass.getSimpleName)
  implicit val executionContext = ExecutionContext.global

  it should "retrieve the nbr offer by month by area" in {
    lazy val analyzerIndex = new AnalyzerIndex()
    analyzerIndex.init
    val offerIndex = Seq("dev-openemploi-offer-pe")
    val zoneOpenEmploi = Seq("http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3")
    val occupation = Seq("http://ontology.datasud.fr/openemploi/data/occupation/44b81c43de028852800314c5b6698173")
    val gte = "2020-06-02T10:10:10.967Z"
    val lte = "2020-10-02T10:10:10.967Z"
    val res =  analyzerIndex.nbrOfferByMonthByArea(offerIndex, zoneOpenEmploi, occupation, gte, lte)
    Await.result(res, Duration.Inf).result.aggregations.names.size should be > 0
  }

  it should "average Salary By Occupation Per Month By Area" in {
    lazy val analyzerIndex = new AnalyzerIndex()
    analyzerIndex.init
    val offerIndex = Seq("dev-openemploi-offer-pe")
    val zoneOpenEmploi = Seq("http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3")
    val occupation = Seq("http://ontology.datasud.fr/openemploi/data/occupation/44b81c43de028852800314c5b6698173")
    val gte = "2020-06-02T10:10:10.967Z"
    val lte = "2020-10-02T10:10:10.967Z"
    val res = analyzerIndex.avgSalaryByOccupationPerMonthByArea(offerIndex, zoneOpenEmploi, occupation, gte, lte)
    Await.result(res, Duration.Inf).result.aggregations.getAgg("incomesByOccupation")
  }

  it should "get the trainings by occupation by area" in {
    lazy val analyzerIndex = new AnalyzerIndex()
    analyzerIndex.init
    val formationIndex = Seq("dev-openemploi-training")
    val jobsArea = Seq("http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3")
    val mainOccupation = Seq("http://ontology.datasud.fr/openemploi/data/occupation/c1806e90b273f350b2cbd4909255950e")
    val dategte  = "2019-11-06T08:56:22.961Z"
    val datelte  = "2021-05-06T07:56:22.961Z"
    val res = analyzerIndex.nbrTrainingsByOccupationByArea(formationIndex, jobsArea, mainOccupation, dategte, datelte)
    Await.result(res, Duration.Inf)
  }

  it should "get occupation By Area" in {
    lazy val analyzerIndex = new AnalyzerIndex()
    analyzerIndex.init
    val occupations = Seq("dev-openemploi-offer-pe")
    val zoneEmploi = Seq("http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3")
    val gte = "2020-05-02T10:10:10.967Z"
    val lte = "2020-10-02T10:10:10.967Z"
    val res = analyzerIndex.occupationByArea(occupations, zoneEmploi, gte, lte)
    Await.result(res, Duration.Inf)
  }

  it should "get skills of occupation" in {
    lazy val analyzerIndex = new AnalyzerIndex()
    analyzerIndex.init
    val skillIndex = Seq("dev-openemploi-skill")
    val occupations = Seq("http://ontology.datasud.fr/openemploi/data/occupation/a35faaa1b1a14a62beb24a8b07e6fea4")
    val res = analyzerIndex.skillsOfOccupation(skillIndex, occupations)
    val skills = Await.result(res, Duration.Inf)
    skills.result.hits.hits.foreach(h => println(h.sourceAsString))
  }
}

/*
  def skillsOfOccupation(skillIndex: Seq[String], occupations: Seq[String]) = {
    IndexClient.rawQuery(RawQuery(skillIndex, Json.parse(Occupation.skillOfOccupation(occupations)).as[JsObject]))
  }
 */