/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services.tasks

import com.mnemotix.ami.AnalysesTestSpec
import com.mnemotix.ami.model.{SalaryPerMonth, Skills}
import com.mnemotix.amqp.api.{AmqpClientConfiguration, AmqpMessage}
import play.api.libs.json.Json

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class SkillPercentTaskSpec extends AnalysesTestSpec {
  val task = new SkillPercentTask("ami.analyze.user.skill.area", AmqpClientConfiguration.exchangeName)

  it should "analyze skill of user" in {
    val skillParam =  new Skills(
      Seq("dev-openemploi-offer-pe"),
      Seq("dev-openemploi-skill"),
      Seq("http://ontology.datasud.fr/openemploi/data/skill/7c61baef079a3ec2c897efa2ae455302","http://ontology.datasud.fr/openemploi/data/skill/ff963050455e47882b08eb044dfb8f84","http://ontology.datasud.fr/openemploi/data/skill/3f3468f0a5ee881c101f7d90cd80db5","http://ontology.datasud.fr/openemploi/data/skill/19be85660e980f07ee0b6935f629e200","http://ontology.datasud.fr/openemploi/data/skill/7ff1b3a44c5240968c8e79407a934ad7","http://ontology.datasud.fr/openemploi/data/skill/166eb3175e5d7fabcc63a8b4222625f"),
      Seq("http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3"),
      "2020-05-02T10:10:10.967Z",
      "2020-10-02T10:10:10.967Z"
    )
    println(Json.prettyPrint(Json.toJson(skillParam)))
    val message = AmqpMessage(Map.empty, Json.toJson(skillParam))
    val resultMessage = Await.result(task.onMessage(message.toReadResult()), Duration.Inf)
    val msg = Json.parse(resultMessage.bytes.utf8String)
    println(Json.prettyPrint(msg))
    val parsing = msg.validate[AmqpMessage]
    parsing.isSuccess shouldBe true
    println(parsing.get.body)
  }
}
