/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class AmiDataAnalyzerSpec extends AnyFlatSpec with Matchers {

  val amiDataAnalyzer = new AmiDataAnalyzer

  it should "Analyze market" in {
    val market = Seq(23L, 24L, 20L, 30L, 12L, 25L)
    println(amiDataAnalyzer.marketAnalyzer(market))
  }

  it should "Analyze de salary" in {
    val salaire = Seq(2000.0, 2004.0, 2050.0)
    println(amiDataAnalyzer.salaireAnalyzer(salaire))
  }

  it should "Analyze training" in {
    val training = Seq(1L, 2L, 4L, 3L, 2L, 5L)
    println(amiDataAnalyzer.trainingAnalyzer(training))
  }

  it should "Analyze competence" in {
    val competenceUser = Seq("competence 1","competence 2","competence 3","competence 4")
    val competenceArea = Seq("competence 5","competence 3","competence 7")
    println(amiDataAnalyzer.competenceAnalyzer(competenceUser, competenceArea))
  }
}