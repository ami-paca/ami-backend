/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services

import akka.actor.ActorSystem
import com.mnemotix.ami.model.Skill
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.SearchResponse
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

class IndexRequestTester extends AnyFlatSpec with Matchers {

  implicit val system = ActorSystem(this.getClass.getSimpleName)
  implicit val executionContext = ExecutionContext.global

  lazy val zoneEmploi = "http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3"
  lazy val occupation = "http://ontology.datasud.fr/openemploi/data/occupation/44b81c43de028852800314c5b6698173"

  IndexClient.init()

  it should "request an date_histogram query" in {

    val query = {
      """{
        | "query": {
        |   "bool" : {
        |    "filter" : [{
        |       "terms": {
        |         "zoneEmploi":["http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3"]
        |       }
        |    },
        |    {
        |       "terms": {
        |         "occupation":["http://ontology.datasud.fr/openemploi/data/occupation/44b81c43de028852800314c5b6698173"]
        |       }
        |     },
        |     {
        |       "range" : {
        |         "dateCreation": {
        |           "gte": "2020-06-02T10:10:10.967Z",
        |           "lte": "2020-10-02T10:10:10.967Z"
        |         }
        |       }
        |     }]
        |   }
        | },
        |  "_source": {
        |        "includes": "*"
        |    },
        |    "size": 0,
        |  "aggs": {
        |     "offersCountByMonth": {
        |       "date_histogram": {
        |         "field" : "dateCreation",
        |         "calendar_interval": "1M",
        |         "format": "MM/YY"
        |       }
        |     }
        |   }
        |}""".stripMargin
    }

    val fut = IndexClient.rawQuery(RawQuery(Seq("dev-openemploi-offer-pe"), Json.parse(query).as[JsObject]))
    val res: Response[SearchResponse] = Await.result(fut, Duration.Inf)
    res.result.aggregations.names.foreach(println(_))
    //println(res.result.aggregations.dateHistogram("offersCountByMonth").buckets.map(buck => buck.date))
    val amiStatComputer = new AmiDataAnalyzerExtractor
     //println(amiStatComputer.offersCount(res))
    println(res.result.aggregations.getAgg("offersCountByMonth"))
    amiStatComputer.offersCount(res).foreach(println(_))

  }

  it should "request do an avg query" in {
    val query = """{
                  | "query": {
                  |   "bool" : {
                  |    "filter" : [{
                  |       "terms": {
                  |         "zoneEmploi":["http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3"]
                  |       }},
                  |      {
                  |     "range" : {
                  |         "dateCreation": {
                  |           "gte": "2020-05-02T10:10:10.967Z",
                  |           "lte": "2020-10-02T10:10:10.967Z"
                  |         }
                  |       }
                  |     }]
                  |   }
                  | },
                  | "_source": {
                  |        "includes": "*"
                  |    },
                  |    "size": 0,
                  | "aggs": {
                  |        "incomesByOccupation": {
                  |            "filter": {
                  |                "terms": {
                  |                    "occupation": ["http://ontology.datasud.fr/openemploi/data/occupation/44b81c43de028852800314c5b6698173"]
                  |                }
                  |            },
                  |            "aggs": {
                  |                "incomesHistogram": {
                  |                    "date_histogram": {
                  |                        "field": "dateCreation",
                  |                        "calendar_interval": "month",
                  |                        "format": "MM/YY"
                  |                    },
                  |                    "aggs": {
                  |                        "avgIncome": {
                  |                            "avg": {
                  |                                "field": "salaire",
                  |                                "missing": 0
                  |                            }
                  |                        }
                  |                    }
                  |                }
                  |            }
                  |        }
                  |    }
                  |}""".stripMargin

    val fut = IndexClient.rawQuery(RawQuery(Seq("dev-openemploi-offer-pe"), Json.parse(query).as[JsObject]))
    val res = Await.result(fut, Duration.Inf)
    res.result.aggregations.names.foreach(println(_))
    println(res.result.aggregations.getAgg("incomesByOccupation"))

  }

  it should "request do an avg training by appelations" in {
    val jobsArea = "http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3"
    val mainOccupation = "http://ontology.datasud.fr/openemploi/data/occupation/c1806e90b273f350b2cbd4909255950e"
    val dategte  = "2019-11-06T08:56:22.961Z"
    val datelte  = "2021-05-06T07:56:22.961Z"
    val query = s"""{
                   | "query": {
                   |   "bool" : {
                   |    "filter" : [{
                   |       "terms": {
                   |         "jobArea":["$jobsArea"]
                   |       }},
                   |    {
                   |       "terms": {
                   |         "mainOccupation": [
                   |                            "http://ontology.datasud.fr/openemploi/data/occupation/c1806e90b273f350b2cbd4909255950e",
                   |                            "http://ontology.datasud.fr/openemploi/data/occupation/4c18e015523e4705ee0241d513c22c48",
                   |                            "http://ontology.datasud.fr/openemploi/data/occupation/ca51f1f37285c54e9e51c000ce6056ce",
                   |                            "http://ontology.datasud.fr/openemploi/data/occupation/95bbfee9aabc374edc1cf91d4f917d73",
                   |                            "http://ontology.datasud.fr/openemploi/data/occupation/29dc26e309a3069ff4ca6d804d3a43da",
                   |                            "http://ontology.datasud.fr/openemploi/data/occupation/44b81c43de028852800314c5b6698173"
                   |                        ]
                   |
                   |       }},
                   |     {
                   |       "range" : {
                   |         "startDate": {
                   |           "gte": "$dategte",
                   |           "lte": "$datelte"
                   |         }
                   |       }
                   |     }
                   |   ]
                   | }
                   | },
                   | "_source": {
                   |        "includes": "*"
                   |    },
                   |    "size": 0,
                   |
                   | "aggs": {
                   |        "trainingsAggs": {
                   |            "filter": {
                   |                "terms": {
                   |                    "mainOccupation": ["$mainOccupation"]
                   |                }
                   |            },
                   |            "aggs": {
                   |                "trainingsCountByMonth": {
                   |                  "date_histogram": {
                   |                    "field" : "dateCreation",
                   |                    "calendar_interval": "1M",
                   |                    "format": "MM/YY",
                   |                    "extended_bounds": {
                   |                            "min": "now-1y",
                   |                            "max": "now+6M"
                   |                        }
                   |                   }
                   |                }
                   |             }
                   |         }
                   |}
                   |}""".stripMargin
    val fut = IndexClient.rawQuery(RawQuery(Seq("dev-openemploi-training"), Json.parse(query).as[JsObject]))
    val res = Await.result(fut, Duration.Inf)
    res.result.aggregations.names.foreach(println(_))
    println(res.result.aggregations.getAgg("trainingsAggs"))
    val computer = new AmiDataAnalyzerExtractor
    computer.trainingByOccupation(res).foreach(println(_))

  }

  it should "get occupations" in {
    val query = """{
                  | "from" : 0, "size" : 50,
                  | "query": {
                  |   "bool" : {
                  |    "filter" : [{
                  |       "terms": {
                  |         "zoneEmploi":["http://openemploi.datasud.fr/data/facf1d74ba25f1398feba861dbacbea3"]
                  |       }},
                  |      {
                  |     "range" : {
                  |         "dateCreation": {
                  |           "gte": "2020-05-02T10:10:10.967Z",
                  |           "lte": "2020-10-02T10:10:10.967Z"
                  |         }
                  |       }
                  |     }]
                  |   }
                  | },
                  | "_source": ["occupation"]
                  |}""".stripMargin

    val fut = IndexClient.rawQuery(RawQuery(Seq("dev-openemploi-offer-pe"), Json.parse(query).as[JsObject]))
    val res = Await.result(fut, Duration.Inf)
    println(res.result.size)
    res.result.hits.hits.foreach(println(_))
    val computer = new AmiDataAnalyzerExtractor
    computer.occupations(res).foreach(println(_))
  }

  it should "get skills of occupation" in {
    val query = """{
                  |  "from" : 0, "size" : 50,
                  | "_source": ["entityId"],
                  | "query": {
                  |       "terms": {
                  |         "hasOccupation":["http://ontology.datasud.fr/openemploi/data/occupation/a35faaa1b1a14a62beb24a8b07e6fea4"]
                  |       }
                  |   }
                  |}""".stripMargin

    val fut = IndexClient.rawQuery(RawQuery(Seq("dev-openemploi-skill"), Json.parse(query).as[JsObject]))
    val res = Await.result(fut, Duration.Inf)
    res.result.hits.hits.foreach(println(_))
    val skills = res.result.hits.hits.map { h =>
      Json.parse(h.sourceAsString).as[Skill]
    }
    println(skills.length)
    val computer = new AmiDataAnalyzerExtractor
    computer.skillsOccupation(res).foreach(println(_))

  }
}

//http://ontology.datasud.fr/openemploi/data/occupation/11734c642f4a465cd55504afec38110d
//"hasOccupation":["http://ontology.datasud.fr/openemploi/data/occupation/c9d3484e43255a28d67226b5a84092e2, http://ontology.datasud.fr/openemploi/data/occupation/a35faaa1b1a14a62beb24a8b07e6fea4, http://ontology.datasud.fr/openemploi/data/occupation/c9d3484e43255a28d67226b5a84092e2, http://ontology.datasud.fr/openemploi/data/occupation/376acd9a9d1618c601fc641c871dda34"]