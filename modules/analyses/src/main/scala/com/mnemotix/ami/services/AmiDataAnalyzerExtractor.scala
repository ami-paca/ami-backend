/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services

import com.mnemotix.ami.model.{Occupation, Skill}
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.SearchResponse
import play.api.libs.json.Json

class AmiDataAnalyzerExtractor {

  def offersCount(res: Response[SearchResponse]) = {
    val aggregations = res.result.aggregations.dateHistogram("offersCountByMonth")
    aggregations.buckets.map { _.docCount}
  }

  def incomesByOccupation(res: Response[SearchResponse]) = {
    val aggregations = res.result.aggregations.getAgg("incomesByOccupation").get.dateHistogram("incomesHistogram")
    aggregations.buckets.map { _.avg("avgIncome").value}
  }

  def trainingByOccupation(res: Response[SearchResponse]) = {
    val aggregations = res.result.aggregations.getAgg("trainingsAggs").get.dateHistogram("trainingsCountByMonth")
    aggregations.buckets.map { _.docCount }
  }

  def occupations(res: Response[SearchResponse]): Array[Occupation] = {
    res.result.hits.hits.map { h =>
      Json.parse(h.sourceAsString).as[Occupation]
    }
  }

  def skillsOccupation(res: Response[SearchResponse]) = {
    res.result.hits.hits.map { h =>
      Json.parse(h.sourceAsString).as[Skill]
    }
  }
}