/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services.tasks

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.amqp.{ReadResult, WriteMessage}
import com.mnemotix.ami.model.{AnalyzerResponse, Occupation, Skill, Skills}
import com.mnemotix.ami.services.{AmiDataAnalyzer, AnalyzerIndex}
import com.mnemotix.amqp.api.AmqpMessage
import com.mnemotix.amqp.api.rpc.AmqpRpcTask
import com.mnemotix.exceptions.MessageParsingException
import play.api.libs.json.Json

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

class SkillPercentTask(override val topic: String, override val exchangeName: String)(implicit override val system: ActorSystem, override val materializer: ActorMaterializer, override val ec: ExecutionContext, val analyzer: AnalyzerIndex) extends AmqpRpcTask  {

  override def onMessage(msg: ReadResult, params: String*)(implicit ec: ExecutionContext): Future[WriteMessage] = {
    Future {
      logger.info("List of params received : " + msg.bytes.utf8String)
      Json.parse(msg.bytes.utf8String).validate[AmqpMessage].isSuccess match {
        case true => {
          val amqpMessage = Json.parse(msg.bytes.utf8String).as[AmqpMessage]
          val parsing = amqpMessage.body.validate[Skills]
          if (parsing.isSuccess) {
            val skills = parsing.get
            val future = analyzer.occupationByArea(
              skills.offerIndex,
              skills.zoneEmploi,
              skills.gte,
              skills.lte
            ).transform {
              case Success(resp) => Try {
                val occupations = resp.result.hits.hits.map { h =>
                  Json.parse(h.sourceAsString).as[Occupation]
                }.map(_.occupation.get).toSeq

                val future_skill =  analyzer.skillsOfOccupation(skills.skillIndex, occupations).transform {
                  case Success(response) => Try {
                    val skillsOfArea = response.result.hits.hits.map(h => Json.parse(h.sourceAsString).as[Skill]).map(_.entityId.get).toSeq
                    val amiDataAnalyzer: AmiDataAnalyzer = new AmiDataAnalyzer
                    val analyzerRs = amiDataAnalyzer.competenceAnalyzer(skills.skillUser, skillsOfArea)
                    getResponseMessage(Json.toJson(AnalyzerResponse(analyzerRs)), "JSON", "OK", msg.properties)
                  }
                  case Failure(exception) => Try {
                    getErrorMessage(exception, msg.properties)
                  }
                }
                Await.result(future_skill, 15.seconds)
              }
              case Failure(err) => Try {
                getErrorMessage(err, msg.properties)
              }
            }
            Await.result(future, 180.seconds)
          }
          else {
            getErrorMessage(new MessageParsingException(s"Query was malformed: ${msg.bytes.utf8String}", null), msg.properties)
          }
        }
        case false => {
          logger.error(s"Unable to parse the message: ${msg.bytes.utf8String}")
          getErrorMessage(new MessageParsingException(s"Unable to parse the message: ${msg.bytes.utf8String}", null), msg.properties)
        }
      }
    }
  }

}
