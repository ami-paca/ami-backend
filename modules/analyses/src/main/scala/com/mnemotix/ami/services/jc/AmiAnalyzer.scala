/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services.jc

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.mnemotix.ami.services.AnalyzerIndex
import com.mnemotix.ami.services.tasks.{GlobalAnalyzeTask, OfferPerMonthTask, SalaryByOccupationTask, SkillPercentTask, TrainingPerMonthTask}
import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.rpc.AmqpRpcController
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

object AmiAnalyzer extends App with LazyLogging {

  implicit val system = ActorSystem("AmiAnalyzer")
  implicit val materializer = ActorMaterializer()
  implicit val ec: ExecutionContext = system.dispatcher

  implicit val analyzer: AnalyzerIndex = new AnalyzerIndex()
  analyzer.init

  logger.info("AMI Analyzer controller starting...")

  val controller = new AmqpRpcController("AMI Analyzer")
  controller.registerTask("ami.analyze.offer.count.month", new OfferPerMonthTask("ami.analyze.offer.count.month", AmqpClientConfiguration.exchangeName))
  controller.registerTask("ami.analyze.offer.salary.mean.month", new SalaryByOccupationTask("ami.analyze.offer.salary.mean.month", AmqpClientConfiguration.exchangeName))
  controller.registerTask("ami.analyze.training.count.month", new TrainingPerMonthTask("ami.analyze.training.count.month", AmqpClientConfiguration.exchangeName))
  controller.registerTask("ami.analyze.user.skill.area", new SkillPercentTask("ami.analyze.user.skill.area", AmqpClientConfiguration.exchangeName))
  controller.registerTask("ami.analyze.global", new GlobalAnalyzeTask("ami.analyze.global", AmqpClientConfiguration.exchangeName))
  val starting = controller.start(true)
  logger.info("AMI Analyzer controller started successfully.")

  sys addShutdownHook {
    Await.result(controller.shutdown, Duration.Inf)
  }
}