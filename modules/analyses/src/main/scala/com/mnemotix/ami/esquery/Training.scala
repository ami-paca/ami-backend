/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.esquery

object Training {
  def trainingsByOccupationByArea(zoneEmploi: Seq[String], occupations: Seq[String], gte: String, lte: String) = {
    s"""{
       |"query": {
       |   "bool" : {
       |    "filter" : [{
       |       "terms": {
       |         "jobArea":[${ESQueryUtil.termsBuilder(zoneEmploi)}]
       |       }},
       |    {
       |       "terms": {
       |         "mainOccupation": [${ESQueryUtil.termsBuilder(occupations)}]
       |       }},
       |     {
       |       "range" : {
       |         "startDate": {
       |           "gte": "$gte",
       |           "lte": "$lte"
       |         }
       |       }
       |     }
       |   ]
       | }
       |},
       |"_source": {
       |        "includes": "*"
       |},
       |"size": 0,
       |"aggs": {
       |        "trainingsAggs": {
       |            "filter": {
       |                "terms": {
       |                    "mainOccupation": [${ESQueryUtil.termsBuilder(occupations)}]
       |                }
       |            },
       |            "aggs": {
       |                "trainingsCountByMonth": {
       |                  "date_histogram": {
       |                    "field" : "dateCreation",
       |                    "calendar_interval": "1M",
       |                    "format": "MM/YY",
       |                    "extended_bounds": {
       |                            "min": "now-1y",
       |                            "max": "now+6M"
       |                        }
       |                   }
       |                }
       |             }
       |         }
       |      }
       |}""".stripMargin
  }
}
