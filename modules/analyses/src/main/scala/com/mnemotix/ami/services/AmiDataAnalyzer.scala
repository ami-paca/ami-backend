/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services

import com.mnemotix.ami.statistic.MannKendallTrendTest
import breeze.stats.{mean, variance}

class AmiDataAnalyzer {

  def marketAnalyzer(metierNbr: Seq[Long]) = {
    val metiers = metierNbr.map(_.toInt).toVector
    val trend = MannKendallTrendTest.mannKendall(metiers)
    val moyenne = mean(metiers.map(_.toDouble))
    val std = variance(metiers.map(_.toDouble))
    val treshholds = treshold(moyenne,std)

    color(metierNbr.map(_.toDouble), treshholds, trend)
  }

  def salaireAnalyzer(salaire: Seq[Double]) = {
    val trend = MannKendallTrendTest.mannKendall(salaire.map(_.toInt).toVector)
    val moyenne = mean(salaire)
    val std = variance(salaire)
    val treshholds = treshold(moyenne,std)

    color(salaire, treshholds, trend)
  }

  def trainingAnalyzer(training: Seq[Long]) = {
    val trainingS = training.map(_.toInt).toVector
    val trend = MannKendallTrendTest.mannKendall(trainingS)
    val moyenne = mean(trainingS.map(_.toDouble))
    val std = variance(trainingS.map(_.toDouble))
    val treshholds = treshold(moyenne,std)

    color(training.map(_.toDouble), treshholds, trend)
  }

  def competenceAnalyzer(competenceUser: Seq[String], competenceArea: Seq[String]) = {

    val dist = jaccard(competenceUser, competenceArea)
    val sim  = 1-dist
    if (sim > 0.8) {
      "VERT"
    }
    else if (sim < 0.8 && sim > 0.5) {
      "ORANGE"
    }
    else "ROUGE"
  }


  def treshold(moyenne:Double, std: Double) = {
    (moyenne - (2 * std),moyenne + (2 * std))
  }

  def color(array: Seq[Double], treshholds: (Double, Double), trend: Int) = {
    if (array.last > treshholds._1 && trend > 0) {
      "VERT"
    }
    else if (array.last < treshholds._1 && trend > 0) {
      "ORANGE"
    }
    else "ROUGE"
  }

  def jaccard(competenceUser: Seq[String], competenceArea: Seq[String]) = {
     competenceArea.slice(0, competenceUser.size)
    val m01 = competenceArea.diff(competenceUser)
    val m10 = competenceUser.diff(competenceArea)
    val m11 = competenceUser.union(competenceArea)

    (m01.size + m10.size).toDouble / (m01.size + m10.size + m11.size).toDouble
  }
}