/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services.tasks

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.amqp.{ReadResult, WriteMessage}
import com.mnemotix.ami.model.{Analyses, TopAnalyzerResponse}
import com.mnemotix.ami.services.{AmiGlobalAnalyzer, AnalyzerIndex}
import com.mnemotix.amqp.api.AmqpMessage
import com.mnemotix.amqp.api.rpc.AmqpRpcTask
import com.mnemotix.exceptions.MessageParsingException
import play.api.libs.json.Json

import scala.concurrent.{ExecutionContext, Future}

class GlobalAnalyzeTask(override val topic: String, override val exchangeName: String)(implicit override val system: ActorSystem, override val materializer: ActorMaterializer, override val ec: ExecutionContext, val analyzer: AnalyzerIndex) extends AmqpRpcTask {
  override def onMessage(msg: ReadResult, params: String*)(implicit ec: ExecutionContext): Future[WriteMessage] = {
    Future {
      logger.info("List of params received : " + msg.bytes.utf8String)
      Json.parse(msg.bytes.utf8String).validate[AmqpMessage].isSuccess match {
        case true => {
          val amqpMessage = Json.parse(msg.bytes.utf8String).as[AmqpMessage]
          val parsing = amqpMessage.body.validate[Analyses]
          if (parsing.isSuccess) {
            val globalAnalyzer = new AmiGlobalAnalyzer()
            val analyses = parsing.get
            val future: Seq[TopAnalyzerResponse] = globalAnalyzer.analyzer(analyses)
            getResponseMessage(Json.toJson(future), "JSON", "OK", msg.properties)
          }
          else {
            getErrorMessage(new MessageParsingException(s"Query was malformed: ${msg.bytes.utf8String}", null), msg.properties)
          }
        }
        case false => {
          logger.error(s"Unable to parse the message: ${msg.bytes.utf8String}")
          getErrorMessage(new MessageParsingException(s"Unable to parse the message: ${msg.bytes.utf8String}", null), msg.properties)
        }
      }
    }
  }
}
