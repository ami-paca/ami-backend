/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.model

import play.api.libs.json.Json

case class OfferPerMonth(offerIndex: Seq[String], zoneEmploiUri: Seq[String], occupationUri: Seq[String], dategte: String, datelte: String)

object OfferPerMonth {
  implicit lazy val format = Json.format[OfferPerMonth]
}

case class SalaryPerMonth(offerIndex: Seq[String], zoneEmploiUri: Seq[String], occupationUri: Seq[String], dategte: String, datelte: String)

object SalaryPerMonth {
  implicit lazy val format = Json.format[SalaryPerMonth]
}

case class TrainingPerMonth(formationIndex: Seq[String], zoneEmploiUri: Seq[String], occupationUri: Seq[String], dategte: String, datelte: String)

object TrainingPerMonth {
  implicit lazy val format = Json.format[TrainingPerMonth]
}

case class Skills(offerIndex: Seq[String], skillIndex: Seq[String], skillUser: Seq[String], zoneEmploi: Seq[String], gte: String, lte: String)

object Skills {
  implicit lazy val format = Json.format[Skills]
}

case class Analyses(offerIndex: Seq[String],
                    formationIndex: Seq[String],
                    skillIndex: Seq[String],
                    zoneEmploiUri: Seq[String],
                    occupationUri: Seq[String],
                    skillUser: Seq[String],
                    dategte: String,
                    datelte: String
                   )

object Analyses {
  implicit lazy val format = Json.format[Analyses]
}