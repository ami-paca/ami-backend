/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services

import com.mnemotix.ami.model.{Analyses, AnalyzerResponse, Occupation, OfferNbrAnalyze, OfferPerMonth, SalaryAnalyze, SalaryPerMonth, Skill, SkillAnalyze, Skills, TopAnalyzerResponse, TrainingAnalyze, TrainingPerMonth}
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import scala.concurrent.duration._

class AmiGlobalAnalyzer(implicit ec: ExecutionContext, analyzerIdx: AnalyzerIndex) extends LazyLogging {



  def analyzer(analyses: Analyses) = {
  val topAnalyzerResponse = analyses.zoneEmploiUri.map { zoneEmploi =>
      analyses.occupationUri.map { occupation =>
        val offerPerMonthResp = offerPerMonth(OfferPerMonth(analyses.offerIndex, Seq(zoneEmploi), Seq(occupation), analyses.dategte, analyses.datelte))

        val salaryByOccupationResp = salaryByOccupation(OfferPerMonth(analyses.offerIndex, Seq(zoneEmploi), Seq(occupation), analyses.dategte, analyses.datelte))

        val skillPercentResp = skillPercent(Skills(analyses.offerIndex, analyses.skillIndex, analyses.skillUser,  Seq(zoneEmploi), analyses.dategte, analyses.datelte))
        val trainingPerMonthResp = trainingPerMonth(TrainingPerMonth(analyses.formationIndex, Seq(zoneEmploi), Seq(occupation), analyses.dategte, analyses.datelte))



        val sbo = salaryByOccupationResp.transform {
          case Success(resp) => Try {
            Some(SalaryAnalyze(resp.get.color))
          }
          case Failure(err) => Try {
            logger.error("an error occured during offer per month analyze", err)
            None
          }
        }

        val ofpm = offerPerMonthResp.transform {
          case Success(resp) => Try {
            Some(OfferNbrAnalyze(resp.get.color))
          }
          case Failure(err) => Try {
            logger.error("an error occured during offer per month analyze", err)
            None
          }
        }

        val spercent = skillPercentResp.transform {
          case Success(resp) => Try {
            Some(SkillAnalyze(resp.get.color))
          }
          case Failure(err) => Try {
            logger.error("an error occured during offer per month analyze", err)
            None
          }
        }

        val tpm = trainingPerMonthResp.transform {
          case Success(resp) => Try {
            Some(TrainingAnalyze(resp.get.color))
          }
          case Failure(err) => Try {
            logger.error("an error occured during offer per month analyze", err)
            None
          }
        }

        val resOfpm = Await.result(ofpm, 60.seconds)
        val resSbo: Option[SalaryAnalyze] = Await.result(sbo, 60.seconds)
        val resSpercent: Option[SkillAnalyze] = Await.result(spercent, 60.seconds)
        val resTpm: Option[TrainingAnalyze] = Await.result(tpm, 60.seconds)

        val nbrColor = colorCounter(resOfpm,resSbo,resSpercent,resTpm)



        TopAnalyzerResponse(zoneEmploi, occupation, resOfpm, resSbo, resSpercent, resTpm,
          nbrColor._1, nbrColor._2, nbrColor._3)
      }
    }.flatten

    topAnalyzerResponse.sortBy(ar => (ar.green, ar.orange)).reverse
  }

  /*

   */

  def colorCounter(offerPerMonthResp:  Option[OfferNbrAnalyze],salaryByOccupationResp:Option[SalaryAnalyze],
                   skillPercentResp: Option[SkillAnalyze], trainingPerMonthResp: Option[TrainingAnalyze]) = {
    val colorOfferPM = if (offerPerMonthResp.isDefined) offerPerMonthResp.get.color else ""
    val colorSalaryBO = if (salaryByOccupationResp.isDefined) salaryByOccupationResp.get.color else ""
    val colorSkillP = if (skillPercentResp.isDefined) skillPercentResp.get.color else ""
    val trainingPerMonth = if (trainingPerMonthResp.isDefined) trainingPerMonthResp.get.color else ""

    val seqColor = Seq(colorOfferPM, colorSalaryBO, colorSkillP, trainingPerMonth)

    (seqColor.filter(s => s == "VERT").size,
    seqColor.filter(s => s == "ORANGE").size,
    seqColor.filter(s => s == "ROUGE").size)

  }


  /*
        "VERT"
    }
    else if (sim < 0.8 && sim > 0.5) {
      "ORANGE"
    }
    else "ROUGE"
   */

  def offerPerMonth(offerPerMonth: OfferPerMonth) = {
    analyzerIdx.nbrOfferByMonthByArea(
      offerPerMonth.offerIndex,
      offerPerMonth.zoneEmploiUri,
      offerPerMonth.occupationUri,
      offerPerMonth.dategte,
      offerPerMonth.datelte
    ).transform {
      case Success(resp) => Try {
        val amiDataAnalyzerExtractor = new AmiDataAnalyzerExtractor
        val amiDataAnalyzer: AmiDataAnalyzer = new AmiDataAnalyzer
        val offers = amiDataAnalyzerExtractor.offersCount(resp)
        val analyzeRs: String = amiDataAnalyzer.marketAnalyzer(offers)
        Some(AnalyzerResponse(analyzeRs))
      }
      case Failure(err) => Try {
        None
      }
    }
  }

  def salaryByOccupation(offerPerMonth: OfferPerMonth) = {
    analyzerIdx.avgSalaryByOccupationPerMonthByArea(
      offerPerMonth.offerIndex,
      offerPerMonth.zoneEmploiUri,
      offerPerMonth.occupationUri,
      offerPerMonth.dategte,
      offerPerMonth.datelte
    ).transform {
      case Success(resp) => Try {
        val amiDataAnalyzerExtractor = new AmiDataAnalyzerExtractor
        val amiDataAnalyzer: AmiDataAnalyzer = new AmiDataAnalyzer
        val offers = amiDataAnalyzerExtractor.incomesByOccupation(resp)
        val analyzer: String = amiDataAnalyzer.salaireAnalyzer(offers)
        Some(AnalyzerResponse(analyzer))
      }
      case Failure(err) => Try {
        None
      }
    }
  }

  def skillPercent(skills: Skills)=  {
    analyzerIdx.occupationByArea(
      skills.offerIndex,
      skills.zoneEmploi,
      skills.gte,
      skills.lte
    ).transform {
      case Success(resp) => Try {
        val occupations = resp.result.hits.hits.map { h =>
          Json.parse(h.sourceAsString).as[Occupation]
        }.map(_.occupation.get).toSeq

        val future_skill =  analyzerIdx.skillsOfOccupation(skills.skillIndex, occupations).transform {
          case Success(response) => Try {
            val skillsOfArea = response.result.hits.hits.map(h => Json.parse(h.sourceAsString).as[Skill]).map(_.entityId.get).toSeq
            val amiDataAnalyzer: AmiDataAnalyzer = new AmiDataAnalyzer
            val analyzerRs = amiDataAnalyzer.competenceAnalyzer(skills.skillUser, skillsOfArea)
            Some(AnalyzerResponse(analyzerRs))
          }
          case Failure(exception) => Try {
            None
          }
        }
        Await.result(future_skill, 15.seconds)
      }
      case Failure(err) => Try {
        None
      }
    }
  }

  def trainingPerMonth(trainingPerMonth: TrainingPerMonth): Future[Option[AnalyzerResponse]] = {
    analyzerIdx.nbrTrainingsByOccupationByArea(
      trainingPerMonth.formationIndex,
      trainingPerMonth.zoneEmploiUri,
      trainingPerMonth.occupationUri,
      trainingPerMonth.dategte,
      trainingPerMonth.datelte
    ).transform {
      case Success(resp) => Try {
        val amiDataAnalyzerExtractor = new AmiDataAnalyzerExtractor
        val amiDataAnalyzer: AmiDataAnalyzer = new AmiDataAnalyzer
        val offers = amiDataAnalyzerExtractor.trainingByOccupation(resp)
        val analyzer: String = amiDataAnalyzer.marketAnalyzer(offers)
        Some(AnalyzerResponse(analyzer))
      }
      case Failure(err) => Try {
        None
      }
    }
  }


}
