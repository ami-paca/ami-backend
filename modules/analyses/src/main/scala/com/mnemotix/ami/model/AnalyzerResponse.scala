/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.model

import play.api.libs.json.Json

case class AnalyzerResponse(color: String)

object AnalyzerResponse {
  implicit lazy val format = Json.format[AnalyzerResponse]
}


case class OfferNbrAnalyze(color: String)

object OfferNbrAnalyze {
  implicit lazy val format = Json.format[OfferNbrAnalyze]
}

case class SalaryAnalyze(color: String)

object SalaryAnalyze {
  implicit lazy val format = Json.format[SalaryAnalyze]
}

case class SkillAnalyze(color: String)

object SkillAnalyze {
  implicit lazy val format = Json.format[SkillAnalyze]
}

case class TrainingAnalyze(color: String)

object TrainingAnalyze {
  implicit lazy val format = Json.format[TrainingAnalyze]
}

case class TopAnalyzerResponse(area: String, occupation: String,offerCountMonth: Option[OfferNbrAnalyze], salaryMeanMonth: Option[SalaryAnalyze], userSkillArea: Option[SkillAnalyze],
                               trainingCountMonth: Option[TrainingAnalyze], green: Int, orange: Int, red: Int)

object TopAnalyzerResponse {
  implicit lazy val format = Json.format[TopAnalyzerResponse]
}