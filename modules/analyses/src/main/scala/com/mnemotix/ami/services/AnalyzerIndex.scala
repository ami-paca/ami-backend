/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.services

import com.mnemotix.ami.esquery.{Occupation, Offers, Salary, Training}
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.ExecutionContext

class AnalyzerIndex(implicit ec: ExecutionContext) {
  def init = IndexClient.init()

  def nbrOfferByMonthByArea(offerIndex: Seq[String], zoneEmploiUri: Seq[String], occupationUri: Seq[String], dategte: String, datelte: String) = {
    IndexClient.rawQuery(RawQuery(offerIndex, Json.parse(Offers.nbrOffersByMonthByArea(zoneEmploiUri, occupationUri, dategte, datelte)).as[JsObject]))
  }

  def avgSalaryByOccupationPerMonthByArea(offerIndex: Seq[String], zoneEmploiUri: Seq[String], occupationUri: Seq[String], dategte: String, datelte: String) = {
    IndexClient.rawQuery(RawQuery(offerIndex, Json.parse(Salary.avgSalaryByOccupationPerMonthByArea(zoneEmploiUri,dategte, datelte,occupationUri)).as[JsObject]))
  }

  def nbrTrainingsByOccupationByArea(formationIndex: Seq[String], zoneEmploiUri: Seq[String], occupationUri: Seq[String], dategte: String, datelte: String) = {
    IndexClient.rawQuery(RawQuery(formationIndex, Json.parse(Training.trainingsByOccupationByArea(zoneEmploiUri, occupationUri, dategte, datelte)).as[JsObject]))
  }

  def occupationByArea(occupationIndex: Seq[String],zoneEmploi: Seq[String], gte: String, lte: String) = {
    IndexClient.rawQuery(RawQuery(occupationIndex, Json.parse(Occupation.occupationByArea(zoneEmploi, gte, lte)).as[JsObject]))
  }

  def skillsOfOccupation(skillIndex: Seq[String], occupations: Seq[String]) = {
    IndexClient.rawQuery(RawQuery(skillIndex, Json.parse(Occupation.skillOfOccupation(occupations)).as[JsObject]))
  }

  def shutdown = IndexClient.shutdown()
}

