/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.esquery

object Offers {
  def nbrOffersByMonthByArea(zoneEmploi: Seq[String], occupation: Seq[String], gte: String, lte: String) = {
/*
"gte": "2020-06-02T10:10:10.967Z",
"lte": "2020-10-02T10:10:10.967Z"
 */
      s"""{
        | "query": {
        |   "bool" : {
        |    "filter" : [{
        |       "terms": {
        |         "zoneEmploi":[${ESQueryUtil.termsBuilder(zoneEmploi)}]
        |       }
        |    },
        |    {
        |       "terms": {
        |         "occupation":[${ESQueryUtil.termsBuilder(occupation)}]
        |       }
        |     },
        |     {
        |       "range" : {
        |         "dateCreation": {
        |           "gte": "$gte",
        |           "lte": "$lte"
        |         }
        |       }
        |     }]
        |   }
        | },
        |  "_source": {
        |        "includes": "*"
        |    },
        |    "size": 0,
        |  "aggs": {
        |     "offersCountByMonth": {
        |       "date_histogram": {
        |         "field" : "dateCreation",
        |         "calendar_interval": "1M",
        |         "format": "MM/YY"
        |       }
        |     }
        |   }
        |}""".stripMargin
  }
}
