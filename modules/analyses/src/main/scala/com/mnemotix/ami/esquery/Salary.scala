/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.esquery

object Salary {

  def avgSalaryByOccupationPerMonthByArea(zoneEmploi: Seq[String], gte: String, lte:String, occupations: Seq[String]) = {
    s"""{
 "query": {
   "bool" : {
    "filter" : [{
       "terms": {
         "zoneEmploi":[${ESQueryUtil.termsBuilder(zoneEmploi)}]
       }},
      {
     "range" : {
         "dateCreation": {
           "gte": "$gte",
           "lte": "$lte"
         }
       }
     }]
   }
 },
 "_source": {
        "includes": "*"
    },
    "size": 0,
 "aggs": {
        "incomesByOccupation": {
            "filter": {
                "terms": {
                    "occupation": [${ESQueryUtil.termsBuilder(occupations)}]
                }
            },
            "aggs": {
                "incomesHistogram": {
                    "date_histogram": {
                        "field": "dateCreation",
                        "calendar_interval": "month",
                        "format": "MM/YY"
                    },
                    "aggs": {
                        "avgIncome": {
                            "avg": {
                                "field": "salaire",
                                "missing": 0
                            }
                        }
                    }
                }
            }
        }
    }
}"""
  }
}
