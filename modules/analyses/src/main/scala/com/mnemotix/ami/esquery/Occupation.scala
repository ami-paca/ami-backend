/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.esquery

object Occupation {

  def occupationByArea(zoneEmploi: Seq[String], gte: String, lte: String) = {
    s"""{
      | "from" : 0, "size" : 50,
      | "query": {
      |   "bool" : {
      |    "filter" : [{
      |       "terms": {
      |         "zoneEmploi":[${ESQueryUtil.termsBuilder(zoneEmploi)}]
      |       }},
      |      {
      |     "range" : {
      |         "dateCreation": {
      |           "gte": "$gte",
      |           "lte": "$lte"
      |         }
      |       }
      |     }]
      |   }
      | },
      | "_source": ["occupation"]
      |}""".stripMargin
  }

  def skillOfOccupation(occupations: Seq[String]) = {
    s"""{
       | "from" : 0, "size" : 50,
       | "_source": ["entityId"],
       | "query": {
       |       "terms": {
       |         "hasOccupation":[${ESQueryUtil.termsBuilder(occupations)}]
       |       }
       |   }
       |}""".stripMargin
  }

}
