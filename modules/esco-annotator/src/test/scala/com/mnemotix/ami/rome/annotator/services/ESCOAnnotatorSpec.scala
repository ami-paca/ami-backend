package com.mnemotix.ami.rome.annotator.services

import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESCOAnnotatorSpec extends FlatSpec with Matchers {
  val escoAnnotator = new ESCOAnnotator

  it should "annotate Vocabulary" in  {
    val fut = escoAnnotator.annotateVocabulary
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "annotate Scheme" in {
    val fut = escoAnnotator.annotateScheme
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "annotate Ict" ignore  {
    val fut = escoAnnotator.annotateIct
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "occupationSkills" in  {
    val fut = escoAnnotator.occupationSkills
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "annotateOccupations" in {
    val fut = escoAnnotator.annotateOccupations
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "annotateSkillGroup" in  {
    val fut = escoAnnotator.annotateSkillGroup
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "annotateSkill" in  {
    val fut = escoAnnotator.annotateSkill
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "annotateSkillHierarchyAnnotate" in {
    val fut = escoAnnotator.annotateSkillHierarchyAnnotate
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "transversalSkillHierarchyAnnotate" in {
    val fut = escoAnnotator.transversalSkillHierarchyAnnotate
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "write annotate Mnx Value Skill" in {
    val fut = escoAnnotator.annotateMnxValueSkill
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "write annotate Mnx value Skill Group" in {
    val fut = escoAnnotator.annotateMnxValueSkillGroup
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "write annotate Mnx Value Occupation" in {
    val fut = escoAnnotator.annotateMnxValueOccupation
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }
}