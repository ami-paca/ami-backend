/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import com.mnemotix.ami.MnxAnalytixWriter
import com.mnemotix.ami.rome.annotator.services.ESCOAnnotatorSpec
import org.scalatest.{FlatSpec, Matchers}

class MnxAnalytixWriterSpec extends FlatSpec with Matchers {

  it should "create an perco label for mini label" in {
    val startTimeMillis = System.currentTimeMillis()
    val label = "Chargé d'affaires dans l'informatique"
    val hlabel = MnxAnalytixWriter.percoLabel(label)
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"The durations is ${durationSeconds} s")
    println(hlabel.get)
    hlabel.isDefined shouldBe true
  }

  it should "create an hidden label for a long label" in {
    val startTimeMillis = System.currentTimeMillis()
    val label = "Planifier les interventions des équipes de piste et la mise à disposition du matériel et des équipements pour la touchée (temps d'immobilisation au sol)"
    val hlabel = MnxAnalytixWriter.percoLabel(label)
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"The durations is ${durationSeconds} s")
    println(hlabel)
    hlabel shouldBe None
  }

  it should "create an hidden label for a label with ()" in {
    val startTimeMillis = System.currentTimeMillis()
    val label = "Planifier (les interventions)"
    val hlabel = MnxAnalytixWriter.percoLabel(label)
    val endTimeMillis: Long = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"The durations is ${durationSeconds} s")
    println(hlabel.get)
    hlabel.isDefined shouldBe true
  }

  it should "create an queryTemplate" in {
    val startTimeMillis = System.currentTimeMillis()
    val label = "Planifier (les interventions)"
    val queryTemplate = MnxAnalytixWriter.queryTemplate(MnxAnalytixWriter.percoLabel(label).get)
    val endTimeMillis: Long = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"The durations is ${durationSeconds} s")
    queryTemplate shouldEqual """{
                                |    "query": {
                                |        "match_phrase": {
                                |            "content": {
                                |                "query": "planifier interventions",
                                |                "slop": 4
                                |            }
                                |        }
                                |    }
                                |}""".stripMargin
  }
}