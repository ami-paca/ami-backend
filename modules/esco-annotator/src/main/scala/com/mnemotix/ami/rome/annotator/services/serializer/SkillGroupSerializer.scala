/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rome.annotator.services.serializer

import com.mnemotix.ami.rome.annotator.helpers.SerializerUtil
import com.mnemotix.ami.rome.annotator.model.SkillGroup

object SkillGroupSerializer {

  def serializer(aMap : Map[String, String], langFile: String) = {
    SkillGroup(
      SerializerUtil.mapValue(aMap, "conceptUri").get,
      SerializerUtil.mapValue(aMap, "preferredLabel").getOrElse("domaine inconnu"),
      inSheme(aMap).toSeq.flatten,
      langFile,
    )
  }

  def inSheme(aMap : Map[String, String]) = {
    if(SerializerUtil.mapValue(aMap, "inScheme").isDefined)  {
      Some(SerializerUtil.mapValue(aMap, "inScheme").get.split(",").toSeq)
    }
    else None
  }
}