package com.mnemotix.ami.rome.annotator.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.rome.annotator.model.{ConceptScheme, IctSkillsCollection, IscoGroup, Occupation, OccupationSkillRel, Skill, SkillGroup, SkillHierarchy, TransversalSkillsCollection}
import com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept.{ConceptIscoGroupWriter, ConceptOccupationWriter, ConceptSchemeWriter, ConceptSkillGroup, ConceptSkillOccupationWriter, ConceptSkillWriter, VocabularyWriter}
import com.mnemotix.lifty.api.LiftyService
import com.mnemotix.lifty.models.LiftyAnnotation

import scala.concurrent.ExecutionContextExecutor

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RdfSerializer(implicit ec: ExecutionContextExecutor) extends LiftyService {
  override def init(): Unit = logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")

  override def shutdown(): Unit = {}

  def rdfConceptScheme: Flow[Seq[ConceptScheme], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[ConceptScheme]].map { conceptSchemes =>
      ConceptSchemeWriter.schemeAnnotation(conceptSchemes)
    }
  }

  def rdfConceptIscoGroup: Flow[Seq[IscoGroup], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[IscoGroup]].map { iscoGroups =>
      ConceptIscoGroupWriter.annotation(iscoGroups)
    }
  }

  def rdfOccupation: Flow[Seq[Occupation], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[Occupation]].map { occupations =>
      ConceptOccupationWriter.annotations(occupations)
    }
  }

  def mnxRdfOccupation: Flow[Seq[Occupation], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[Occupation]].map { occupations =>
      ConceptOccupationWriter.mnxAnnotations(occupations)
    }
  }

  def rdfSkillGroup: Flow[Seq[SkillGroup], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[SkillGroup]].map { skillGroups =>
      ConceptSkillGroup.annotations(skillGroups)
    }
  }

  def mnxRdfSkillGroup: Flow[Seq[SkillGroup], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[SkillGroup]].map { skillGroups =>
      ConceptSkillGroup.mnxAnnotations(skillGroups)
    }
  }

  def rfdConceptSkillOccupation: Flow[Seq[OccupationSkillRel], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[OccupationSkillRel]].map { occupationSkillRels =>
      ConceptSkillOccupationWriter.annotation(occupationSkillRels)
    }
  }

  def rdfConceptIctCollectionAnnotations: Flow[Seq[IctSkillsCollection], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[IctSkillsCollection]].map { ictSkillsCollections =>
      ConceptSkillWriter.ictCollectionAnnotations(ictSkillsCollections)
    }
  }

  def rdfConceptSkills: Flow[Seq[Skill], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[Skill]].map { skills =>
      ConceptSkillWriter.skillAnnotations(skills)
    }
  }

  def mnxRdfConceptSkills: Flow[Seq[Skill], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[Skill]].map { skills =>
      ConceptSkillWriter.mnxAnnotations(skills)
    }
  }

  def rdfSkillHierarchy: Flow[Seq[SkillHierarchy], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[SkillHierarchy]].map { skillHierarchies =>
      ConceptSkillWriter.skillHierarchy(skillHierarchies)
    }
  }

  def rdfTransversalSkillsCollection: Flow[Seq[TransversalSkillsCollection], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[TransversalSkillsCollection]].map {transversalSkillsCollection =>
      ConceptSkillWriter.transversalSkillsCollection(transversalSkillsCollection)
    }
  }

  def rdfVocabulaire = VocabularyWriter.vocabulaireAnnotation
}