package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import com.mnemotix.ami.{AnnotationHelper, MnxAnalytixWriter}

import java.net.URI
import com.mnemotix.ami.rome.annotator.helpers.EscoConfig
import com.mnemotix.ami.rome.annotator.model.Occupation
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// todo LiftyAnnotation(uriAppellation, "a", createIRI(s"${EscoConfig.mnOntoUri}Occupation"), 1.0)

object ConceptOccupationWriter {
  def annotations(occupations: Seq[Occupation]): Seq[LiftyAnnotation] = {
    occupations.flatMap { occupation =>
      val occupationAnnotation = Seq(LiftyAnnotation(new URI(occupation.uri.trim), s"${EscoConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/esco"), 1.0))
      val prefLabel = occupation.prefLabel.flatMap { pref =>
        AnnotationHelper.annotationLiteralsLang(new URI(occupation.uri.trim), Some(pref.trim),"http://www.w3.org/2004/02/skos/core#", "prefLabel", occupation.lang).toSeq
      }
      val altLabel = occupation.altLabel.flatMap { alt =>
        AnnotationHelper.annotationLiteralsLang(new URI(occupation.uri.trim), Some(alt.trim),"http://www.w3.org/2004/02/skos/core#", "altLabel", occupation.lang).toSeq
      }
      val inscheme = occupation.inScheme.map { scheme =>
        // LiftyAnnotation(new URI(iscoGroup.uri), "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(scheme), 1.0)
        LiftyAnnotation(new URI(occupation.uri.trim), "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(scheme.trim), 1.0)
        //AnnotationHelper.annotationLiteralsLang(new URI(occupation.uri), Some(scheme),"http://www.w3.org/2004/02/skos/core#", "inScheme", occupation.lang).toSeq
      }
      val concept = LiftyAnnotation(new URI(occupation.uri.trim), "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0)
      occupationAnnotation ++ prefLabel ++ altLabel ++ inscheme ++ Seq(concept)
    }
  }

  def mnxAnnotations(occupations: Seq[Occupation]) = {
    occupations.flatMap { occupation =>
      val uri = new URI(occupation.uri.trim)
      MnxAnalytixWriter.annotation(uri, occupation.prefLabel, EscoConfig.percoOccupationIndexName, EscoConfig.percoRecogType)

    }
  }
}