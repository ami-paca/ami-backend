package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import java.net.URI

import com.mnemotix.ami.rome.annotator.helpers.EscoConfig
import com.mnemotix.ami.rome.annotator.model.{OccupationSkillRel}
import com.mnemotix.lifty.helpers.AnnotationsUtils.createIRI
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



object ConceptSkillOccupationWriter {
  def annotation(occupationSkillRel: Seq[OccupationSkillRel]) = {
    occupationSkillRel.map { osr =>
      LiftyAnnotation(new URI(osr.skillUri.trim), s"${EscoConfig.mnOntoUri}hasOccupation", createIRI(osr.occupationUri.toString.trim), 1.0)
      //LiftyAnnotation(uriSkill, s"http://www.w3.org/2004/02/skos/core#related", createIRI(appelationsURI.toString), 1.0)
    }
  }
}
