package com.mnemotix.ami.rome.annotator.helpers

import com.typesafe.config.ConfigFactory

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object EscoConfig {
  lazy val conf = Option(ConfigFactory.load().getConfig("esco")).getOrElse(ConfigFactory.empty())
  lazy val romeName = conf.getString("es.index.name")
  lazy val mnxOntoUri = conf.getString("annotator.ontology.mnx.uri")
  lazy val mnOntoUri = conf.getString("annotator.ontology.mm.uri")
  lazy val amiOntoUri = conf.getString("annotator.ontology.ami.uri")

  lazy val schemeLoc = conf.getString("file.scheme.loc")
  lazy val schemeLang = conf.getString("file.scheme.lang")
  lazy val ictSkillsLoc = conf.getString("file.ictSkills.loc")
  lazy val ictSkillsLang = conf.getString("file.ictSkills.lang")

  lazy val iscogroupLoc = conf.getString("file.iscogroup.loc")
  lazy val iscogroupLang = conf.getString("file.iscogroup.lang")

  lazy val languageSkillsCollectionLoc = conf.getString("file.languageSkillsCollection.loc")
  lazy val languageSkillsCollectionLang = conf.getString("file.languageSkillsCollection.lang")

  lazy val occupationsLoc = conf.getString("file.occupations.location")
  lazy val occupationsLang = conf.getString("file.occupations.lang")

  lazy val occupationSkillRelLoc = conf.getString("file.occupationSkillRel.loc")

  lazy val skillGroupsLoc = conf.getString("file.skillGroups.loc")
  lazy val skillGroupsLang= conf.getString("file.skillGroups.lang")

  lazy val skillsLoc = conf.getString("file.skills.loc")
  lazy val skillsLang= conf.getString("file.skills.lang")

  lazy val skillsHierarchyLoc = conf.getString("file.skillsHierarchy.loc")
  lazy val skillsHierarchyLang= conf.getString("file.skillsHierarchy.lang")

  lazy val transversalSkillLoc = conf.getString("file.transversalSkill.loc")
  lazy val transversalSkillLang= conf.getString("file.transversalSkill.lang")

  lazy val percoSkillIndexName = conf.getString("perco.skill.index.name")
  lazy val percoOccupationIndexName = conf.getString("perco.occupation.index.name")
  lazy val percoSkillGroupIndexName = conf.getString("perco.skillGroup.index.name")
  lazy val percoRecogType = conf.getString("perco.recogType")
}