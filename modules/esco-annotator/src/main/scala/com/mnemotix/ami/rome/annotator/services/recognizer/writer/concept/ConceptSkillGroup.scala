/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import com.mnemotix.ami.{AnnotationHelper, MnxAnalytixWriter}

import java.net.URI
import com.mnemotix.ami.rome.annotator.helpers.EscoConfig
import com.mnemotix.ami.rome.annotator.model.SkillGroup
import com.mnemotix.lifty.helpers.AnnotationsUtils.createIRI
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation


object ConceptSkillGroup {

  def annotations(skillGroup: Seq[SkillGroup]): Seq[LiftyAnnotation] = {
    skillGroup.flatMap { sg =>
      val annotations = Seq(LiftyAnnotation(new URI(sg.uri.trim), s"${EscoConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/esco"), 1.0),
        LiftyAnnotation(new URI(sg.uri.trim), "a", createIRI(s"${EscoConfig.mnOntoUri}Skill"), 1.0))

      val prefLabel = AnnotationHelper.annotationLiteralsLang(new URI(sg.uri.trim), Some(sg.prefLabel.trim),"http://www.w3.org/2004/02/skos/core#", "prefLabel", sg.lang).toSeq

      val inscheme = sg.inScheme.map { scheme =>
        LiftyAnnotation(new URI(sg.uri.trim), "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(scheme.trim), 1.0)
        //AnnotationHelper.annotationLiteralsLang(new URI(sg.uri), Some(scheme),"http://www.w3.org/2004/02/skos/core#", "inScheme", sg.lang).toSeq
      }
      val concept = LiftyAnnotation(new URI(sg.uri.trim), "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0)
      annotations ++ prefLabel ++ inscheme ++ Seq(concept)
    }
  }

  def mnxAnnotations(skillGroup: Seq[SkillGroup]): Seq[LiftyAnnotation] = {
    skillGroup.flatMap { sg =>
      val sgURI = new URI(sg.uri.trim)
      MnxAnalytixWriter.annotation(sgURI, Seq(sg.prefLabel.trim), EscoConfig.percoSkillGroupIndexName, EscoConfig.percoRecogType)
    }
  }
}