package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import com.mnemotix.ami.{AnnotationHelper, MnxAnalytixWriter}

import java.net.URI
import com.mnemotix.ami.rome.annotator.helpers.EscoConfig
import com.mnemotix.ami.rome.annotator.model.{IctSkillsCollection, Skill, SkillHierarchy, TransversalSkillsCollection}
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation

import scala.collection.immutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * oep:coreSkill -> compétence de bases
 * oep:specificSkill -> compétences spécifiques
 * oep:softSkill -> savoirEtre
 * oep:expertSkill -> savoirFaire
 */

// todo add LiftyAnnotation(uriCompetenceDeBase, "a", createIRI(s"${EscoConfig.mnOntoUri}Skill"), 1.0

object ConceptSkillWriter {


    def ictCollectionAnnotations(ictSkillsCollection: Seq[IctSkillsCollection]) = {
      ictSkillsCollection.flatMap { skill =>
        val annotations = Seq(LiftyAnnotation(new URI(skill.uri.trim), s"${EscoConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/esco"), 1.0),
          LiftyAnnotation(new URI(skill.uri.trim), "a", createIRI(s"${EscoConfig.mnOntoUri}Skill"), 1.0))

        val broaderUris = skill.broaderUri.toSeq.flatten.map { broader =>
          LiftyAnnotation(new URI(skill.uri.trim), "http://www.w3.org/2004/02/skos/core#broader", createIRI(broader.trim) ,1.0)
        }

        val prefAnnotation = AnnotationHelper.annotationLiteralsLang(new URI(skill.uri.trim), Some(skill.prefLabel.trim), "http://www.w3.org/2004/02/skos/core#", "prefLabel", skill.lang).toSeq

        val altAnnotation = skill.altLabel.toSeq.flatten.map { alt =>
          AnnotationHelper.annotationLiteralsLang(new URI(skill.uri.trim), Some(alt.trim), "http://www.w3.org/2004/02/skos/core#", "altLabel", skill.lang).toSeq
        }.flatten

        val noteAnnotation = AnnotationHelper.annotationLiteralsLang(new URI(skill.uri.trim), Some(skill.description.trim), "http://www.w3.org/2004/02/skos/core#", "note", skill.lang).toSeq


        annotations ++ broaderUris ++ prefAnnotation ++ altAnnotation ++ noteAnnotation
      }
    }

  def skillAnnotations(skills: Seq[Skill]) = {
    skills.flatMap { skill =>
      val annotations = Seq(LiftyAnnotation(new URI(skill.uri.trim), s"${EscoConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/esco"), 1.0),
        LiftyAnnotation(new URI(skill.uri.trim), "a", createIRI(s"${EscoConfig.mnOntoUri}Skill"), 1.0))

      val prefAnnotation = AnnotationHelper.annotationLiteralsLang(new URI(skill.uri.trim), Some(skill.prefLabel.trim), "http://www.w3.org/2004/02/skos/core#", "prefLabel", skill.lang).toSeq
      val altAnnotation = skill.altLabel.map { alt =>
        AnnotationHelper.annotationLiteralsLang(new URI(skill.uri.trim), Some(alt.trim), "http://www.w3.org/2004/02/skos/core#", "altLabel", skill.lang).toSeq
      }.flatten
      val inscheme = skill.inScheme.map { scheme =>
        LiftyAnnotation(new URI(skill.uri.trim), "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(scheme.trim), 1.0)
        //AnnotationHelper.annotationLiteralsLang(new URI(skill.uri), Some(scheme), "http://www.w3.org/2004/02/skos/core#", "inScheme", skill.lang).toSeq
      }

      val concept = LiftyAnnotation(new URI(skill.uri.trim), "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0)

      annotations ++ prefAnnotation ++ altAnnotation ++ inscheme ++ Seq(concept)
    }
  }

  def mnxAnnotations(skills: Seq[Skill]) = {
    skills.flatMap { skill =>
      val uri = new URI(skill.uri.trim)
      MnxAnalytixWriter.annotation(uri, Seq(skill.prefLabel), EscoConfig.percoSkillIndexName, EscoConfig.percoRecogType)
    }
  }

  def skillHierarchy(skillHierarchy: Seq[SkillHierarchy]) = {
    skillHierarchy.flatMap { skill =>
      val level0 = Seq(LiftyAnnotation(new URI(skill.level0.trim), s"${EscoConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/esco"), 1.0),
        LiftyAnnotation(new URI(skill.level0.trim), "a", createIRI(s"${EscoConfig.mnOntoUri}Skill"), 1.0),
        LiftyAnnotation(new URI(skill.level0.trim), "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0)
      )

      val level1 = {
        if (skill.Level1.isDefined) {
          Some(Seq(LiftyAnnotation(new URI(skill.Level1.get.trim), s"${EscoConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/esco"), 1.0),
            LiftyAnnotation(new URI(skill.Level1.get.trim), "a", createIRI(s"${EscoConfig.mnOntoUri}Skill"), 1.0),
            LiftyAnnotation(new URI(skill.Level1.get.trim), "http://www.w3.org/2004/02/skos/core#broader", createIRI(skill.level0.trim), 1.0),
            LiftyAnnotation(new URI(skill.Level1.get.trim), "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0)
          ) ++ AnnotationHelper.annotationLiteralsLang(new URI(skill.Level1.get.trim), Some(skill.level1PrefLabel.get.trim), "http://www.w3.org/2004/02/skos/core#", "prefLabel", skill.lang).toSeq
          )
        } else None
      }
      val level2 = {
        if (skill.Level2.isDefined) {
          Some(
            Seq(LiftyAnnotation(new URI(skill.Level2.get.trim), s"${EscoConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/esco"), 1.0),
              LiftyAnnotation(new URI(skill.Level2.get.trim), "a", createIRI(s"${EscoConfig.mnOntoUri}Skill"), 1.0),
              LiftyAnnotation(new URI(skill.Level2.get.trim), "http://www.w3.org/2004/02/skos/core#broader", createIRI(skill.Level1.get.trim), 1.0),
              LiftyAnnotation(new URI(skill.Level2.get.trim), "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0)
            ) ++ AnnotationHelper.annotationLiteralsLang(new URI(skill.Level2.get.trim), Some(skill.level2PrefLabel.get.trim), "http://www.w3.org/2004/02/skos/core#", "prefLabel", skill.lang).toSeq
          )
        } else None
      }

      val level3  = {
        if (skill.Level3.isDefined) {
          Some(
            Seq(LiftyAnnotation(new URI(skill.Level3.get.trim), s"${EscoConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/esco"), 1.0),
              LiftyAnnotation(new URI(skill.Level3.get.trim), "a", createIRI(s"${EscoConfig.mnOntoUri}Skill"), 1.0),
              LiftyAnnotation(new URI(skill.Level3.get.trim), "http://www.w3.org/2004/02/skos/core#broader", createIRI(skill.Level2.get.trim), 1.0),
              LiftyAnnotation(new URI(skill.Level3.get.trim), "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0)

            ) ++ AnnotationHelper.annotationLiteralsLang(new URI(skill.Level3.get.trim), Some(skill.level3PrefLabel.get.trim), "http://www.w3.org/2004/02/skos/core#", "prefLabel", skill.lang).toSeq
          )
        } else None
      }

      level0 ++ level1.toSeq.flatten ++ level2.toSeq.flatten ++ level3.toSeq.flatten
    }
  }

  def transversalSkillsCollection(transversalSkillsCollection :Seq[TransversalSkillsCollection]) = {
    transversalSkillsCollection.flatMap { transversalSkill =>
      val skillAnnotation = Seq(LiftyAnnotation(new URI(transversalSkill.uri.trim), s"${EscoConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/esco"), 1.0),
        LiftyAnnotation(new URI(transversalSkill.uri.trim), "a", createIRI(s"${EscoConfig.mnOntoUri}Skill"), 1.0),
        LiftyAnnotation(new URI(transversalSkill.uri.trim), "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0)
      )

      val prefAnnotation = AnnotationHelper.annotationLiteralsLang(new URI(transversalSkill.uri.trim), Some(transversalSkill.prefLabel.trim), "http://www.w3.org/2004/02/skos/core#", "prefLabel", transversalSkill.lang).toSeq

      val altAnnotation = transversalSkill.altLabel.map { alt =>
        AnnotationHelper.annotationLiteralsLang(new URI(transversalSkill.uri.trim), Some(alt.trim), "http://www.w3.org/2004/02/skos/core#", "altLabel", transversalSkill.lang).toSeq
      }.flatten

      val broaderAnnotation = transversalSkill.broaderUri.map { tr =>
        LiftyAnnotation(new URI(transversalSkill.uri.trim), "http://www.w3.org/2004/02/skos/core#broader", createIRI(tr.trim), 1.0)
      }

      skillAnnotation ++ prefAnnotation ++ altAnnotation ++ broaderAnnotation
    }
  }
}