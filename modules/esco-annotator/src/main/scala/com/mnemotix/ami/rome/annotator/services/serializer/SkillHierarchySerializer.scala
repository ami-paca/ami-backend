/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rome.annotator.services.serializer

import com.mnemotix.ami.rome.annotator.helpers.SerializerUtil
import com.mnemotix.ami.rome.annotator.model.SkillHierarchy

object SkillHierarchySerializer {

  def serializer(aMap : Map[String, String], langFile: String) = {
    SkillHierarchy(
      SerializerUtil.mapValue(aMap, "Level 0 URI").get,
      SerializerUtil.mapValue(aMap, "Level 0 preferred term").get,
      SerializerUtil.mapValue(aMap, "Level 1 URI"),
      SerializerUtil.mapValue(aMap, "Level 1 preferred term"),
      SerializerUtil.mapValue(aMap, "Level 2 URI"),
      SerializerUtil.mapValue(aMap, "Level 2 preferred term"),
      SerializerUtil.mapValue(aMap, "Level 3 URI"),
      SerializerUtil.mapValue(aMap, "Level 3 preferred term"),
      SerializerUtil.mapValue(aMap, "Description"),
      SerializerUtil.mapValue(aMap, "Scope note"),
      langFile
    )
  }
}
