package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import java.net.URI

import com.mnemotix.ami.rome.annotator.helpers.EscoConfig
import com.mnemotix.ami.rome.annotator.model.ConceptScheme
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object ConceptSchemeWriter {
  def schemeAnnotation(conceptSchemes: Seq[ConceptScheme]): Seq[LiftyAnnotation] = {
    conceptSchemes.flatMap { conceptScheme =>
      val schemeInfoAnnotation = Seq(
        LiftyAnnotation(new URI(conceptScheme.uri.trim), "a", createIRI("http://www.w3.org/2004/02/skos/core#ConceptScheme"), 1.0),
        LiftyAnnotation(new URI(conceptScheme.uri.trim), "http://www.w3.org/2004/02/skos/core#prefLabel", createLiteral(conceptScheme.prefLabel.trim, conceptScheme.lang), 1.0),
        LiftyAnnotation(new URI(conceptScheme.uri.trim), s"${EscoConfig.mnxOntoUri}schemeOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/esco"), 1.0),
      )

      val descriptionAnnotation = if (conceptScheme.description.isDefined) {
        Some(Seq(LiftyAnnotation(new URI(conceptScheme.uri.trim), "http://www.w3.org/2004/02/skos/core#definition", createLiteral(conceptScheme.description.get.trim, conceptScheme.lang), 1.0)))
      }
      else None

      val titleAnnotation = if (conceptScheme.title.isDefined) {
        Some(Seq(LiftyAnnotation(new URI(conceptScheme.uri.trim), "http://purl.org/dc/elements/1.1/title", createLiteral(conceptScheme.title.get.trim, conceptScheme.lang), 1.0)))
      }
      else None

      val topConceptAnnotation = conceptScheme.topConcept.map { topConceptUri =>
        LiftyAnnotation(new URI(conceptScheme.uri.trim), "http://www.w3.org/2004/02/skos/core#hasTopConcept", createIRI(topConceptUri.trim), 1.0)
      }

      schemeInfoAnnotation ++ descriptionAnnotation.toSeq.flatten ++ titleAnnotation.toSeq.flatten ++ topConceptAnnotation
    }

  }
}
