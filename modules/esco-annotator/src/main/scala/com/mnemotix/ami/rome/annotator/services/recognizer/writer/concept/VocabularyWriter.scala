package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import java.net.URI

import com.mnemotix.ami.rome.annotator.helpers.EscoConfig
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object VocabularyWriter {

  def vocabulaireAnnotation: Seq[LiftyAnnotation] = {
    Seq(
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}vocabulary/esco"), "a", createIRI(s"${EscoConfig.mnxOntoUri}Vocabulary"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}vocabulary/esco"), "http://www.w3.org/2004/02/skos/core#prefLabel", createLiteral("European Skills, Competences, Qualifications and Occupations (ESCO)", "fr"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}vocabulary/esco"), "http://www.w3.org/2004/02/skos/core#altLabel", createLiteral("ESCO", "fr"), 1.0)
    )
  }
}
