/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rome.annotator.services.serializer

import com.mnemotix.ami.rome.annotator.helpers.SerializerUtil
import com.mnemotix.ami.rome.annotator.model.LanguageSkillsCollection

object LanguageSkillsCollectionSerializer {

  def serializer(aMap : Map[String, String], langFile: String) = {
    LanguageSkillsCollection(
      SerializerUtil.mapValue(aMap, "conceptUri").get,
      SerializerUtil.mapValue(aMap, "skillType").get,
      SerializerUtil.mapValue(aMap, "reuseLevel").get,
      SerializerUtil.mapValue(aMap, "preferredLabel").get,
      altLabels(aMap),
      SerializerUtil.mapValue(aMap, "description").get,
      broaderUris(aMap),
      SerializerUtil.mapValue(aMap, "broaderConceptPT").get,
      langFile,
    )
  }

  def altLabels(aMap : Map[String, String]) = {
    if(SerializerUtil.mapValue(aMap, "altLabels").isDefined)  {
      Some(SerializerUtil.mapValue(aMap, "altLabels").get.split("\\|").toSeq)
    }
    else None
  }

  def broaderUris(aMap: Map[String, String]) = {
    if(SerializerUtil.mapValue(aMap, "broaderConceptUri").isDefined)  {
      Some(SerializerUtil.mapValue(aMap, "broaderConceptUri").get.split("\\|").toSeq)
    }
    else None
  }
}
