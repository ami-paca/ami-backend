/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rome.annotator.services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.model.ContentTypes.`text/csv(UTF-8)`.charset
import akka.stream.IOResult
import akka.stream.alpakka.csv.scaladsl.{CsvParsing, CsvToMap}
import akka.stream.scaladsl.{FileIO, Flow, Sink, Source}
import com.mnemotix.ami.rome.annotator.helpers.EscoConfig
import com.mnemotix.ami.rome.annotator.model.{ConceptScheme, Occupation, Skill, SkillGroup, SkillHierarchy, TransversalSkillsCollection}
import com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept.{ConceptSkillWriter, VocabularyWriter}
import com.mnemotix.ami.rome.annotator.services.serializer.{ConceptSchemeSerializer, IctSkillsCollectionSerializer, LanguageSkillsCollectionSerializer, OccupationSerializer, OccupationSkillRelSerializer, SkillGroupSerializer, SkillHierarchySerializer, SkillSerializer, TransversalSkillsCollectionSerializer}
import com.mnemotix.lifty.exceptions.LiftyExtractException
import com.mnemotix.lifty.services.{CSVExtractor, RDFFileOutput}

import java.nio.charset.StandardCharsets
import java.nio.file.Paths
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

class ESCOAnnotator {

  lazy val liftyFileOutput = new RDFFileOutput()

  def annotateVocabulary = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    implicit val system: ActorSystem = ActorSystem("annotateVocabulary")
    Source(VocabularyWriter.vocabulaireAnnotation.to[scala.collection.immutable.Seq]).grouped(10).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)
  }

  def annotateScheme = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    implicit val system: ActorSystem = ActorSystem("annotateScheme")
    lazy val rdfSerializer =new RdfSerializer


    def extractAsync: Source[Map[String, String], Future[IOResult]] = {
      try {
        FileIO.fromPath(Paths.get(EscoConfig.schemeLoc))
          .via(CsvParsing.lineScanner(delimiter = ',', maximumLineLength = 1000 * 1024, quoteChar = '"'))
          .via(CsvToMap.toMapAsStrings(StandardCharsets.UTF_8))
      }
      catch {
        case t: Throwable =>
          throw new LiftyExtractException("An error occured during the resource extraction process", Some(t))
      }
    }

    lazy val lang = EscoConfig.schemeLang

    val serializer: Flow[Map[String, String], ConceptScheme , NotUsed] = {
      Flow[Map[String, String]].map { aMap =>
        val training = ConceptSchemeSerializer.serializer(aMap, lang)
        training
      }
    }
    extractAsync.via(serializer).grouped(200).via(rdfSerializer.rdfConceptScheme).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)
  }

  def annotateIct = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    implicit val system: ActorSystem = ActorSystem("annotateIct")
    lazy val rdfSerializer = new RdfSerializer

    lazy val liftyCSVExtractor = new CSVExtractor(EscoConfig.ictSkillsLoc, None)
    lazy val lang = EscoConfig.iscogroupLang

    val serializer = {
      Flow[Map[String, String]].map { aMap =>
        val training = IctSkillsCollectionSerializer.serializer(aMap, lang)
        training
      }
    }
    liftyCSVExtractor.extractAsync.via(serializer).grouped(200).via(rdfSerializer.rdfConceptIctCollectionAnnotations).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)
  }

  def occupationSkills = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    implicit val system: ActorSystem = ActorSystem("languageSkills")
    lazy val rdfSerializer =new RdfSerializer

    lazy val liftyCSVExtractor = new CSVExtractor(EscoConfig.occupationSkillRelLoc, None)

    val serializer = {
      Flow[Map[String, String]].map { aMap =>
        val training = OccupationSkillRelSerializer.serializer(aMap, "fr")
        training
      }
    }
    liftyCSVExtractor.extractAsync.via(serializer).grouped(200).via(rdfSerializer.rfdConceptSkillOccupation).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)

  }

  def annotateOccupations = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    implicit val system: ActorSystem = ActorSystem("annotateOccupation")
    lazy val rdfSerializer =new RdfSerializer

    lazy val liftyCSVExtractor = new CSVExtractor(EscoConfig.occupationsLoc, None)
    lazy val lang = EscoConfig.occupationsLang

    val serializer: Flow[Map[String, String], Occupation, NotUsed] = {
      Flow[Map[String,String]].map { aMap =>
        val training = OccupationSerializer.serializer(aMap, lang)
        training
      }
    }
    liftyCSVExtractor.extractAsync.via(serializer).grouped(200).via(rdfSerializer.rdfOccupation).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)
  }

  def annotateSkillGroup = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    implicit val system: ActorSystem = ActorSystem("annotateSkillGroup")
    lazy val rdfSerializer =new RdfSerializer

    lazy val liftyCSVExtractor = new CSVExtractor(EscoConfig.skillGroupsLoc, None)
    lazy val lang = EscoConfig.skillGroupsLang

    val serializer: Flow[Map[String, String], SkillGroup, NotUsed] = {
      Flow[Map[String,String]].map { aMap =>
        val training = SkillGroupSerializer.serializer(aMap, lang)
        training
      }
    }
    liftyCSVExtractor.extractAsync.via(serializer).grouped(200).via(rdfSerializer.rdfSkillGroup).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)
  }

  def annotateSkill = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    implicit val system: ActorSystem = ActorSystem("annotateSkill")
    lazy val rdfSerializer =new RdfSerializer

    lazy val liftyCSVExtractor = new CSVExtractor(EscoConfig.skillsLoc, None)
    lazy val lang = EscoConfig.skillsLang

    val serializer: Flow[Map[String, String], Skill, NotUsed] = {
      Flow[Map[String,String]].map { aMap =>
        val training = SkillSerializer.serializer(aMap, lang)
        training
      }
    }
    liftyCSVExtractor.extractAsync.via(serializer).grouped(200).via(rdfSerializer.rdfConceptSkills).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)
  }

 def annotateSkillHierarchyAnnotate = {
   implicit val ec: ExecutionContextExecutor = ExecutionContext.global
   implicit val system: ActorSystem = ActorSystem("annotateSkillHierarchy")
   lazy val rdfSerializer =new RdfSerializer
   lazy val liftyCSVExtractor = new CSVExtractor(EscoConfig.skillsHierarchyLoc, None)
   lazy val lang = EscoConfig.skillsHierarchyLang

   val serializer: Flow[Map[String, String], SkillHierarchy, NotUsed] = {
     Flow[Map[String,String]].map { aMap =>
       val training = SkillHierarchySerializer.serializer(aMap, lang)
       training
     }
   }
   liftyCSVExtractor.extractAsync.via(serializer).grouped(200).via(rdfSerializer.rdfSkillHierarchy).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)
 }

  def transversalSkillHierarchyAnnotate = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    implicit val system: ActorSystem = ActorSystem("transversalSkillHierarchy")
    lazy val rdfSerializer =new RdfSerializer
    lazy val liftyCSVExtractor = new CSVExtractor(EscoConfig.transversalSkillLoc, None)
    lazy val lang = EscoConfig.transversalSkillLang

    val serializer: Flow[Map[String, String], TransversalSkillsCollection, NotUsed] = {
      Flow[Map[String,String]].map { aMap =>
        val training = TransversalSkillsCollectionSerializer.serializer(aMap, lang)
        training
      }
    }
    liftyCSVExtractor.extractAsync.via(serializer).grouped(200).via(rdfSerializer.rdfTransversalSkillsCollection).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)
  }

  def annotateMnxValueSkill = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    implicit val system: ActorSystem = ActorSystem("MnxAnnotationSkill")
    lazy val rdfSerializer = new RdfSerializer

    lazy val skillliftyCSVExtractor = new CSVExtractor(EscoConfig.skillsLoc, None)
    lazy val skillLang = EscoConfig.skillsLang

    val skillSerializer: Flow[Map[String, String], Skill, NotUsed] = {
      Flow[Map[String,String]].map { aMap =>
        val training = SkillSerializer.serializer(aMap, skillLang)
        training
      }
    }

    skillliftyCSVExtractor.extractAsync.via(skillSerializer).grouped(200).via(rdfSerializer.mnxRdfConceptSkills).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)

  }

  def annotateMnxValueSkillGroup = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    implicit val system: ActorSystem = ActorSystem("MnxAnnotationSkillGroup")

    lazy val skillGroupsLiftyCSVExtractor = new CSVExtractor(EscoConfig.skillGroupsLoc, None)
    lazy val skillGroupsLang = EscoConfig.skillGroupsLang
    lazy val rdfSerializer = new RdfSerializer

    val skillGroupsSerializer: Flow[Map[String, String], SkillGroup, NotUsed] = {
      Flow[Map[String,String]].map { aMap =>
        val training = SkillGroupSerializer.serializer(aMap, skillGroupsLang)
        training
      }
    }
    skillGroupsLiftyCSVExtractor.extractAsync.via(skillGroupsSerializer).grouped(200).via(rdfSerializer.mnxRdfSkillGroup).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)
  }

  def annotateMnxValueOccupation = {
    implicit val ec: ExecutionContextExecutor = ExecutionContext.global
    implicit val system: ActorSystem = ActorSystem("MnxAnnotationOccupation")

    lazy val occupationsLiftyCSVExtractor = new CSVExtractor(EscoConfig.occupationsLoc, None)
    lazy val occupationsLang = EscoConfig.occupationsLang
    lazy val rdfSerializer = new RdfSerializer

    val occupationsSerializer: Flow[Map[String, String], Occupation, NotUsed] = {
      Flow[Map[String,String]].map { aMap =>
        val training = OccupationSerializer.serializer(aMap, occupationsLang)
        training
      }
    }
    occupationsLiftyCSVExtractor.extractAsync.via(occupationsSerializer).grouped(200).via(rdfSerializer.mnxRdfOccupation).via(liftyFileOutput.bulkWrite).runWith(Sink.ignore)
  }
}