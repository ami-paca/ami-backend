package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import com.mnemotix.ami.AnnotationHelper

import java.net.URI
import com.mnemotix.ami.rome.annotator.helpers.EscoConfig
import com.mnemotix.ami.rome.annotator.model.IscoGroup
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// todo check broader relation

object ConceptIscoGroupWriter {

  def annotation(iscoGroups: Seq[IscoGroup]) = {
    iscoGroups.flatMap { iscoGroup =>
      val iscoGroupsAnnotation = Seq(
        LiftyAnnotation(new URI(iscoGroup.uri.trim), s"${EscoConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/esco"), 1.0),
        LiftyAnnotation(new URI(iscoGroup.uri.trim), "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0),
        LiftyAnnotation(new URI(iscoGroup.uri.trim), "a", createIRI(s"${EscoConfig.mnOntoUri}Occupation"), 1.0)
      )
      val codeAnnotations = AnnotationHelper.annotationLiterals(new URI(iscoGroup.uri.trim), Some(iscoGroup.code.trim),"http://www.w3.org/2004/02/skos/core#", "note", "http://www.w3.org/2001/XMLSchema#string")
      val prefLabel = AnnotationHelper.annotationLiteralsLang(new URI(iscoGroup.uri.trim), Some(iscoGroup.prefLabel.trim),"http://www.w3.org/2004/02/skos/core#", "prefLabel", "fr")
      val inScheme = iscoGroup.inScheme.map { scheme =>
        LiftyAnnotation(new URI(iscoGroup.uri.trim), "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(scheme.trim), 1.0)
      }
      iscoGroupsAnnotation ++ codeAnnotations.toSeq ++ prefLabel.toSeq ++ inScheme
    }
  }
}