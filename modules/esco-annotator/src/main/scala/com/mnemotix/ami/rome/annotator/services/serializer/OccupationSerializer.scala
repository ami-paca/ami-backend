/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rome.annotator.services.serializer

import com.mnemotix.ami.rome.annotator.helpers.SerializerUtil
import com.mnemotix.ami.rome.annotator.model.{Occupation}

object OccupationSerializer {

  def serializer(aMap : Map[String, String], langFile: String) = {
    Occupation(
      SerializerUtil.mapValue(aMap, "conceptUri").get,
      SerializerUtil.mapValue(aMap, "iscoGroup").get,
      prefLabel(aMap, langFile),
      altLabels(aMap).toSeq.flatten,
      SerializerUtil.mapValue(aMap, "status").get,
      SerializerUtil.mapValue(aMap, "modifiedDate").get,
      SerializerUtil.mapValue(aMap, "regulatedProfessionNote").get,
      scopeNote(aMap).toSeq.flatten,
      SerializerUtil.mapValue(aMap, "definition"),
      inScheme(aMap).toSeq.flatten,
      SerializerUtil.mapValue(aMap, "description").getOrElse(""),
      langFile
    )
  }

  def prefLabel(aMap : Map[String, String], lang: String)= {
    if (lang == "fr") {
      SerializerUtil.mapValue(aMap, "preferredLabel").get.split("/").toSeq
    }
    else Seq(SerializerUtil.mapValue(aMap, "preferredLabel").get)
  }

  def altLabels(aMap : Map[String, String]) = {
    if(SerializerUtil.mapValue(aMap, "altLabels").isDefined)  {
      Some(SerializerUtil.mapValue(aMap, "altLabels").get.split("\n").toSeq)
    }
    else None
  }

  def scopeNote(aMap : Map[String, String]) = {
    if(SerializerUtil.mapValue(aMap, "scopeNote").isDefined)  {
      Some(SerializerUtil.mapValue(aMap, "scopeNote").get.split("\n").toSeq)
    }
    else None
  }

  def inScheme(aMap : Map[String, String]) = {
    if(SerializerUtil.mapValue(aMap, "inScheme").isDefined)  {
      Some(SerializerUtil.mapValue(aMap, "inScheme").get.split("\n").toSeq)
    }
    else None
  }
}

/*

conceptType	conceptUri	iscoGroup	preferredLabel	altLabels	hiddenLabels	status	modifiedDate	regulatedProfessionNote	scopeNote	definition	inScheme	description

case class Occupations(uri: String, iscoGroup: String, prefLabel: String, altLabel: Seq[String], status: String, modifiedDate: String, regulatedProfessionNote: String, scopeNote: Seq[String], definition: String, inScheme: Seq[String], description: String)

 */