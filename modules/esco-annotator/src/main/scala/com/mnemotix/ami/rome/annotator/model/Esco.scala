package com.mnemotix.ami.rome.annotator.model

import play.api.libs.json.{Json, _}
import play.api.libs.functional.syntax._
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

case class ConceptScheme(uri: String, prefLabel: String, title: Option[String], description: Option[String], topConcept: Seq[String], lang: String)

case class IctSkillsCollection(uri: String, conceptType: String, skillType: String, reuseLevel: String, prefLabel: String, altLabel: Option[Seq[String]], description: String, broaderUri: Option[Seq[String]], broaderConceptIPT: String, lang: String)

case class IscoGroup(uri: String, code: String, prefLabel: String, altLabel: String, inScheme: Seq[String], description: String, lang: String)

case class LanguageSkillsCollection(uri: String, skillType: String, reuseLevel: String, prefLabel: String, altLabel: Option[Seq[String]], description: String, broaderUri: Option[Seq[String]], broaderConceptIPT: String, lang: String)

case class Occupation(uri: String, iscoGroup: String, prefLabel: Seq[String], altLabel: Seq[String], status: String, modifiedDate: String, regulatedProfessionNote: String, scopeNote: Seq[String], definition: Option[String], inScheme: Seq[String], description: String, lang: String)

case class SkillRelation()

case class TransversalSkillsCollection(uri: String,  skillType: String, reuseLevel: String, prefLabel: String, altLabel: Seq[String], description: String, broaderUri: Seq[String], broaderConceptIPT: String, lang: String)

case class SkillHierarchy(level0: String, level0PrefLabel: String, Level1: Option[String], level1PrefLabel:  Option[String], Level2:  Option[String], level2PrefLabel:  Option[String], Level3:  Option[String], level3PrefLabel:  Option[String], description:  Option[String], scopeNote:  Option[String], lang: String)

case class Skill(uri: String, skillType: String, reuseLevel: String, prefLabel: String, altLabel: Seq[String], hiddenLabel: Option[String], status: String, modifiedDate: String
                 ,scopeNote: Option[String], definition: Option[String], inScheme: Seq[String], description: Option[String], lang: String)

case class SkillGroup(uri: String, prefLabel: String, inScheme: Seq[String], lang: String)

case class OccupationSkillRel(occupationUri: String, skillUri: String)