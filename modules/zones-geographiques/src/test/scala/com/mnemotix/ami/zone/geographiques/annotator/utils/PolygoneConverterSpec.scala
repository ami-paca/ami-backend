package com.mnemotix.ami.zone.geographiques.annotator.utils

import akka.stream.scaladsl.Sink
import com.mnemotix.ami.zone.geographiques.annotator.GeographiqueSpec
import com.mnemotix.ami.zone.geographiques.annotator.model.Geographique
import com.mnemotix.ami.zone.geographiques.annotator.services.GeographiqueSerializer
import com.mnemotix.lifty.services.CSVExtractor

import scala.collection.immutable
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


class PolygoneConverterSpec extends GeographiqueSpec {
  it should "converte polygone" in {
    val serializer= new GeographiqueSerializer
    lazy val liftyCSVExtractor = new CSVExtractor("modules/zones-geographiques/src/test/resources/zones-emploi/wkt/D-zones-emploi-59.csv", None)
    val res: Future[immutable.Seq[Geographique]] = liftyCSVExtractor.extractAsync.via(serializer.serialize2010).runWith(Sink.seq)
    val geo = Await.result(res, Duration.Inf)
    println(geo(0).LIBZE2010)
    println(geo(0).wkt)
    println(geo.size)

    val coordinates = PolygoneConverter.converter(geo(0).wkt)
    println(coordinates.size)
    println(coordinates(0))
  }
}
