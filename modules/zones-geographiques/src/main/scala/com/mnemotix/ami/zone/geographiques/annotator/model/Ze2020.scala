package com.mnemotix.ami.zone.geographiques.annotator.model

import play.api.libs.json.Json

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

case class Geometry2020(`type`: Option[String], coordinates: List[List[List[Coordinates]]])

object Geometry2020  {
  implicit lazy val format = Json.format[Geometry2020]
}

case class Properties2020(code_ze2020: Option[String], lib_ze2020: Option[String], fid: Option[Int], code_com: Option[String],
                          code_ze2010: Option[String], lib_ze2010: Option[String], geometry: Geometry2020)

object Properties2020  {
  implicit lazy val format = Json.format[Properties2020]
}

case class Features2020(`type`: Option[String], properties: Option[Properties2020])

object Features2020  {
  implicit lazy val format = Json.format[Features2020]
}

case class Ze2020(`type`: Option[String],
                  name: Option[String],
                  crs: CRS,
                  features: Option[Seq[Features2020]]
                 )

object Ze2020  {
  implicit lazy val format = Json.format[Ze2020]
}