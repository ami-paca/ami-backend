package com.mnemotix.ami.zone.geographiques.annotator.services.annotations

import java.net.URI

import com.mnemotix.ami.zone.geographiques.annotator.helpers.ZonesGeoConfig
import com.mnemotix.ami.zone.geographiques.annotator.model.{Geographique, Ze2020}
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.{LiftyConfig, UriFactory, UriUtils}
import com.mnemotix.lifty.models.LiftyAnnotation
import com.mnemotix.synaptix.core.utils.CryptoUtils

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object GeoWriter {
  def annotation(geo: Geographique): Seq[LiftyAnnotation] = {
    val uriPolygone = new URI(s"${LiftyConfig.dataURI}${CryptoUtils.md5sum(geo.wkt)}")
    Seq(LiftyAnnotation(uriPolygone, "a", createIRI(s"${ZonesGeoConfig.ontologyMNX}Polygon"), 1.0),
      LiftyAnnotation(uriPolygone, "a", createIRI(s"${ZonesGeoConfig.ontologyAMI}JobArea"), 1.0),
      LiftyAnnotation(uriPolygone, s"${ZonesGeoConfig.ontologyMNX}asWKT", createLiteral(geo.wkt, createIRI("http://www.opengis.net/ont/geosparql#wktLiteral")), 1.0),
      LiftyAnnotation(uriPolygone, s"http://purl.org/dc/terms/title", createLiteral(geoNom(geo), createIRI("http://www.w3.org/2001/XMLSchema#string")), 1.0),
      LiftyAnnotation(uriPolygone, s"http://purl.org/dc/terms/identifier", createLiteral(geoCode(geo), createIRI("http://www.w3.org/2001/XMLSchema#string")), 1.0)
    )
  }

  def geoNom(geographique: Geographique) = {
    if (geographique.LIBZE2010.isDefined) {
      geographique.LIBZE2010.get
    }
    else geographique.nom.get
  }

  def geoCode(geographique: Geographique) = {
    if (geographique.code.isDefined) {
      geographique.code.get
    }
    else geographique.code_ze2020.get
  }
}

