package com.mnemotix.ami.zone.geographiques.annotator.services

import java.io.File

import akka.Done
import akka.actor.ActorSystem
import akka.stream.{ClosedShape, IOResult}
import akka.stream.scaladsl.{GraphDSL, RunnableGraph, Sink, Source}
import com.mnemotix.ami.zone.geographiques.annotator.helpers.ZonesGeoConfig
import com.mnemotix.lifty.services.{CSVExtractor, RDFFileOutput}

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class GeographiqueAnnotator {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)

  lazy val liftyFileOutput = new RDFFileOutput()
  lazy val serializer = new GeographiqueSerializer
  lazy val recognizer = new GeographiqueRecognizer()

  def init: Unit = {
    liftyFileOutput.init()
  }

  def annotate: Seq[Future[Done]] = {
    val d = new File(ZonesGeoConfig.inputDir)
    val files = {
      if (d.exists && d.isDirectory) {
        d.listFiles.filter(_.isFile).toList
      } else {
        List[File]()
      }
    }
    
    val futureRes = files.map { file =>
      graph(file.getAbsolutePath)
    }

    futureRes

  }

  def graph(filePath: String): Future[Done] = {
    val liftyCSVExtractor=  new CSVExtractor(filePath, None)
    lazy val topHeadSink = Sink.ignore

    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
        import GraphDSL.Implicits._
        liftyCSVExtractor.extractAsync ~> serializer.serialize2010 ~> recognizer.recognizerAsync ~> liftyFileOutput.bulkWrite ~> topHS
        ClosedShape
    }).run()
  }

}
