package com.mnemotix.ami.zone.geographiques.annotator.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.zone.geographiques.annotator.model.Geographique
import com.mnemotix.ami.zone.geographiques.annotator.services.annotations.GeoWriter
import com.mnemotix.lifty.api.LiftyService
import com.mnemotix.lifty.models.LiftyAnnotation

import scala.concurrent.ExecutionContextExecutor

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class GeographiqueRecognizer(implicit ec: ExecutionContextExecutor) extends LiftyService{

  override def init(): Unit =  logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")

  override def shutdown(): Unit = {}

  def recognizerAsync: Flow[Geographique, Seq[LiftyAnnotation], NotUsed] = {
    Flow[Geographique].map { geo =>
      GeoWriter.annotation(geo)
    }
  }
}