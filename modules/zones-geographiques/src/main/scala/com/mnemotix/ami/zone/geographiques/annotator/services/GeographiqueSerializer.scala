package com.mnemotix.ami.zone.geographiques.annotator.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.zone.geographiques.annotator.model.{Geographique, Ze2020}
import play.api.libs.json.{JsValue, Json}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class GeographiqueSerializer {
  def serialize2010: Flow[Map[String, String], Geographique, NotUsed] = {
    Flow[Map[String, String]].map { aMap =>
      serializer(aMap)
    }
  }

  def mapValue(aMap: Map[String, String], key: String): Option[String] = if (aMap.get(key).isDefined && !aMap.get(key).get.trim.isEmpty && aMap.get(key).get != " ") Some(aMap.get(key).get.trim) else None

  def serializer(aMap: Map[String, String]): Geographique = {
    println(aMap)

    Geographique(
      mapValue(aMap, "WKT").get,
      mapValue(aMap, "code"),
      mapValue(aMap, "nom"),
      mapValue(aMap, "CODGEO"),
      mapValue(aMap, "LIBGEO"),
      mapValue(aMap, "REG"),
      mapValue(aMap, "DEP"),
      mapValue(aMap, "ZE2010"),
      mapValue(aMap, "LIBZE2010"),
      mapValue(aMap, "code_ze2020"),
      mapValue(aMap, "lib_ze2020"),
      mapValue(aMap, "fid"),
      mapValue(aMap, "code_com"),
      mapValue(aMap, "code_ze2010"),
      mapValue(aMap, "lib_ze2010")
    )
  }

  def serialize2020: Flow[String, Ze2020, NotUsed] = {
    Flow[String].map { s =>
      val json: JsValue = Json.parse(s)
      json.as[Ze2020]
    }
  }
}