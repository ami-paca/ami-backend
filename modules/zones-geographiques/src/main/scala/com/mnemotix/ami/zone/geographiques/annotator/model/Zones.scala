package com.mnemotix.ami.zone.geographiques.annotator.model

import play.api.libs.json.Json

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



case class Coordinates(x: Double, y: Double)

object Coordinates  {
  implicit lazy val format = Json.format[Coordinates]
}

case class Polygon(coordinates: List[List[Coordinates]])

object Polygon  {
  implicit lazy val format = Json.format[Polygon]
}

case class Geometry(`type`: String, coordinates: List[List[Coordinates]])

object Geometry  {
  implicit lazy val format = Json.format[Geometry]
}

case class PropertiesFeat(code: String, nom: String, CODGEO: String, LIBGEO: String, REG: String, DEP: String, ZE2010: String, LIBZE2010: String)

object PropertiesFeat  {
  implicit lazy val format = Json.format[PropertiesFeat]
}

case class Features(`type`:  String, properties: PropertiesFeat, geometry: Geometry)

object Features  {
  implicit lazy val format = Json.format[Features]
}

case class Properties(name: String)

object Properties  {
  implicit lazy val format = Json.format[Properties]
}

case class CRS(`type`: String, properties: Properties)

object CRS  {
  implicit lazy val format = Json.format[CRS]
}

case class Zones(`type`: String, crs: CRS, features: Seq[Features])

object Zones  {
  implicit lazy val format = Json.format[Zones]
}

case class Geographique(wkt: String, code: Option[String], nom:  Option[String], CODGEO:  Option[String], LIBGEO:  Option[String], REG:  Option[String], DEP:  Option[String], ZE2010:  Option[String], LIBZE2010: Option[String],
                        code_ze2020: Option[String],
                        lib_ze2020: Option[String],
                        fid: Option[String],
                        code_com: Option[String],
                        code_ze2010: Option[String],
                        lib_ze2010: Option[String])