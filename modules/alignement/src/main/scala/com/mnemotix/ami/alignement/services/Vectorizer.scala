/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import com.mnemotix.analytix.nlp.helpers.TextProcessingConfig
import com.mnemotix.analytix.nlp.textprocessing.SynaptixRawTextProcessing
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer

import java.io.FileInputStream

class Vectorizer {

  lazy val modelFile = new FileInputStream(TextProcessingConfig.wordsEmbeddingModel)
  lazy val model = WordVectorSerializer.readBinaryModel(modelFile, true, true)
  lazy val synaptixRawTextProcessing = new SynaptixRawTextProcessing("french")


  def vectorize(sentence: String): Array[Double] = {
    def vectorizerHelper(listsentence: Seq[String]): Array[Double] = {
      val listVector = listsentence.map { word =>
        model.getWordVector(word)
      }.filterNot(_ == null)
      if (listVector.size > 0) {
        listVector reduceLeft { (x, y) =>
          (x,y).zipped.map(_+_)
        }
      }
      else Array.ofDim(model.vectorSize())
    }
    val words = synaptixRawTextProcessing.words(sentence)
    vectorizerHelper(words)
  }
}
