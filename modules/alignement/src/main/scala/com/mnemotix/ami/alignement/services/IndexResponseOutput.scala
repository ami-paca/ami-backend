/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.synaptix.core.GenericService
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.bulk.BulkResponse
import com.sksamuel.elastic4s.requests.indexes.IndexResponse
import com.sksamuel.elastic4s.requests.update.UpdateResponse

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration

class IndexResponseOutput(implicit ec: ExecutionContext) extends GenericService {
  override def init: Unit = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
  }

  var failure:Int = 0
  var success: Int = 0

  def outputIndexResponse: Flow[Seq[Future[Response[IndexResponse]]], Unit, NotUsed] = {
    Flow[Seq[Future[Response[IndexResponse]]]].map(indexResponses =>  Future.sequence(indexResponses)).map { responses =>
      val indexesResponses: Seq[Response[IndexResponse]] = Await.result(responses, Duration.Inf)
      //indexesResponses.map(indexResponse => logger.info(s"status: ${indexResponse.status}; success: ${indexResponse.isSuccess}; failure: ${indexResponse.isError}"))
      indexesResponses.foreach { indexResponse =>
        if (indexResponse.isSuccess) success = success + 1
        else failure = failure + 1
      }
      logger.info(s"There where $success success and $failure failure")
    }
  }

  def bulkResponse: Flow[Future[Response[BulkResponse]], Unit, NotUsed] = {
    Flow[Future[Response[BulkResponse]]].map { rsp =>
     val bulkResponse = Await.result(rsp, Duration.Inf)
      if (bulkResponse.isSuccess) success = success + bulkResponse.result.successes.size
      else failure = failure + bulkResponse.result.failures.size
    }
  }

  def outputUpdateResponse: Flow[Seq[Future[Response[UpdateResponse]]], Unit, NotUsed] = {
    Flow[Seq[Future[Response[UpdateResponse]]]].map(indexResponses =>  Future.sequence(indexResponses)).map { responses =>
      val indexesResponses: Seq[Response[UpdateResponse]] = Await.result(responses, Duration.Inf)

      indexesResponses.foreach { indexResponse =>

        if (indexResponse.isSuccess) success = success + 1
        else  {
          logger.error(indexResponse.body.getOrElse(" "))
          failure = failure + 1
        }
      }
      logger.info(s"There where $success success and $failure failure")
      //indexesResponses.map(indexResponse => logger.info(s"status: ${indexResponse.status}; success: ${indexResponse.isSuccess}; failure: ${indexResponse.isError}"))
    }
  }

  override def shutdown: Unit = {}
}
