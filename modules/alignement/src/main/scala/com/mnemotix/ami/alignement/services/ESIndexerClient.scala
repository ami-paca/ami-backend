/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.alignement.model.{Alignement, Cible, DataAlignerResult}
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.{ESIndexable, ESMappingDefinitions}
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.bulk.BulkResponse
import com.sksamuel.elastic4s.requests.indexes.admin.IndexExistsResponse
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration

class ESIndexerClient(implicit ec: ExecutionContext) extends LazyLogging  {

  lazy val mppn: ESMappingDefinitions = ESMappingDefinitions(Json.parse(
    """{
      |  "properties": {
      |         "cible": {
      |            "properties": {
      |               "confidence": {
      |                "type": "long"
      |               },
      |               "dataProperty": {
      |                 "type": "keyword"
      |               },
      |               "scheme": {
      |                 "type": "keyword"
      |               },
      |               "uri": {
      |                  "type": "keyword"
      |               },
      |               "vocabulary": {
      |                 "type": "keyword"
      |               }
      |             }
      |           },
      |            "scheme": {
      |               "type": "keyword"
      |             },
      |             "uri": {
      |               "type": "keyword"
      |             },
      |             "vocabulary": {
      |                "type": "keyword"
      |             }
      |           }
      |         }
      |""".stripMargin
  ))



  def init: Unit = IndexClient.init()

  def createIndex(indexName: String): (Boolean, String) = {
    val exist = isExist(indexName)
    if (exist.result.exists) (true, indexName)
    else {
      (Await.result(IndexClient.createIndex(indexName, mppn.toMappingDefinition()), Duration.Inf).isSuccess, indexName)
    }
  }

  def flow(indexName: String): Flow[Seq[DataAlignerResult], Future[Response[BulkResponse]], NotUsed] = {
    Flow[Seq[DataAlignerResult]].grouped(50).map { datas =>
      insertDatas(indexName, datas.flatten)
    }
  }

  def insertDatas(indexName: String, dataAlignerResults:Seq[DataAlignerResult]): Future[Response[BulkResponse]] = {
    val indexables = dataAlignerResults.map { data =>
      ESIndexable( java.util.UUID.randomUUID.toString,
        Some(Json.toJson(Alignement(data.source.uri, data.source.vocabulary, data.source.scheme, Seq(Cible(data.cible.uri, data.cible.vocabulary, data.cible.scheme, data.confiance, data.dataProperty)))).as[JsObject])
      )
    }
    IndexClient.bulkInsert(indexName, indexables:_*)
  }

  private def isExist(indexName: String): Response[IndexExistsResponse] = {
    Await.result(IndexClient.indexExists(indexName), Duration.Inf)
  }
}