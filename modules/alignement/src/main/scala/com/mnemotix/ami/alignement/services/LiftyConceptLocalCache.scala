/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}
import com.mnemotix.ami.alignement.model.DataAligner
import com.mnemotix.synaptix.cache.helpers.CacheConfig

import java.net.URI
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

class LiftyConceptLocalCache {

  implicit val system = ActorSystem()
  implicit val executionContext: ExecutionContext = system.dispatcher
  lazy val vectorizer = new Vectorizer
  lazy val extracter = new RDFExtracter()

  def init: Unit = {
    extracter.init
  }

  def schemeUri = {
    extracter.schemes.map { fut =>
      fut.map { scheme =>
        scheme.sheme
      }
    }
  }


  def run: Seq[Future[Done]] = {
    val schemes = Await.result(schemeUri, Duration.Inf)
    schemes map { sheme =>
      val datas = extracter.dataAligner(sheme)
      graph(datas, sheme)
    }
  }

  def graph(dataAligner: Future[Seq[DataAligner]], schemeUri: String): Future[Done] = {
    val labelProcesser = new LabelProcesser(vectorizer, cacheName(schemeUri))
    labelProcesser.init
    val flow: Source[Seq[Future[Unit]], NotUsed] = Source.future(dataAligner).via(labelProcesser.flow)
    flow.runWith(Sink.ignore)
  }

  def cacheName(schemeUri: String) = {
    val path = new URI(schemeUri).getPath
    val schemeName = path.substring(path.lastIndexOf("/") + 1)
    s"${CacheConfig.cacheDirLoc}/dev-openemploi-${schemeName}"
  }
}
