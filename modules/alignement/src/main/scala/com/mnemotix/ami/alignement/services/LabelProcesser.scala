/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Flow
import akka.util.ByteString
import com.mnemotix.ami.TextProcessing
import com.mnemotix.ami.alignement.model.DataAligner
import com.mnemotix.synaptix.cache.RocksDBStore
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

import java.nio.charset.StandardCharsets
import scala.concurrent.{ExecutionContext, Future}

class LabelProcesser(vectorizer: Vectorizer, cacheName: String)(implicit system: ActorSystem, executionContext: ExecutionContext) extends LazyLogging{

    val UTF8: String = StandardCharsets.UTF_8.name
    val store = new RocksDBStore(cacheName)

  def init = store.init

  def flow: Flow[Seq[DataAligner], Seq[Future[Unit]], NotUsed] = {
    Flow[Seq[DataAligner]].map { datas =>
      val toDump = labelProcess(datas)
      logger.info(s"We will dump ${toDump.size} data to align on ${cacheName}")
      dump(toDump)
    }
  }

  def labelProcess(dataAligners :Seq[DataAligner]): Seq[DataAligner] = {
    dataAligners.filter { dataAligner =>
      val newLabel = TextProcessing.words(dataAligner.label, Some(7), false)
      newLabel.isDefined
    }.map { dataAligner =>
      val newLabel = TextProcessing.words(dataAligner.label, Some(7), false)
      val v = vector(newLabel.get)
      DataAligner(dataAligner.uri, newLabel.get, dataAligner.vocabulary, dataAligner.scheme, Some(v))
    }
  }

  def vector(label: String) = {
    vectorizer.vectorize(label)
  }

  def dump(dataAligners :Seq[DataAligner]) = {
    dataAligners.map { dataAligner =>
      val k = dataAligner.uri.getBytes(UTF8)
      val json = Json.toJson(dataAligner)
      val v = ByteString(Json.prettyPrint(json).getBytes(UTF8))
      store.put(k, v)
    }
  }
}
