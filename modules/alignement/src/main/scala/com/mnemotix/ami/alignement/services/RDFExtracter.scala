/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import com.mnemotix.ami.alignement.model.{DataAligner, Scheme}
import com.mnemotix.synaptix.rdf.client.RDFClient

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class RDFExtracter(implicit ec: ExecutionContext) {

  def init: Boolean = {
    RDFClient.init()
  }

  implicit lazy val conn = {
    RDFClient.getReadConnection(("dev-openemploi"))
  }

  val vocSchemeQryString = """PREFIX ope: <http://openemploi.datasud.fr/ontology/>
                             |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                             |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
                             |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                             |
                             |SELECT ?voc ?scheme ?label
                             |WHERE {
                             | ?voc rdf:type mnx:Vocabulary .
                             | ?scheme rdf:type skos:ConceptScheme .
                             | ?scheme skos:prefLabel ?label .
                             | ?scheme mnx:schemeOf ?voc .
                             |}""".stripMargin

  def conceptInfo(schemeURI: String) =
                  s"""PREFIX ope: <http://openemploi.datasud.fr/ontology/>
                      |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                      |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
                      |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                      |PREFIX dsud: <http://ontology.datasud.fr/openemploi/data/>
                      |
                      |SELECT ?uriScheme ?label ?concept ?voc
                      |WHERE {
                      |    BIND(<$schemeURI> AS ?uriScheme) .
                      |    ?concept rdf:type skos:Concept .
                      |	   ?concept skos:prefLabel ?label .
                      |    ?concept skos:inScheme ?uriScheme .
                      |    ?uriScheme mnx:schemeOf ?voc .
                      |}""".stripMargin


  def schemes: Future[Seq[Scheme]] = {
    var schemesList = new ListBuffer[Scheme]()
    val futureschemesAsync = RDFClient.select(vocSchemeQryString).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        schemesList += Scheme(bs.getValue("voc").stringValue(), bs.getValue("scheme").stringValue(), bs.getValue("label").stringValue())
      }
      schemesList.toSeq
    }
    futureschemesAsync onComplete {
      case Success(i) => println(s"The schemes query succeed. The number of shemes : ${i.size}")
      case Failure(t) => println("There is a failure here", t)
    }
    futureschemesAsync
  }

  def dataAligner(conceptSchemes: Seq[String]):  Future[Seq[DataAligner]] = {
    val futureAlignerAsync = conceptSchemes.map { conceptScheme =>
      dataAligner(conceptScheme)
    }
    val future = Future.sequence(futureAlignerAsync).map(_.flatten)

    future onComplete {
      case Success(i) => println(s"The concept query succeed. The number of concept : ${i.size}")
      case Failure(t) => println("There is a failure here", t)
    }
    future
  }

  def dataAligner(conceptScheme: String):  Future[Seq[DataAligner]] = {
    val dataAlignerList = new ListBuffer[DataAligner]()
    val futureAlignerAsync =  RDFClient.select(conceptInfo(conceptScheme)).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        dataAlignerList += DataAligner(bs.getValue("concept").stringValue(), bs.getValue("label").stringValue(), bs.getValue("voc").stringValue()
          ,bs.getValue("uriScheme").stringValue(), None)
      }
      dataAlignerList.toSeq
    }

    futureAlignerAsync onComplete {
      case Success(i) => println(s"The concept query succeed for scheme ${conceptScheme}. The number of concept : ${i.size}")
      case Failure(t) => println("There is a failure here", t)
    }
    futureAlignerAsync
  }
}