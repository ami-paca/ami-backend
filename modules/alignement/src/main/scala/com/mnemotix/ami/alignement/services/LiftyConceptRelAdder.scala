/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}
import com.mnemotix.ami.alignement.model.{DataAligner, DataAlignerResult}
import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.cache.helpers.CacheConfig
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

import java.net.URI
import java.nio.charset.StandardCharsets
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

class LiftyConceptRelAdder(indexName: String) extends LazyLogging {
  implicit val system = ActorSystem()
  implicit val executionContext: ExecutionContext = system.dispatcher
  lazy val extracter = new RDFExtracter()
  lazy val similarityComputer = new SimilarityComputer

  val schemeFilter = Seq("http://ontology.datasud.fr/openemploi/data/scheme/4")

  val schemeCouple = Seq(("http://ontology.datasud.fr/openemploi/data/scheme/2","http://data.europa.eu/esco/concept-scheme/occupations"),
    ("http://ontology.datasud.fr/openemploi/data/scheme/3","http://data.europa.eu/esco/concept-scheme/skills")
  )

  def schemeUri = {
    extracter.schemes.map { fut =>
      fut.map { scheme =>
        scheme.sheme
      }
    }
  }

  def run: Seq[Unit] = {
    extracter.init
    val schemes = Await.result(schemeUri, Duration.Inf)
    logger.info(s"we will compute relation for ${schemes.size} schemes")

    schemes.filterNot(scheme => schemeFilter.contains(scheme)).map { scheme =>
      val datasStartTime = System.currentTimeMillis()
      val futDatas: Future[Seq[DataAligner]] = extracter.dataAligner(scheme)
      val res = graph(futDatas, scheme)
      Await.result(res, Duration.Inf)
      logger.info(s">> Time took to compute data alignement for ${scheme} => {} s", (System.currentTimeMillis() - datasStartTime)/ 100)

    }
  }

  def runCouple = {
    extracter.init
    schemeCouple.map { couple =>
      val datasStartTime = System.currentTimeMillis()
      val futDatas: Future[Seq[DataAligner]] = extracter.dataAligner(couple._1)
      val futDatas2 = extracter.dataAligner(couple._2)

      val c1 = Await.result(getDatas(futDatas, couple._1), Duration.Inf)
      val c2 = Await.result(getDatas(futDatas2, couple._2), Duration.Inf)
      val res = graphCouple(c1, c2)
      Await.result(res, Duration.Inf)
      logger.info(s">> Time took to compute data alignement for ${couple._1} and ${couple._2} => {} s", (System.currentTimeMillis() - datasStartTime)/ 100)
    }
  }

  def graphCouple(c: Seq[DataAligner], c2: Seq[DataAligner]) = {
    val indexer = new ESIndexerClient()
    val indexResponseOutput = new IndexResponseOutput()
    indexer.init
    indexer.createIndex(indexName)
    Source.future(Future(c, c2)).via(similarityComputer.flow2).via(indexer.flow(indexName)).via(indexResponseOutput.bulkResponse).runWith(Sink.ignore)
  }

  def graph(futDatas: Future[Seq[DataAligner]], scheme: String): Future[Done] = {
    val indexer = new ESIndexerClient()
    val indexResponseOutput = new IndexResponseOutput()
    indexer.init
    indexer.createIndex(indexName)
    val datas = getDatas(futDatas, scheme)
    Source.future(datas).via(similarityComputer.flow).via(indexer.flow(indexName)).via(indexResponseOutput.bulkResponse).runWith(Sink.ignore)
  }

  def getDatas(datas: Future[Seq[DataAligner]], scheme: String) = {
      val UTF8: String = StandardCharsets.UTF_8.name
      val cache = cacheName(scheme)
      val store = new RocksDBStore(cache)
      store.init

      datas.map { fut =>
        fut.map { data =>
          val futContent = store.get(data.uri)
          val option = Await.result(futContent, Duration.Inf)
          option
        }.filter(_.isDefined).map { option =>
          val content = Json.parse(option.get.utf8String).as[DataAligner]
          //val content = Await.result(futContent, Duration.Inf).toSeq.map(_.utf8String).map(Json.parse(_).as[DataAligner])
          content
        }
      }
  }

  def cacheName(schemeUri: String) = {
    val path = new URI(schemeUri).getPath
    val schemeName = path.substring(path.lastIndexOf("/") + 1)
    s"${CacheConfig.cacheDirLoc}/dev-openemploi-${schemeName}"
  }
}