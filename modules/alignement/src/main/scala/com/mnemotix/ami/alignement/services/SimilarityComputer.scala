/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.alignement.model.{ConceptVector, DataAligner, DataAlignerResult}
import com.github.jelmerk.knn.scalalike._
import com.github.jelmerk.knn.scalalike.hnsw._
import com.mnemotix.analytix.nlp.distance.StringMetrics
import com.typesafe.scalalogging.LazyLogging

class SimilarityComputer extends LazyLogging {

  def flow: Flow[Seq[DataAligner], Seq[DataAlignerResult], NotUsed] = {
    Flow[Seq[DataAligner]].map { datas =>
      similarity(datas) ++ inclusion(datas).map(_.toSeq).flatten
    }
  }

  def flow2 = {
    Flow[(Seq[DataAligner], Seq[DataAligner])].map { double =>
      similarity(double._1, double._2) ++ inclusion(double._1, double._2).map(_.toSeq).flatten
    }
  }

  def similarity(source: Seq[DataAligner]): Seq[DataAlignerResult]  = {
    logger.info(s"we will compute inner similarity for ${source.size} vectors")
    val vectors = toConceptVector(source)
    val index = populateIndex(vectors.map(_._1))
    vectors.map { v =>
      val results = predict(index, v._1, 10)
      results.filter(_.distance() < 0.4).map { r =>
        DataAlignerResult(v._2, extractDataAligner(source, r.item().id),  r.distance(), "skos:related")

      }
    }.flatten
  }

  def inclusion(source: Seq[DataAligner]): Seq[Option[DataAlignerResult]] = {
    for {
      x <- source
      y <- source
      i = if (x.uri != y.uri) {
        val xWords = x.label.split(" ")
        val yWords = y.label.split(" ")
        if (xWords.size > yWords.size &&  xWords.intersect(yWords).size > 2) {
          Some(DataAlignerResult(y, x, 0.7, "skos:narrower"))
        }
        else if (yWords.size > xWords.size &&  xWords.intersect(yWords).size > 2)  {
          Some(DataAlignerResult(y, x, 0.7, "skos:narrower"))

        }
        else None
      }
      else None

    } yield (i)
  }

  def inclusion(source: Seq[DataAligner], cible: Seq[DataAligner]): Seq[Option[DataAlignerResult]] = {
    for {
      s <- source
      c <- cible
      i = if (s.label.indexOf(c.label) > -1) Some(DataAlignerResult(s, c, s.label.indexOf(c.label), "skos:narrower")) else None
    } yield i
  }

  def similarity(source: Seq[DataAligner], cible: Seq[DataAligner]): Seq[DataAlignerResult] = {
    val vectorsSource = toConceptVector(source)
    val vectorsCible = toConceptVector(cible)
    val index = populateIndex(vectorsCible.map(_._1))

    vectorsSource.map { couple =>
      val results = predict(index, couple._1, 10)
      results.filter(_.distance() < 0.4).map { r =>
        if (r.distance() == 0.0) {
          DataAlignerResult(couple._2, extractDataAligner(cible, r.item().id) ,r.distance(),"skos:exactMatch")
        }
        else {
          DataAlignerResult(couple._2, extractDataAligner(cible, r.item().id) ,r.distance(),"skos:closeMatch")
        }
      }
    }.flatten
  }

  def stringCompute(s1: String, s2: String): Double = {
    val levenshtein = StringMetrics.levenshtein(s1, s2)
    val ngram = StringMetrics.ngram(s1, s2, 3).getOrElse(1.0)
    val jaroWinkler = StringMetrics.jaroWinkler(s1, s2)
    val diceSorensen = StringMetrics.diceSorensen(s1, s2, 3).getOrElse(1.0)
    val ratcliffObershelp = StringMetrics.ratcliffObershelp(s1,s2).getOrElse(1.0)
    val comp = (levenshtein + ngram + jaroWinkler + diceSorensen + ratcliffObershelp) / 5
    comp
  }

  def extractDataAligner(dataAligners: Seq[DataAligner], id: String): DataAligner = {
    dataAligners.filter(_.uri == id).last
  }

  def toConceptVector(dataAligners: Seq[DataAligner]): Seq[(ConceptVector, DataAligner)] = {
    dataAligners.filter(_.vector.isDefined).map {  data =>
      (ConceptVector(data.uri, data.vector.get.toArray.map(_.toFloat)), data)
    }
  }

  def populateIndex(conceptVector: Seq[ConceptVector]): HnswIndex[String, Array[Float], ConceptVector, Float] = {
    val dimensions = conceptVector(0).vector.length

    val hnswIndex: HnswIndex[String, Array[Float], ConceptVector, Float] = HnswIndex[String, Array[Float], ConceptVector, Float](dimensions, floatCosineDistance, maxItemCount = conceptVector.size)
    hnswIndex.addAll(conceptVector, listener = (workDone: Int, max: Int) =>
      logger.info(s"Added $workDone out of $max words to the index")
    )
    hnswIndex
  }

  def predict(hnswIndex: HnswIndex[String, Array[Float], ConceptVector, Float], percoVector: ConceptVector, k: Int): Seq[SearchResult[ConceptVector, Float]] = {
    hnswIndex.findNearest(percoVector.vector, k)
  }

}
