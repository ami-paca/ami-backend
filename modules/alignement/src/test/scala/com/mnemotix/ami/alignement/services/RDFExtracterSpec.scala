/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import com.mnemotix.ami.alignement.AlignementSpec

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class RDFExtracterSpec extends AlignementSpec {

  it should "get all scheme of database" in {
    val rdfExtracter = new RDFExtracter()
    rdfExtracter.init
    val fut = rdfExtracter.schemes
    val schemes = Await.result(fut, Duration.Inf)
    schemes.map(println(_))
    schemes.size should be > 0
  }

  it should "get datas to do an alignement" in {
    val rdfExtracter = new RDFExtracter()
    rdfExtracter.init
    val fut = rdfExtracter.dataAligner("http://ontology.datasud.fr/openemploi/data/scheme/2")
    val scheme2 = Await.result(fut, Duration.Inf)
    scheme2.map(println(_))
    println(scheme2.size)
    scheme2.size should be > 0
  }

}
