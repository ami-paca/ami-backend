/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import com.mnemotix.ami.TextProcessing
import com.mnemotix.ami.alignement.AlignementSpec
import com.mnemotix.synaptix.cache.helpers.CacheConfig

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

class LabelProcesserSpec extends AlignementSpec {

  it should "vectorize a label" in {
    val vectorizer = new Vectorizer
    val cacheName = "testLabelProcesser"
    val labelProcesser = new LabelProcesser(vectorizer, cacheName)

    val rdfExtracter = new RDFExtracter()
    rdfExtracter.init
    val fut = rdfExtracter.dataAligner("http://ontology.datasud.fr/openemploi/data/scheme/2")
    val scheme2 = Await.result(fut, Duration.Inf)
    println(scheme2(0).label)
    val cleaned = TextProcessing.words(scheme2(0).label, Some(7), false)
    println(cleaned)
    val array = labelProcesser.vector(cleaned.getOrElse(scheme2(0).label))
    println(array)
    array.size should be > 0
  }

  it should "process label" in {
    val vectorizer = new Vectorizer
    val cacheName = "testLabelProcesser"
    val labelProcesser = new LabelProcesser(vectorizer, cacheName)
    labelProcesser.init
    val rdfExtracter = new RDFExtracter()
    rdfExtracter.init
    val fut = rdfExtracter.dataAligner("http://ontology.datasud.fr/openemploi/data/scheme/2")
    val scheme2 = Await.result(fut, Duration.Inf)
    val cleaned = labelProcesser.labelProcess(scheme2)

    println(s"before cleaned ${scheme2.size}")
    println(s"after cleaned ${cleaned.size}")

    cleaned.size should be > 0
  }

  it should "dump data aligners" in {
    val vectorizer = new Vectorizer
    val cacheName = s"${CacheConfig.cacheDirLoc}test-2"
    val labelProcesser = new LabelProcesser(vectorizer, cacheName)
    labelProcesser.init

    val rdfExtracter = new RDFExtracter()
    rdfExtracter.init
    val fut = rdfExtracter.dataAligner("http://ontology.datasud.fr/openemploi/data/scheme/2")
    val scheme2 = Await.result(fut, Duration.Inf)

    val cleanerStart = System.currentTimeMillis()
    val cleaned = labelProcesser.labelProcess(scheme2).take(200)
    println(s">> Time took to clean data ${cleaned.size} => ${(System.currentTimeMillis() - cleanerStart)/ 100} s")

    val datasStartTime = System.currentTimeMillis()
    val futDump = Future.sequence(labelProcesser.dump(cleaned))
    val res = Await.result(futDump, Duration.Inf)
    println(s">> Time took to dump data ${res.size} => ${(System.currentTimeMillis() - datasStartTime)/ 100} s")
    println(res.size)
    res.size should be > 0
  }

}
