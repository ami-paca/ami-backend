/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import com.mnemotix.ami.alignement.AlignementSpec
import com.mnemotix.synaptix.cache.helpers.CacheConfig

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

class LiftyConceptRelAdderSpec extends AlignementSpec {

  it should "get values from cache" in {
    val relAdder = new LiftyConceptRelAdder("dev-openemploi-alignement-inner")

    val vectorizer = new Vectorizer
    val cacheName = s"${CacheConfig.cacheDirLoc}test-2"
    val labelProcesser = new LabelProcesser(vectorizer, cacheName)
    labelProcesser.init

    val rdfExtracter = new RDFExtracter()
    rdfExtracter.init
    val fut = rdfExtracter.dataAligner("http://ontology.datasud.fr/openemploi/data/scheme/2")
    val scheme2 = Await.result(fut, Duration.Inf).take(200)
    val f = Future(scheme2)
    val datas = relAdder.getDatas(f, "http://ontology.datasud.fr/openemploi/data/scheme/2")
    val res = Await.result(datas, Duration.Inf)
    res.foreach(r => println(r.vector))
  }

  it should "add relation" in {
    val relAdder = new LiftyConceptRelAdder("dev-openemploi-alignement-inner")
    relAdder.run
  }

  it should "add outer relation" in {
    val relAdder = new LiftyConceptRelAdder("dev-openemploi-alignement-outer")
    relAdder.runCouple
  }

}
