/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.alignement.services

import com.mnemotix.ami.alignement.AlignementSpec

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

class LiftyConceptLocalCacheSpec extends AlignementSpec {
  it should "extract the cache name" in {
    val liftyConceptLocalCache = new LiftyConceptLocalCache
    val schemeURI = "http://ontology.datasud.fr/openemploi/data/scheme/2"
    liftyConceptLocalCache.cacheName(schemeURI) shouldEqual "dev-openemploi-2"
  }

  it should "add lifty concept to cache" in {
    val liftyConceptLocalCache = new LiftyConceptLocalCache
    liftyConceptLocalCache.init
    val fut = liftyConceptLocalCache.run
    val oneFut = Future.sequence(fut)
    Await.result(oneFut, Duration.Inf)
  }
}