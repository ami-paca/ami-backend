/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rncp.annotator.model

case class Statistiques(annee: Option[String], nombreCertifies: Option[String], nombreCertifiesVae:Option[String], tauxInsertionGlobal6mois: Option[String],
                        tauxInsertionMetier6mois: Option[String], tauxInsertionMertier2ans: Option[String])

case class Legal(publicationsDecretGeneral: Option[String], publicationDecretCreation: Option[String], datePremier: Option[String], dateDernier:Option[String])

case class Jury(juryFI: Option[String], juryCA: Option[String], juryFC: Option[String], juryCQ: Option[String], juryCL: Option[String], juryVAE: Option[String])

case class BlocCompetences(code: Option[String], libelle: Option[String], competences: Option[String])

case class Certificateur(siretCertificateur: Option[String], nomeCertificateur: Option[String])

case class Partenaire(siretPartenaire: Option[String], nomPartenaire: Option[String], habilitationPartenaire: Option[String])

case class InfoFiche(idFiche: Option[String], numeroFiche: Option[String], intitule: Option[String], nouvelle_certification: Option[String], ancienne_certification: Option[String])

case class Abrege(code: Option[String], libelle: Option[String])

case class Formacode(code: Option[String], libelle: Option[String])

case class NomenclatureEU(niveau: Option[String], intitule: Option[String])

case class CodeNsf(nsf: Option[String], intitule: Option[String])

case class CcnInfo(numero: Option[String], libelle: Option[String])

case class Ccn(ccn1: Option[CcnInfo], ccn2: Option[CcnInfo], ccn3: Option[CcnInfo])

case class CodesRome(code: Option[String], libelle: Option[String])

case class EnregistrementInfo(dateFinEnregistrement: Option[String], typeEnregistrement: Option[String])

case class AccessibiliteTOM(accessible_nouvelle_caledonie: Option[String], accessible_polynesie_francaise: Option[String])

case class Rncp(fiche: InfoFiche,
                abrege: Abrege,
                formacode: Option[Seq[Formacode]],
                nomenclatureEurope: Option[NomenclatureEU],
                ccn: Option[CcnInfo],
                codeNsf: Option[Seq[CodeNsf]],
                certificateur: Option[Seq[Certificateur]],
                partenaire: Option[Seq[Partenaire]],
                objectifdsContexte: Option[String],
                activiteVisees: Option[String],
                capacites_attestees: Option[String],
                secteursActivite: Option[String],
                typeEmploisAccessibles: Option[String],
                codes_rome: Option[Seq[CodesRome]],
                jury: Option[Jury],
                legal: Option[Legal],
                statistiques: Option[Statistiques],
                enregistrementInfo: Option[EnregistrementInfo],
                blocCompetences: Option[Seq[BlocCompetences]],
                isActive: Option[String]
               )
