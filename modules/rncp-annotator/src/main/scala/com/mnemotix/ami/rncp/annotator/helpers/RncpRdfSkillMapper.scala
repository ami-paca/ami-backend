/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rncp.annotator.helpers

import java.net.URI

import com.mnemotix.ami.rncp.annotator.model.{BlocCompetences, Rncp}
import com.mnemotix.lifty.models.LiftyAnnotation
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig

object RncpRdfSkillMapper {

  val dct = "http://purl.org/dc/terms/"
  val mm = "https://ontologies.mindmatcher.org/carto/"
  val mnx = "http://ns.mnemotix.com/ontologies/2019/8/generic-model/"
  val ami = "http://ontology.datasud.fr/openemploi/"
  val rdfs = "http://www.w3.org/2000/01/rdf-schema#"

  def rncpSkill(rncp: Rncp): Seq[LiftyAnnotation] = {
    Seq(
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}vocabulary/rncp"), "http://www.w3.org/2004/02/skos/core#prefLabel", createLiteral("Référentiel des compétences RNCP","fr"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}vocabulary/rncp"), "http://www.w3.org/2004/02/skos/core#altLabel", createLiteral("RNCP","fr"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}vocabulary/rncp"), "a", createIRI(s"${mnx}Vocabulary"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}rncp/scheme/3"), "a", createIRI("http://www.w3.org/2004/02/skos/core#ConceptScheme"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}rncp/scheme/3"), "http://www.w3.org/2004/02/skos/core#prefLabel", createLiteral("Compétences","fr"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}rncp/scheme/3"), s"${mnx}schemeOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/rncp"), 1.0)
    ) ++
    blocCompetencesAnnotation(rncp.blocCompetences).toSeq.flatten
  }
  def blocCompetencesAnnotation(blocCompetences: Option[Seq[BlocCompetences]]) = {
    if (blocCompetences.isDefined) {
      Some(blocCompetences.get.flatMap { bloc =>
        if (bloc.competences.isDefined && bloc.code.isDefined) {
          val groupUri = RncpUriFactory.groupSkill(bloc.code.get)
          val comps = RncpTextProcessing.skillExtractor(bloc.competences.get)
          val groupAnnotation = Seq(LiftyAnnotation(groupUri,  "a", createIRI(s"${ami}SkillGroup"), 1.0),
            LiftyAnnotation(groupUri, s"${rdfs}label", createLiteral(bloc.libelle.get, createIRI("http://www.w3.org/2001/XMLSchema#string")), 1.0))
          val comp = comps.flatMap { comp =>
            val compUri = RncpUriFactory.skillURI(comp)
            Seq(LiftyAnnotation(compUri, s"${mnx}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/rncp"), 1.0),
              LiftyAnnotation(compUri, s"${ami}memberOf", createIRI(groupUri.toString), 1.0),
              LiftyAnnotation(compUri, "a", createIRI(s"${mm}Skill"), 1.0),
              LiftyAnnotation(compUri, "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(s"${LiftyConfig.dataURI}rncp/scheme/3"), 1.0),
              LiftyAnnotation(compUri, "a", createIRI(s"${mm}Skill"), 1.0),
              LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}rncp/scheme/3"), "http://www.w3.org/2004/02/skos/core#hasTopConcept", createIRI(compUri.toString), 1.0)
            ) ++ competenceLiteralsFR(compUri, Some(comp), "http://www.w3.org/2004/02/skos/core#", "prefLabel").toSeq
          }
          groupAnnotation ++ comp
        }
        else None

      })
    }
    else None
  }

  def competenceLiteralsFR[A](appelationsURI: URI, literal: Option[A], ontologyURI: String, property: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(appelationsURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, "fr"), 1.0))
    }
    else None
  }
}
// competenceLiteralsFR(uriCompetenceDeBase, libelle, "http://www.w3.org/2004/02/skos/core#", "prefLabel").toSeq
