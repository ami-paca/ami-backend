/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rncp.annotator.helpers

import java.net.URI

import com.mnemotix.ami.rncp.annotator.model.{BlocCompetences, CodesRome, Rncp}
import com.mnemotix.lifty.models.LiftyAnnotation
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}


object RdfMapper {

  val dct = "http://purl.org/dc/terms/"
  val mm = "https://ontologies.mindmatcher.org/carto/"
  val mnx = "http://ns.mnemotix.com/ontologies/2019/8/generic-model/"
  val ami = "http://ontology.datasud.fr/openemploi/"

  def annotation(rncp: Rncp): Seq[LiftyAnnotation] = {
    certification(rncp) ++ tagging(rncp)
  }

  def certification(rncp: Rncp) = {
    val uriCertification = RncpUriFactory.certificationURI(rncp.fiche.numeroFiche.get)
    Seq(LiftyAnnotation(uriCertification, "a", createIRI(s"${ami}Certification"), 1.0),
      LiftyAnnotation(uriCertification, s"${dct}title", createLiteral(rncp.fiche.intitule.get, createIRI("http://www.w3.org/2001/XMLSchema#string")), 1.0))
  }

  def tagging(rncp: Rncp) = {
    val uriCertification = RncpUriFactory.certificationURI(rncp.fiche.numeroFiche.get)
    val uriTagging = RncpUriFactory.taggingURI(uriCertification.toString)
    taggingOccupation(uriCertification, uriTagging, rncp.codes_rome).toSeq.flatten ++
    taggingSkill(uriCertification, uriTagging, rncp.blocCompetences).toSeq.flatten
  }


  def taggingOccupation(uriCertification: URI, uriTagging: URI, codes_rome: Option[Seq[CodesRome]]): Option[Seq[LiftyAnnotation]] = {
    if (codes_rome.isDefined) {
      Some(codes_rome.get.flatMap { code_rome =>
        val uriRome = RncpUriFactory.skosRomeURI(code_rome.code.get)
        Seq(LiftyAnnotation(uriTagging, s"${mnx}hasEntity", createIRI(uriCertification.toString), 1.0),
          LiftyAnnotation(uriTagging, s"${mm}hasOccupation", createIRI(uriRome.toString), 1.0),
          LiftyAnnotation(uriRome, "a", createIRI(s"${mm}Occupation"), 1.0))
        }
      )
    }
    else None
  }

  def taggingSkill(uriCertification: URI, uriTagging: URI, blocCompetences: Option[Seq[BlocCompetences]]): Option[Seq[LiftyAnnotation]] = {
    if (blocCompetences.isDefined) {
      Some( blocCompetences.get.flatMap { bloc =>
        if (bloc.competences.isDefined && bloc.code.isDefined) {
          val comps = RncpTextProcessing.skillExtractor(bloc.competences.get)
          comps.flatMap { comp =>
            val compUri = RncpUriFactory.skillURI(comp)
            Seq(LiftyAnnotation(uriTagging, s"${mnx}hasEntity", createIRI(uriCertification.toString), 1.0),
              LiftyAnnotation(uriTagging, s"${mm}hasSkill", createIRI(compUri.toString), 1.0),
              LiftyAnnotation(compUri, "a", createIRI(s"${mm}Skill"), 1.0))
          }
        }
        else None
      })
    }
    else None
  }
}

