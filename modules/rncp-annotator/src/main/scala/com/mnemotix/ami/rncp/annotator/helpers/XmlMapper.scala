/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rncp.annotator.helpers

import com.mnemotix.ami.rncp.annotator.model.{Abrege, BlocCompetences, Certificateur, CodeNsf, CodesRome, Formacode, InfoFiche, Jury, NomenclatureEU, Partenaire, Rncp}

import scala.collection.immutable

object XmlMapper {
  def mapper(elem: scala.xml.Node): Rncp = {
    Rncp(infoFiche(elem), abrege(elem), formacode(elem), Some(nomenclatureEU(elem)),None, codeNsf(elem),certificateur(elem), partenaire(elem),
      if ((elem \ "OBJECTIFS_CONTEXTE").isEmpty) None else Some((elem \ "OBJECTIFS_CONTEXTE").text),
    if ((elem \ "ACTIVITES_VISEES").isEmpty) None else Some((elem \ "ACTIVITES_VISEES").text),
    if ((elem \ "CAPACITES_ATTESTEES").isEmpty) None else Some((elem \ "CAPACITES_ATTESTEES").text),
    if ((elem \ "SECTEURS_ACTIVITE").isEmpty) None else Some((elem \ "SECTEURS_ACTIVITE").text),
    if ((elem \ "TYPE_EMPLOI_ACCESSIBLES").isEmpty) None else Some((elem \ "TYPE_EMPLOI_ACCESSIBLES").text),
      codesRomes(elem),
      None,
      None,
      None,
      None,
      blocCompetences(elem),
      if ((elem \ "ACTIF").isEmpty) None else Some((elem \ "ACTIF").text)
    )
  }

  def infoFiche(elem: scala.xml.Node): InfoFiche = {
    InfoFiche(
      if ((elem \ "ID_FICHE").isEmpty) None else Some((elem \ "ID_FICHE").text),
      if ((elem \ "NUMERO_FICHE").isEmpty) None else Some((elem \ "NUMERO_FICHE").text),
      if ((elem \ "INTITULE").isEmpty) None else Some((elem \ "INTITULE").text),
      if ((elem \ "NOUVELLE_CERTIFICATION").isEmpty) None else Some((elem \ "NOUVELLE_CERTIFICATION").text),
      if ((elem \ "ANCIENNE_CERTIFICATION").isEmpty) None else Some((elem \ "ANCIENNE_CERTIFICATION").text),
    )
  }

  def abrege(elem: scala.xml.Node): Abrege = {
    Abrege(
      if ((elem \ "ABREGE" \ "CODE").isEmpty) None else Some((elem \ "ABREGE" \ "CODE").text),
      if ((elem \ "ABREGE" \ "LIBELLE").isEmpty) None else Some((elem \ "ABREGE" \ "LIBELLE").text)
    )
  }

  def formacode(elem: scala.xml.Node) = {
    if ((elem \ "FORMACODES").isEmpty) {
      None
    }
    else {
     Some((elem \ "FORMACODES" \ "FORMACODE").map { n =>
        Formacode(
          if ((n \ "CODE").isEmpty) None else Some((n \ "CODE").text),
          if ((n \ "LIBELLE").isEmpty) None else Some((n \ "LIBELLE").text)
        )
      })
    }
  }

  def nomenclatureEU(elem: scala.xml.Node) = {
    NomenclatureEU(
      if ((elem \ "NOMENCLATURE_EUROPE" \ "NIVEAU").isEmpty) None else Some((elem \ "NOMENCLATURE_EUROPE" \ "NIVEAU").text),
      if ((elem \ "NOMENCLATURE_EUROPE" \ "INTITULE").isEmpty) None else Some((elem \ "NOMENCLATURE_EUROPE" \ "INTITULE").text)
    )
  }

  def codeNsf(elem: scala.xml.Node): Option[immutable.Seq[CodeNsf]] = {
    if ((elem \ "CODES_NSF").isEmpty) {
      None
    }
    else {
      Some((elem \ "CODES_NSF" \ "NSF" ).map { n =>
        CodeNsf(
          if ((n \ "CODE").isEmpty) None else Some((n \ "CODE").text),
          if ((n \ "LIBELLE").isEmpty) None else Some((n \ "INTITULE").text)
        )
      })
    }
  }

  def certificateur(elem: scala.xml.Node) = {
    if ((elem \ "CERTIFICATEURS").isEmpty) {
      None
    }
    else {
      Some((elem \ "CERTIFICATEURS" \ "CERTIFICATEUR").map { n =>
        Certificateur(
          if ((n \ "SIRET_CERTIFICATEUR").isEmpty) None else Some((n \ "SIRET_CERTIFICATEUR").text),
          if ((n \ "NOM_CERTIFICATEUR").isEmpty) None else Some((n \ "NOM_CERTIFICATEUR").text)
        )
      })
    }
  }

  def partenaire(elem: scala.xml.Node) = {
    if ((elem \ "PARTENAIRES").isEmpty) {
      None
    }
    else {
      Some((elem \ "PARTENAIRES" \ "PARTENAIRE").map { n =>
        Partenaire(
          if ((n \ "SIRET_PARTENAIRE").isEmpty) None else Some((n \ "SIRET_PARTENAIRE").text),
          if ((n \ "NOM_PARTENAIRE").isEmpty) None else Some((n \ "NOM_PARTENAIRE").text),
          if ((n \ "HABILITATION_PARTENAIRE").isEmpty) None else Some((n \ "HABILITATION_PARTENAIRE").text)
        )
      })
    }
  }

  def codesRomes(elem:scala.xml.Node) = {
    if ((elem \\ "CODES_ROME").isEmpty) {
      None
    }
    else {
      Some((elem \ "CODES_ROME" \ "ROME").map { n =>
        CodesRome(
          if ((n  \ "CODE").isEmpty) None else Some((n \ "CODE").text),
          if ((n \ "LIBELLE").isEmpty) None else Some((n \ "LIBELLE").text)
        )
      })
    }
  }

  def jury(elem:scala.xml.Node) = {
    Jury(
      if ((elem \ "JURY_FI").isEmpty) None else Some((elem \ "JURY_FI").text),
      if ((elem \ "JURY_CA").isEmpty) None else Some((elem \ "JURY_CA").text),
      if ((elem \ "JURY_FC").isEmpty) None else Some((elem \ "JURY_FC").text),
      if ((elem \ "JURY_CQ").isEmpty) None else Some((elem \ "JURY_CQ").text),
      if ((elem \ "JURY_CL").isEmpty) None else Some((elem \ "JURY_CL").text),
      if ((elem \ "JURY_VAE").isEmpty) None else Some((elem \ "JURY_VAE").text),
    )
  }

  def legal(elem: scala.xml.Node) = {
    None
  }

  def blocCompetences(elem: scala.xml.Node): Option[immutable.Seq[BlocCompetences]] = {
    if ((elem \ "BLOCS_COMPETENCES").isEmpty) {
      None
    }
    else {
      Some((elem \ "BLOCS_COMPETENCES" \ "BLOC_COMPETENCES").map { n =>
        BlocCompetences(
          if ((n \ "CODE").isEmpty) None else Some((n \ "CODE").text),
          if ((n \ "LIBELLE").isEmpty) None else Some((n \ "LIBELLE").text),
          if ((n \ "LISTE_COMPETENCES").isEmpty) None else Some((n \ "LISTE_COMPETENCES").text)
        )
      })
    }
  }
}
