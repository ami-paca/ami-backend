/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rncp.annotator.helpers

import java.net.URI

import com.mnemotix.lifty.helpers.{CryptoUtils, LiftyConfig}

object RncpUriFactory {

  def certificationURI(certificationCode: String): URI = {
    new URI(s"${LiftyConfig.dataURI}certification/${CryptoUtils.md5sum(certificationCode)}")
  }

  def skillURI(skillCode: String): URI = {
    new URI(s"${LiftyConfig.dataURI}skill/rncp/${CryptoUtils.md5sum(skillCode)}")
  }

  def groupSkill(groupID: String): URI = {
    new URI(s"${LiftyConfig.dataURI}skillGroup/rncp/${CryptoUtils.md5sum(groupID)}")
  }

  def taggingURI(certicationURI: String): URI = {
    new URI(s"${LiftyConfig.dataURI}tagging/${certicationURI}")
  }

  def skosRomeURI(skos: String): URI = {
    new URI(s"${LiftyConfig.dataURI}occupation/${CryptoUtils.md5sum(skos)}")
  }

}