/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rncp.annotator.helpers

import org.jsoup.Jsoup

import scala.collection.JavaConverters._

object RncpTextProcessing {

  def skillExtractor(groupSkill: String): Seq[String] = {
    if (groupSkill.contains("<ins>") && groupSkill.contains("</ins>")) {
      caseIns(groupSkill).map(cleanStartOfString(_))
    }
    else if (groupSkill.contains("<ul>") && groupSkill.contains("</ul>") && groupSkill.contains("<li>") && groupSkill.contains("</li>")) {
      caseUL(groupSkill).map(cleanStartOfString(_))
    }
    else if (groupSkill.contains("<p><strong>Descriptif")) {
      caseBr(groupSkill).map(cleanStartOfString(_))
    }
    else {
      caseP(groupSkill).map(cleanStartOfString(_))
    }
  }

  def cleanComp(comps: Seq[String]) = {
    comps.map { comp =>
      comp.replaceAll("\\s*\\p{Punct}+\\s*$", "")
    }
  }

  def caseIns(groupSkill: String) = {
    val doc = Jsoup.parse(groupSkill)
    val elements = doc.body().getElementsByTag("ins")
    asScalaBuffer(elements).map {element =>
      element.text()
    }.toSeq
  }

  def caseUL(groupSkill: String) = {
    val groups = groupSkill.split("\\<p\\>Modalités d'évaluation \\:\\<\\/p\\>")
    val doc = Jsoup.parse(groups(0))
    val elements = doc.body().getElementsByTag("li")
    asScalaBuffer(elements).map {element =>
      element.text()
    }.toSeq
  }

  def caseBr(groupSkill: String) = {
    val doc = groupSkill.split("<br />")
    doc.filterNot(_.contains("Descriptif")).filterNot(_.contains("évaluation")).filterNot(_.contains("candidat")).filterNot(_.contains("certificat"))
      .filterNot(_.contains("Attestation"))
  }

  def caseP(groupSkill: String) = {
    val doc = Jsoup.parse(groupSkill)
    val elements = doc.body().getElementsByTag("p")
    asScalaBuffer(elements).map {element =>
      element.text()
    }.toSeq.filterNot(_.isEmpty)
  }

  def cleanStartOfString(str: String): String = {
    str.replaceAll("(?m)^\\w?,\\w?", "").trim
  }
}
