/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rncp.annotator.services

import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{GraphDSL, RunnableGraph, Sink}
import com.mnemotix.lifty.services.RDFFileOutput

class FormationAnnotator(filePath: String) {

  implicit val system: ActorSystem = ActorSystem("formationAnnotator")

  val extractor = new FormationExtractor(filePath)
  val rdfmapper = new FormationRdfMapper
  val output =  new RDFFileOutput()

  lazy val topHeadSink = Sink.ignore


  def annotator[Done] = {
    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
        import GraphDSL.Implicits._
        extractor.extract ~> rdfmapper.mapper ~> output.bulkWrite ~> topHS
        ClosedShape
    }).run()
  }

}
