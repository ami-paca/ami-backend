/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rncp.annotator.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.rncp.annotator.helpers.RdfMapper
import com.mnemotix.ami.rncp.annotator.model.Rncp
import com.mnemotix.lifty.models.LiftyAnnotation


class FormationRdfMapper {
  def mapper: Flow[Rncp, Seq[LiftyAnnotation], NotUsed] = {
    Flow[Rncp].map { rncp =>
      RdfMapper.annotation(rncp)
    }
  }
}