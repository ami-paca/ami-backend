/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rncp.annotator.helpers


import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class RncpTextProcessingSpec extends AnyFlatSpec with Matchers {

  it should "parse competences with p and C1 " in {
    val competences = """<p>C1 - Vérifier le matériel embarqué afin d’assurer la fiabilité et la permanence des remontées d’information au cours de la collecte</p>
                        |<p><br></p>
                        |<p>C2 - Appliquer la procédure de vérification des éléments mécaniques et hydrauliques du véhicule de collecte afin de pouvoir réaliser une tournée en toute sécurité</p>
                        |<p><br></p>
                        |<p>C3 - Communiquer efficacement au responsable les éventuels dysfonctionnements rencontrés et apporter sous sa responsabilité les solutions de premier niveau (réinitialisation de benne, gonflage des pneus…) afin d’optimiser la sécurité de la collecte et le taux d’utilisation des véhicules</p>
                        |<p><br></p>
                        |<p>C4 - Nettoyer efficacement le véhicule de collecte en fin de tournée en tenant compte des règles d’hygiène spécifiques au transport de biodéchets et ce afin de préserver les futurs utilisateurs du véhicule et être en accord avec la réglementation</p>
                        |<p><br></p>
                        |<p>C5 - Porter efficacement des EPI afin de se protéger des risques professionnels</p>
                        |<p><br></p>
                        |<p>C6 - Sécuriser d’éventuelles victimes pendant une tournée afin d'adopter les bons gestes en attendant l’arrivée des secours</p>
                        |<p><br></p>
                        |<p>C7 - Adopter les comportements adaptés à une collecte nocturne afin de se préserver des risques spécifiques à une activité de nuit</p>
                        |<p><br></p>
                        |<p>C8 - Rédiger efficacement et sereinement un constat d’accident afin d’éviter au maximum les conflits liés aux accidents de la route</p>
                        |<p><br></p>
                        |<p>C9 - Sécuriser le véhicule à l’arrêt en fonction des situations rencontrées lors de la collecte</p>
                        |<p><br></p>""".stripMargin
    val comps = RncpTextProcessing.caseP(competences)
    println(comps.size)
    comps.foreach(println(_))
    RncpTextProcessing.skillExtractor(competences).size shouldBe 9
  }

  it should "parse competences with p and <ins> " in {
    val competences = """<p><ins>Réalise les opérations de Rénovation et/ou de Modernisation</ins> · Collecter les informations afin de préparer l'intervention dans conditions maitrisées, en appliquant la réglementation relative aux métiers d'ascensoriste. · Réaliser l'inventaire du matériel et de l'outillage nécessaire, dans le but d'intervenir dans des conditions maitrisées, en prenant en compte les spécificités de l'opération. · Prendre en compte la documentation applicable « dossier de modernisation et/ou de rénovation », afin de proposer des actions adaptées à l'intervention, tout en respectant la réglementation. · S'équiper et utiliser les équipements de protection individuelle adaptés, afin de réaliser l'intervention en toute sécurité, en prenant en compte des risques. · Consigner et mettre en place les signalisations nécessaires, afin de protéger l'environnement, en tenant en compte des spécificités de l'intervention. · Lire et exploiter les schémas électromécaniques et hydrauliques d'un ascenseur, afin de réaliser les opérations de modernisation et/ou de rénovation, suivant les méthodes d'installation et les notices technique de montages (dossier de modernisation). · Utiliser les outils nécessaires aux interventions sur les parties électromécaniques d'un ascenseur, dans le but de démonter et remonter l'ensemble mécanique (vidange et changement de courroies sur des treuils, changement de butées à billes, des vantaux de portes...), selon les schémas des équipements. · Câbler et réaliser les raccordements sur des équipements électriques à remplacer (moteurs, armoires, pendentif), afin de rendre l'installation plus économe en énergie, en tenant en compte des schémas électriques de l'installation. · Démonter et remonter des ensembles électromécaniques sur un ascenseur, afin de changer des pièces (garnitures de coulisseaux, contacts des sélecteurs mécaniques, contacts des serrures de portes, garnitures de freins, balais des collecteurs de moteurs à courant continu...), selon les schémas des équipements. · Effectuer les réglages et les essais afin de renseigner le rapport d'essai, avant la remise en service de l'ascenseur, tout en transmettant par écrit les informations techniques. · Effectuer le nettoyage des lieux afin de respecter l'environnement de l'intervention.   <ins>Effectuer la réception des travaux</ins> · Valider le rapport d'intervention avec le supérieur hiérarchique, afin de s'assurer que les travaux ont été faits conformément au dossier de modernisation et/ou de rénovation et conforme aux critères de performances. · Réaliser les ajustements, si nécessaire, et la mise en service, dans le but de s'assurer que l'ascenseur répond aux exigences de fonctionnement, selon les normes et la réglementation applicable. · Rédiger une note d'intervention (commentaires, schémas, croquis à main levée…), afin d'avoir une trace écrite de l'intervention, tout en transmettant les informations sur l'ascenseur. Partager des informations techniques au sein de son équipe, et proposer des améliorations dans son travail, afin de réaliser un retour d'expérience de l'intervention. </p>""".stripMargin
    val comps = RncpTextProcessing.caseIns(competences)
    comps.foreach(println(_))
    RncpTextProcessing.skillExtractor(competences).size shouldBe 2

  }

  it should "parse competences with modalite d'évaluation" in {
    val competences = """<ul>
                        |<li>Situer l'espèce choisie dans son environnement,</li>
                        |<li>Décrire les interventions réalisées ou observées en fonction de son degré d'autonomie,</li>
                        |<li>Prendre en compte les protocoles sécuritaires et sanitaires en vigueur dans le parc,</li>
                        |<li>Décrire les changements de comportements décelés, le cas échéant,</li>
                        |<li>Proposer un plan cohérent, des illustrations, des schémas et une qualité rédactionnelle,</li>
                        |<li>Analyser l'évolution de sa pratique de terrain et se situe par rapport au rôle du soigneur animalier.</li>
                        |<li>Proposer des actions correctives pour les activités conduites,</li>
                        |<li>S'exprimer correctement et apporter des réponses pertinentes aux questions posées,</li>
                        |<li>Mobiliser ses connaissances techniques et ses expériences pratiques,</li>
                        |<li>Utiliser des supports de qualité à la transmission du message dans le délai imparti,</li>
                        |<li>Utiliser un vocabulaire précis et professionnel.</li>
                        |</ul>
                        |<p>Modalités d'évaluation :</p>
                        |<ul>
                        |<li>Dossier décrivant les activités de soins quotidiens apportés à une espèce choisie (écrit individuel - coef.2),</li>
                        |<li>Analyse de pratique devant le jury de professionnel (oral individuel - Durée : 20min - coef.2)</li>
                        |</ul>
                        |<p> </p>""".stripMargin

    val comps = RncpTextProcessing.caseUL(competences)
    comps.foreach(println(_))
    RncpTextProcessing.skillExtractor(competences).size shouldBe 11
  }

  it should "parse competences without Modalités d'évaluation" in {
    val competences = """"<ul>
                        |<li>Situer l'espèce choisie dans son environnement,</li>
                        |<li>Décrire les interventions réalisées ou observées en fonction de son degré d'autonomie,</li>
                        |<li>Prendre en compte les protocoles sécuritaires et sanitaires en vigueur dans le parc,</li>
                        |<li>Décrire les changements de comportements décelés, le cas échéant,</li>
                        |<li>Proposer un plan cohérent, des illustrations, des schémas et une qualité rédactionnelle,</li>
                        |<li>Analyser l'évolution de sa pratique de terrain et se situe par rapport au rôle du soigneur animalier.</li>
                        |<li>Proposer des actions correctives pour les activités conduites,</li>
                        |<li>S'exprimer correctement et apporter des réponses pertinentes aux questions posées,</li>
                        |<li>Mobiliser ses connaissances techniques et ses expériences pratiques,</li>
                        |<li>Utiliser des supports de qualité à la transmission du message dans le délai imparti,</li>
                        |<li>Utiliser un vocabulaire précis et professionnel.</li>
                        |</ul>""".stripMargin
    val comps = RncpTextProcessing.caseUL(competences)
    comps.foreach(println(_))
    RncpTextProcessing.skillExtractor(competences).size shouldBe 11
  }

  it should "parse competences with p strong and br" in {
    val competences=  """<p><strong>Descriptif :</strong><br />•  adapter son plan de communication au contexte et au marché de l'entreprise<br />•  bâtir une stratégie de communication  <br />•  Mettre en place des enquêtes internes et externes<br />•  Participer à élaborer la stratégie de commuication et définir les enjeux et objectifs<br /><strong>Modalités d'évaluation :</strong> <br />Études de cas :  À partir d’un SWOT, l’étudiant doit définir une stratégie de communication et à partir d’une analyse de marché, l’étudiant doit identifier la problématique et définir le dispositif d’écoute à mettre en place.<br />cas de Com'outside : le candidat doit traiter de problématiques d'entreprise visant a définir une stratégie de communication et à présenter des recommandations.<br />Management de projet : le candidat pourra s'appuyer sur son expérience de mise en  œuvre d'un projet de communication .<br /><strong>Attestation :</strong><br />Un certificat des compétences attestées selon les modalités d'évaluation décrites sera délivré à chaque candidat/e à l’issue de la validation du 1er bloc de compétences.</p>"""
  }
}
