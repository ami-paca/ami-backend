package com.mnemotix.ami.rome.annotator.services

import com.mnemotix.ami.rome.annotator.services.job.{AmiRomeAnnotatorJob, RomeAnnotatorJob}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object JobRunner extends App {

  lazy val initiator = new Initiator
  lazy val amiRomeAnnotatorJob = new AmiRomeAnnotatorJob
  lazy val romeAnnotatorJob = new RomeAnnotatorJob


  def annotators: Boolean = {
    if (initiator.init) {
      val annotator = amiRomeAnnotatorJob.run()
      initiator.close
      true
    }
    else {
      Thread.sleep(5000)
      annotators
    }
  }
}