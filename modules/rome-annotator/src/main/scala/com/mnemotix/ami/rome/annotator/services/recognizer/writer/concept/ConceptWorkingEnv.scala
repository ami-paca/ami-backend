package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import java.net.URI
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.ami.rome.annotator.helpers.{RomeConfig, RomeUriFactory}
import com.mnemotix.ami.rome.annotator.model.EnvironnementsTravail
import com.mnemotix.lifty.helpers.AnnotationsUtils.createIRI
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object ConceptWorkingEnv {
  def annotation(environnementsTravail: Option[List[EnvironnementsTravail]]): Option[Seq[LiftyAnnotation]] = {
    if (environnementsTravail.isDefined) {
      val eTA: Seq[Option[Seq[LiftyAnnotation]]] = environnementsTravail.get.map { environnementTravail =>
        if (environnementTravail.code.isDefined) {
          val envWorkURI = RomeUriFactory.envWorkURI(environnementTravail.code.get)
          val conceptOf = Seq(LiftyAnnotation(envWorkURI, s"${RomeConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/rome"), 1.0))

          val literals: Seq[LiftyAnnotation] = environnementTravailLiterals(envWorkURI, environnementTravail.code, "http://www.w3.org/2004/02/skos/core#", "notation", "http://www.w3.org/2001/XMLSchema#string").toSeq ++ environnementTravailLiterals(envWorkURI, environnementTravail.libelle, "http://www.w3.org/2004/02/skos/core#", "prefLabel", "http://www.w3.org/2001/XMLSchema#string").toSeq
          Some(conceptOf ++ literals ++ Seq(LiftyAnnotation(envWorkURI, "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0),
            LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}scheme/4"), "http://www.w3.org/2004/02/skos/core#hasTopConcept", createIRI(envWorkURI.toString), 1.0),
              LiftyAnnotation(envWorkURI, "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(s"${LiftyConfig.dataURI}scheme/4"), 1.0)))
        }
        else None
      }
      Some(eTA.flatten.flatten)
    }
    else None
  }

  def environnementTravailLiterals[A](codeOccupationURI: URI, literal: Option[A], ontologyURI: String, property: String, datatype: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(codeOccupationURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, createIRI(datatype)), 1.0))
    }
    else None
  }

}