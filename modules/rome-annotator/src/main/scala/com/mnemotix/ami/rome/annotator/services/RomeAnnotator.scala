package com.mnemotix.ami.rome.annotator.services

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{Broadcast, GraphDSL, Merge, RunnableGraph, Sink}
import com.mnemotix.ami.rome.annotator.model.FicheMetier
import com.mnemotix.lifty.models.LiftyAnnotation
import com.mnemotix.lifty.services.RDFFileOutput

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RomeAnnotator {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)

  lazy val liftyFileOutput = new RDFFileOutput()
  lazy val ficheMetierSerializer = new FicheMetierSerializer()
  lazy val conceptRecognizer = new Recognizer()
  lazy val esExtractor = new ESExtractor()

  lazy val topHeadSink = Sink.ignore

  lazy val qry =
    """
      |{
      | "from" : 0, "size" : 550,
      | "query": {
      |   "match_all": {}
      | }
      |}
      |""".stripMargin

  def init: Unit = {
    liftyFileOutput.init()
  }

  def annotator: Future[Done] = {
    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
        import GraphDSL.Implicits._
        esExtractor.extractAsync(qry) ~> ficheMetierSerializer.serialize ~> conceptRecognizer.recognizerAsync ~> liftyFileOutput.bulkWrite ~> topHS
        ClosedShape
    }).run()
  }

  def annotatorSkills: Future[Done] = {
    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
        import GraphDSL.Implicits._
        esExtractor.extractAsync(qry) ~> ficheMetierSerializer.serialize ~> conceptRecognizer.competencesAsync ~> liftyFileOutput.bulkWrite ~> topHS
        ClosedShape
    }).run()
  }

}
