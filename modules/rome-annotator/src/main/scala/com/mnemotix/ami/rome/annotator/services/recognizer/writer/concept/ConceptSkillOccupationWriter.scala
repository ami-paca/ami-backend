package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import java.net.URI

import com.mnemotix.ami.rome.annotator.helpers.{RomeConfig, RomeUriFactory}
import com.mnemotix.ami.rome.annotator.model.{Appellations, CompetencesDeBase, GroupesCompetencesSpecifiques}
import com.mnemotix.lifty.helpers.AnnotationsUtils.createIRI
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object ConceptSkillOccupationWriter {
  def annotation(ficheMetierCode:Option[String],  appellations: Option[List[Appellations]], competencesDeBase: Option[List[CompetencesDeBase]], groupesCompetencesSpecifiques: Option[List[GroupesCompetencesSpecifiques]]):Option[Seq[LiftyAnnotation]] = {
    val uriSkillDeBase: Option[List[URI]] = if (competencesDeBase.isDefined) {
      Some(competencesDeBase.get.filter(_.code.isDefined).map { skill =>
        RomeUriFactory.skillURI(skill.code.get, "competence")
      })
    }
    else None

    val uriSkillSpecifique: Option[Seq[URI]] =
      if (groupesCompetencesSpecifiques.isDefined) {
      val res: Seq[Option[List[URI]]] =  groupesCompetencesSpecifiques.get.map { competences =>
        if (competences.competences.isDefined) {
          Some(competences.competences.get.filter(_.code.isDefined).map(competence => RomeUriFactory.skillURI(competence.code.get, "competence")))
        }
        else None
      }
        Some(res.flatten.flatten.seq)
    }
    else None

    val urisSkill = uriSkillDeBase.toSeq.flatten ++ uriSkillSpecifique.toSeq.flatten

    Some(annotationSkills(urisSkill, appellations).toSeq.flatten ++ annotationMetierSkills(urisSkill, ficheMetierCode).toSeq.flatten)
  }

  def annotationSkills(urisSkill: Seq[URI], appellations: Option[List[Appellations]]): Option[Seq[LiftyAnnotation]] = {
    if (appellations.isDefined) {
      val annotations: Seq[Seq[LiftyAnnotation]] = appellations.get.filter(_.code.isDefined).map { appellation =>
         val appelationsURI = RomeUriFactory.occupationRomeURI(appellation.code.get)
        urisSkill.flatMap { uriSkill =>
          Seq(
          LiftyAnnotation(uriSkill, s"${RomeConfig.mnOntoUri}hasOccupation", createIRI(appelationsURI.toString), 1.0)
            //LiftyAnnotation(uriSkill, s"http://www.w3.org/2004/02/skos/core#related", createIRI(appelationsURI.toString), 1.0)
          )
        }
      }
      Some(annotations.flatten)
    }
    else None
  }

  def annotationMetierSkills(urisSkill: Seq[URI], ficheMetierCode:Option[String]): Option[Seq[LiftyAnnotation]] = {
    if (ficheMetierCode.isDefined) {
        val appelationsURI = RomeUriFactory.skosRomeURI(ficheMetierCode.get)
        val annotations = urisSkill.flatMap { uriSkill =>
          Seq(
          LiftyAnnotation(uriSkill, s"${RomeConfig.mnOntoUri}hasOccupation", createIRI(appelationsURI.toString), 1.0)
            //LiftyAnnotation(uriSkill, s"http://www.w3.org/2004/02/skos/core#related", createIRI(appelationsURI.toString), 1.0)
          )
        }
      Some(annotations)
    }
    else None
  }
}
