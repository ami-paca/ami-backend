package com.mnemotix.ami.rome.annotator.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.rome.annotator.model.FicheMetier
import com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept.{ConceptOccupationWriter, ConceptProDomainWriter, ConceptSchemeWriter, ConceptSkillGroup, ConceptSkillOccupationWriter, ConceptSkillWriter, ConceptWorkingEnv, VocabularyWriter}
import com.mnemotix.ami.rome.annotator.services.recognizer.writer.{CompetenceAnnotationWriter, OccupationAnnotationWriter, WorkingAnnotationWriter}
import com.mnemotix.lifty.api.LiftyService
import com.mnemotix.lifty.models.LiftyAnnotation

import scala.concurrent.ExecutionContextExecutor

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Recognizer(implicit ec: ExecutionContextExecutor) extends LiftyService {
  override def init(): Unit = logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")

  override def shutdown(): Unit = {}

  def recognizerAsync: Flow[Seq[FicheMetier], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[FicheMetier]].map { ficheMetiers =>
      ficheMetiers.flatMap { ficheMetier =>
        CompetenceAnnotationWriter.annotation(ficheMetier.competencesDeBase, ficheMetier.groupesCompetencesSpecifiques) ++
        OccupationAnnotationWriter.annotation(ficheMetier) ++
          WorkingAnnotationWriter.annotation(ficheMetier.environnementsTravail).toSeq.flatten ++
        ConceptOccupationWriter.annotations(ficheMetier.code, ficheMetier.appellations).toSeq.flatten ++
          ConceptProDomainWriter.annotation(ficheMetier.code, ficheMetier.libelle, ficheMetier.definition, ficheMetier.domaineProfessionnel).toSeq.flatten ++
          ConceptSchemeWriter.schemeAnnotation ++ VocabularyWriter.vocabulataireAnnotation ++
          ConceptSkillOccupationWriter.annotation(ficheMetier.code, ficheMetier.appellations, ficheMetier.competencesDeBase, ficheMetier.groupesCompetencesSpecifiques).toSeq.flatten ++
        ConceptSkillWriter.annotations(ficheMetier.competencesDeBase, ficheMetier.groupesCompetencesSpecifiques) ++
          ConceptSkillGroup.annotations(ficheMetier.competencesDeBase, ficheMetier.groupesCompetencesSpecifiques) ++
        ConceptWorkingEnv.annotation(ficheMetier.environnementsTravail).toSeq.flatten
      }
    }
  }

  def competencesAsync: Flow[Seq[FicheMetier], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[FicheMetier]].map { ficheMetiers =>
      ficheMetiers.flatMap { ficheMetier =>
        ConceptSchemeWriter.schemeAnnotation ++ ConceptSkillWriter.annotations(ficheMetier.competencesDeBase, ficheMetier.groupesCompetencesSpecifiques) ++
          ConceptSkillGroup.annotations(ficheMetier.competencesDeBase, ficheMetier.groupesCompetencesSpecifiques) ++ CompetenceAnnotationWriter.annotation(ficheMetier.competencesDeBase, ficheMetier.groupesCompetencesSpecifiques)
      }
    }
  }
}