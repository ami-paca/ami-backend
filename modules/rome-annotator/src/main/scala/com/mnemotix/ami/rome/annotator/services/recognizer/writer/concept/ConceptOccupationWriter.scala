package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import java.net.URI

import com.mnemotix.ami.rome.annotator.helpers.{RomeConfig, RomeUriFactory}
import com.mnemotix.ami.rome.annotator.model.Appellations
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object ConceptOccupationWriter {
  def annotations(code: Option[String], appellations: Option[List[Appellations]]): Option[Seq[LiftyAnnotation]] = {
    if (code.isDefined && appellations.isDefined) {
      val codeMetierURI: URI = RomeUriFactory.skosRomeURI(code.get)
      val liftyAnnotations: Seq[Option[Seq[LiftyAnnotation]]] = appellations.get.map { appellation =>
        appellationsAnnotation(codeMetierURI, appellation)
      }
      Some(liftyAnnotations.flatten.flatten.toSeq)
    }
    else None
  }

  def appellationsAnnotation(codeMetierURI: URI, appellation: Appellations): Option[Seq[LiftyAnnotation]] = {
    if (appellation.code.isDefined) {
      val appelationsURI = RomeUriFactory.occupationRomeURI(appellation.code.get)
      val conceptOf = Seq(LiftyAnnotation(appelationsURI, s"${RomeConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/rome"), 1.0),
        LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}scheme/2"), "http://www.w3.org/2004/02/skos/core#hasTopConcept", createIRI(appelationsURI.toString), 1.0)
      )
      val annotationNotation =  LabelUtil.occupationLiterals(appelationsURI, appellation.code, "http://www.w3.org/2004/02/skos/core#", "notation", "http://www.w3.org/2001/XMLSchema#string").toSeq
      val annotationSchemeRelated = Seq(LiftyAnnotation(appelationsURI, "http://www.w3.org/2004/02/skos/core#related", createIRI(codeMetierURI.toString), 1.0),
        LiftyAnnotation(appelationsURI, "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(s"${LiftyConfig.dataURI}scheme/2"), 1.0))
      val labels: Seq[LiftyAnnotation] = prefLabelAnnotation(appelationsURI, appellation.libelle).toSeq ++
        altLabelsAnnotation(appelationsURI, appellation.libelle).toSeq.flatten ++ hiddenLabelAnnotation(appelationsURI, "fr", appellation.libelle).toSeq.flatten
      Some(annotationSchemeRelated ++ labels ++ annotationNotation ++ conceptOf)
    }
    else None
  }

  def prefLabelAnnotation(appelationsURI: URI, libelle: Option[String]): Option[LiftyAnnotation] = {
    val prefLabels = LabelUtil.labels(libelle)
    if (prefLabels.isDefined) {
      LabelUtil.occupationLiteralsFR(appelationsURI, Some(prefLabels.get(0)),"http://www.w3.org/2004/02/skos/core#", "prefLabel")
    }
    else None
  }

  def hiddenLabelAnnotation(appelationsURI: URI, lang: String, label: Option[String]): Option[Seq[LiftyAnnotation]] = {
    val hiddensLabels: Option[Seq[String]] = LabelUtil.labels(label)
    val hiddenLabesAnnotation = {
      if (hiddensLabels.isDefined) {
        Some((LabelUtil.occupationLiteralsFR(appelationsURI, LabelUtil.hiddenLabel("fr", hiddensLabels.get(0)), "http://www.w3.org/2004/02/skos/core#", "hiddenLabel") ++
          LabelUtil.occupationLiteralsFR(appelationsURI, LabelUtil.hiddenLabel("fr", hiddensLabels.get(1)), "http://www.w3.org/2004/02/skos/core#", "hiddenLabel")).toSeq)
      }
      else None
    }
    hiddenLabesAnnotation
  }

  def altLabelsAnnotation(appelationsURI: URI, libelle: Option[String]): Option[Seq[LiftyAnnotation]] = {
    val altsLabels = LabelUtil.labels(libelle)
    val altsLabelsAnnotation: Option[Seq[LiftyAnnotation]] = {
      if (altsLabels.isDefined) {
        Some((LabelUtil.occupationLiteralsFR(appelationsURI, Some(altsLabels.get(0)), "http://www.w3.org/2004/02/skos/core#", "altLabel") ++
          LabelUtil.occupationLiteralsFR(appelationsURI, Some(altsLabels.get(1)), "http://www.w3.org/2004/02/skos/core#", "altLabel")).toSeq)
      }
      else None
    }
    altsLabelsAnnotation
  }
}