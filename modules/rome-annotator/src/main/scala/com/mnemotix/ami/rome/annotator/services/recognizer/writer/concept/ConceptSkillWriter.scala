package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import com.mnemotix.ami.MnxAnalytixWriter

import java.net.URI
import com.mnemotix.ami.rome.annotator.helpers.{RomeConfig, RomeUriFactory}
import com.mnemotix.ami.rome.annotator.model.{Competences, CompetencesDeBase, GrandDomaine, GroupesCompetencesSpecifiques, NoeudCompetence, RacineCompetence}
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation

import scala.collection.immutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * oep:coreSkill -> compétence de bases
 * oep:specificSkill -> compétences spécifiques
 * oep:softSkill -> savoirEtre
 * oep:expertSkill -> savoirFaire
 */


object ConceptSkillWriter {

    def annotations(competencesDeBase: Option[List[CompetencesDeBase]], groupesCompetencesSpecifiques: Option[List[GroupesCompetencesSpecifiques]]) = {
      groupesCompetencesSpecifiquesAnnotate(groupesCompetencesSpecifiques).toSeq.flatten ++ competencesDeBaseAnnotate(competencesDeBase).toSeq.flatten
    }

    def groupesCompetencesSpecifiquesAnnotate(groupesCompetencesSpecifiques: Option[List[GroupesCompetencesSpecifiques]]): Option[Seq[LiftyAnnotation]] = {
      if (groupesCompetencesSpecifiques.isDefined) {
        val la: Seq[Option[Seq[LiftyAnnotation]]] = groupesCompetencesSpecifiques.get.map { groupeComptenceSpecifique =>
          if (groupeComptenceSpecifique.competences.isDefined) {
            competencesSpecifiques(groupeComptenceSpecifique.competences)
          }
          else None
        }
        Some(la.flatten.flatten.toSeq)
      }
      else None
    }

  def competencesDeBaseAnnotate(competencesDeBase: Option[List[CompetencesDeBase]]): Option[Seq[LiftyAnnotation]] = {
    if (competencesDeBase.isDefined) {
      val competenceDeBaseLA: Seq[Option[Seq[LiftyAnnotation]]] = competencesDeBase.get.map { competencesDeBase =>
        if (competencesDeBase.code.isDefined) {
          val typeDeCompetence =  if (competencesDeBase.typeCompetence.isDefined && competencesDeBase.typeCompetence.get == "savoirFaire") true else false
          val uriCompetenceDeBase = RomeUriFactory.skillURI(competencesDeBase.code.get, "competence")
          Some(
              competencesAnnotations(uriCompetenceDeBase, competencesDeBase.code,competencesDeBase.libelle, true, typeDeCompetence) ++
                noeudCompetence(uriCompetenceDeBase, competencesDeBase.noeudCompetence).toSeq.flatten
          )
        }
        else None
      }
      Some(competenceDeBaseLA.flatten.flatten.toSeq)
    }
    else None
  }

  def competencesSpecifiques(competences: Option[List[Competences]]): Option[immutable.Seq[LiftyAnnotation]] = {
      if (competences.isDefined) {
        val la = competences.get.map { competence =>
          if (competence.code.isDefined) {
            val uriCompetenceDeBase = RomeUriFactory.skillURI(competence.code.get, "competence")
            val typeDeCompetence =  if (competence.typeCompetence.isDefined && competence.typeCompetence.get == "savoirFaire") true else false
            val cdba = competencesAnnotations(uriCompetenceDeBase, competence.code, competence.libelle, false, typeDeCompetence)

            val competenceAnnotations: Option[Seq[LiftyAnnotation]] = if (competence.noeudCompetence.isDefined) {
              Some(noeudCompetence(uriCompetenceDeBase, competence.noeudCompetence.get).toSeq.flatten)
            } else None
            Some(competenceAnnotations.toSeq.flatten ++ cdba)
          }
          else None
        }
        Some(la.flatten.flatten.toSeq)
      }
      else None
    }

  def noeudCompetence(uriCompetenceDeBase: URI, noeudCompetence: NoeudCompetence): Option[Seq[LiftyAnnotation]] = {
    if (noeudCompetence.code.isDefined) {
      val uriNoeudCompetence = RomeUriFactory.skillURI(noeudCompetence.code.get, "noeudCompetence")


      Some(
        noeudCompetenceAnnotations(uriCompetenceDeBase, uriNoeudCompetence, noeudCompetence.libelle, noeudCompetence.code) ++
          racineCompetences(uriCompetenceDeBase, uriNoeudCompetence, noeudCompetence.racineCompetence).toSeq.flatten
      )
    }
    else None
  }

  def racineCompetences(uriCompetenceDeBase: URI, uriNoeudCompetence: URI, racineCompetence: Option[RacineCompetence]): Option[Seq[LiftyAnnotation]] = {
    if (racineCompetence.isDefined) {
      if (racineCompetence.get.code.isDefined) {
        val uriRacineCompetence = RomeUriFactory.skillURI(racineCompetence.get.code.get, "racineCompetence")
        Some(racineCompetencesAnnnotations(uriRacineCompetence, uriCompetenceDeBase, uriNoeudCompetence, racineCompetence.get.libelle, racineCompetence.get.code))
      }
      else None
    }
    else None
  }

  /*
    def mnxAnnotations(skills: Seq[Skill]) = {
    skills.flatMap { skill =>
      val uri = new URI(skill.uri.trim)
      MnxAnalytixWriter.annotation(uri, Seq(skill.prefLabel), EscoConfig.percoSkillIndexName, EscoConfig.percoRecogType)
    }
  }
   */

  def racineCompetencesAnnnotations(uriRacineCompetence:URI, uriCompetenceDeBase: URI, uriNoeudCompetence: URI, libelle: Option[String], code: Option[String]) = {

    val analytix: Seq[LiftyAnnotation] = MnxAnalytixWriter.annotation(uriRacineCompetence, libelle.toSeq, RomeConfig.percoSkillIndexName, RomeConfig.percoRecogType)
     Seq(LiftyAnnotation(uriRacineCompetence, s"${RomeConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/rome"), 1.0),
        LiftyAnnotation(uriRacineCompetence, "a", createIRI(s"${RomeConfig.mnOntoUri}Skill"), 1.0),
        LiftyAnnotation(uriRacineCompetence, "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(s"${LiftyConfig.dataURI}scheme/3"), 1.0),
        LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}scheme/3"), "http://www.w3.org/2004/02/skos/core#hasTopConcept", createIRI(uriCompetenceDeBase.toString), 1.0),
        LiftyAnnotation(uriCompetenceDeBase, "http://www.w3.org/2004/02/skos/core#broader", createIRI(uriRacineCompetence.toString), 1.0),
        LiftyAnnotation(uriCompetenceDeBase, "http://www.w3.org/2004/02/skos/core#related", createIRI(uriNoeudCompetence.toString), 1.0),
        LiftyAnnotation(uriNoeudCompetence, "http://www.w3.org/2004/02/skos/core#broader", createIRI(uriRacineCompetence.toString) ,1.0)) ++
      (competenceLiteralsFR(uriRacineCompetence, libelle, "http://www.w3.org/2004/02/skos/core#", "prefLabel") ++
        hiddenLabelAnnotation(uriRacineCompetence, "fr", libelle)).toSeq ++ analytix

  }

    def competencesAnnotations(uriCompetenceDeBase: URI, competenceCode: Option[String], libelle: Option[String], isCore: Boolean, isExpert:Boolean) = {
      val analytix: Seq[LiftyAnnotation] = MnxAnalytixWriter.annotation(uriCompetenceDeBase, libelle.toSeq, RomeConfig.percoSkillIndexName, RomeConfig.percoRecogType)

      val isCoreOrSpecific = if (isCore) {
        competenceLiterals(uriCompetenceDeBase, Some(true), s"${RomeConfig.amiOntoUri}", "coreSkill", "http://www.w3.org/2001/XMLSchema#boolean")
      } else {
        competenceLiterals(uriCompetenceDeBase, Some(true), s"${RomeConfig.amiOntoUri}", "specificSkill", "http://www.w3.org/2001/XMLSchema#boolean")
      }
      val competenceType = if (isExpert) {
        competenceLiterals(uriCompetenceDeBase, Some(true), s"${RomeConfig.amiOntoUri}", "expertSkill", "http://www.w3.org/2001/XMLSchema#boolean")
      } else {
        None
      }
      isCoreOrSpecific.toSeq ++ competenceType.toSeq ++ Seq(LiftyAnnotation(uriCompetenceDeBase, s"${RomeConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/rome"), 1.0),
        LiftyAnnotation(uriCompetenceDeBase, "a", createIRI(s"${RomeConfig.mnOntoUri}Skill"), 1.0),
        LiftyAnnotation(uriCompetenceDeBase, "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(s"${LiftyConfig.dataURI}scheme/3"), 1.0)
      ) ++ competenceLiteralsFR(uriCompetenceDeBase, libelle, "http://www.w3.org/2004/02/skos/core#", "prefLabel").toSeq ++
      (competenceLiterals(uriCompetenceDeBase, competenceCode, "http://www.w3.org/2004/02/skos/core#", "notation", "http://www.w3.org/2001/XMLSchema#string")++ hiddenLabelAnnotation(uriCompetenceDeBase, "fr", libelle)) ++ analytix

    }

  def noeudCompetenceAnnotations(uriCompetenceDeBase: URI, uriNoeudCompetence: URI, libelle: Option[String], code: Option[String]) = {
    val analytix: Seq[LiftyAnnotation] = MnxAnalytixWriter.annotation(uriNoeudCompetence, libelle.toSeq, RomeConfig.percoSkillIndexName, RomeConfig.percoRecogType)


    Seq(LiftyAnnotation(uriNoeudCompetence, s"${RomeConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/rome"), 1.0),
      LiftyAnnotation(uriNoeudCompetence, "a", createIRI(s"${RomeConfig.mnOntoUri}Skill"), 1.0),
      LiftyAnnotation(uriNoeudCompetence, "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(s"${LiftyConfig.dataURI}scheme/3"), 1.0)) ++
      competenceLiteralsFR(uriNoeudCompetence, libelle, "http://www.w3.org/2004/02/skos/core#", "prefLabel").toSeq ++
      competenceLiterals(uriNoeudCompetence, code, "http://www.w3.org/2004/02/skos/core#", "notation", "http://www.w3.org/2001/XMLSchema#string").toSeq ++
      hiddenLabelAnnotation(uriNoeudCompetence, "fr", libelle).toSeq
  }


    def hiddenLabelAnnotation(uriRacineCompetence: URI, lang: String, label: Option[String]): Option[LiftyAnnotation] = {
      if (label.isDefined) {
        val hidden: Option[String] = LabelUtil.hiddenLabel(lang,label.get)
        competenceLiteralsFR(uriRacineCompetence, hidden, "http://www.w3.org/2004/02/skos/core#", "hiddenLabel")
      }
      else None
    }

    def competenceLiterals[A](appelationsURI: URI, literal: Option[A], ontologyURI: String, property: String, datatype: String): Option[LiftyAnnotation] = {
      if (literal.isDefined) {
        Some(LiftyAnnotation(appelationsURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, createIRI(datatype)), 1.0))
      }
      else None
    }

    def competenceLiteralsFR[A](appelationsURI: URI, literal: Option[A], ontologyURI: String, property: String): Option[LiftyAnnotation] = {
      if (literal.isDefined) {
        Some(LiftyAnnotation(appelationsURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, "fr"), 1.0))
      }
      else None
    }
}