package com.mnemotix.ami.rome.annotator.helpers

import java.net.URI

import com.mnemotix.lifty.helpers.LiftyConfig

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object RomeUriFactory {

  def skosRomeURI(skos: String): URI = {
    new URI(s"${LiftyConfig.dataURI}occupation/${RomeIDFactory.skosRomeID(skos)}")
  }

  def occupationRomeURI(codeRome: String): URI = {
    new URI(s"${LiftyConfig.dataURI}occupation/${RomeIDFactory.occupationID(codeRome)}")
  }

  def skillURI(skillCode: String, skillType: String): URI = {
    new URI(s"${LiftyConfig.dataURI}skill/${RomeIDFactory.skillID(skillCode, skillType)}")
  }

  def groupSkill(skillUris: Seq[String]): URI = {
    new URI(s"${LiftyConfig.dataURI}skillGroup/${RomeIDFactory.skillGroupID(skillUris)}")
  }

  def envWorkURI(envWorkId: String): URI = {
    new URI(s"${LiftyConfig.dataURI}envWork/${RomeIDFactory.envWorkID(envWorkId)}")
  }
}