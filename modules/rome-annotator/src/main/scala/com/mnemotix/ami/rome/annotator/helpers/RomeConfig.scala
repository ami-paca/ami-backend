package com.mnemotix.ami.rome.annotator.helpers

import com.typesafe.config.ConfigFactory

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object RomeConfig {
  lazy val conf = Option(ConfigFactory.load().getConfig("rome")).getOrElse(ConfigFactory.empty())
  lazy val romeName = conf.getString("es.index.name")
  lazy val mnxOntoUri = conf.getString("annotator.ontology.mnx.uri")
  lazy val mnOntoUri = conf.getString("annotator.ontology.mm.uri")
  lazy val amiOntoUri = conf.getString("annotator.ontology.ami.uri")

  lazy val percoSkillIndexName = conf.getString("perco.skill.index.name")
  lazy val percoOccupationIndexName = conf.getString("perco.occupation.index.name")
  lazy val percoSkillGroupIndexName = conf.getString("perco.skillGroup.index.name")
  lazy val percoRecogType = conf.getString("perco.recogType")
}
