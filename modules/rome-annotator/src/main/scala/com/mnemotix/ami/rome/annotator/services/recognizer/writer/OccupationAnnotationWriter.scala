package com.mnemotix.ami.rome.annotator.services.recognizer.writer

import java.net.URI

import com.mnemotix.ami.rome.annotator.helpers.{RomeConfig, RomeUriFactory}
import com.mnemotix.ami.rome.annotator.model.{Appellations, FicheMetier}
import com.mnemotix.lifty.models.LiftyAnnotation
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object OccupationAnnotationWriter {

  def annotation(ficheMetier: FicheMetier): Seq[LiftyAnnotation] = {
    ficheMetierAnnotation(ficheMetier.code).toSeq.flatten ++ appellationAnnotation(ficheMetier.appellations).toSeq.flatten
  }

  def ficheMetierAnnotation(codeOccupation: Option[String]): Option[Seq[LiftyAnnotation]] = {
    if (codeOccupation.isDefined) {
      val uriCodeOccupation: URI = RomeUriFactory.skosRomeURI(codeOccupation.get)
      val liftyAnnotationOccupation = LiftyAnnotation(uriCodeOccupation, "a", createIRI(s"${RomeConfig.mnOntoUri}Occupation"), 1.0)
      Some(Seq(liftyAnnotationOccupation))
    }
    else None
  }

  def appellationAnnotation(appellations: Option[List[Appellations]]): Option[Seq[LiftyAnnotation]] = {
    if (appellations.isDefined) {
       val annotations: Seq[LiftyAnnotation] = appellations.get.filter(appellation => appellation.code.isDefined).map { appellation =>
        val uriAppellation: URI = RomeUriFactory.occupationRomeURI(appellation.code.get)
        LiftyAnnotation(uriAppellation, "a", createIRI(s"${RomeConfig.mnOntoUri}Occupation"), 1.0)
      }
      Some(annotations)
    }
    else None
  }

  def occupationLiterals[A](codeOccupationURI: URI, literal: Option[A], ontologyURI: String, property: String, datatype: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(codeOccupationURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, createIRI(datatype)), 1.0))
    }
    else None
  }

}
