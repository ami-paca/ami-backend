package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import com.mnemotix.ami.MnxAnalytixWriter

import java.net.URI
import com.mnemotix.ami.rome.annotator.helpers.{RomeConfig, RomeUriFactory}
import com.mnemotix.ami.rome.annotator.model.{DomaineProfessionnel, GrandDomaine}
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// todo check broader relation

object ConceptProDomainWriter {
  def annotation(code: Option[String], libelle: Option[String], definition: Option[String], domaineProfessionnelObj: Option[DomaineProfessionnel]): Option[Seq[LiftyAnnotation]] = {
    metier(code, domaineProfessionnelObj, libelle, definition)
  }

  def metier(code: Option[String], domaineProfessionnelObj: Option[DomaineProfessionnel],libelle: Option[String], definition: Option[String]): Option[Seq[LiftyAnnotation]] = {
    if (code.isDefined) {
      val codeMetierURI: URI = RomeUriFactory.skosRomeURI(code.get)
      val metiersAnnotations = metierAnnotations(codeMetierURI, code.get, libelle: Option[String], definition: Option[String])
      Some(metiersAnnotations ++ domaineProfessionnel(codeMetierURI, domaineProfessionnelObj).toSeq.flatten)
    }
    else None
  }

  def metierAnnotations(codeMetierURI: URI, code: String, libelle: Option[String], definition: Option[String]) = {
    val analytix: Seq[LiftyAnnotation] = MnxAnalytixWriter.annotation(codeMetierURI, libelle.toSeq, RomeConfig.percoOccupationIndexName, RomeConfig.percoRecogType)

    Seq(LiftyAnnotation(codeMetierURI, s"${RomeConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/rome"), 1.0),
    LiftyAnnotation(codeMetierURI, "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0),
      LiftyAnnotation(codeMetierURI, "a", createIRI(s"${RomeConfig.mnOntoUri}Occupation"), 1.0),
      LiftyAnnotation(codeMetierURI, "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(s"${LiftyConfig.dataURI}scheme/1"), 1.0)) ++
    hiddenLabelAnnotation(codeMetierURI,"fr",libelle).toSeq ++
    metierLiterals(codeMetierURI,Some(code), "http://www.w3.org/2004/02/skos/core#", "notation", "http://www.w3.org/2001/XMLSchema#string").toSeq ++
    metierLiterals(codeMetierURI, libelle, "http://www.w3.org/2004/02/skos/core#", "prefLabel").toSeq ++
    metierLiterals(codeMetierURI, definition, "http://www.w3.org/2004/02/skos/core#", "note", "http://www.w3.org/2001/XMLSchema#string").toSeq ++ analytix
  }

  def domaineProfessionnel(codeMetierURI: URI, domaineProfessionnel: Option[DomaineProfessionnel]): Option[Seq[LiftyAnnotation]] = {
    if (domaineProfessionnel.isDefined && domaineProfessionnel.get.code.isDefined) {
      val codeOccupationURI: URI = RomeUriFactory.skosRomeURI(domaineProfessionnel.get.code.get)
      val analytix: Seq[LiftyAnnotation] = MnxAnalytixWriter.annotation(codeOccupationURI, domaineProfessionnel.get.libelle.toSeq, RomeConfig.percoOccupationIndexName, RomeConfig.percoRecogType)

      val conceptOf = Seq(LiftyAnnotation(codeOccupationURI, s"${RomeConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/rome"), 1.0))
      val liftyAnnotations = Seq(LiftyAnnotation(codeOccupationURI, "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0),
        LiftyAnnotation(codeOccupationURI, "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(s"${LiftyConfig.dataURI}scheme/1"), 1.0),
        LiftyAnnotation(codeOccupationURI, "a", createIRI(s"${RomeConfig.mnOntoUri}Occupation"), 1.0),
        LiftyAnnotation(codeMetierURI, "http://www.w3.org/2004/02/skos/core#broader", createIRI(codeOccupationURI.toString), 1.0))
      val annotationsLiterals = metierLiterals(codeOccupationURI, domaineProfessionnel.get.libelle, "http://www.w3.org/2004/02/skos/core#", "prefLabel", "http://www.w3.org/2001/XMLSchema#string") ++
        metierLiterals(codeOccupationURI, domaineProfessionnel.get.code, "http://www.w3.org/2004/02/skos/core#", "notation", "http://www.w3.org/2001/XMLSchema#string")
      val grandDomaineAnnotations = grandDomaine(codeOccupationURI, domaineProfessionnel.get.grandDomaine).toSeq.flatten
      Some(conceptOf ++ liftyAnnotations ++ annotationsLiterals ++ grandDomaineAnnotations ++ analytix)
    }
    else None
  }

  def grandDomaine(domaineProfessionnelURI: URI, grandDomaine: Option[GrandDomaine]): Option[Seq[LiftyAnnotation]] = {
    if (grandDomaine.isDefined && grandDomaine.get.code.isDefined) {
      val grandDomaineURI: URI = RomeUriFactory.skosRomeURI(grandDomaine.get.code.get)
      val conceptOf = Seq(LiftyAnnotation(grandDomaineURI, s"${RomeConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/rome"), 1.0))
      val analytix: Seq[LiftyAnnotation] = MnxAnalytixWriter.annotation(grandDomaineURI, grandDomaine.get.libelle.toSeq, RomeConfig.percoOccupationIndexName, RomeConfig.percoRecogType)

      val liftyAnnotations: Seq[LiftyAnnotation] = Seq(LiftyAnnotation(grandDomaineURI, "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0),
        LiftyAnnotation(grandDomaineURI, "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(s"${LiftyConfig.dataURI}scheme/1"), 1.0),
        LiftyAnnotation(grandDomaineURI, "a", createIRI(s"${RomeConfig.mnOntoUri}Occupation"), 1.0),
        LiftyAnnotation(domaineProfessionnelURI ,"http://www.w3.org/2004/02/skos/core#broader", createIRI(grandDomaineURI.toString), 1.0),
        LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}scheme/1"),"http://www.w3.org/2004/02/skos/core#hasTopConcept",createIRI(grandDomaineURI.toString), 1.0))
      val annotationsLiterals: Iterable[LiftyAnnotation] = metierLiterals(grandDomaineURI, grandDomaine.get.libelle, "http://www.w3.org/2004/02/skos/core#", "prefLabel", "http://www.w3.org/2001/XMLSchema#string") ++
        metierLiterals(grandDomaineURI, grandDomaine.get.code, "http://www.w3.org/2004/02/skos/core#", "notation", "http://www.w3.org/2001/XMLSchema#string")
      Some(conceptOf ++ liftyAnnotations ++ annotationsLiterals)
    }
    else None
  }

  def metierLiterals[A](codeOccupationURI: URI, literal: Option[A], ontologyURI: String, property: String, datatype: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(codeOccupationURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, createIRI(datatype)), 1.0))
    }
    else None
  }

  def metierLiterals[A](codeOccupationURI: URI, literal: Option[A], ontologyURI: String, property: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(codeOccupationURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, "fr"), 1.0))
    }
    else None
  }

  def hiddenLabelAnnotation(codeOccupationURI: URI, lang: String, label: Option[String]): Option[LiftyAnnotation] = {
    if (label.isDefined) {
      val hidden: Option[String] = LabelUtil.hiddenLabel(lang,label.get)
      metierLiterals(codeOccupationURI, hidden, "http://www.w3.org/2004/02/skos/core#", "hiddenLabel")
    }
    else None
  }
}