package com.mnemotix.ami.rome.annotator.services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Keep, Sink, Source}
import com.mnemotix.ami.rome.annotator.model.FicheMetier
import com.mnemotix.lifty.services.RDFFileUploader
import com.mnemotix.synaptix.rdf.client.models.Done
import com.typesafe.scalalogging.LazyLogging

import scala.collection.immutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Initiator extends LazyLogging {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)

  lazy val rdfFileUploader = new RDFFileUploader()

  lazy val qry =
    """
      |{
      | "from" : 0, "size" : 550,
      | "query": {
      |   "match_all": {}
      | }
      |}
      |""".stripMargin

  def init: Boolean = {
    val esExtractor = new ESExtractor()
    val ficheMetierSerializer = new FicheMetierSerializer()

    val ficheMetier: Future[immutable.Seq[Seq[FicheMetier]]] = esExtractor.extractAsync(qry).via(ficheMetierSerializer.serialize).runWith(Sink.seq)
    Await.result(ficheMetier, Duration.Inf).flatten.size > 400
  }

  def close: Seq[Done] = {
    val fut = rdfFileUploader.bulkLoad(None)
    fut.onComplete {
      case Success(files) => for (file <- files) logger.info(file.message)
      case Failure(t) => logger.info("An error has occurred: " + t.getMessage)
    }
    Await.result(fut, Duration.Inf)
  }
}