package com.mnemotix.ami.rome.annotator.services.recognizer.writer

import com.mnemotix.ami.rome.annotator.helpers.{RomeConfig, RomeUriFactory}
import com.mnemotix.ami.rome.annotator.model.EnvironnementsTravail
import com.mnemotix.lifty.helpers.AnnotationsUtils.createIRI
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object WorkingAnnotationWriter {
  def annotation(environnementsTravail: Option[List[EnvironnementsTravail]]): Option[Seq[LiftyAnnotation]] = {
    if (environnementsTravail.isDefined) {
     val eTA: Seq[Option[LiftyAnnotation]] = environnementsTravail.get.map { environnementTravail =>
         if (environnementTravail.code.isDefined) {
           val envWorkURI = RomeUriFactory.envWorkURI(environnementTravail.code.get)
           Some(LiftyAnnotation(envWorkURI, "a", createIRI(s"${RomeConfig.amiOntoUri}WorkingEnvironnement"), 1.0))
         }
         else None
      }
      Some(eTA.flatten)
    }
    else None
  }
}