/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import com.mnemotix.ami.rome.annotator.helpers.{RomeConfig, RomeUriFactory}
import com.mnemotix.ami.rome.annotator.model.{CompetencesDeBase, GroupesCompetencesSpecifiques}
import com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept.ConceptSkillWriter.competenceLiterals
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI}
import com.mnemotix.lifty.models.LiftyAnnotation

object ConceptSkillGroup {

  def annotations(competencesDeBase: Option[List[CompetencesDeBase]], groupesCompetencesSpecifiques: Option[List[GroupesCompetencesSpecifiques]]): Seq[LiftyAnnotation] = {
    skillGroupSpecifiques(groupesCompetencesSpecifiques) ++  skillGroup(competencesDeBase, groupesCompetencesSpecifiques)
  }

  def skillGroupSpecifiques(groupesCompetencesSpecifiques: Option[List[GroupesCompetencesSpecifiques]]): Seq[LiftyAnnotation] = {
      val n2 = groupesCompetencesSpecifiques.get.map { specs =>
        specs.competences.get.map { comp =>
          val ndc = comp.noeudCompetence.get.code.get
          val c = comp.code.get
          (ndc, c)
        }
      }.flatten.toList
    val blocks = n2.groupBy(_._1).collect {
      case e => e._1 -> e._2.map(_._2).distinct
    }
    blocksAnnotations(blocks, true)
  }

  def skillGroup(competencesDeBase: Option[List[CompetencesDeBase]], groupesCompetencesSpecifiques: Option[List[GroupesCompetencesSpecifiques]]): Seq[LiftyAnnotation] = {
    val n = competencesDeBase.get.map { cpdb =>
      val ndc = cpdb.noeudCompetence.code.get
      val c = cpdb.code.get
      (ndc, c)
    }
    val n2 = groupesCompetencesSpecifiques.get.map { specs =>
      specs.competences.get.map { comp =>
        val ndc = comp.noeudCompetence.get.code.get
        val c = comp.code.get
        (ndc, c)
      }
    }
    val noeuds = n ++ n2.toSeq.flatten
    val blocks: Map[String, List[String]] = noeuds.groupBy(_._1).collect {
      case e => e._1 -> e._2.map(_._2).distinct
    }
    blocksAnnotations(blocks, false)
  }

  def blocksAnnotations(blocks:  Map[String, List[String]], isSpecific: Boolean): Seq[LiftyAnnotation] = {
    blocks.filter(_._2.size > 1).flatMap { block =>
      val uris = block._2.map(code => RomeUriFactory.skillURI(code, "competence"))
      val blockUri = RomeUriFactory.groupSkill(uris.map(_.toString))

      val specifiques = {
        if (isSpecific) {
          competenceLiterals(blockUri, Some(true), s"${RomeConfig.amiOntoUri}", "specificSkill", "http://www.w3.org/2001/XMLSchema#boolean")
        }
        else None
      }
      val groupAnnotations: Seq[LiftyAnnotation] = Seq(LiftyAnnotation(blockUri, "a", createIRI(s"${RomeConfig.amiOntoUri}SkillGroup"), 1.0)) ++ specifiques.toSeq
      val annotations: Seq[LiftyAnnotation] = uris.map(uri => LiftyAnnotation(uri, s"${RomeConfig.amiOntoUri}memberOf", createIRI(blockUri.toString), 1.0))
      groupAnnotations ++ annotations
    }.toSeq
  }
}