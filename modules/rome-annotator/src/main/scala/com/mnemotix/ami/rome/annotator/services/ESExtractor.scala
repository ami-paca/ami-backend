package com.mnemotix.ami.rome.annotator.services

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import com.mnemotix.ami.rome.annotator.helpers.RomeConfig
import com.mnemotix.lifty.api.LiftyService
import com.mnemotix.synaptix.index.IndexClient
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.SearchResponse
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.{ExecutionContext, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESExtractor(implicit ec: ExecutionContext) extends LiftyService with LazyLogging {
  override def init(): Unit = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
    IndexClient.init()
  }

  override def shutdown(): Unit = {}

  def extractAsync(qry: String): Source[Response[SearchResponse], NotUsed] = {
    val resp: Future[Response[SearchResponse]] = IndexClient.rawQuery(RawQuery(Seq(RomeConfig.romeName), Json.parse(qry).as[JsObject]))
    Source.future(resp)
  }
}