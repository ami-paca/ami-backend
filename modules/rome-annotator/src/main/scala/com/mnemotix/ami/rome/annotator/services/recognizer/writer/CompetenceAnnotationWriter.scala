package com.mnemotix.ami.rome.annotator.services.recognizer.writer

import java.net.URI

import com.mnemotix.ami.rome.annotator.helpers.{RomeConfig, RomeUriFactory}
import com.mnemotix.ami.rome.annotator.model.{CompetencesDeBase, GrandDomaine, GroupesCompetencesSpecifiques, NoeudCompetence, RacineCompetence}
import com.mnemotix.lifty.models.LiftyAnnotation
import com.mnemotix.lifty.helpers.AnnotationsUtils.createIRI

import scala.collection.immutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Skill
 */

object CompetenceAnnotationWriter {

  def annotation(competencesDeBase: Option[List[CompetencesDeBase]], groupesCompetencesSpecifiques: Option[List[GroupesCompetencesSpecifiques]]): Seq[LiftyAnnotation] = {
    competencesDeBaseAnnotations(competencesDeBase).toSeq.flatten ++ groupesCompetencesSpecifiquesAnnotations(groupesCompetencesSpecifiques).toSeq.flatten
  }

  def groupesCompetencesSpecifiquesAnnotations(groupesCompetencesSpecifiques: Option[List[GroupesCompetencesSpecifiques]]): Option[Seq[LiftyAnnotation]] = {
    if (groupesCompetencesSpecifiques.isDefined) {
       val gcsA: Seq[Option[Seq[LiftyAnnotation]]] = groupesCompetencesSpecifiques.get.map { groupeCompetenceSpecifique =>
        if (groupeCompetenceSpecifique.competences.isDefined) {
          val gCS: Seq[Option[Seq[LiftyAnnotation]]] = groupeCompetenceSpecifique.competences.get.map { competence =>
            if (competence.code.isDefined) {
              val uriCompetenceDeBase = RomeUriFactory.skillURI(competence.code.get, "competence")
              val noeudCompetenceAnnotations: Option[Seq[LiftyAnnotation]] = if (competence.noeudCompetence.isDefined) {
                noeudDeBaseAnnotation(competence.noeudCompetence.get )
               }
               else None
              Some(Seq(LiftyAnnotation(uriCompetenceDeBase, "a", createIRI(s"${RomeConfig.mnOntoUri}Skill"), 1.0)) ++ noeudCompetenceAnnotations.toSeq.flatten)
            }
            else None
          }
          Some(gCS.flatten.flatten.toSeq)
        }
        else None
      }
      Some(gcsA.flatten.flatten.toSeq)
    }
    else None
  }

  def competencesDeBaseAnnotations(competencesDeBase: Option[List[CompetencesDeBase]]): Option[immutable.Seq[LiftyAnnotation]] = {
    if (competencesDeBase.isDefined) {
      val annotations = competencesDeBase.get.filter(competenceDeBase => competenceDeBase.code.isDefined).map { competenceDeBase =>
        val uriCompetenceDeBase: URI = RomeUriFactory.skillURI(competenceDeBase.code.get, "competence")
        Seq(LiftyAnnotation(uriCompetenceDeBase, "a", createIRI(s"${RomeConfig.mnOntoUri}Skill"), 1.0)) ++ noeudDeBaseAnnotation(competenceDeBase.noeudCompetence).toSeq.flatten

      }
      Some(annotations.flatten.toSeq)
    }
    else None
  }

  def noeudDeBaseAnnotation(noeudCompetence: NoeudCompetence): Option[Seq[LiftyAnnotation]] = {
     if (noeudCompetence.code.isDefined) {
       val uriNoeudCompetence = RomeUriFactory.skillURI(noeudCompetence.code.get, "noeudCompetence")
       Some(Seq(LiftyAnnotation(uriNoeudCompetence, "a", createIRI(s"${RomeConfig.mnOntoUri}Skill"), 1.0)) ++
       racineDeCompetence(noeudCompetence.racineCompetence).toSeq)
     }
     else None
  }

  def racineDeCompetence(grandDomaine: Option[RacineCompetence]): Option[LiftyAnnotation] = {
    if (grandDomaine.isDefined) {
      if (grandDomaine.get.code.isDefined) {
        val uriRacineNoeudCompetence = RomeUriFactory.skillURI(grandDomaine.get.code.get, "racineCompetence")
        Some(LiftyAnnotation(uriRacineNoeudCompetence, "a", createIRI(s"${RomeConfig.mnOntoUri}Skill"), 1.0))
      }
      else None
    }
    else None
  }
}