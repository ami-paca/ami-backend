package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import java.net.URI

import com.mnemotix.ami.rome.annotator.helpers.RomeUriFactory
import com.mnemotix.ami.rome.annotator.model.FicherMetierTest
import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ConceptProDomainWriterSpec extends FlatSpec with Matchers {

  val ficheMetier = FicherMetierTest.ficheMetier
  val codeMetierURI = RomeUriFactory.skosRomeURI(ficheMetier.code.get)

  it should "return annotations for grandDomaine" in {
    val grandDomaine = ConceptProDomainWriter grandDomaine(new URI("wwww.granddomaine.com"), ficheMetier.domaineProfessionnel.get.grandDomaine)
    grandDomaine.get.foreach(println(_))
    grandDomaine.isDefined shouldEqual true
    grandDomaine.size should be > 0
  }

  it should "return annotations for domaineProfessionnel" in {
    val domaineProfessionnel = ConceptProDomainWriter domaineProfessionnel(new URI("www.prodomaine.com"), ficheMetier.domaineProfessionnel)
    domaineProfessionnel.get.foreach(println(_))
    domaineProfessionnel.isDefined shouldEqual true
    domaineProfessionnel.size should be > 0
  }

  it should "return annotations for metier" in {
    val metier = ConceptProDomainWriter.annotation(ficheMetier.code, ficheMetier.libelle, ficheMetier.definition, ficheMetier.domaineProfessionnel)
    metier.get.foreach(println(_))
    metier.isDefined shouldEqual (true)
  }
}