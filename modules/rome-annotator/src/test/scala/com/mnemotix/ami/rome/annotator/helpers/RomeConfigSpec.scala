package com.mnemotix.ami.rome.annotator.helpers

import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RomeConfigSpec extends FlatSpec with Matchers {

  behavior of "RomeConfig"

  it should "read the ROME config file" in {
    RomeConfig.mnxOntoUri shouldEqual("http://ns.mnemotix.com/ontologies/2019/8/generic-model/")
    RomeConfig.mnOntoUri shouldEqual("https://mindmatcher.org/ontology/")
    RomeConfig.amiOntoUri shouldEqual("http://openemploi.datasud.fr/ontology/")
  }
}