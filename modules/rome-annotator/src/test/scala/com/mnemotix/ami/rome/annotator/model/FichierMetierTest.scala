package com.mnemotix.ami.rome.annotator.model

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object FicherMetierTest {

  val appelations = List(Appellations(Some("11987"),Some("Chauffeur / Chauffeuse de machines agricoles"),Some("Chauffeur / Chauffeuse de machines agricoles"),Some(false)),
    Appellations(Some("13254"),Some("Conducteur / Conductrice d'engins d'exploitation agricole"),Some("Conducteur / Conductrice d'engins d'exploitation agricole"),Some(false)),
    Appellations(Some("13255"),Some("Conducteur / Conductrice d'engins d'exploitation forestière"),Some("Conducteur / Conductrice d'engins d'exploitation forestière"),Some(false)),
    Appellations(Some("12862"),Some("Conducteur / Conductrice d'abatteuses"),Some("Conducteur / Conductrice d'abatteuses"),Some(false)),
    Appellations(Some("38879"),Some("Conducteur / Conductrice de tracto-benne"),Some("Conducteur / Conductrice de tracto-benne"),Some(false)),
    Appellations(Some("140877"),Some("Conducteur / Conductrice de tracteur enjambeur"),Some("Conducteur / Conductrice de tracteur enjambeur"),Some(false)),
    Appellations(Some("38875"),Some("Conducteur / Conductrice de pulvérisateur"),Some("Conducteur / Conductrice de pulvérisateur"),Some(false)))

  val competencesDeBase = List(CompetencesDeBase(Some("125978"),Some("Préparer le matériel, les matériaux et les outillages"), NoeudCompetence(Some("00173"),Some("Manipulation des outils et des équipements"), Some(RacineCompetence(Some("00007"),Some("Compétences transverses")))),Some("SavoirFaire"),Some("R"),Some("R")),
    CompetencesDeBase(Some("122704"),Some("Identifier le type d'intervention"),NoeudCompetence(Some("00222"),Some("Organisation d'une action ou pilotage d'un projet"),Some(RacineCompetence(Some("00007"),Some("Compétences transverses")))),Some("SavoirFaire"),Some("R"),Some("R")))

  val competences1 = List(Competences(Some("120405"),Some("Utiliser un engin nécessitant une habilitation"),Some(NoeudCompetence(Some("00045"),Some("Conduite d'engins de chantier et de manutention"),Some(RacineCompetence(Some("00016"),Some("Engins de chantier et de manutention"))))), Some("SavoirFaire"),Some("R"),Some("R")),
    Competences(Some("200039"),Some("Engins compacts (CACES R 482-A) - A partir du 01/01/2020"),Some(NoeudCompetence(Some("00149"),Some("Habilitations à la conduite d'engins de chantier et de manutention"),Some(RacineCompetence(Some("00016"),Some("Engins de chantier et de manutention"))))),Some("Savoir"),None,None),
    Competences(Some("200040"),Some("Engins d’extraction à déplacement séquentiel (CACES R 482-B1) - A partir du 01/01/2020"),Some(NoeudCompetence(Some("00149"),Some("Habilitations à la conduite d'engins de chantier et de manutention"),Some(RacineCompetence(Some("00016"),Some("Engins de chantier et de manutention"))))),Some("Savoir"),None,None))

  val competences2 = List(Competences(Some("117548"),Some("Stocker un produit"),Some(NoeudCompetence(Some("00138"),Some("Gestion des stocks et inventaires"),Some(RacineCompetence(Some("00029"),Some("Logistique"))))),Some("SavoirFaire"),Some("R"),Some("R")),
    Competences(Some("117489"),Some("Débarder une grume"),Some(NoeudCompetence(Some("00320"),Some("Sylviculture - élagage"),Some(RacineCompetence(Some("00002"),Some("Agriculture - sylviculture - pêche"))))),Some("SavoirFaire"),Some("R"),Some("R")),
    Competences(Some("117492"),Some("Empiler des grumes"),Some(NoeudCompetence(Some("00320"),Some("Sylviculture - élagage"),Some(RacineCompetence(Some("00002"),Some("Agriculture - sylviculture - pêche"))))),Some("SavoirFaire"),Some("R"),Some("R")),
    Competences(Some("100019"),Some("Caractéristiques des écosystèmes"),Some(NoeudCompetence(Some("00295"),Some("Sciences de la vie et de la terre"),Some(RacineCompetence(Some("00047"),Some("Sciences"))))),Some("Savoir"),None,None))

  val ficheMetier = new FicheMetier(Some("A1101"), Some("Conduite d'engins agricoles et forestiers"),
    Some("Réalise des travaux mécanisés agricoles, sylvicoles ou forestiers (préparation des sols, semis, récolte, abattage d'arbres, ...) selon les objectifs de production (quantité, qualité, ...), la commande du client, les règles d'hygiène, de sécurité et la réglementation environnementale."),
    Some("Cet emploi/métier est accessible avec un CAP/BEP dans le secteur agricole (production agricole, agroéquipement, ...) ou forestier (travaux forestiers, ...).\nIl est également accessible avec une expérience professionnelle dans le même secteur sans diplôme particulier.\nUn Certificat de Spécialisation Agricole -CSA- tracteurs et machines agricoles (utilisation et maintenance) peut en faciliter l'accès.\nUn Bac ou un BTS peut être demandé selon la technicité des engins utilisés.\nLes permis C, C1, CE, C1E (précédemment C et EC) peuvent être requis."),
    Some("L'activité de cet emploi/métier s'exerce au sein de structures agricoles (exploitation, Entreprise de Travaux Agricoles -ETA-, Coopérative d'Utilisation du Matériel Agricole -CUMA-, ...) ou forestières (pépinières forestières, Entreprises de Travaux Forestiers - ETF, exploitations forestières, ...), en contact avec différents intervenants (techniciens de coopératives, clients, ...).\nElle varie selon le secteur (agricole, forestier), le type d'équipement et le degré de mécanisation de la structure.\nL'activité peut impliquer la manipulation de charges.\nLe port d'Equipements de Protection Individuelle -EPI- (casque anti-bruit, lunettes, pantalon de sécurité, ...) peut être exigé.\nCet emploi/métier peut s'exercer en horaires fractionnés, en soirée, les fins de semaines, jours fériés et être soumis à des pics d'activité."),
    Some("R"),
    Some("I"),
    Some("8341"),
    Some(false),
    Some(DomaineProfessionnel(Some("A11"), Some("Engins agricoles et forestiers"), Some(GrandDomaine(Some("A"),Some("Agriculture et Pêche, Espaces naturels et Espaces verts, Soins aux animaux"))))),
    Some(appelations),
  Some(competencesDeBase),
  Some(List(GroupesCompetencesSpecifiques(Some(competences1)), GroupesCompetencesSpecifiques(Some(competences2)))),
  Some(List(EnvironnementsTravail(Some("23833"),Some("Coopérative agricole"),Some("Structure")),
    EnvironnementsTravail(Some("23902"),Some("Entreprise de travaux agricoles"),Some("Structure")),
    EnvironnementsTravail(Some("23904"),Some("Entreprise de travaux forestiers"),Some("Structure")),
    EnvironnementsTravail(Some("23929"),Some("Exploitation agricole"),Some("Structure")),
    EnvironnementsTravail(Some("23932"),Some("Exploitation forestière"),Some("Structure")),
    EnvironnementsTravail(Some("23933"),Some("Exploitation viticole / vinicole"),Some("Structure")),
    EnvironnementsTravail(Some("24033"),Some("Pépinière"),Some("Structure")),
    EnvironnementsTravail(Some("24844"),Some("Travail en indépendant"),Some("Condition")))),
  Some(List()),
  Some(List(MobilitesProchesVersMetiers(Some(GrandDomaine(Some("A1416"),Some("Polyculture, élevage")))))),
  Some(List(MobilitesProchesVersMetiers(Some(GrandDomaine(Some("I1603"),Some("Maintenance d'engins de chantier, levage, manutention et de machines agricoles")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("A1202"),Some("Entretien des espaces naturels")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("F1302"),Some("Conduite d'engins de terrassement et de carrière")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("A1414"),Some("Horticulture et maraîchage")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("A1405"),Some("Arboriculture et viticulture")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("A1302"),Some("Contrôle et diagnostic technique en agriculture")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("A1205"),Some("Sylviculture")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("N4101"),Some("Conduite de transport de marchandises sur longue distance")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("K2111"),Some("Formation professionnelle")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("A1203"),Some("Aménagement et entretien des espaces verts")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("A1201"),Some("Bûcheronnage et élagage")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("D1402"),Some("Relation commerciale grands comptes et entreprises")))),
    MobilitesProchesVersMetiers(Some(GrandDomaine(Some("N1104"),Some("Manoeuvre et conduite d'engins lourds de manutention")))))),
  Some(List()),Some(List()),Some(List()),Some(List()),Some(List()),Some(List()))
}