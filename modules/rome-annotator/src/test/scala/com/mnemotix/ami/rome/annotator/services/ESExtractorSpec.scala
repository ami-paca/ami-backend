package com.mnemotix.ami.rome.annotator.services

import java.io.{File, FileWriter}
import java.net.URI

import akka.actor.ActorSystem
import akka.stream.scaladsl.Sink
import com.mnemotix.ami.rome.annotator.helpers.RomeUriFactory
import com.mnemotix.ami.rome.annotator.model.FicheMetier
import com.mnemotix.lifty.helpers.{CryptoUtils, LiftyConfig}
import org.scalatest.FlatSpec
import org.scalatest.matchers.should.Matchers

import scala.collection.immutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESExtractorSpec extends FlatSpec with Matchers {
  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
  implicit val executionContext: ExecutionContextExecutor = ExecutionContext.global
  lazy val ficheMetierSerializer = new FicheMetierSerializer()

  it should "retrieve the fiche metier" in {

    val fileWriter = new FileWriter(new File("modules/rome-annotator/src/test/resources/romeCount.csv"), true)


    val qryString =
      """
        |{
        | "from" : 0, "size" : 550,
        | "query": {
        |   "match_all": {}
        | }
        |}
        |""".stripMargin
    val esXtractor = new ESExtractor()
    val res = esXtractor.extractAsync(qryString).via(ficheMetierSerializer.serialize).runWith(Sink.seq)
    val metiers = Await.result(res, Duration.Inf).toSeq.flatten
    metiers.foreach{ metier =>
      val code = metier.code.get
      val appelations = metier.appellations.get
      val competencesDeBase = metier.competencesDeBase.get
      val competecenceSpecs = metier.groupesCompetencesSpecifiques.get

      val nodeAll: Seq[String] = competencesDeBase.map(_.noeudCompetence.code.get) ++ competecenceSpecs.map(_.competences.get.map(_.noeudCompetence.get.code.get)).toSeq.flatten
      val nodes = nodeAll.distinct

      val competencesAll = competencesDeBase.map(_.code.get) ++ competecenceSpecs.map(_.competences.get.map(_.code.get)).flatten.toSeq
      val competences = competencesAll.distinct

      val cDeBase = metier.competencesDeBase.get.map(_.code.get)
      val cSpecs = metier.groupesCompetencesSpecifiques.get.map(_.competences.get.map(_.code.get)).flatten.toSeq


     // println(s"le metier = $code - nombre d'appelations = ${appelations.size} - nombre de competences de base = ${competencesDeBase.distinct.size} - nombre de groupes competences specifique = ${competecenceSpecs.distinct.size}")
      //competecenceSpecs.foreach(comps => println( s"pour chaque  groupe de competence specifique = ${comps.competences.get.size}"))
      //println(s"nombre de competences distincts = ${competences.size} - nombre de noeud de competences distincts = ${nodes.size}")

      fileWriter.write(s""""$code", "${appelations.distinct.size}", "${cDeBase.distinct.size}", "${cSpecs.distinct.size}", "${competences.distinct.size}", "${nodes.distinct.size}"\n""")
    }
    fileWriter.close()

  }

  it should "retrieve the noeuds de competence" ignore {
    val fileWriter = new FileWriter(new File("modules/rome-annotator/src/test/resources/nodes.csv"), true)
    val qryString =
      """
        |{
        | "from" : 0, "size" : 550,
        | "query": {
        |   "match_all": {}
        | }
        |}
        |""".stripMargin
    val esXtractor = new ESExtractor()
    val res = esXtractor.extractAsync(qryString).via(ficheMetierSerializer.serialize).runWith(Sink.seq)
    val metiers = Await.result(res, Duration.Inf).toSeq.flatten
    val noeuds: immutable.Seq[(String, String)] = metiers.map { metier =>
      val competencesDeBase = metier.competencesDeBase.get
      val competecenceSpecs = metier.groupesCompetencesSpecifiques.get

      val idsn = competencesDeBase.map { cdb =>
        cdb.noeudCompetence.code.get
      }
      val idsn2 = competecenceSpecs.map { cs =>
        cs.competences.get.map { allcompetences =>
          allcompetences.noeudCompetence.get.code.get
        }
      }.flatten.toSeq


      idsn ++ idsn2

      val n = competencesDeBase.map { cpdb =>
        val ndc = cpdb.noeudCompetence.code.get
        val c = cpdb.code.get
        (ndc, c)
      }
      val n2 = competecenceSpecs.map { specs =>
        specs.competences.get.map { comp =>
          val ndc = comp.noeudCompetence.get.code.get
          val c = comp.code.get
          (ndc, c)
        }
      }

       n ++ n2.flatten
      //n ++ n2.flatten
    }.flatten
    noeuds.groupBy(_._1).collect {
      case e => e._1 -> e._2.map(_._2).distinct.size
    }.foreach { ns =>
      fileWriter.write(s"${ns._1}, ${ns._2} \n")
    }
    fileWriter.close()
  }

  it should "create some block de competence" ignore {
    val fileWriter = new FileWriter(new File("modules/rome-annotator/src/test/resources/blockByMM.csv"), true)
    val qryString =
      """
        |{
        | "from" : 0, "size" : 550,
        | "query": {
        |   "match_all": {}
        | }
        |}
        |""".stripMargin
    val esXtractor = new ESExtractor()
    val res = esXtractor.extractAsync(qryString).via(ficheMetierSerializer.serialize).runWith(Sink.seq)
    val metiers: immutable.Seq[FicheMetier] = Await.result(res, Duration.Inf).toSeq.flatten

    val blocs = metiers.map { metier =>
      val competencesDeBase = metier.competencesDeBase.get
      val competecenceSpecs = metier.groupesCompetencesSpecifiques.get

      val n = competencesDeBase.map { cpdb =>
        val ndc = cpdb.noeudCompetence.code.get
        val c = cpdb.code.get
        (ndc, c)
      }
      val n2 = competecenceSpecs.map { specs =>
        specs.competences.get.map { comp =>
          val ndc = comp.noeudCompetence.get.code.get
          val c = comp.code.get
          (ndc, c)
        }
      }

      val noeuds = n ++ n2.toSeq.flatten
      val blocks_M = noeuds.groupBy(_._1).collect {
        case e => e._1 -> e._2.map(_._2).distinct
      }.foreach(b => fileWriter.write(s"${metier.code.get}, ${b._1}, ${b._2.mkString(" ")}, ${b._2.size} \n"))
    }
    fileWriter.close()
  }


  it should "write bloc de competence" in {


    val fileWriter = new FileWriter(new File("modules/rome-annotator/src/test/resources/cblock.csv"), true)

    val qryString =
      """
        |{
        | "from" : 0, "size" : 550,
        | "query": {
        |   "match_all": {}
        | }
        |}
        |""".stripMargin
    val esXtractor = new ESExtractor()
    val res = esXtractor.extractAsync(qryString).via(ficheMetierSerializer.serialize).runWith(Sink.seq)
    val metiers: immutable.Seq[FicheMetier] = Await.result(res, Duration.Inf).toSeq.flatten

    metiers.foreach { metier =>
      val competencesDeBase = metier.competencesDeBase.get
      val competecenceSpecs = metier.groupesCompetencesSpecifiques.get

      val n = competencesDeBase.map { cpdb =>
        val ndc = cpdb.noeudCompetence.code.get
        val c = cpdb.code.get
        (ndc, c)
      }
      val n2 = competecenceSpecs.map { specs =>
        specs.competences.get.map { comp =>
          val ndc = comp.noeudCompetence.get.code.get
          val c = comp.code.get
          (ndc, c)
        }
      }
      val noeuds = n ++ n2.toSeq.flatten
      val blocks: Map[String, List[String]] = noeuds.groupBy(_._1).collect {
        case e => e._1 -> e._2.map(_._2).distinct
      }

      blocks.filter(_._2.size > 1).map { block =>
        val uris = block._2.map(code => RomeUriFactory.skillURI(code, "competence"))
        val blockUri = RomeUriFactory.groupSkill(uris.map(_.toString))
        val ids = block._2.mkString(";")
        fileWriter.write(s"${blockUri},${ids}\n")
      }
    }
  }


  it should "create some metier/competence file" ignore  {
    val fileWriter = new FileWriter(new File("modules/rome-annotator/src/test/resources/romeComps.csv"), true)
    val qryString =
      """
        |{
        | "from" : 0, "size" : 550,
        | "query": {
        |   "match_all": {}
        | }
        |}
        |""".stripMargin
    val esXtractor = new ESExtractor()
    val res = esXtractor.extractAsync(qryString).via(ficheMetierSerializer.serialize).runWith(Sink.seq)
    val metiers = Await.result(res, Duration.Inf).toSeq.flatten

    val blocs = metiers.map { metier =>
      val competencesDeBase = metier.competencesDeBase.get
      val competecenceSpecs = metier.groupesCompetencesSpecifiques.get

      val c1 = competencesDeBase.map { cpdb =>
        cpdb.code.get
      }
      val c2 = competecenceSpecs.map { specs =>
        specs.competences.get.map { comp =>
          comp.code.get
        }
      }

      val competences = (c1 ++ c2.flatten).distinct
      fileWriter.write(s"${metier.code.get}, ${competences.mkString(" ")} \n")
    }
    fileWriter.close()
  }

  it should "create some competences label uris file" in  {
    def simpleROMEID(code: String) = {
      CryptoUtils.md5sum(code)
    }

    def simpleURI(code: String) = {
      new URI(s"${LiftyConfig.dataURI}skill/${simpleROMEID(code)}")
    }

    val fileWriter = new FileWriter(new File("modules/rome-annotator/src/test/resources/competence_uris.csv"), true)
    val qryString =
      """
        |{
        | "from" : 0, "size" : 550,
        | "query": {
        |   "match_all": {}
        | }
        |}
        |""".stripMargin
    val esXtractor = new ESExtractor()
    val res = esXtractor.extractAsync(qryString).via(ficheMetierSerializer.serialize).runWith(Sink.seq)
    val metiers = Await.result(res, Duration.Inf).toSeq.flatten

    val competences: immutable.Seq[List[String]] = metiers.map { metier =>
      val competencesDeBase = metier.competencesDeBase.get
      val competecenceSpecs = metier.groupesCompetencesSpecifiques.get

      val c1 = competencesDeBase.map { cpdb =>
        cpdb.code.get
      }
      val c2 = competecenceSpecs.map { specs =>
        specs.competences.get.map { comp =>
          comp.code.get
        }
      }

      (c1 ++ c2.flatten).distinct


      //fileWriter.write(s"${metier.code.get}, ${competences.mkString(" ")} \n")
    }
    competences.flatten.distinct.map { comp =>
      val newUri = RomeUriFactory.skillURI(comp, "competence")
      val oldUri = simpleURI(comp)
      fileWriter.write(s"${comp}, ${newUri.toString}, ${oldUri.toString} \n")
    }
    fileWriter.close()
  }


}