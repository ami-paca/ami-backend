package com.mnemotix.ami.rome.annotator.services.recognizer.writer

import com.mnemotix.ami.rome.annotator.model.FicherMetierTest
import com.mnemotix.lifty.models.LiftyAnnotation
import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WorkingAnnotationWriterSpec extends FlatSpec with Matchers {

  val ficheMetier = FicherMetierTest.ficheMetier

  it should "make working annotation writer" in {
    val waw: Option[Seq[LiftyAnnotation]] = WorkingAnnotationWriter.annotation(ficheMetier.environnementsTravail)
    waw.isDefined shouldEqual(true)
    waw.get.size should be > 0
  }
}