package com.mnemotix.ami.rome.annotator.services.recognizer.writer.concept

import java.net.URI

import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ConceptOccupationWriterSpec extends FlatSpec with Matchers {
  it should "create alt label for Débardeur forestier / Débardeuse forestière" in {
    val debardeur: Option[Seq[String]] = LabelUtil.labels(Some("Débardeur forestier / Débardeuse forestière"))
    println(debardeur)
    debardeur.isDefined shouldEqual(true)
    debardeur.get(0) shouldEqual "Débardeur forestier"
    debardeur.get(1) shouldEqual "Débardeuse forestière"
    debardeur.get.size shouldEqual(2)
  }

  it should "create alt label for Conducteur / Conductrice d'engins de débardage" in {
    val conducteur: Option[Seq[String]] = LabelUtil.labels(Some("Conducteur / Conductrice d'engins de débardage"))
    println(conducteur)
    conducteur.isDefined shouldEqual(true)
    conducteur.get(0) shouldEqual "Conducteur d'engins de débardage"
    conducteur.get(1) shouldEqual "Conductrice d'engins de débardage"
    conducteur.get.size shouldEqual(2)
  }

  it should "create alt label for Débardeur / Débardeuse" in {
    val debardeur: Option[Seq[String]] = LabelUtil.labels(Some("Débardeur / Débardeuse"))
    println(debardeur)
    debardeur.isDefined shouldEqual(true)
    debardeur.get(0) shouldEqual "Débardeur"
    debardeur.get(1) shouldEqual "Débardeuse"
    debardeur.get.size shouldEqual(2)
  }

  it should "create alt label for Pilote de machines d'abattage" in {
    val pilotes: Option[Seq[String]] = LabelUtil.labels(Some("Pilote de machines d'abattage"))
    println(pilotes)
    pilotes.isDefined shouldEqual(true)
    pilotes.get(0) shouldEqual "Pilote de machines d'abattage"
    pilotes.get(1) shouldEqual "Pilote de machines d'abattage"
    pilotes.get.size shouldEqual(2)
  }

  it should "create an hidden label" in {
    val hidden = LabelUtil.hiddenLabel("fr", "Pilote de machines d'abattage")
    val debardeur = LabelUtil.hiddenLabel("fr", "Débardeur")
    println(hidden)
    println(debardeur)
    hidden.isDefined shouldEqual(true)
    debardeur.isDefined shouldEqual true
    debardeur.get shouldEqual("""+"débardeur"""")
    hidden.get shouldEqual("""+"pilote machines d'abattage"~3""")
  }

  it should "alt label annotation" in {
    val annotations = ConceptOccupationWriter.altLabelsAnnotation(new URI("www.uri.com"), Some("Débardeur / Débardeuse"))
    annotations.isDefined shouldEqual(true)
    println(annotations)
    annotations.get.size shouldEqual(2)
  }

  it should "hidden label annotation" in {
    val annotations = ConceptOccupationWriter.hiddenLabelAnnotation(new URI("www.uri.com"), "fr", Some("Débardeur / Débardeuse"))
    annotations.isDefined shouldEqual(true)
    println(annotations)
    annotations.get.size shouldEqual(2)
  }

  it should "pref label annotation" in {
    val annotations = ConceptOccupationWriter.prefLabelAnnotation(new URI("www.uri.com"), Some("Débardeur / Débardeuse"))
    annotations.isDefined shouldEqual(true)
    println(annotations)
  }
}


