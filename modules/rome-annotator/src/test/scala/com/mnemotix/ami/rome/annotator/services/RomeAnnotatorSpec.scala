package com.mnemotix.ami.rome.annotator.services

import com.mnemotix.synaptix.rdf.client.RDFClient
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.mutable.ListBuffer
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.concurrent.duration.Duration
import scala.io.Source
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RomeAnnotatorSpec extends FlatSpec with Matchers {
  val romeAnnotator = new RomeAnnotator

  it should "annotate skills annotator" ignore  {
    val fut = romeAnnotator.annotatorSkills
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  it should "annotate rome annotator" in {
    val fut = romeAnnotator.annotator
    val done = Await.result(fut, Duration.Inf)
    println(done)
  }

  implicit val executionContext: ExecutionContextExecutor = ExecutionContext.global
  implicit lazy val conn = RDFClient.getReadConnection(("dev-openemploi"))
  RDFClient.init()


  def spqrlr(notation: String) = s"""PREFIX ope: <http://openemploi.datasud.fr/ontology/>
                 |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                 |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
                 |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                 |
                 |SELECT ?uri WHERE {
                 |    ?uri rdf:type skos:Concept .
                 |    ?uri skos:notation "${notation}"
                 |}""".stripMargin

  def metier(notation: String) = {
    var applicationKindTypeVocabularies = new ListBuffer[String]()

    val futureApplicationKindIndividualsAsync = RDFClient.select(spqrlr(notation)).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        applicationKindTypeVocabularies += (bs.getValue("uri").stringValue())
      }
      applicationKindTypeVocabularies.toSeq
    }

    futureApplicationKindIndividualsAsync onComplete {
      case Success(i) => println(s"The application-kind request succeed. The number of applicationKindType : ${i.size}")
      case Failure(t) => println("There is a failure here", t)
    }
    futureApplicationKindIndividualsAsync
  }


  it should "test if an rome code is in the store" ignore {
    //val romes = Source.fromFile("/Users/prlherisson/Documents/mnemotix/projets/open-emploi/rome.txt").getLines.toList.distinct

    val romes =("H1504 H2601 H2602 H2603 H2604 H2605 H1101 H1208 H1303 I1301 I1302 I1304 I1305 I1306 I1308 I1309 I1310 I1603 I1604").split(" ")
    /*val notHere= romes.filter { rome =>
      println(rome)
      val res = Await.result(metier(rome), Duration.Inf)
      res.size == 0
    }
    println(notHere.size)

     */

    val resultats = romes.map { rome =>
      val res = Await.result(metier(rome), Duration.Inf)
      res

    }
    println(resultats.flatten.size)

    resultats.flatten.foreach(println(_))
  }
}