package com.mnemotix.ami.rome.annotator.helpers

import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RomeIDFactorySpec extends FlatSpec with Matchers {

  behavior of "RomeIDFactory"

  it should "do md5sum of occupation ID" in {
    val occupation = "A1101"
    RomeIDFactory.occupationID(occupation) shouldEqual "22359513c56a9d9b34677edbd8a421ca"
  }

  it should "do md5sum of skill ID" in {
    val skill = "00222"
    RomeIDFactory.skillID(skill, "competence")
  }

  it should "do md5sum of skos env Work ID" in {
    val envWork = "23902"
    RomeIDFactory.envWorkID(envWork) shouldEqual "64ff30d936f1e1bd373f36684ca4eda4"
  }
}