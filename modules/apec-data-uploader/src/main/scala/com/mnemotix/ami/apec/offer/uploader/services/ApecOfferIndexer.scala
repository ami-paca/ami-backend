package com.mnemotix.ami.apec.offer.uploader.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.apec.offer.uploader.helpers.ApecConfig
import com.mnemotix.ami.apec.offer.uploader.model.ApecOE
import com.mnemotix.ami.connectors.model.EOffer
import com.mnemotix.ami.connectors.services.ESIndexer
import com.mnemotix.ami.connectors.services.index.ESMapping
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.index.elasticsearch.models.ESIndexable
import com.sksamuel.elastic4s.http.JavaClient
import com.sksamuel.elastic4s.{ElasticClient, ElasticDsl, ElasticProperties, Response}
import com.sksamuel.elastic4s.requests.bulk.BulkResponse
import com.sksamuel.elastic4s.requests.common.RefreshPolicy
import com.sksamuel.elastic4s.requests.indexes.IndexResponse
import com.sksamuel.elastic4s.requests.update.UpdateResponse
import com.typesafe.scalalogging.LazyLogging
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ApecOfferIndexer(implicit ec: ExecutionContext,  esIndexer: ESIndexer) extends LazyLogging{

  def init = {
    IndexClient.init()
    val res = esIndexer.createIndex(ApecConfig.apecofferIdxName, ESMapping.mappnPEOfferDef)
  }

  def index: Flow[(EOffer, Option[String]), Future[Response[IndexResponse]], NotUsed] = {
    Flow[(EOffer, Option[String])].filter(_._2.isDefined).map { eoffer =>
      val json = Json.toJson(eoffer._1)
      esIndexer.insertDocument(ApecConfig.apecofferIdxName, eoffer._2.get, json.as[JsObject])
    }
  }

  def bulkIndex: Flow[ESIndexable, Future[Response[BulkResponse]], NotUsed] = {
    Flow[ESIndexable].grouped(1000).map(indexables => esIndexer.bulkInsert(ApecConfig.apecofferIdxName, indexables))
  }

  def update: Flow[ESIndexable, Future[Response[UpdateResponse]], NotUsed] = {
    Flow[ESIndexable].map(indexable => IndexClient.updateById(ApecConfig.apecofferIdxName, indexable))
  }

  def toIndexable: Flow[(EOffer, Option[String]), ESIndexable, NotUsed] = {
    Flow[(EOffer, Option[String])].filter(_._2.isDefined).map { eoffer =>
      ESIndexable(eoffer._2.get, Some(
        Json.toJson(eoffer._1).as[JsObject]
      ))
    }
  }

  def toUpdatable: Flow[Option[(String, String)], ESIndexable, NotUsed] = {
    Flow[Option[(String, String)]].filter(_.isDefined).map { tuple =>
      ESIndexable(tuple.get._1, Some(Json.obj(
        "occupation" -> tuple.get._2
      )))
    }
  }

  def sinkIndexResponse: Flow[Future[Response[IndexResponse]], Unit, NotUsed] = {
    Flow[Future[Response[IndexResponse]]].map { responses =>
      val indexesResponse  = Await.result(responses, Duration.Inf)
      val logging = logger.info(s"status: ${indexesResponse.status}; success: ${indexesResponse.isSuccess}; failure: ${indexesResponse.isError}")
    }
  }

  def sinkBulkIndexResponse: Flow[Future[Response[BulkResponse]], Unit, NotUsed] = {
    Flow[Future[Response[BulkResponse]]].map { responses =>
      val indexesResponse  = Await.result(responses, Duration.Inf)
      val logging = logger.info(s"status: ${indexesResponse.status}; success: ${indexesResponse.isSuccess}; failure: ${indexesResponse.isError}")
    }
  }

  def sinkUpdateResponse = {
    Flow[Future[Response[UpdateResponse]]].map { response =>
      val rps = Await.result(response, Duration.Inf)
      val logging = logger.info(s"status: ${rps.status}; success: ${rps.isSuccess}; failure: ${rps.isError}")

    }
  }
}