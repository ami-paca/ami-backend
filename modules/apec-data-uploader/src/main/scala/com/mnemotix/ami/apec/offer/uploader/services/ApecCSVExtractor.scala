package com.mnemotix.ami.apec.offer.uploader.services

import java.nio.charset.StandardCharsets
import java.nio.file.Paths

import akka.stream.IOResult
import akka.stream.alpakka.csv.scaladsl.{CsvFormatting, CsvParsing, CsvToMap}
import akka.stream.scaladsl.{FileIO, Source}
import com.mnemotix.lifty.api.LiftyService
import com.mnemotix.lifty.exceptions.LiftyExtractException
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.Future

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ApecCSVExtractor(filePath: String) extends LiftyService with LazyLogging {

  def extractAsync: Source[Map[String, String], Future[IOResult]] = {
    try {
      FileIO.fromPath(Paths.get(filePath))
        .via(CsvParsing.lineScanner(delimiter = ';', maximumLineLength = 100 * 1024))
        .via(CsvToMap.toMapAsStrings(StandardCharsets.UTF_8))
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the resource extraction process", t)
        throw new LiftyExtractException("An error occured during the resource extraction process", Some(t))
    }
  }

  /**
   * to init a service
   */
  override def init(): Unit =  logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")

  /**
   * to shutdown a service
   */
  override def shutdown(): Unit = {}
}