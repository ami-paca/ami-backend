package com.mnemotix.ami.apec.offer.uploader.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.apec.offer.uploader.model.ApecOE

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ApecOfferSerializer {

  def serialize: Flow[Map[String, String], ApecOE, NotUsed] = {
    Flow[Map[String, String]].map { aMap =>
      serializer(aMap)
    }
  }

  def mapValue(aMap: Map[String, String], key: String): Option[String] = if (aMap.get(key).isDefined && !aMap.get(key).get.trim.isEmpty && aMap.get(key).get != " ") Some(aMap.get(key).get.trim) else None

  def serializer(aMap: Map[String, String]): ApecOE = {
    ApecOE(
      mapValue(aMap, "NUMERO_OFFRE"),
      mapValue(aMap, "OFF_LIEU"),
      mapValue(aMap, "OFF_CP"),
      mapValue(aMap, "OFF_DEPT"),
      mapValue(aMap, "OFF_DEPT_CODE"),
      mapValue(aMap, "OFF_FONCTION1"),
      mapValue(aMap, "OFF_FONCTION2"),
      mapValue(aMap, "OFF_EXPERIENCE"),
      mapValue(aMap, "OFF_SALAIRE_MIN"),
      mapValue(aMap, "OFF_SALAIRE_MAX"),
      mapValue(aMap, "OFF_DEPLACEMENT"),
      mapValue(aMap, "OFF_PUBLICATION"),
      mapValue(aMap, "OFF_PRISE_DE_POSTE"),
      mapValue(aMap, "OFF_TYPE_POSTE"),
      mapValue(aMap, "OFF_TYPE_CNT"),
      mapValue(aMap, "OFF_NBRE_POSTES"),
      mapValue(aMap, "OFF_TPS_TRAVAIL"),
      mapValue(aMap, "OFF_TEXTE_OFFRE"),
      mapValue(aMap, "OFF_TEXTE_PROFIL"),
      mapValue(aMap, "OFF_TEXTE_ENTREPRISE")
    )
  }
}

