package com.mnemotix.ami.apec.offer.uploader.services

import java.net.URI
import java.text.SimpleDateFormat

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.apec.offer.uploader.model.{ApecOE, Villes}
import com.mnemotix.ami.connectors.helpers.DateFormatUtil
import com.mnemotix.ami.connectors.model.{EOffer, Entreprise, Geoloc, LieuTravail}
import com.mnemotix.ami.connectors.services.RDFExtracter
import com.mnemotix.ami.connectors.services.utils.{GeoPoint, PolygonUtils}
import com.mnemotix.lifty.helpers.{CryptoUtils}

import scala.concurrent.ExecutionContextExecutor

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ApecToOffer(villes: Seq[Villes])(implicit val ec: ExecutionContextExecutor) {

  lazy val DATE_FORMAT = "dd/MM/yyyy"

  lazy val dateCollecte = DateFormatUtil.getDate(DateFormatUtil.DATE_FORMAT)
  lazy val rdfExtracter = new RDFExtracter()
  rdfExtracter.init
  lazy val zoneGeoWkts = rdfExtracter.zoneGeoWkt


  def converter: Flow[ApecOE, (EOffer, Option[String]), NotUsed] = {
    Flow[ApecOE].map { apecOE =>
      (toEOffer(apecOE), apecOE.numero_offre)
    }
  }

  def toEOffer(apecOE: ApecOE) = {
    EOffer(
      intitule(apecOE),
      apecOE.off_texte_offre,
      None,
      Some(dateCollecte),
      dates(apecOE.off_publication),
      isAlternance(apecOE.off_type_cnt),
      apecOE.off_experience,
      apecOE.off_type_cnt,
      apecOE.off_tps_travail,
      apecOE.off_texte_profil,
      apecOE.off_function2,
      None,
      entreprise(apecOE),
      geoloc(apecOE),
      None,
      None,
      None,
      None,
      salaire(apecOE),
      zoneEmploi(apecOE),
      None, None
    )
  }

  def zoneEmploi(e: ApecOE): Option[String] = {
    if (e.off_lieu.isDefined) {
      val villeFinded = villes.filter(_.nomMaj == e.off_lieu.get)
      if (villeFinded.size > 0) {
        val point = GeoPoint(villeFinded(0).lat, villeFinded(0).lon)
        val uris = zoneGeoWkts.filter(couple => PolygonUtils.isPointInPolygon(couple._2, point)).map(couple => couple._1)
        if (uris.size > 0) Some(uris.last) else None
      }
      else None
    }
    else None
  }

  def intitule(apecOE: ApecOE) = {
    if (apecOE.off_function1.isDefined) {
      Some(s"${apecOE.off_function1.get}".trim)
    }
    else None
  }

  def geoloc(e: ApecOE) = {
    if (e.off_lieu.isDefined) {
      val villeFinded = villes.filter(_.nomMaj == e.off_lieu.get)
      if (villeFinded.size > 0) {
        Some(Geoloc(villeFinded(0).lat, villeFinded(0).lon))
      }
      else None
    }
    else None
  }

  def salaire(apecOE: ApecOE) = {
    if (apecOE.off_salaire_min.isDefined && apecOE.off_salaire_max.isDefined) {
      salaireParse(s"${apecOE.off_salaire_min.get}  - ${apecOE.off_salaire_max.get}".trim)
    }
    else None
  }

  def salaireParse(slr: String) = {
   val splited =  slr.toLowerCase.split("-")
    if (splited.size > 0) {
      val s = s"${splited(0).trim}000".toInt
      Some(s)
    }
    else None
  }

  def occupation(apecOE: ApecOE) = {
    if (apecOE.off_function1.isDefined) {
     Some(appelationsApecURI(apecOE.off_function1.get).toString)

    }
    else None
  }

  def appelationsApecURI(code: String): URI = {
    new URI(s"http://ontology.datasud.fr/openemploi/data/occupation/${CryptoUtils.md5sum(code)}")
  }

  def entreprise(apecOE: ApecOE) = {
    if (apecOE.off_texte_entreprise.isDefined) {
      Some(Entreprise(
        None,
        apecOE.off_texte_entreprise,
        None,
        None
      ))
    }
    else None
  }

  def dates(date: Option[String]) = {

    if (date.isDefined) {
      val dateFormat = new SimpleDateFormat(DATE_FORMAT)
      Some(dateFormat.parse(date.get).toInstant.toEpochMilli)
    }
    else None
  }

  def isAlternance(off_types_cnt: Option[String]) = {
    if (off_types_cnt.isDefined) {
      val bool = if (off_types_cnt.get.contains("Alternance") || off_types_cnt.get.contains("alternance")) true else false
      Some(bool)
    }
    else None
  }


}
