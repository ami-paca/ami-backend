package com.mnemotix.ami.apec.offer.uploader.model

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

case class ApecOE(numero_offre: Option[String],
                  off_lieu: Option[String],
                  off_cp:  Option[String],
                  off_dept: Option[String],
                  off_dept_code: Option[String],
                  off_function1: Option[String],
                  off_function2: Option[String],
                  off_experience: Option[String],
                  off_salaire_min: Option[String],
                  off_salaire_max: Option[String],
                  off_deplacement: Option[String],
                  off_publication: Option[String],
                  off_prise_de_poste: Option[String],
                  off_type_poste: Option[String],
                  off_type_cnt: Option[String],
                  off_nbre_postes: Option[String],
                  off_tps_travail: Option[String],
                  off_texte_offre: Option[String],
                  off_texte_profil: Option[String],
                  off_texte_entreprise: Option[String])

