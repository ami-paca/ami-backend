package com.mnemotix.ami.apec.offer.uploader.services

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{GraphDSL, RunnableGraph, Sink}
import com.mnemotix.ami.apec.offer.uploader.helpers.ApecConfig
import com.mnemotix.ami.connectors.services.ESIndexer
import com.mnemotix.lifty.services.CSVExtractor

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Uploader {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)

  implicit val esIndexer = new ESIndexer()

  lazy val liftyCSVExtractor = new ApecCSVExtractor(ApecConfig.apecInputFile)
  lazy val compcsvExtractor = new ApecCSVExtractor(ApecConfig.apecComp)
  lazy val serializer = new ApecOfferSerializer
  lazy val compSerializer = new ApecCompSerializer
  lazy val apecAppelation = new ApecRDFAppelation()
  lazy val indexer = new ApecOfferIndexer

  lazy val topHeadSink = Sink.ignore


  def init: Unit = {
    indexer.init
    apecAppelation.init
  }

  def upload = {

    lazy val loadVille = new LoadVilles(ApecConfig.villeFile)
    val villes = Await.result(loadVille.load, Duration.Inf)
    val converter = new ApecToOffer(villes)


    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
        import GraphDSL.Implicits._
        liftyCSVExtractor.extractAsync ~> serializer.serialize ~> converter.converter ~> indexer.index ~> indexer.sinkIndexResponse  ~> topHS
        ClosedShape
    }).run()
  }

  def bulkUploader = {
    lazy val loadVille = new LoadVilles(ApecConfig.villeFile)
    val villes = Await.result(loadVille.load, Duration.Inf)
    val converter = new ApecToOffer(villes)


    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
        import GraphDSL.Implicits._
        liftyCSVExtractor.extractAsync ~> serializer.serialize ~> converter.converter ~> indexer.toIndexable ~> indexer.bulkIndex ~> indexer.sinkBulkIndexResponse  ~> topHS
        ClosedShape
    }).run()
  }

  def updater: Future[Done] = {
    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
      import GraphDSL.Implicits._
        compcsvExtractor.extractAsync ~> compSerializer.serialize ~> apecAppelation.converter ~> indexer.toUpdatable ~> indexer.update ~> indexer.sinkUpdateResponse ~> topHS
      ClosedShape
    }).run()
  }
}