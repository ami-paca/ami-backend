/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.apec.offer.uploader.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.apec.offer.uploader.model.{CompApec}
import com.mnemotix.ami.connectors.helpers.ConnectorConfig
import com.mnemotix.synaptix.rdf.client.RDFClient

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}


class ApecRDFAppelation(implicit ec: ExecutionContext) {

  def init = {
    RDFClient.init()
  }

  implicit lazy val conn = RDFClient.getReadConnection(ConnectorConfig.rdfStoreRepoName)

  def qry(notation: String) = {
    s""" PREFIX ope: <http://openemploi.datasud.fr/ontology/>
      |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
      |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      |
      |SELECT DISTINCT ?conceptMetier
      |FROM NAMED <http://ontology.datasud.fr/openemploi/graph/apec/concept/>
      |WHERE {
      |    GRAPH ?g {
      |    ?conceptMetier rdf:type skos:Concept .
      |   	?conceptMetier skos:inScheme <http://ontology.datasud.fr/openemploi/data/scheme/1> .
      |    ?topConcept rdf:type skos:Concept .
      |    <http://ontology.datasud.fr/openemploi/data/scheme/1>  skos:hasTopConcept ?topConcept .
      |    ?conceptMetier skos:notation "$notation" .
      |    ?conceptMetier (skos:broader|^skos:narrower)/(skos:broader|^skos:narrower)/(skos:broader|^skos:narrower) ?topConcept
      |  }
      |}""".stripMargin
  }

  def converter: Flow[CompApec, Option[(String, String)], NotUsed] = {
    Flow[CompApec].map { compApec =>
      if (compApec.N_offre.isDefined && compApec.CodeMetier.isDefined && compApec.CodeMetier.get != "0") {
        val id = compApec.N_offre.get
        val codes = Await.result(conceptMetier(compApec.CodeMetier.get), Duration.Inf)
        if (codes.size > 0) {
          Some(s"${id}W", codes(0))
        }
        else None
      }
      else {
        None
      }
    }
  }

  def conceptMetier(notation: String): Future[Seq[String]] = {
    var applicationKindTypeVocabularies = new ListBuffer[String]()
    val futureApplicationKindIndividualsAsync = RDFClient.select(qry(notation)).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        applicationKindTypeVocabularies += bs.getValue("conceptMetier").stringValue()
      }
      applicationKindTypeVocabularies.toSeq
    }

    futureApplicationKindIndividualsAsync onComplete {
      case Success(i) => println(s"The application-kind request succeed. The number of applicationKindType : ${i.size}")
      case Failure(t) => println("There is a failure here", t)
    }
    futureApplicationKindIndividualsAsync
  }





}
