package com.mnemotix.ami.apec.offer.uploader.services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Sink}
import com.mnemotix.ami.apec.offer.uploader.model.Villes

import scala.collection.immutable
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class LoadVilles(filePath: String) {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)

  lazy val apecCsvExtractor = new ApecCSVExtractor(filePath)

  def serialize: Flow[Map[String, String], Villes, NotUsed] = {
    Flow[Map[String, String]].map { aMap =>
      serializer(aMap)
    }
  }

  def mapValue(aMap: Map[String, String], key: String): Option[String] = if (aMap.get(key).isDefined && !aMap.get(key).get.trim.isEmpty && aMap.get(key).get != " ") Some(aMap.get(key).get.trim) else None

  def serializer(aMap: Map[String, String]): Villes = {
    Villes(
      nomVille(aMap),
      lonLat(aMap, "Latitude"),
      lonLat(aMap, "Longitude")
    )
  }

  def nomVille(aMap: Map[String, String]) = {
    if (mapValue(aMap, "MAJ").isDefined) {
      mapValue(aMap, "MAJ").get
    }
    else {
      mapValue(aMap, "Nom Ville").get.toUpperCase
    }
  }

  def lonLat(aMap: Map[String, String], key: String) = {
    if (mapValue(aMap, key).isDefined && mapValue(aMap, key).get.trim != "-") {
      mapValue(aMap, key).get.toFloat
    }
    else 0.0F
  }


  def load: Future[immutable.Seq[Villes]] = {
    apecCsvExtractor.extractAsync.via(serialize).runWith(Sink.seq)
  }

}
