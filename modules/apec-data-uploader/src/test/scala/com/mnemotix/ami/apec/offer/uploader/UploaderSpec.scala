package com.mnemotix.ami.apec.offer.uploader

import akka.actor.ActorSystem
import com.mnemotix.ami.apec.offer.uploader.services.Uploader
import com.mnemotix.ami.connectors.model.EOffer
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.SearchResponse
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import org.scalatest.concurrent.ScalaFutures
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class UploaderSpec extends FlatSpec with Matchers with BeforeAndAfterAll with ScalaFutures {

  it should "bulk upload file" ignore  {
    def uploader = new Uploader
    uploader.init
    Await.result(uploader.bulkUploader, Duration.Inf)
  }

  it should "update offer" in {
    def uploader=new Uploader
    uploader.init
    Await.result(uploader.updater, Duration.Inf)
  }

  it should "request dev-openemploi-offer-apec" in {
    implicit val executionContext: ExecutionContextExecutor = ExecutionContext.global

    IndexClient.init()

    val futureResponseSearch = IndexClient.rawQuery(RawQuery(Seq("dev-openemploi-offer-apec"), Json.parse(
      """
        |{
        | "query": {
        |  "match_all": {}
        | },
        | "_source": {
        |  "includes": "*"
        | },
        | "from": 0,
        | "size": 11
        |}
        |""".stripMargin).as[JsObject]))

    val resp: Response[SearchResponse] = Await.result(futureResponseSearch, Duration.Inf)
    resp.result.hits.hits.foreach {
      hit =>
       println(hit)

    }
  }
}

/*
val futureResponseSearch: Future[Response[SearchResponse]] = IndexClient.rawQuery(RawQuery(Seq("dev-openemploi-offer-apec"), Json.parse(
      """
        |{
        | "from" : 0, "size" : 1000,
        | "query": {
        |   "match_all": {}
        | }
        |}
        |""".stripMargin
    ).as[JsObject]))
    val resp: Response[SearchResponse] = Await.result(futureResponseSearch, Duration.Inf)
 */