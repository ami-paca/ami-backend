/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.apec.offer.uploader.test


import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Sink}
import com.mnemotix.ami.apec.offer.uploader.services.{ApecCSVExtractor, ApecOfferSerializer}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}


class ApecOfferFileProcessing extends AnyFlatSpec with Matchers {

  implicit val system = ActorSystem(this.getClass.getSimpleName)
  implicit val executionContext = ExecutionContext.global

  lazy val offerComp =new ApecCSVExtractor("/Users/prlherisson/Documents/mnemotix/projets/open-emploi/Base offres_complément variables métiers_15102020.csv")
  lazy val offerApec = new ApecCSVExtractor("/Users/prlherisson/Documents/mnemotix/projets/open-emploi/offresEmploi_apecv2-95K.csv")

  lazy val apecOfferSerializer = new ApecOfferSerializer


  var i = 0

  /*
    100 size of
    1
   */

  it should "concat the two files" in {


    //val done= Source.fromIterator(() => newOffer.toIterator).runWith(FileIO.toPath(Paths.get("/Users/prlherisson/Documents/mnemotix/projets/open-emploi/offresEmploi_apecv3-95K.csv")))



//FileIO.toPath(Paths.get("/Users/prlherisson/Documents/mnemotix/projets/open-emploi/offresEmploi_apecv3-95K.csv"))
  }

}
