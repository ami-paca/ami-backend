package com.mnemotix.ami.apec.concept.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.apec.concept.model.ApecMetier

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ApecMetierSerializer {

  def serialize: Flow[Map[String, String], ApecMetier, NotUsed] = {
    Flow[Map[String, String]].map { aMap =>
      serializer(aMap)
    }
  }

  def mapValue(aMap: Map[String, String], key: String): Option[String] = if (aMap.get(key).isDefined && !aMap.get(key).get.trim.isEmpty && aMap.get(key).get != " ") Some(aMap.get(key).get.trim) else None

  def serializer(aMap: Map[String, String]): ApecMetier = {
    ApecMetier(
      mapValue(aMap, "Code_INTITULÉ"),
      mapValue(aMap, "INTITULÉ"),
      mapValue(aMap, "Code_MÉTIER"),
      mapValue(aMap, "MÉTIER"),
      mapValue(aMap, "Code_FAMILLE"),
      mapValue(aMap, "FAMILLE"),
      mapValue(aMap, "Code_JOB46"),
      mapValue(aMap, "JOB46"),
      mapValue(aMap, "Code_JOB10"),
      mapValue(aMap, "JOB10")
    )
  }


}
