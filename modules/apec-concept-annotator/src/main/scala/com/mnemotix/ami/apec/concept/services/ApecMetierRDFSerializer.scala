package com.mnemotix.ami.apec.concept.services

import java.net.URI

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.apec.concept.helpers.ApecMetierUriFactory
import com.mnemotix.ami.apec.concept.model.ApecMetier
import com.mnemotix.ami.apec.concept.utils.{AnnotationWriters, AnnotationsCommons, RDFUtils}
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ApecMetierRDFSerializer {
  def recognizerAsync: Flow[ApecMetier, Seq[LiftyAnnotation], NotUsed] = {
    Flow[ApecMetier].map { apecMetier =>
      annotations(apecMetier)
    }
  }

  def annotations(apecMetier: ApecMetier): Seq[LiftyAnnotation] = {
    val grandDomaineA = if (apecMetier.codeGrandDomaine.isDefined) Some(grandDomaine(apecMetier.grandDomaineIntitule, apecMetier.codeGrandDomaine))  else None
    val domaineProA = if (apecMetier.codeGrandDomaine.isDefined && apecMetier.codeDomainePro.isDefined) {
      val uriGrandDomaine = ApecMetierUriFactory.skosApecConceptURI(RDFUtils.libelleCleaner(apecMetier.grandDomaineIntitule.get.trim))
      Some(domainePro(uriGrandDomaine, apecMetier.domaineProLibelle, apecMetier.codeDomainePro))
    }
    else None

    val familleA = if (apecMetier.codeDomainePro.isDefined && apecMetier.codeFamille.isDefined) {
      val uriDomainePro = ApecMetierUriFactory.skosApecConceptURI(RDFUtils.libelleCleaner(apecMetier.domaineProLibelle.get.trim))
      Some(famille(uriDomainePro, apecMetier.codeFamilleLibelle, apecMetier.codeFamille))
    }
    else None

    val occupationA = if (apecMetier.codeFamille.isDefined && apecMetier.codeMetier.isDefined) {
      val uriCodeFamille = ApecMetierUriFactory.skosApecConceptURI(RDFUtils.libelleCleaner(apecMetier.codeFamilleLibelle.get.trim))
      Some(metier(uriCodeFamille, apecMetier.metierLibelle, apecMetier.codeMetier))
    }
    else None

    val appelationA = if (apecMetier.codeMetier.isDefined && apecMetier.codeAppelation.isDefined) {
      val uriOccupation = ApecMetierUriFactory.skosApecConceptURI(RDFUtils.libelleCleaner(apecMetier.metierLibelle.get.trim))
      Some(appelation(uriOccupation, apecMetier.appellationLibelle, apecMetier.codeAppelation))
    }
    else None

    (grandDomaineA.toSeq ++ domaineProA.toSeq ++ familleA.toSeq ++ occupationA.toSeq ++ appelationA.toSeq).flatten.seq ++ AnnotationsCommons.schemeAnnotation

  }

  def grandDomaine(libelle: Option[String], code: Option[String]): Seq[LiftyAnnotation] = {
    val uriGrandDomaine = ApecMetierUriFactory.skosApecConceptURI(RDFUtils.libelleCleaner(libelle.get.trim))
    AnnotationWriters.grandDomaineAnnotations(uriGrandDomaine, libelle, code)
  }

  def domainePro(uriGrandDomaine: URI, libelle: Option[String], code: Option[String]): Seq[LiftyAnnotation] = {
      val uriDomainePro = ApecMetierUriFactory.skosApecConceptURI(RDFUtils.libelleCleaner(libelle.get.trim))
    AnnotationWriters.domaineProAnnotations(uriDomainePro, uriGrandDomaine, libelle, code)
  }

  def famille(uriDomainePro: URI, libelle: Option[String], code: Option[String]): Seq[LiftyAnnotation] = {
      val uriFamily = ApecMetierUriFactory.skosApecConceptURI(RDFUtils.libelleCleaner(libelle.get.trim))
    AnnotationWriters.familleAnnotations(uriFamily, uriDomainePro, libelle, code)
  }

  def metier(familleURI: URI, libelle: Option[String], code: Option[String]): Seq[LiftyAnnotation] = {
      val uriOccupation = ApecMetierUriFactory.skosApecConceptURI(RDFUtils.libelleCleaner(libelle.get.trim))
    AnnotationWriters.metierAnnotations(uriOccupation, familleURI, libelle, code)
  }

  def appelation(occupationURI: URI, libelle: Option[String], code: Option[String]): Seq[LiftyAnnotation] = {
    val uriAppelation = ApecMetierUriFactory.skosApecConceptURI(libelle.get.trim)
    AnnotationWriters.appelationAnnotation(uriAppelation, libelle, code, occupationURI)
  }
}