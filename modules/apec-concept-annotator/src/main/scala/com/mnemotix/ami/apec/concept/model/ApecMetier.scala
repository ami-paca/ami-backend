package com.mnemotix.ami.apec.concept.model

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * JOB10 GrandDomaine
 * Code_JOB10 Code GrandDomaine -> skosApecConceptURI
 * JOB46 domaineProfessionnel
 * Code_JOB46 CodeDomaineProfessionnel -> skosApecConceptURI
 * FAMILLE famille
 * Code_FAMILLE codeFamille -> skosApecConceptURI
 * MÉTIER ficheMetier
 * Code_MÉTIER codeMetier -> skosApecConceptURI
 * INTITULÉ appellation
 * Code_INTITULÉ codeAppellation -> appelationsApecURI
 */

case class ApecMetier(codeAppelation: Option[String], appellationLibelle: Option[String], codeMetier: Option[String], metierLibelle: Option[String],
                      codeFamille: Option[String], codeFamilleLibelle: Option[String], codeDomainePro: Option[String], domaineProLibelle: Option[String],
                      codeGrandDomaine: Option[String], grandDomaineIntitule: Option[String])
