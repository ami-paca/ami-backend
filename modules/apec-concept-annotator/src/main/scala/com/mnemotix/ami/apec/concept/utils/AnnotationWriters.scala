package com.mnemotix.ami.apec.concept.utils

import java.net.URI

import com.mnemotix.ami.apec.concept.helpers.ApecMetierConfig
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object AnnotationWriters {

  def grandDomaineAnnotations(grandDomaineURI: URI, libelle: Option[String], code: Option[String]) = {
    AnnotationsCommons.occupationsGeneral(grandDomaineURI) ++
    AnnotationsCommons.schemeOneAnnotations(grandDomaineURI, libelle, code) ++
      AnnotationsCommons.libelleAnnotations(grandDomaineURI, libelle, code).toSeq ++
      Seq(LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/1"),"http://www.w3.org/2004/02/skos/core#hasTopConcept",createIRI(grandDomaineURI.toString), 1.0))
  }

  def domaineProAnnotations(domaineProUri: URI, grandDomaineUri: URI, libelle: Option[String], code: Option[String]) = {
    AnnotationsCommons.occupationsGeneral(domaineProUri) ++
      AnnotationsCommons.schemeOneAnnotations(domaineProUri, libelle, code) ++
      AnnotationsCommons.libelleAnnotations(domaineProUri, libelle, code).toSeq ++
      Seq(LiftyAnnotation(domaineProUri, "http://www.w3.org/2004/02/skos/core#broader", createIRI(grandDomaineUri.toString), 1.0))
  }

  def familleAnnotations(familleURI: URI, uriDomainePro: URI, libelle: Option[String], code: Option[String]): Seq[LiftyAnnotation] = {
    AnnotationsCommons.occupationsGeneral(familleURI) ++
      AnnotationsCommons.schemeOneAnnotations(familleURI, libelle, code) ++
      AnnotationsCommons.libelleAnnotations(familleURI, libelle, code).toSeq ++
      Seq(LiftyAnnotation(familleURI, "http://www.w3.org/2004/02/skos/core#broader", createIRI(uriDomainePro.toString), 1.0))
  }

  def metierAnnotations(occupationURI:URI, familleURI: URI, libelle: Option[String], code: Option[String]): Seq[LiftyAnnotation] = {
    AnnotationsCommons.occupationsGeneral(occupationURI) ++
      AnnotationsCommons.schemeOneAnnotations(occupationURI, libelle, code) ++
      AnnotationsCommons.libelleAnnotations(occupationURI, libelle, code).toSeq ++
      Seq(LiftyAnnotation(occupationURI, "http://www.w3.org/2004/02/skos/core#broader", createIRI(familleURI.toString), 1.0))

  }

  def appelationAnnotation(uriAppelation: URI, libelle: Option[String], code: Option[String], metierURI: URI): Seq[LiftyAnnotation] = {
    AnnotationsCommons.occupationsGeneral(uriAppelation) ++
    AnnotationsCommons.libelleAnnotations(uriAppelation, libelle, code) ++
    Seq(LiftyAnnotation(uriAppelation, "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(s"${LiftyConfig.dataURI}apec/scheme/2"), 1.0),
      LiftyAnnotation(uriAppelation, "http://www.w3.org/2004/02/skos/core#related", createIRI(metierURI.toString), 1.0))
  }
}