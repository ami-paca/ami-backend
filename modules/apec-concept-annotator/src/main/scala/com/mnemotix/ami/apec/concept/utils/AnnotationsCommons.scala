package com.mnemotix.ami.apec.concept.utils

import java.net.URI

import com.mnemotix.ami.apec.concept.helpers.ApecMetierConfig
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object AnnotationsCommons {
  def schemeOneAnnotations(codeMetierURI: URI, libelle: Option[String], code: Option[String]): Seq[LiftyAnnotation] = {
    Seq(LiftyAnnotation(codeMetierURI, "a", createIRI("http://www.w3.org/2004/02/skos/core#Concept"), 1.0),
      LiftyAnnotation(codeMetierURI, "http://www.w3.org/2004/02/skos/core#inScheme", createIRI(s"${LiftyConfig.dataURI}apec/scheme/1"), 1.0))
  }

  def libelleAnnotations(uri: URI, libelle: Option[String], code: Option[String]): Seq[LiftyAnnotation] = {
      RDFUtils.literals(uri, libelle,"http://www.w3.org/2004/02/skos/core#", "prefLabel", "http://www.w3.org/2001/XMLSchema#string").toSeq ++
      RDFUtils.literals(uri, RDFUtils.hiddenLabel("fr", libelle.get), "http://www.w3.org/2004/02/skos/core#", "hiddenLabel","http://www.w3.org/2001/XMLSchema#string").toSeq ++
      RDFUtils.literals(uri,libelle, "http://www.w3.org/2004/02/skos/core#", "altLabel", "http://www.w3.org/2001/XMLSchema#string").toSeq ++
        RDFUtils.literals(uri, code, "http://www.w3.org/2004/02/skos/core#", "notation", "http://www.w3.org/2001/XMLSchema#string").toSeq
  }

  def occupationsGeneral(uriCodeOccupation: URI ) = {
    Seq(LiftyAnnotation(uriCodeOccupation, "a", createIRI(s"${ApecMetierConfig.mnOntoUri}Occupation"), 1.0),
      LiftyAnnotation(uriCodeOccupation, s"${ApecMetierConfig.mnxOntoUri}conceptOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/apec"), 1.0))
  }

  def schemeAnnotation: Seq[LiftyAnnotation] = {
    Seq(LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}vocabulary/apec"), "http://www.w3.org/2004/02/skos/core#prefLabel", createLiteral("Référentiel des Métiers APEC","fr"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}vocabulary/apec"), "a", createIRI(s"${ApecMetierConfig.mnxOntoUri}Vocabulary"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}vocabulary/apec"), "http://www.w3.org/2004/02/skos/core#altLabel", createLiteral("APEC","fr"), 1.0)
    ) ++ Seq(
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/1"), "a", createIRI("http://www.w3.org/2004/02/skos/core#ConceptScheme"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/1"), "http://www.w3.org/2004/02/skos/core#prefLabel", createLiteral("Domaines Professionels","fr"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/1"), s"${ApecMetierConfig.mnxOntoUri}schemeOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/apec"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/2"), "a", createIRI("http://www.w3.org/2004/02/skos/core#ConceptScheme"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/2"), "http://www.w3.org/2004/02/skos/core#prefLabel", createLiteral("Métiers","fr"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/2"), s"${ApecMetierConfig.mnxOntoUri}schemeOf", createIRI(s"${LiftyConfig.dataURI}vocabulary/apec"), 1.0)
    )
  }
}

/*
LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/3"), "a", createIRI("http://www.w3.org/2004/02/skos/core#ConceptScheme"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/3"), "http://www.w3.org/2004/02/skos/core#prefLabel", createLiteral("Compétences","fr"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/3"), s"${ApecMetierConfig.mnxOntoUri}schemeOf", createIRI(s"${ApecMetierConfig.mnxOntoUri}vocabulary/apec"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/4"), "a", createIRI("http://www.w3.org/2004/02/skos/core#ConceptScheme"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/4"), "http://www.w3.org/2004/02/skos/core#prefLabel", createLiteral("Environnements de travail","fr"), 1.0),
      LiftyAnnotation(new URI(s"${LiftyConfig.dataURI}apec/scheme/4"), s"${ApecMetierConfig.mnxOntoUri}schemeOf", createIRI(s"${ApecMetierConfig.mnxOntoUri}vocabulary/apec"), 1.0)
 */