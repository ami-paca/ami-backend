/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami

import com.mnemotix.lifty.helpers.AnnotationsUtils
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.models.LiftyAnnotation

import java.net.URI

object LabelUtil {

  lazy val annotationUtils = new AnnotationsUtils(None)

  def labels(libelle: Option[String]): Option[Seq[String]] = {
    if (libelle.isDefined) {
      val libelleSeq: Array[String] = libelle.get.split("/")
      Some(Seq(masculineAppelation(libelleSeq), feminineAppelation(libelleSeq)))
    }
    else None
  }

  def masculineAppelation(libelleSeq: Array[String]): String = {
    if (libelleSeq.size > 1) {
      val libelleMasculine = libelleSeq(0).trim
      val wordsMasc = libelleMasculine.split(" ")
      val wordsFem = libelleSeq(1).trim.split(" ")
      if (wordsMasc.size > 1 ) {
        libelleMasculine
      }
      else if (wordsMasc.size == 1 && wordsFem.size == 1) {
        libelleMasculine
      }
      else if (wordsMasc.size == 1 && wordsFem.size > 1) {
        s"${libelleMasculine.trim} ${wordsFem.tail.mkString(" ")}"
      }
      else libelleMasculine
    }
    else libelleSeq.mkString(" ")
  }

  def feminineAppelation(libelleSeq: Array[String]): String = {
    if (libelleSeq.size == 1) libelleSeq(0)
    else libelleSeq(1).trim

  }

  def occupationLiteralsFR[A](appelationsURI: URI, literal: Option[A], ontologyURI: String, property: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(appelationsURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, "fr"), 1.0))
    }
    else None
  }

  def occupationLiterals[A](appelationsURI: URI, literal: Option[A], ontologyURI: String, property: String, datatype: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(appelationsURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, createIRI(datatype)), 1.0))
    }
    else None
  }

  lazy val MAX_LABEL_SIZE: Int = 50
  lazy val DEFAULT_WINDOW_SIZE: Int = 3

  lazy val STOP_WORDS: Map[String, Seq[String]] = Map(
    "fr" -> Seq("ai","aie","aient","aies","ait","as","au","aura","aurai","auraient","aurais","aurait","auras","aurez","auriez","aurions","aurons","auront","aux","avaient","avais","avait","avec","avez","aviez","avions","avons","ayant","ayez","ayons","ce","ceci","cela","celà","ces","cet","cette","dans","de","des","du","elle","en","es","est","et","étaient","étais","était","étant","été","étée","étées","étés","êtes","étiez","étions","eu","eue","eues","eûmes","eurent","eus","eusse","eussent","eusses","eussiez","eussions","eut","eût","eûtes","eux","fûmes","furent","fus","fusse","fussent","fusses","fussiez","fussions","fut","fût","fûtes","ici","il","ils","je","la","le","les","leur","leurs","lui","ma","mais","me","même","mes","moi","mon","ne","nos","notre","nous","on","ont","ou","par","pas","pour","qu","que","quel","quelle","quelles","quels","qui","sa","sans","se","sera","serai","seraient","serais","serait","seras","serez","seriez","serions","serons","seront","ses","soi","soient","sois","soit","sommes","son","sont","soyez","soyons","suis","sur","ta","te","tes","toi","ton","tu","un","une","vos","votre","vous"),
    "en" -> Seq("a","about","above","after","again","against","all","also","am","an","and","another","any","are","aren't","as","at","back","be","because","been","before","being","below","between","both","but","by","can't","cannot","could","couldn't","did","didn't","do","does","doesn't","doing","don't","down","during","each","even","ever","every","few","first","five","for","four","from","further","get","go","goes","had","hadn't","has","hasn't","have","haven't","having","he","he'd","he'll","he's","her","here","here's","hers","herself","high","him","himself","his","how","how's","however","i","i'd","i'll","i'm","i've","if","in","into","is","isn't","it","it's","its","itself","just","least","less","let's","like","long","made","make","many","me","more","most","mustn't","my","myself","never","new","no","nor","not","now","of","off","old","on","once","one","only","or","other","ought","our","ours","ourselves","out","over","own","put","said","same","say","says","second","see","seen","shan't","she","she'd","she'll","she's","should","shouldn't","since","so","some","still","such","take","than","that","that's","the","their","theirs","them","themselves","then","there","there's","these","they","they'd","they'll","they're","they've","this","those","three","through","to","too","two","under","until","up","very","was","wasn't","way","we","we'd","we'll","we're","we've","well","were","weren't","what","what's","when","when's","where","where's","whether","which","while","who","who's","whom","why","why's","with","won't","would","wouldn't","you","you'd","you'll","you're","you've","your","yours","yourself","yourselves")
  )

  def hiddenLabel(lang: String, label: String): Option[String] = {
    annotationUtils.hiddenLabel(label, Some(6))
  }
}