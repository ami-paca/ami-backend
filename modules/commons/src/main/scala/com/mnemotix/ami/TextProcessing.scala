/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami

import com.mnemotix.analytix.commons.utils.TextUtils

import java.util.regex.Pattern
import scala.io.Source

object TextProcessing {

  lazy val frPattern: Pattern = Pattern.compile("[\\d:,\"\'\\`\\_\\|?!\n\r@;]+")

  val stopWordsConfing = ("l'|d'")
  val carry = new Carry


  def words(text: String, maxLabelSize: Option[Int], stem: Boolean): Option[String] = {
    val normalized = TextUtils.normalize(text)
    val input_text: String = normalized.trim.toLowerCase.replaceAll("\\{.*?\\}", "")
      .replaceAll(stopWordsConfing, "")
      .replaceAll("\\[.*?\\]", "").replaceAll("\\((.*?)\\)", "$1")
      .replaceAll("[^A-Za-z0-9(),!?@\'\\`\"\\_\n]", " ")
      .replaceAll("[/]"," ").replaceAll(";"," ").replaceAll("[^\\w\\s]", "")

    val elts = input_text.split(" ").filter(label => label.length > 0).filterNot(label => stopWords.contains(label.trim))

    val toString = if (stem) {
      elts.map(label => carry.carry(label))
    }
    else elts

    val elt = {
      if (toString.length == 0 || toString.length > maxLabelSize.getOrElse(4)) None
      else Some(toString.mkString(" "))
    }
    elt
  }

  def stopWords = {
    val frenchFile = Source.fromResource("stopwords_fr.txt")
    frenchFile.getLines()
  }
}