/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami

import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.models.LiftyAnnotation

import java.net.URI

object AnnotationHelper {

  def hiddenLabelAnnotation(uriRacineCompetence: URI, lang: String, label: Option[String]): Option[LiftyAnnotation] = {
    if (label.isDefined) {
      val hidden: Option[String] = LabelUtil.hiddenLabel(lang,label.get)
      annotationLiterals(uriRacineCompetence, hidden, "http://www.w3.org/2004/02/skos/core#", "hiddenLabel", lang)
    }
    else None
  }

  def annotationLiterals[A](appelationsURI: URI, literal: Option[A], ontologyURI: String, property: String, datatype: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(appelationsURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, createIRI(datatype)), 1.0))
    }
    else None
  }

  def annotationLiteralsLang[A](appelationsURI: URI, literal: Option[A], ontologyURI: String, property: String, lang: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(appelationsURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, lang), 1.0))
    }
    else None
  }

}