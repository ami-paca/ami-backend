/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami

import com.mnemotix.lifty.helpers.{AnnotationsUtils, CryptoUtils, PrimitiveDatatypesIRI}
import com.mnemotix.lifty.helpers.AnnotationsUtils.createIRI
import com.mnemotix.lifty.models.LiftyAnnotation

import java.net.URI

object MnxAnalytixWriter {
  val annotationsUtils = new AnnotationsUtils(Some("french"))
  val mnxOntoUri = "http://ns.mnemotix.com/ontologies/2019/8/generic-model/"

  def annotation(concept: URI, labels: Seq[String], indexName: String, recogType: String) = {
    if (recogType == "ESPERCOLATION") {
      esPercolation(concept, labels, indexName)
    }
    else if (recogType == "WORDEMBEDDING") {
      wordEmbedding(concept, labels, indexName)
    }
    else esPercolation(concept, labels, indexName)
  }

  def wordEmbedding(concept: URI, labels: Seq[String], indexName: String) = {
    val recogType = "wordEmbedding"
    val entityClassName = "skos:Concept"
    val id = CryptoUtils.md5sum(concept.toString, labels.mkString(" "))
    val recogs = Seq(LiftyAnnotation(concept, s"${mnxOntoUri}hasEntityRecog", createIRI(s"${mnxOntoUri}$recogType/$id"), 1.0),
      LiftyAnnotation(new URI(s"${mnxOntoUri}$recogType/$id"), s"a", createIRI(s"${mnxOntoUri}WordEmbedding"), 1.0)
    )
    val textEntry = labels.filter{ label =>
      val pLabel = percoLabel(label)
      pLabel.isDefined
    }.mkString(" ")
    val annotationsLiteral = AnnotationHelper.annotationLiterals(new URI(s"${mnxOntoUri}$recogType/$id"), Some(textEntry), s"${mnxOntoUri}", "textEntry", PrimitiveDatatypesIRI.STRINGIRIDATATYPE.toString).toSeq ++
      AnnotationHelper.annotationLiterals(new URI(s"${mnxOntoUri}$recogType/$id"), Some(entityClassName), s"${mnxOntoUri}", "entityClassName", PrimitiveDatatypesIRI.STRINGIRIDATATYPE.toString).toSeq ++
      AnnotationHelper.annotationLiterals(new URI(s"${mnxOntoUri}$recogType/$id"), Some(indexName), s"${mnxOntoUri}", "indexName", PrimitiveDatatypesIRI.STRINGIRIDATATYPE.toString).toSeq
    recogs ++ annotationsLiteral
  }

  def esPercolation(concept: URI, labels: Seq[String], indexName: String) = {
    val recogType = "esPercolation"
    val id = CryptoUtils.md5sum(concept.toString, labels.mkString(" "))
    val recogs = Seq(LiftyAnnotation(concept, s"${mnxOntoUri}hasEntityRecog", createIRI(s"${mnxOntoUri}$recogType/$id"), 1.0),
      LiftyAnnotation(new URI(s"${mnxOntoUri}$recogType/$id"), s"a", createIRI(s"${mnxOntoUri}ESPercolation"), 1.0)
    )
    val annotationsLiteral = labels.filter{ label =>
      val pLabel = percoLabel(label)
      pLabel.isDefined
    }.map { label =>
      val pLabel = percoLabel(label)
      val qTemplate = queryTemplate(pLabel.get)
      AnnotationHelper.annotationLiterals(new URI(s"${mnxOntoUri}$recogType/$id"), pLabel, s"${mnxOntoUri}", "percoLabel", PrimitiveDatatypesIRI.STRINGIRIDATATYPE.toString).toSeq ++
        AnnotationHelper.annotationLiterals(new URI(s"${mnxOntoUri}$recogType/$id"), Some(qTemplate), s"${mnxOntoUri}", "queryTemplate", PrimitiveDatatypesIRI.STRINGIRIDATATYPE.toString).toSeq ++
        AnnotationHelper.annotationLiterals(new URI(s"${mnxOntoUri}$recogType/$id"), Some(indexName), s"${mnxOntoUri}", "indexName", PrimitiveDatatypesIRI.STRINGIRIDATATYPE.toString).toSeq
    }.toSeq.flatten
    recogs ++ annotationsLiteral
  }

  def percoLabel(label: String) = {
    TextProcessing.words(label, None, false)
  }

  def queryTemplate(percoLabel: String) = {
    s"""{
       |    "query": {
       |        "match_phrase": {
       |            "content": {
       |                "query": "${percoLabel}",
       |                "slop": 4
       |            }
       |        }
       |    }
       |}""".stripMargin
  }
}
