/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami

class Carry {

  lazy val VOWELS = "aeiouyâàëéêèïîôûùœæ"
  lazy val V = s"[${VOWELS}]"
  lazy val C = s"[^${VOWELS}]"

  lazy val LC = s"^${C}+"
  lazy val TV = s"${V}+$$"
  lazy val M = s"(${V}+${C}+)"

  def computeM(text: String) = {
    val rs = text.replaceFirst(LC, "").replaceFirst(TV, "")
    M.r.findAllMatchIn(rs).size
  }

  lazy val STEP1: Seq[(Int, String, String)] = Seq(
    (0, "issaient", ""),
    (0, "ellement", "el"),
    (0, "issement", ""),
    (0, "alement", "al"),
    (0, "eraient", ""),
    (0, "iraient", ""),
    (0, "eassent",""),
    (0, "ussent",""),
    (0, "amment",""),
    (0, "emment",""),
    (0, "issant",""),
    (0, "issent",""),
    (0, "assent",""),
    (0, "eaient",""),
    (0, "issait",""),
    (0, "èrent",""),
    (0, "erent",""),
    (0, "irent",""),
    (0, "erait",""),
    (0, "irait",""),
    (0, "iront",""),
    (0, "eront",""),
    (0, "ement",""),
    (0, "aient",""),
    (0, "îrent",""),
    (0, "eont",""),
    (0, "eant",""),
    (0, "eait",""),
    (0, "ient",""),
    (0, "ent",""),
    (0, "ont",""),
    (0, "ant",""),
    (0, "eât",""),
    (0, "ait",""),
    (0, "at",""),
    (0, "ât",""),
    (0, "it",""),
    (0, "ît",""),
    (0, "t",""),
    (0, "uction",""),
    (1, "ication",""),
    (1, "iation",""),
    (1, "ation",""),
    (0, "ition",""),
    (0, "tion",""),
    (1, "ateur",""),
    (1, "teur",""),
    (0, "eur",""),
    (0, "ier",""),
    (0, "er",""),
    (0, "ir",""),
    (0, "r",""),
    (0, "eassiez",""),
    (0, "issiez",""),
    (0, "assiez",""),
    (0, "ussiez",""),
    (0, "issez",""),
    (0, "assez",""),
    (0, "eriez",""),
    (0, "iriez",""),
    (0, "erez",""),
    (0, "irez",""),
    (0, "iez",""),
    (0, "ez",""),
    (0, "erai",""),
    (0, "irai",""),
    (0, "eai",""),
    (0, "ai",""),
    (0, "i",""),
    (0, "ira",""),
    (0, "era",""),
    (0, "ea",""),
    (0, "a",""),
    (0, "f", "v"),
    (0, "yeux", "oeil"),
    (0, "eux",""),
    (0, "aux", "al"),
    (0, "x",""),
    (0, "issante",""),
    (1, "atrice",""), // Added
    (0, "eresse",""),
    (0, "eante",""),
    (0, "easse",""),
    (0, "eure",""),
    (0, "esse",""),
    (0, "asse",""),
    (0, "ance",""),
    (0, "ence",""),
    (0, "aise",""),
    (0, "euse",""),
    (0, "oise", "o"),
    (0, "isse",""),
    (0, "ante",""),
    (0, "ouse", "ou"),
    (0, "ière",""),
    (0, "ete",""),
    (0, "ète",""),
    (0, "iere",""),
    (0, "aire",""),
    (1, "ure",""),
    (0, "erie",""),
    (0, "étude",""),
    (0, "etude",""),
    (0, "itude",""),
    (0, "ade",""),
    (0, "isme",""),
    (0, "age",""),
    (0, "trice",""),
    (0, "cque", "c"),
    (0, "que", "c"),
    (0, "eille", "eil"),
    (0, "elle",""),
    (0, "able",""),
    (0, "iste",""),
    (0, "ulle", "ul"),
    (0, "gue", "g"),
    (0, "ette",""),
    (0, "nne", "n"),
    (0, "itée",""),
    (0, "ité",""),
    (0, "té",""),
    (0, "ée",""),
    (0, "é",""),
    (0, "usse",""),
    (0, "aise",""),
    (0, "ate",""),
    (0, "ite",""),
    (0, "ee",""),
    (0, "e",""),
    (0, "issements",""),
    (0, "issantes",""),
    (1, "ications",""),
    (0, "eassions",""),
    (0, "eresses",""),
    (0, "issions",""),
    (0, "assions",""),
    (1, "atrices",""), // Added
    (1, "iations",""),
    (0, "issants",""),
    (0, "ussions",""),
    (0, "ements",""),
    (0, "eantes",""),
    (0, "issons",""),
    (0, "assons",""),
    (0, "easses",""),
    (0, "études",""),
    (0, "etudes",""),
    (0, "itudes",""),
    (0, "issais",""),
    (0, "trices",""),
    (0, "eilles", "eil"),
    (0, "irions",""),
    (0, "erions",""),
    (1, "ateurs",""),
    (1, "ations",""),
    (0, "usses",""),
    (0, "tions",""),
    (0, "ances",""),
    (0, "entes",""),
    (1, "teurs",""),
    (0, "eants",""),
    (0, "ables",""),
    (0, "irons",""),
    (0, "irais",""),
    (0, "ences",""),
    (0, "ients",""),
    (0, "ieres",""),
    (0, "eures",""),
    (0, "aires",""),
    (0, "erons",""),
    (0, "esses",""),
    (0, "euses",""),
    (0, "ulles", "ul"),
    (0, "cques", "c"),
    (0, "elles",""),
    (0, "ables",""),
    (0, "istes",""),
    (0, "aises",""),
    (0, "asses",""),
    (0, "isses",""),
    (0, "oises", "o"),
    (0, "tions",""),
    (0, "ouses", "ou"),
    (0, "ières",""),
    (0, "eries",""),
    (0, "antes",""),
    (0, "ismes",""),
    (0, "erais",""),
    (0, "eâtes",""),
    (0, "eâmes",""),
    (0, "itées",""),
    (0, "ettes",""),
    (0, "ages",""),
    (0, "eurs",""),
    (0, "ents",""),
    (0, "ètes",""),
    (0, "etes",""),
    (0, "ions",""),
    (0, "ités",""),
    (0, "ites",""),
    (0, "ates",""),
    (0, "âtes",""),
    (0, "îtes",""),
    (0, "eurs",""),
    (0, "iers",""),
    (0, "iras",""),
    (0, "eras",""),
    (0, "ures",""),
    (0, "ants",""),
    (0, "îmes",""),
    (0, "ûmes",""),
    (0, "âmes",""),
    (0, "ades",""),
    (0, "eais",""),
    (0, "eons",""),
    (0, "ques", "c"),
    (0, "gues", "g"),
    (0, "nnes", "n"),
    (0, "ttes",""),
    (0, "îtes",""),
    (0, "tés",""),
    (0, "ons",""),
    (0, "ais",""),
    (0, "ées",""),
    (0, "ees",""),
    (0, "ats",""),
    (0, "eas",""),
    (0, "ts",""),
    (0, "rs",""),
    (0, "as",""),
    (0, "es",""),
    (0, "fs", "v"),
    (0, "és",""),
    (0, "is",""),
    (0, "s",""),
    (0, "eau",""),
    (0, "au",""))

  lazy val STEP2: Seq[(Int, String, String)] = Seq(
    (1, "ation",""),
    (1, "ition",""),
    (1, "tion",""),
    (1, "ent",""),
    (1, "el",""),
    (0, "i",""))

  lazy val STEP3: Seq[(Int, String, String)] = Seq(
    (0, "ll", "l"),
    (0, "mm", "m"),
    (0, "nn", "n"),
    (0, "pp", "p"),
    (0, "tt", "t"),
    (0, "ss", "s"),
    (0, "y",""),
    (0, "t",""),
    (0, "qu", "c"))


  def compute(text: String) = {
    val t = text.replaceAll(LC, "")
      .replaceAll(TV, "")

    t.matches(M)
  }

  def applyRules(rules: Seq[(Int, String, String)], stem: String): String = {

    def applyRulesHelper(rule: (Int, String, String), oldStem: String): String = {
      if (oldStem.endsWith(rule._2)) {
        val newStem: String = oldStem.slice(0, (oldStem.size - rule._2.length)) + rule._3
        val m = computeM(newStem)
        if (m <= rule._1) applyRulesHelper(rule, newStem) else newStem
      }
      else oldStem
    }

    val rs = rules.find(rule => stem.endsWith(rule._2))


    if (rs.isDefined) {
      applyRulesHelper(rs.get, stem)
    }
    else stem

  }

  def carry(word: String) = {
    val stem = word.toLowerCase()
    val s = applyRules(STEP1, stem)
    val s2 = applyRules(STEP2, s)
    val s3 = applyRules(STEP3, s2)
    if (s3.isEmpty) stem else s3
  }
}