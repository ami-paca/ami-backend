package com.mnemotix.ami.datasud.annotator.services.serializer

import com.mnemotix.ami.datasud.annotator.data.test.MapDataSud
import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class LieuFormationSerializerSpec extends FlatSpec with Matchers {

  it should "retrieve code INSEE" in {
    val codeInsee = LieuFormationSerializer.postalInsee(MapDataSud.myMap, "lieuform_adresse_codeInseeCommune")
    codeInsee.isDefined shouldEqual(true)
    codeInsee.get shouldEqual(4112)
  }

  it should "retrieve a long lat" in {
    val longlat = LieuFormationSerializer.longlat(MapDataSud.myMap, "lieuform_latitude")
    longlat.isDefined shouldEqual(true)
    longlat.get shouldEqual(5.78827F)
  }

  it should "retrieve a lieuFormation" in {
    val lieuFormation = LieuFormationSerializer.serializer(MapDataSud.myMap)
    lieuFormation.lieuFormAdresseLigne1.isDefined shouldEqual(true)
    lieuFormation.lieuFormAdresseLigne2.isDefined shouldEqual(true)
    lieuFormation.lieuFormAdresseLigne1.get shouldEqual "L'Agora"
  }
}