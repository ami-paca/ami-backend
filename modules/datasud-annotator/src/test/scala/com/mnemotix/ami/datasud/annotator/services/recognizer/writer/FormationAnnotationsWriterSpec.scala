package com.mnemotix.ami.datasud.annotator.services.recognizer.writer

import java.net.URI

import com.mnemotix.ami.datasud.annotator.data.test.DatasudFormation
import com.mnemotix.ami.datasud.annotator.helpers.DatasudFormationUriFactory
import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class FormationAnnotationsWriterSpec extends FlatSpec with Matchers {

  it should "retrieve the string of duration" in {
    //val chaineDeChar = FormationAnnotationsWriter.duration(DatasudFormation.datasudFormation.sessDebut, DatasudFormation.datasudFormation.seesFin)
    //chaineDeChar shouldEqual("2906200230062020")
  }

  it should "retrieve the string of a lieu de formation" in {
    val lieuDeFormation = FormationAnnotationsWriter.address(DatasudFormation.lieuFormation)
    lieuDeFormation shouldEqual("L'AgoraChemin Champs de PruniersMANOSQUE41004112")
  }

  it should "retrieve the string of a training" in {
    val lieuDeFormation = FormationAnnotationsWriter.address(DatasudFormation.lieuFormation)
    val training = FormationAnnotationsWriter.training(lieuDeFormation, Some("100% English"), DatasudFormation.datasudFormation.formIntituleFormation, DatasudFormation.datasudFormation.sessDebut, DatasudFormation.datasudFormation.seesFin)
    training shouldEqual "L'AgoraChemin Champs de PruniersMANOSQUE41004112100% English2906200230062020"
  }

  it should "retrieve training annotation" in {
    val lieuDeFormation = FormationAnnotationsWriter.address(DatasudFormation.lieuFormation)
    val training = FormationAnnotationsWriter.training(lieuDeFormation, Some("100% English"), DatasudFormation.datasudFormation.formIntituleFormation,DatasudFormation.datasudFormation.sessDebut, DatasudFormation.datasudFormation.seesFin)
    val uriTraining: URI = DatasudFormationUriFactory.trainingURI(training)
    val liftyAnnotations = FormationAnnotationsWriter.trainingAnnotation(uriTraining, DatasudFormation.datasudFormation)
    liftyAnnotations.isDefined shouldEqual(true)
    liftyAnnotations.get.size shouldEqual(13)
  }

  it should "retrieve tagging Annotation" in {
    val lieuDeFormation = FormationAnnotationsWriter.address(DatasudFormation.lieuFormation)
    val training = FormationAnnotationsWriter.training(lieuDeFormation, Some("100% English"), DatasudFormation.datasudFormation.formIntituleFormation,DatasudFormation.datasudFormation.sessDebut, DatasudFormation.datasudFormation.seesFin)
    val uriTraining: URI = DatasudFormationUriFactory.trainingURI(training)
    val taggingAnnotations = FormationAnnotationsWriter.taggingAnnotation(Some(uriTraining), DatasudFormation.datasudFormation.formationTags)
    taggingAnnotations.isDefined shouldEqual(true)
    taggingAnnotations.get.size shouldEqual(9)
  }
}