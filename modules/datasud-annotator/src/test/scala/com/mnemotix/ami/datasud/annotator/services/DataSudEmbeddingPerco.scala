/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.datasud.annotator.services

import java.io.{BufferedWriter, File, FileWriter}
import java.net.URI
import java.nio.charset.StandardCharsets

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}
import au.com.bytecode.opencsv.CSVWriter
import com.github.jelmerk.knn.scalalike.hnsw.HnswIndex
import com.mnemotix.ami.datasud.annotator.helpers.{DatasudConfig, DatasudFormationUriFactory}
import com.mnemotix.ami.datasud.annotator.model.{DatasudFormationModels, Skill, SkillInfo}
import com.mnemotix.ami.datasud.annotator.services.recognizer.writer.FormationAnnotationsWriter.{address, training}
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.ami.percolator.services.PercolateEmbeddingPEOffer.{blocComp, competenceToString, conceptEmbedding, learnEmbedding, logger, memoireTampon}
import com.mnemotix.analytix.percolation.model.{PercoVector, PercolatorText}
import com.mnemotix.analytix.percolation.services.{ConceptEmbeddingSummarizer, ESClient, ESConceptExtractor}
import com.mnemotix.analytix.percolation.services.embedding.HsnwPercolationModel
import com.mnemotix.lifty.services.CSVExtractor
import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.index.IndexClient
import com.typesafe.scalalogging.LazyLogging
import collection.JavaConverters._

import scala.collection.mutable.ListBuffer
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor}
import scala.concurrent.duration.Duration

object DataSudEmbeddingPerco extends App with LazyLogging {
/*
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("datasud")

  implicit val esPercolatorClient = new ESClient()
  implicit val esConceptExtract = new ESConceptExtractor("dev-openemploi-skill")
  implicit val conceptEmbedding = new ConceptEmbeddingSummarizer("french")

  lazy val rdfExtracter = new RDFExtractor()
  lazy val extractor = new RDFExtracter()
  lazy val serializer = new DatasudFormationSerializer()

  rdfExtracter.init
  extractor.init

  val UTF8: String = StandardCharsets.UTF_8.name
  val store = new RocksDBStore(PercolatorConfig.competenceTrainingRecogs)
  store.init

  IndexClient.init()

  val groupSkills: Seq[Skill] = Await.result(rdfExtracter.skillGroup, Duration.Inf)
  val occupations = Await.result(rdfExtracter.metierRome, Duration.Inf)

  val skillsInfo: Seq[SkillInfo] = Await.result(rdfExtracter.skills, Duration.Inf)

  val mapSkillNotation = skillsInfo.map(info => (info.uri, info.notation)).toMap
  lazy val liftyCSVExtractor = new CSVExtractor(DatasudConfig.csvfilePath, None)
  lazy val topHeadSink = Sink.ignore

  logger.info(s"extracting data...")
  val startExtrartTime = System.currentTimeMillis()
  val extractedFut  = liftyCSVExtractor.extractAsync via serializer.serialize runWith(Sink.seq)
  val extracted = Await.result(extractedFut, Duration.Inf)
  println(extracted.size)
  val csvConverted = percolatorText(extracted)
  logger.info(s">> Time took to request ${csvConverted.size} offers description => {} s", (System.currentTimeMillis() - startExtrartTime)/ 100)


  logger.info(s"computing embeddings...")
  val startEmbTime = System.currentTimeMillis()
  val model = learnEmbedding
  logger.info(s">> Time took to request ${model.size} offers description => {} s", (System.currentTimeMillis() - startEmbTime)/ 100)
  logger.info("computing embeddings done")

  logger.info("percolation")
  val startPercoTime = System.currentTimeMillis()
  val percolateFut = perco(model, csvConverted.toArray)
  val percolate = Await.result(percolateFut, Duration.Inf).flatten
  logger.info(s">> Time took to compute percolation for ${csvConverted.size} text => {} s", (System.currentTimeMillis() - startPercoTime)/ 100)


  val fileWriter = new FileWriter(new File("modules/datasud-annotator/src/test/resources/data/output/formationDump_embedding.csv"), true)
  val outputFile = new BufferedWriter(fileWriter)
  val csvWriter = new CSVWriter(outputFile)
  val csvSchema = Array("id","competencesDestription", "cblockDescription")
  var listOfRecords = new ListBuffer[Array[String]]()
  listOfRecords += csvSchema
  percolate.map(per => listOfRecords.append(per))
  val allContent = listOfRecords
  csvWriter.writeAll(allContent.toList.asJava)
  csvWriter.close()
  fileWriter.close()
  outputFile.close()

  def percolatorText(datas: Seq[DatasudFormationModels]) = {
    val percolatorTexts = datas map { data =>
      val addressValue = address(data.lieuFormation)
      val trainingValue = training(addressValue, data.offormRaisonSocialeFormateur, data.formIntituleFormation, data.sessDebut, data.seesFin)
      val uriAmiTraining: Option[URI] = if (trainingValue.trim.isEmpty || trainingValue.trim == "") None else Some(DatasudFormationUriFactory.trainingURI(trainingValue))
      PercolatorText(uriAmiTraining.get.toString, Seq(data.formObjectifFormation.getOrElse(""), data.formContenuFormation.getOrElse("")))
    }

    percolatorTexts
  }

  def learnEmbedding: HnswIndex[String, Array[Float], PercoVector, Float] = {
    val startTime = System.currentTimeMillis()
    val hsnwPercolationModel = new HsnwPercolationModel()
    val modelFut = hsnwPercolationModel.fit
    val model: HnswIndex[String, Array[Float], PercoVector, Float] = Await.result(modelFut, Duration.Inf)
    logger.info(s">> Time took to compute ${model.size} embeddings => {} s", (System.currentTimeMillis() - startTime)/ 100)
    model
  }

  def perco(model: HnswIndex[String, Array[Float], PercoVector, Float], percolatorTxts: Array[PercolatorText]) = {
    val hsnwPercolationModel = new HsnwPercolationModel()

    logger.info(s"we will percolate ${percolatorTxts.length} documents on an index of ${model.size} of concept")

    val source = Source.fromIterator(() => (percolatorTxts).iterator).grouped(100).map { texts =>
      val knnStartTime = System.currentTimeMillis()
      val lines = texts.map { percolatorTxt =>
        val description = percolatorTxt.texts.mkString(" ")
        val descriptionEmb = conceptEmbedding.embeddingHelper(description)
        val percoVector = new PercoVector(percolatorTxt.docId, descriptionEmb.map(_.toFloat))
        val results = hsnwPercolationModel.predict(model, percoVector, 20)
        val compUris = results.map(_.item().id)
        val notations = competenceToString(compUris)
        val bloc = blocComp(compUris).mkString(";")
        Array(percolatorTxt.docId,notations,bloc)
      }
      logger.info(s">> Time took to compute similarity for ${texts.size} text => {} s", (System.currentTimeMillis() - knnStartTime)/ 100)
      lines
    }

    source.runWith(Sink.seq)
  }

  def competenceToString(competences: Seq[String]) = {
    /* val newUris = oldNewUris.filter { obj =>
      competences.contains(obj.old_uri)
    }.map(_.new_uri) */
    competencesInfo(competences).map(_.notation).mkString(";")
  }

  def competencesInfo(uris: Seq[String]) = {
    skillsInfo.filter { skill =>
      uris.contains(skill.uri)
    }.distinct
  }

  def blocComp(comps: Seq[String]): Seq[String] = {
    groupSkills.filter { groupSkill =>
      comps.contains(groupSkill.uri)
    }.map { groupSkill =>
      groupSkill.groupUri
    }.flatten.flatten.distinct
  }
*/
}
