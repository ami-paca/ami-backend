package com.mnemotix.ami.datasud.annotator.services.serializer

import com.mnemotix.ami.datasud.annotator.data.test.MapDataSud
import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class FormationTagsSerializerSpec extends FlatSpec with Matchers {
  it should "serialize formacode" in {
    val codes = FormationTagsSerializer.formacode(MapDataSud.myMap)
    codes.isDefined shouldEqual true
    codes.get.size shouldEqual(1)
  }

  it should "serialize rome" in {
    val codes = FormationTagsSerializer.rome(MapDataSud.myMap)
    codes.isDefined shouldEqual true
    codes.get.size shouldEqual(3)
  }

  it should "serialize nsf" in {
    val codes = FormationTagsSerializer.nsf(MapDataSud.myMap)
    codes.isDefined shouldEqual(true)
    codes.get.size shouldEqual(1)
  }
}