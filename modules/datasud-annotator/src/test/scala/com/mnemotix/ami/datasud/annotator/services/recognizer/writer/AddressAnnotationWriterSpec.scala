package com.mnemotix.ami.datasud.annotator.services.recognizer.writer

import java.net.URI

import com.mnemotix.ami.datasud.annotator.data.test.DatasudFormation
import com.mnemotix.ami.datasud.annotator.helpers.DatasudFormationUriFactory
import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class AddressAnnotationWriterSpec extends FlatSpec with Matchers {

  it should "return liftyAnnotation point" in {
    val pointURI = DatasudFormationUriFactory.pointURI(DatasudFormation.lieuFormation.lieuFormLatitude.get.toString, DatasudFormation.lieuFormation.lieuFormLongitude.get.toString)
    val literalAnnotation = AddressAnnotationWriter.pointLiteralsAnnotations(Some(pointURI), DatasudFormation.lieuFormation)
    literalAnnotation.isDefined shouldEqual(true)
    literalAnnotation.get.size shouldEqual(3)
  }

  it should "return liftyAnnotations for adress" in {
    val addressURI = new URI("http://openemploi.datasud.fr/data/address/35bf3d13710bb8d80b312c57509fe587")
    val addressAnntation = AddressAnnotationWriter.addressLiteralsAnnotations(Some(addressURI),DatasudFormation.lieuFormation, Some("Zone 301"))
    addressAnntation.isDefined shouldEqual(true)
    addressAnntation.get.size shouldEqual(7)
  }
}