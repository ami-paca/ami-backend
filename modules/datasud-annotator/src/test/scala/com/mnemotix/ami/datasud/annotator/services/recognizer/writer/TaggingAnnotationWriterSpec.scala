package com.mnemotix.ami.datasud.annotator.services.recognizer.writer

import java.net.URI

import com.mnemotix.ami.datasud.annotator.data.test.DatasudFormation
import com.mnemotix.ami.datasud.annotator.helpers.DatasudFormationUriFactory
import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class TaggingAnnotationWriterSpec extends FlatSpec with Matchers  {
  it should "retieve annotations for tagging of code Rome" in {
    val lieuDeFormation = FormationAnnotationsWriter.address(DatasudFormation.lieuFormation)
    val training = FormationAnnotationsWriter.training(lieuDeFormation, Some("100% English"), Some("Ba") ,DatasudFormation.datasudFormation.sessDebut, DatasudFormation.datasudFormation.seesFin)
    val uriTraining: URI = DatasudFormationUriFactory.trainingURI(training)

    val liftyAnnoations = TaggingAnnotationWriter.occupations(uriTraining, DatasudFormation.formationTags.formRome)
    liftyAnnoations.isDefined shouldEqual(true)
    liftyAnnoations.get.size shouldEqual(9)
  }
}
