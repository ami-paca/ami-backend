package com.mnemotix.ami.datasud.annotator.services

import com.mnemotix.ami.datasud.annotator.DatasudAnnotatorSpec

import scala.io.Source

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RomeTest extends DatasudAnnotatorSpec{

  it should "see if all the DATASUD ROME code are in ROME" in {
    val datasudeRome = Source.fromFile("/Users/prlherisson/Documents/mnemotix/projets/open-emploi/datasudROME.txt").getLines.toList.distinct
    println(s"the number of rome code in datasud carif is ${datasudeRome.size}")

    val rome = Source.fromFile("/Users/prlherisson/Documents/mnemotix/projets/open-emploi/rome.txt").getLines.toList.distinct
    println(s"the number of rome code in ${rome.size}")
    println("code in carif not in rome " + datasudeRome.filterNot(rome.contains(_)))
  }

  it should "see if all the ROME URI" in {
    val datasudeRome = Source.fromFile("/Users/prlherisson/Documents/mnemotix/projets/open-emploi/romeCarif.txt").getLines.toList.distinct
    println(s"the number of rome uri in datasud carif is ${datasudeRome.size}")

    val rome = Source.fromFile("/Users/prlherisson/Documents/mnemotix/projets/open-emploi/rome_uri.txt").getLines.toList.distinct
    println(s"the number of rome uri in ${rome.size}")
    println("uri in carif not in rome " + datasudeRome.filterNot(rome.contains(_)))
  }

}
