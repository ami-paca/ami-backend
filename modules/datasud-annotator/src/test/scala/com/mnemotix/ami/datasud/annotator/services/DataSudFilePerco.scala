/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.datasud.annotator.services

import java.io.{BufferedWriter, File, FileWriter}
import java.net.URI
import java.nio.charset.StandardCharsets
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.stream.scaladsl.Sink
import akka.util.ByteString
import au.com.bytecode.opencsv.CSVWriter
import com.mnemotix.ami.datasud.annotator.helpers.{DatasudConfig, DatasudFormationUriFactory}
import com.mnemotix.ami.datasud.annotator.model.{CompetenceNewOld, DatasudFormationModels, Skill, SkillInfo}
import com.mnemotix.ami.datasud.annotator.services.recognizer.writer.FormationAnnotationsWriter.{address, training}
import com.mnemotix.ami.percolator.helpers.PercolatorConfig
import com.mnemotix.ami.percolator.model.{CompetenceNewOld, RecogsCompetenceTraining}
import com.mnemotix.analytix.percolation.model.PercolatorText
import com.mnemotix.lifty.services.{CSVExtractor, RDFFileOutput}
import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.index.IndexClient
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.SearchResponse
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

import collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.concurrent.duration.Duration

object DataSudFilePerco extends App with LazyLogging {
  /*
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("datasud")

  lazy val rdfExtracter = new RDFExtractor()
  lazy val extractor = new RDFExtracter()

  rdfExtracter.init
  extractor.init

  val UTF8: String = StandardCharsets.UTF_8.name
  val store = new RocksDBStore(PercolatorConfig.competenceTrainingRecogs)
  store.init

  IndexClient.init()

  val groupSkills: Seq[Skill] = Await.result(rdfExtracter.skillGroup, Duration.Inf)
  val occupations = Await.result(rdfExtracter.metierRome, Duration.Inf)
  val wkt = Await.result(rdfExtracter.wkt, Duration.Inf)
  val skillsInfo: Seq[SkillInfo] = Await.result(rdfExtracter.skills, Duration.Inf)

  val mapSkillNotation = skillsInfo.map(info => (info.uri, info.notation)).toMap
  val zoneGeoWkts = extractor.zoneGeoWkt

  lazy val liftyFileOutput = new RDFFileOutput()
  lazy val annotationRecognizer = new DatasudFormationAnnotationRecognizer()
  lazy val serializer = new DatasudFormationSerializer()

  lazy val liftyCSVExtractor = new CSVExtractor(DatasudConfig.csvfilePath, None)

  lazy val topHeadSink = Sink.ignore

  var i = 0
  val fileWriter = new FileWriter(new File("modules/datasud-annotator/src/test/resources/data/output/formationDump_competence.csv"), true)
  val outputFile = new BufferedWriter(fileWriter)
  val csvWriter = new CSVWriter(outputFile)
  val csvSchema = Array("id","competencesDestription", "cblockDescription")
  var listOfRecords = new ListBuffer[Array[String]]()
  listOfRecords += csvSchema

  val extractedFut  = liftyCSVExtractor.extractAsync via serializer.serialize runWith(Sink.seq)
  val extracted = Await.result(extractedFut, Duration.Inf)
  println(extracted.size)

  val csvConverted = percolatorText(extracted)
  val content = trainingPercolateIterate(Some(0), Some(4000), 100, csvConverted.size, csvConverted, "dev-openemploi-skill")
  val allContent =  listOfRecords ++ content
  csvWriter.writeAll(allContent.toList.asJava)
  csvWriter.close()
  fileWriter.close()
  outputFile.close()

  def percolatorText(datas: Seq[DatasudFormationModels]) = {
    val percolatorTexts = datas map { data =>
      val addressValue = address(data.lieuFormation)
      val trainingValue = training(addressValue, data.offormRaisonSocialeFormateur, data.formIntituleFormation, data.sessDebut, data.seesFin)
      val uriAmiTraining: Option[URI] = if (trainingValue.trim.isEmpty || trainingValue.trim == "") None else Some(DatasudFormationUriFactory.trainingURI(trainingValue))
      PercolatorText(uriAmiTraining.get.toString, Seq(data.formObjectifFormation.getOrElse(""), data.formContenuFormation.getOrElse("")))
    }

    percolatorTexts

    /*
    val resultat = percolator.percolate("dev-openemploi-training", Seq("description"), Some("entityId"), "dev-openemploi-skill", 100, Some(0), Some(4000),
    "modules/offer-percolator/src/test/resources/csv/training.csv")
    */
  }

  def trainingPercolateIterate(from: Option[Int], size: Option[Int], by: Int, max: Int, trainings: Seq[PercolatorText],
                               percoIdxName: String): Seq[Array[String]] = {
    def trainingIterateHelper(current: Int, lignes :Seq[Array[String]]): Seq[Array[String]] = {
      if (current < max) {
        val m = if (current + by > max) max else current + by
        val selectedTrainings = trainings.slice(current, m)
        val aMap = selectedTrainings.map(offer =>  Map("percoLabel" -> textCleaner(offer.texts.mkString(" ").trim)))

        val multiRes: Future[Response[SearchResponse]] = IndexClient.multiPercolate(Seq(percoIdxName), aMap, from, size)
        val oldMap = esResultatSelection(multiRes, selectedTrainings)
        val newMap  = esIndexable(oldMap)
        val newLines = newMap.map { ligne =>
          val comp = {
            if (ligne._2.size > 10) {
              ligne._2.slice(0,10)
            }
            else ligne._2
          }
          val competenceString = competenceToString(comp)
          val blockCompetence = blocComp(comp)
          Array(ligne._1, competenceString, blockCompetence.mkString(";"))
        }

        val memory = memoireTampon(newLines)
        Await.result(Future.sequence(memory), Duration.Inf)

        logger.info(s"processed :  ${m} / ${max} for percolation")
        trainingIterateHelper(current + by, lignes ++ newLines)
      }
      else lignes
    }
    trainingIterateHelper(0, Seq())
  }

  def esResultatSelection(resultat: Future[Response[SearchResponse]], allTexts: Seq[PercolatorText])  = {
    val start = System.currentTimeMillis()
    val response = Await.result(resultat, Duration.Inf)
    val elapsedTimeMillis = System.currentTimeMillis()-start
    logger.info(s"perco take = ${TimeUnit.MILLISECONDS.toSeconds(elapsedTimeMillis)} s")
    logger.info(s"success = ${response.isSuccess}")
    response.result.hits.hits.map { hit =>
      val json = Json.parse(hit.sourceAsString)
      val entityId = (json \ "entityId").as[String]
      val docs = hit.fields("_percolator_document_slot").asInstanceOf[Seq[Int]]
      (entityId, docs.map(num => allTexts(num).docId))
    }.toMap
  }

  def esIndexable(oldMap: Map[String, Seq[String]]) = {
    oldMap.foldLeft(Map[String, Seq[String]]().withDefaultValue(Seq())) {
      case (m, (a, bs)) => bs.foldLeft(m)((map, b) => map.updated(b, m(b) :+ a))
    }
  }

  def competenceToString(competences: Seq[String]) = {
    /* val newUris = oldNewUris.filter { obj =>
      competences.contains(obj.old_uri)
    }.map(_.new_uri) */
    competencesInfo(competences).map(_.notation).mkString(";")
  }

  def competencesInfo(uris: Seq[String]) = {
    skillsInfo.filter { skill =>
      uris.contains(skill.uri)
    }.distinct
  }

  def blocComp(comps: Seq[String]): Seq[String] = {
    groupSkills.filter { groupSkill =>
      comps.contains(groupSkill.uri)
    }.map { groupSkill =>
      groupSkill.groupUri
    }.flatten.flatten.distinct
  }

  def textCleaner(text:String) = {
    val cleaned = text.replaceAll("[\\PL]", " ").trim.replaceAll(" +", " ")
    cleaned
  }

  def memoireTampon(newLines: Iterable[Array[String]]) = {
    newLines.map { line =>
      val uri = line(0).toString.getBytes(UTF8)
      val json = Json.toJson(RecogsCompetenceTraining(line(0), line(1), line(2)))
      val value = ByteString(Json.prettyPrint(json).getBytes(UTF8))
      store.put(uri, value)
    }
  }

   */
}