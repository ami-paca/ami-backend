package com.mnemotix.ami.datasud.annotator.helpers

import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DatasudFormationUriFactorySpec extends FlatSpec with Matchers  {

  behavior of "DatasudFormationUriFactory"

  it should "return the uri of the ontology" in {
    DatasudFormationUriFactory.uriOntology.toString shouldEqual "http://openemploi.datasud.fr/data/"
  }

  it should "return the uri of an datasud formation organization" in {
    DatasudFormationUriFactory.formationOrganizationURI("flamban").toString() shouldEqual "http://openemploi.datasud.fr/data/organization/6196193738311c589d8b44276c45d1ce"
  }

  it should "return the uri of an datasud address" in {
    DatasudFormationUriFactory.addressURI("flamban").toString shouldEqual "http://openemploi.datasud.fr/data/address/6196193738311c589d8b44276c45d1ce"
  }

  it should "return the uri of a point" in {
    DatasudFormationUriFactory.pointURI("flam", "ban").toString shouldEqual "http://openemploi.datasud.fr/data/point/6196193738311c589d8b44276c45d1ce"
  }

  it should "return the uri of a coderome" in {
    DatasudFormationUriFactory.skosRomeURI("flamban").toString shouldEqual "http://openemploi.datasud.fr/data/occupation/6196193738311c589d8b44276c45d1ce"
  }

  it should "return the uri of a training" in {
    DatasudFormationUriFactory.trainingURI("flamban").toString shouldEqual "http://openemploi.datasud.fr/data/training/6196193738311c589d8b44276c45d1ce"
  }

  it should "return the uri of a tagging" in {
    DatasudFormationUriFactory.taggingURI("flamban").toString shouldEqual "http://openemploi.datasud.fr/data/tagging/flamban"
  }
}