/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.datasud.annotator.services

import java.io.{BufferedWriter, File, FileWriter}
import java.net.URI

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink, Source}
import au.com.bytecode.opencsv.CSVWriter
import com.mnemotix.ami.datasud.annotator.helpers.{DatasudConfig, DatasudFormationUriFactory, GeoPoint, PolygonUtils, RomeConfig}
import com.mnemotix.ami.datasud.annotator.model.{DatasudFormationModels, LieuFormation, Skill, SkillInfo}
import com.mnemotix.ami.datasud.annotator.services.recognizer.writer.FormationAnnotationsWriter.{address, training}
import com.mnemotix.lifty.services.{CSVExtractor, RDFFileOutput}
import com.mnemotix.synaptix.index.IndexClient
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

import collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}

// todo add uris

object DataSudCsv extends App with LazyLogging {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("datasud")

  lazy val rdfExtracter = new RDFExtractor()
  lazy val extractor = new RDFExtracter()

  rdfExtracter.init
  extractor.init

  IndexClient.init()

  val groupSkills: Seq[Skill] = Await.result(rdfExtracter.skillGroup, Duration.Inf)
  val occupations = Await.result(rdfExtracter.metierRome, Duration.Inf)
  val wkt = Await.result(rdfExtracter.wkt, Duration.Inf)
  val skillsInfo: Seq[SkillInfo] = Await.result(rdfExtracter.skills, Duration.Inf)

  val mapSkillNotation = skillsInfo.map(info => (info.uri, info.notation)).toMap
  val zoneGeoWkts = extractor.zoneGeoWkt

  lazy val liftyFileOutput = new RDFFileOutput()
  lazy val annotationRecognizer = new DatasudFormationAnnotationRecognizer()
  lazy val serializer = new DatasudFormationSerializer()

  lazy val liftyCSVExtractor = new CSVExtractor(DatasudConfig.csvfilePath, None)

  lazy val topHeadSink = Sink.ignore

  var i = 0
  val fileWriter = new FileWriter(new File("modules/datasud-annotator/src/test/resources/data/output/formationDump_3.csv"), true)
  val outputFile = new BufferedWriter(fileWriter)
  val csvWriter = new CSVWriter(outputFile)
  val csvSchema = Array("id","OF_Responsable","form_formacode","form_nsf","form_rome","form_intitule_formation", "form_objectif_formation","form_contenu_formation","form_Resultat_Attendu",
"form_Certifiante","form_niveau_entree","form_objectif_general_formation","form_code_RNCP","form_code_certifinfo","act_Public_vise","act_niveau_entree_obligatoire","act_nombre_heures_centre",
    "act_nombre_heures_entreprise","act_type_formation","ofform_raison_sociale_formateur","sess_debut","sess_fin","act_nombre_heure_totale","act_financeur", "zoneEmploi",
    "code-diplome","nb_ci_diff","Complément fiche RNCP","nom-niveau")
  var listOfRecords = new ListBuffer[Array[String]]()
  listOfRecords += csvSchema

  val extractedFut  = liftyCSVExtractor.extractAsync via serializer.serialize runWith(Sink.seq)
  val extracted = Await.result(extractedFut, Duration.Inf)
  println(extracted.size)
  val csvConverted = toCsv(extracted)
  println(csvConverted.size)

  csvWriter.writeAll((listOfRecords ++ csvConverted).toList.asJava)
  csvWriter.close()
  fileWriter.close()
  outputFile.close()
  System.exit(0)

  /*
    def annotate: Future[Done] = {
      RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
        val f: Flow[Map[String, String], DatasudFormationModels, NotUsed] = serializer.serialize

        (topHS) =>
          import GraphDSL.Implicits._
          liftyCSVExtractor.extractAsync ~> serializer.serialize ~> toCsv
          ClosedShape
      }).run()
    }

   */

  //def annotate = liftyCSVExtractor.extractAsync via serializer.serialize via toCsv runWith(Sink.seq)

// started from 38011
  def toCsv(datas: Seq[DatasudFormationModels]) = {

    datas.map { data =>

        i = i + 1
        val addressValue = address(data.lieuFormation)
        val trainingValue = training(addressValue, data.offormRaisonSocialeFormateur, data.formIntituleFormation, data.sessDebut, data.seesFin)
        val uriAmiTraining: Option[URI] = if (trainingValue.trim.isEmpty || trainingValue.trim == "") None else Some(DatasudFormationUriFactory.trainingURI(trainingValue))

        /*
        val competencesDescription: Option[Array[SkillInfo]] = competenceDescription(data.formObjectifFormation)
        val competencesDescriptionString = {
          if (competencesDescription.isDefined) {
            competenceToString(competencesDescription.get.map(_.uri)).mkString(";")
          }
          else " "
        }

         */


        val zEString = zoneEmploi(data.lieuFormation)

        val zoneEmploiString: String = {
          if (zEString.isDefined) {
            wktLibelle(zEString.get).title.trim
          }
          else " "
        }
        val formResp = data.formationResponsable.toSeq.mkString(" ")


        logger.info(s"${i} / ${datas.size}")
        val toWrite = Array(uriAmiTraining.get.toString, formResp, data.formationTags.formFormacode.toSeq.flatten.mkString(";"),data.formationTags.formNsf.toSeq.flatten.mkString(";"),
          data.formationTags.formRome.toSeq.flatten.mkString(";"), data.formIntituleFormation.getOrElse(" "), data.formObjectifFormation.getOrElse(" "), data.formContenuFormation.getOrElse(" "),
          data.formResultatAttendu.getOrElse(" "), data.formCertifiante.getOrElse(" ").toString, data.formNiveauEntree.getOrElse(" "), data.formObjectifGeneralFormation.getOrElse(" "), data.formationTags.formCodeRNCP.getOrElse(" "),
          data.formationTags.formCodeCertinfo.getOrElse(" "), data.actPublicVise.getOrElse(" "), data.actNiveauEntreeObligatoire.getOrElse(" ").toString, data.actNombreHeuresCentre.getOrElse(" ").toString,
          data.actNombreHeuresEntreprise.getOrElse(" ").toString, data.actTypeFormation.getOrElse(" ").toString, data.offormRaisonSocialeFormateur.getOrElse(" "),  data.sessDebut.getOrElse(" "),
          data.seesFin.getOrElse(" "), data.actNombreHeureTotale.getOrElse(" ").toString, data.actFinanceur.getOrElse(" ")
           ,zoneEmploiString, data.codeDiplome.getOrElse(" "), data.nbCiDiff.getOrElse(" "), data.complementFicheRNCP.getOrElse(" "), data.nomNiveau.getOrElse(" "))
        toWrite
    }
  }

  def zoneEmploi(lieuFormation: LieuFormation) = {
    if ( lieuFormation.lieuFormLatitude.isDefined && lieuFormation.lieuFormLongitude.isDefined) {

      val point = GeoPoint(lieuFormation.lieuFormLongitude.get.toDouble,lieuFormation.lieuFormLatitude.get.toDouble)
      val uris: Seq[String] = zoneGeoWkts.filter(couple => PolygonUtils.isPointInPolygon(couple._2, point)).map(couple => couple._1)
      if (uris.size > 0) {
        Some(uris.last)
      }
      else None
    }
    else None
  }

  def wktLibelle(uri: String) = {
    wkt.filter(_.uri == uri).last
  }

  def competenceDescription(desc: Option[String]) = {
    if (desc.isDefined) {
      val competenceDescription = IndexClient.percolate(Seq("dev-openemploi-skill"), Map("percoLabel" -> desc.get), Some(0), Some(5))
      val labels = Await.result(competenceDescription, Duration.Inf).result.hits.hits.map { hit =>
        val json = Json.parse(hit.sourceAsString)
        SkillInfo((json \ "entityId").get.as[String],(json \ "prefLabel").get.toString()," ")
      }
      Some(labels)
    }
    else None
  }

  def competenceToString(uris: Seq[String]) = {
    uris.map { uri =>
      mapSkillNotation.get(uri)
    }.toSeq.flatten
  }
}