package com.mnemotix.ami.datasud.annotator.helpers

import com.typesafe.config.{Config, ConfigFactory}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object DatasudConfig {

  lazy val conf = Option(ConfigFactory.load().getConfig("datasud")).getOrElse(ConfigFactory.empty())
  lazy val ontologyMnx =  conf.getString("annotator.mnx.ontology.uri")
  lazy val csvfilePath = conf.getString("annotator.file.csv.path")

  lazy val rdf: Config = Option(ConfigFactory.load().getConfig("rdf")).getOrElse(ConfigFactory.empty())
  lazy val rdfuser = rdf.getString("user")
  lazy val rdfpassword = rdf.getString("password")
  lazy val rdfroot = rdf.getString("root.uri")
  lazy val rdfStoreRepoName = rdf.getString("repository.name")
}
