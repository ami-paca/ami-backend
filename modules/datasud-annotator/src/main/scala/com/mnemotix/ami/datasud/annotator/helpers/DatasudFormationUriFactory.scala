package com.mnemotix.ami.datasud.annotator.helpers

import java.net.URI

import com.mnemotix.lifty.helpers.{LiftyConfig, UriFactory}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object DatasudFormationUriFactory {
  val uriOntology = LiftyConfig.dataURI

  def formationOrganizationURI(formationResponsable: String): URI = {
    new URI(s"${uriOntology.toString}organization/${DatasudFormationResourceIDFactory.formationResponsableID(formationResponsable)}")
  }

  def addressURI(lieuformation: String): URI = {
    new URI(s"${uriOntology.toString}address/${DatasudFormationResourceIDFactory.addressID(lieuformation)}")
  }

  def pointURI(lat: String, lon: String): URI = {
    new URI(s"${uriOntology.toString}point/${DatasudFormationResourceIDFactory.pointID(lat, lon)}")
  }

  def skosRomeURI(skos: String): URI = {
    new URI(s"${LiftyConfig.dataURI}occupation/${DatasudFormationResourceIDFactory.formaCodeID(skos.trim)}")
  }

  def trainingURI(training: String): URI = {
    new URI(s"${uriOntology.toString}training/${DatasudFormationResourceIDFactory.trainingID(training)}")
  }

  def taggingURI(trainingID: String): URI = {
    new URI(s"${uriOntology.toString}tagging/${trainingID}")
  }
}