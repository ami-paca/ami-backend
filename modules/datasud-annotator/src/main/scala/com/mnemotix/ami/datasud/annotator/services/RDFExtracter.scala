/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.datasud.annotator.services

import com.mnemotix.ami.datasud.annotator.helpers.{DatasudConfig, GeoPoint}
import com.mnemotix.ami.datasud.annotator.model.ZoneGeoWkt
import com.mnemotix.synaptix.rdf.client.RDFClient
import org.locationtech.jts.io.WKTReader

import scala.collection.mutable.ListBuffer
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

class RDFExtracter(implicit ec: ExecutionContext) {

  def init = {
    RDFClient.init()
  }

  implicit lazy val conn = RDFClient.getReadConnection((DatasudConfig.rdfStoreRepoName))

  val spqrlr = """PREFIX ope: <http://openemploi.datasud.fr/ontology/>
                 |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                 |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
                 |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                 |
                 |SELECT ?uri ?wkt WHERE {
                 |    ?uri rdf:type <http://ns.mnemotix.com/ontologies/2019/8/generic-model/Polygon> .
                 |    ?uri mnx:asWKT ?wkt
                 |}""".stripMargin

  def wkt: Future[Seq[ZoneGeoWkt]] = {
    var applicationKindTypeVocabularies = new ListBuffer[ZoneGeoWkt]()
    val futureApplicationKindIndividualsAsync = RDFClient.select(spqrlr).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        applicationKindTypeVocabularies += ZoneGeoWkt(bs.getValue("uri").stringValue(), bs.getValue("wkt").stringValue())
      }
      applicationKindTypeVocabularies.toSeq
    }

    futureApplicationKindIndividualsAsync onComplete {
      case Success(i) => println(s"The application-kind request succeed. The number of applicationKindType : ${i.size}")
      case Failure(t) => println("There is a failure here", t)
    }
    futureApplicationKindIndividualsAsync
  }

  def zoneGeoWkt = {
    val wktReader = new WKTReader()
    val zoneGeoWkts = Await.result(wkt, Duration.Inf)
    zoneGeoWkts.map { zone =>
      val wktVal = wktReader.read(zone.wkt)
      val departement: List[GeoPoint] = wktVal.getCoordinates.map(c => GeoPoint(c.y, c.x)).toList
      (zone.uri, departement)
    }
  }
}
