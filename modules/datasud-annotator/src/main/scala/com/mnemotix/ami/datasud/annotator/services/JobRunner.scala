package com.mnemotix.ami.datasud.annotator.services

import java.nio.file.{Path, Paths}

import akka.stream.IOResult
import akka.stream.scaladsl.{FileIO, Source}
import akka.util.ByteString
import com.mnemotix.ami.datasud.annotator.helpers.DatasudConfig
import com.mnemotix.ami.datasud.annotator.services.job.DatasudAnnotatorJob
import com.mnemotix.lifty.models.LiftyProcessStatus
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.Future

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object JobRunner extends App with LazyLogging {

  lazy val filePath: String = DatasudConfig.csvfilePath
  lazy val datasudAnnotatorJob = new DatasudAnnotatorJob
  lazy val initiator = new Initiator

  def annotator(): Boolean = {
    if (initiator.init) {
      datasudAnnotatorJob.run()
      initiator.close
      true
    }
    else {
      logger.info(s"the ${filePath} is not available")
      Thread.sleep(5000)
      annotator
    }
  }
}