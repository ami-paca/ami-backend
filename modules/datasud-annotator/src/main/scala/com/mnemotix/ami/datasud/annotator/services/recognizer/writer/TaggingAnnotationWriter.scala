package com.mnemotix.ami.datasud.annotator.services.recognizer.writer

import java.net.URI

import com.mnemotix.ami.datasud.annotator.helpers.{DatasudConfig, DatasudFormationUriFactory, RomeConfig}
import com.mnemotix.ami.datasud.annotator.model.FormationTags
import com.mnemotix.lifty.helpers.AnnotationsUtils.createIRI
import com.mnemotix.lifty.models.LiftyAnnotation
import com.mnemotix.synaptix.core.utils.CryptoUtils

import scala.reflect.io.Path

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object TaggingAnnotationWriter {

  def annotations(uriTraining: URI, formationTags: FormationTags): Seq[LiftyAnnotation] = {
    occupations(uriTraining, formationTags.formRome).toSeq.flatten
  }

  def occupations(uriTraining: URI, codeRomes: Option[Seq[String]]): Option[Seq[LiftyAnnotation]] = {

    if (codeRomes.isDefined) {
      val occupations = codeRomes.get.map { codeRome =>
        val codeRomeURI = DatasudFormationUriFactory.skosRomeURI(codeRome.trim)

          Path("romeCarif.txt").createFile().appendAll(s"${codeRomeURI.toString}\n")

        Seq(
         LiftyAnnotation(uriTraining, s"${RomeConfig.mnOntoUri}hasOccupation", createIRI(codeRomeURI.toString), 1.0),
         LiftyAnnotation(codeRomeURI,"a", createIRI(s"${RomeConfig.mnOntoUri}Occupation"), 1.0)
        )
      }.flatten
      Some(occupations)
    }
    else None
  }
}

