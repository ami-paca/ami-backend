package com.mnemotix.ami.datasud.annotator.services.serializer

import com.mnemotix.ami.datasud.annotator.model.FormationTags
import com.mnemotix.ami.datasud.annotator.services.utils.SerializerUtils

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object FormationTagsSerializer {
  def serializer(aMap : Map[String, String]): FormationTags = {
    FormationTags(
      formacode(aMap),
      nsf(aMap),
      rome(aMap),
      SerializerUtils.mapValue(aMap, "form_code_RNCP"),
      SerializerUtils.mapValue(aMap, "form_code_certifinfo")
    )
  }

  def formacode(aMap: Map[String, String]): Option[Seq[String]] = {
    val formacode = SerializerUtils.mapValue(aMap, "form_formacode")
    if (formacode.isDefined) {
      Some(formacode.get.split(";").toSeq)
    }
    else None
  }

  def rome(aMap: Map[String, String]): Option[Seq[String]] = {
    val rome = SerializerUtils.mapValue(aMap, "form_rome")
    if (rome.isDefined) {
      Some(rome.get.split(";").toSeq)
    }
    else None
  }

  def nsf(aMap: Map[String, String]): Option[Seq[String]] = {
    val nsf = SerializerUtils.mapValue(aMap, "form_nsf")
    if (nsf.isDefined) {
      Some(nsf.get.split(";").toSeq)
    }
    else None
  }
}