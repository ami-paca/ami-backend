package com.mnemotix.ami.datasud.annotator.services.serializer

import com.mnemotix.ami.datasud.annotator.model.LieuFormation
import com.mnemotix.ami.datasud.annotator.services.utils.SerializerUtils

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object LieuFormationSerializer {
  def serializer(aMap: Map[String, String]): LieuFormation = {
    LieuFormation(
      SerializerUtils.mapValue(aMap, "lieuform_adresse_ligne1"),
      SerializerUtils.mapValue(aMap, "lieuform_adresse_ligne2"),
      postalInsee(aMap, "lieuform_adresse_codepostal"),
      SerializerUtils.mapValue(aMap, "lieuform_adresse_ville"),
      SerializerUtils.mapValue(aMap, "lieuform_adresse_Codedepartement"),
      SerializerUtils.mapValue(aMap, "lieuform_adresse_Libelledepartement"),
      SerializerUtils.mapValue(aMap, "lieuform_adresse_codeInseeCommune"),
      longlat(aMap, "lieuform_latitude"),
      longlat(aMap, "lieuform_longitude")
    )
  }

  def longlat(aMap: Map[String, String], key: String): Option[Float] = {
    val latlong = SerializerUtils.mapValue(aMap, key)
    if (latlong.isDefined) {
      Some(latlong.get.toFloat)
    }
    else None
  }

  def postalInsee(aMap: Map[String, String], key: String): Option[Int] = {
    val postalInsee = SerializerUtils.mapValue(aMap, key)
    if (postalInsee.isDefined) {
      Some(postalInsee.get.toInt)
    }
    else None
  }
}