package com.mnemotix.ami.datasud.annotator.services.serializer

import java.util.Date

import com.mnemotix.ami.datasud.annotator.model.DatasudFormationModels
import com.mnemotix.ami.datasud.annotator.services.utils.SerializerUtils

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object TrainingSerializer {
  def serializer(aMap : Map[String, String]): DatasudFormationModels = {
    DatasudFormationModels(
      SerializerUtils.mapValue(aMap, "OF_Responsable"),
      FormationTagsSerializer.serializer(aMap),
      SerializerUtils.mapValue(aMap, "form_intitule_formation"),
      SerializerUtils.mapValue(aMap, "form_objectif_formation"),
      SerializerUtils.mapValue(aMap, "form_contenu_formation"),
      SerializerUtils.mapValue(aMap, "form_Resultat_Attendu"),
      None,
      None,
      SerializerUtils.mapValue(aMap, "form_niveau_entree"),
      SerializerUtils.mapValue(aMap, "form_objectif_general_formation"),
      SerializerUtils.mapValue(aMap, "form_niveau_sortie"),
      SerializerUtils.mapValue(aMap, "act_Public_vise"),
      LieuFormationSerializer.serializer(aMap),
      optionInt(aMap, "act_niveau_entree_obligatoire"),
      optionInt(aMap, "act_nombre_heures_entreprise"),
      SerializerUtils.mapValue(aMap, "act_type_formation"),
      SerializerUtils.mapValue(aMap, "ofform_raison_sociale_formateur"),
      SerializerUtils.mapValue(aMap, "url_ation"),
      None,
      SerializerUtils.mapValue(aMap, "act_restauration"),
      SerializerUtils.mapValue(aMap, "act_transport"),
      SerializerUtils.mapValue(aMap, "sess_debut"),
      SerializerUtils.mapValue(aMap, "sess_fin"),
      SerializerUtils.mapValue(aMap, "act_modalite_recrutement"),
      optionInt(aMap, "act_nombre_heure_totale"),
      optionInt(aMap, "act_nombre_heures_centre"),
      SerializerUtils.mapValue(aMap, "actFinanceur"),
      SerializerUtils.mapValue(aMap, "code-diplome"),
      SerializerUtils.mapValue(aMap, "nb_ci_diff"),
      SerializerUtils.mapValue(aMap, "Complément fiche RNCP"),
      SerializerUtils.mapValue(aMap, "nom-niveau")
    )
  }

  def optionInt(aMap: Map[String, String], key: String): Option[Int] = {
    val toInt = SerializerUtils.mapValue(aMap, key)
    if (toInt.isDefined) {
      Some(toInt.get.toInt)
    }
    else None
  }

  def optionDate(aMap: Map[String, String], key: String): Option[Date] = {
    val date = SerializerUtils.mapValue(aMap, key)
    if (date.isDefined) {
      val dateString = date.get
      val format = new java.text.SimpleDateFormat("dd/MM/yyyy")
      Some(format.parse(dateString))
    }
    else None
  }
}