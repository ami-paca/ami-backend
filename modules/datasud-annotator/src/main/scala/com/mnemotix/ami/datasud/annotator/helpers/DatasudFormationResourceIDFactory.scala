package com.mnemotix.ami.datasud.annotator.helpers

import com.mnemotix.lifty.helpers.CryptoUtils

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object DatasudFormationResourceIDFactory {

  def formationResponsableID(formationResponsable: String): String = {
    CryptoUtils.md5sum(formationResponsable)
  }

  def addressID(lieuformation: String): String = {
    CryptoUtils.md5sum(lieuformation)
  }

  def pointID(lat: String, lon: String): String = {
    CryptoUtils.md5sum(lat, lon)
  }

  def occupationID(code: String): String = {
    CryptoUtils.md5sum(code)
  }

  def certinfoID(certInfo: String): String = {
    CryptoUtils.md5sum(certInfo)
  }

  def rncpID(rncp: String): String = {
    CryptoUtils.md5sum(rncp)
  }

  def nsfID(nsf: String): String = {
    CryptoUtils.md5sum(nsf)
  }

  def formaCodeID(formaCode: String): String = {
    CryptoUtils.md5sum(formaCode)
  }

  def trainingID(training: String): String = {
    CryptoUtils.md5sum(training)
  }
}