package com.mnemotix.ami.datasud.annotator.services.recognizer.writer

import java.net.URI

import com.mnemotix.ami.datasud.annotator.helpers.{DatasudConfig, GeoPoint, PolygonUtils, RomeConfig}
import com.mnemotix.ami.datasud.annotator.model.LieuFormation
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object AddressAnnotationWriter {

  def annotations(organisationName: Option[String], lieuFormation: LieuFormation, uriOrganization: Option[URI], addressURI: Option[URI], pointURI: Option[URI], amiTrainingURI: Option[URI]): Seq[LiftyAnnotation] = {
    val trainingAnnotation = if (amiTrainingURI.isDefined && uriOrganization.isDefined) Some(LiftyAnnotation(amiTrainingURI.get, s"${RomeConfig.amiOntoUri}isProvidedBy", createIRI(s"${uriOrganization.get}"), 1.0)) else None
    val addressAnnotation = if (addressURI.isDefined && uriOrganization.isDefined) Some(LiftyAnnotation(uriOrganization.get, s"${RomeConfig.mnxOntoUri}hasAddress", createIRI(s"${addressURI.get}"), 1.0)) else None
    val pointAnnotation = if (addressURI.isDefined && pointURI.isDefined) Some(LiftyAnnotation(addressURI.get, s"${RomeConfig.mnxOntoUri}hasGeometry", createIRI(s"${pointURI.get}"), 1.0)) else None

    val organizationAnnotation: Option[Seq[LiftyAnnotation]] = if (uriOrganization.isDefined) {
     Some(Seq(LiftyAnnotation(uriOrganization.get, "a", createIRI(s"${DatasudConfig.ontologyMnx}Organization"), 1.0)) ++
        addressLiterals(uriOrganization.get, organisationName, s"http://xmlns.com/foaf/0.1/", "name", "http://www.w3.org/2001/XMLSchema#string").toSeq)
    }
    else None

    trainingAnnotation.toSeq ++ organizationAnnotation.toSeq.flatten ++ addressAnnotation.toSeq ++ pointAnnotation.toSeq ++ addressLiteralsAnnotations(addressURI, lieuFormation, organisationName).toSeq.flatten ++ pointLiteralsAnnotations(pointURI, lieuFormation).toSeq.flatten
  }

  def addressLiteralsAnnotations(addressURI: Option[URI], lieuFormation: LieuFormation, organisationName: Option[String]): Option[Seq[LiftyAnnotation]] = {
    if (addressURI.isDefined) {
      Some((
          addressLiterals(addressURI.get, lieuFormation.lieuFormAdresseCodePostal, s"${DatasudConfig.ontologyMnx}", "postalCode", "http://www.w3.org/2001/XMLSchema#int") ++
          addressLiterals(addressURI.get, lieuFormation.lieuFormAdresseVille, s"${DatasudConfig.ontologyMnx}", "city", "http://www.w3.org/2001/XMLSchema#string") ++
          addressLiterals(addressURI.get, lieuFormation.lieuFormAdresseLigne2, s"${DatasudConfig.ontologyMnx}", "street2", "http://www.w3.org/2001/XMLSchema#string") ++
          addressLiterals(addressURI.get, lieuFormation.lieuFormAdresseLigne1, s"${DatasudConfig.ontologyMnx}", "street1", "http://www.w3.org/2001/XMLSchema#string")
      ).toSeq ++
      Seq(LiftyAnnotation(addressURI.get, "a", createIRI(s"${DatasudConfig.ontologyMnx}Address"), 1.0))
      )
    }
    else None
  }

  // addressLiterals(addressURI.get, lieuFormation.lieuFormAdresseCodeInseeCommune, "ami", "codeeInseeCommune", "http://www.w3.org/2001/XMLSchema#string")

  def pointLiteralsAnnotations(pointURI: Option[URI], lieuFormation: LieuFormation): Option[Seq[LiftyAnnotation]] = {
    if (pointURI.isDefined) {
      Some(
        (pointLiterals(pointURI.get, lieuFormation.lieuFormLatitude, s"${DatasudConfig.ontologyMnx}", "lat", "http://www.w3.org/2001/XMLSchema#string") ++
          pointLiterals(pointURI.get, lieuFormation.lieuFormLongitude, s"${DatasudConfig.ontologyMnx}", "long", "http://www.w3.org/2001/XMLSchema#string")).toSeq
        ++ Seq(LiftyAnnotation(pointURI.get, "a", createIRI(s"${DatasudConfig.ontologyMnx}Point"), 1.0))
      )
    }
    else None
  }

  def zoneEmploi(addressURI: Option[URI], lieuFormation: LieuFormation, zoneGeoWkts: Seq[(String, List[GeoPoint])]): Option[Seq[LiftyAnnotation]] = {
    if (addressURI.isDefined && lieuFormation.lieuFormLatitude.isDefined && lieuFormation.lieuFormLongitude.isDefined) {

      val point = GeoPoint(lieuFormation.lieuFormLongitude.get.toDouble,lieuFormation.lieuFormLatitude.get.toDouble)
      val uris: Seq[String] = zoneGeoWkts.filter(couple => PolygonUtils.isPointInPolygon(couple._2, point)).map(couple => couple._1)
      if (uris.size > 0) {
        Some(Seq(
          LiftyAnnotation(addressURI.get,s"${RomeConfig.amiOntoUri}hasZoneEmploi", createIRI(uris.last), 1.0)
        ))
      }
      else None
    }
    else None
  }

  def addressLiterals[A](addressURI: URI, literal: Option[A], ontologyURI: String, property: String, datatype: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(addressURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, createIRI(datatype)), 1.0))
    }
    else None
  }

  def pointLiterals[A](pointURI: URI, literal: Option[A], ontologyURI: String, property: String, datatype: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(pointURI, s"${ontologyURI}${property}", createLiteral(literal.get.toString, createIRI(datatype)), 1.0))
    }
    else None
  }
}