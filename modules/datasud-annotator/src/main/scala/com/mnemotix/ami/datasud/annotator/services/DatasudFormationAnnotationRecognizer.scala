package com.mnemotix.ami.datasud.annotator.services

import java.net.URI

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.ami.datasud.annotator.helpers.{DatasudFormationUriFactory, GeoPoint}
import com.mnemotix.ami.datasud.annotator.model.DatasudFormationModels
import com.mnemotix.ami.datasud.annotator.services.recognizer.writer.{AddressAnnotationWriter, FormationAnnotationsWriter}
import com.mnemotix.ami.datasud.annotator.services.recognizer.writer.FormationAnnotationsWriter.address
import com.mnemotix.lifty.api.LiftyService
import com.mnemotix.lifty.models.LiftyAnnotation

import scala.concurrent.ExecutionContextExecutor

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DatasudFormationAnnotationRecognizer(implicit ec: ExecutionContextExecutor) extends LiftyService {

  override def init(): Unit =  logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")

  override def shutdown(): Unit = {}

  def recognizerAsync: Flow[DatasudFormationModels, Seq[LiftyAnnotation], NotUsed] = {
    Flow[DatasudFormationModels].map { datasudFormation =>
      FormationAnnotationsWriter.annotations(datasudFormation)
    }
  }

  def zoneEmploiAsync(zoneEmploi: Seq[(String, List[GeoPoint])]): Flow[DatasudFormationModels, Seq[LiftyAnnotation], NotUsed] = {
    Flow[DatasudFormationModels].map { datasudFormation =>
      val addressValue = address(datasudFormation.lieuFormation)
      val uriAddress: Option[URI] = if (addressValue.trim.isEmpty || addressValue.trim == "") None else Some(DatasudFormationUriFactory.addressURI(addressValue))
      AddressAnnotationWriter.zoneEmploi(uriAddress, datasudFormation.lieuFormation, zoneEmploi).toSeq.flatten
    }
  }
}
