package com.mnemotix.ami.datasud.annotator.services.recognizer.writer

import java.net.URI
import java.text.SimpleDateFormat
import java.util.Date

import com.mnemotix.ami.datasud.annotator.helpers.{DatasudConfig, DatasudFormationUriFactory, GeoPoint, RomeConfig}
import com.mnemotix.ami.datasud.annotator.model.{DatasudFormationModels, FormationTags, LieuFormation}
import com.mnemotix.lifty.helpers.AnnotationsUtils.{createIRI, createLiteral}
import com.mnemotix.lifty.helpers.LiftyConfig
import com.mnemotix.lifty.models.LiftyAnnotation

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object FormationAnnotationsWriter {

  def annotations(datasudFormationModels: DatasudFormationModels): Seq[LiftyAnnotation] = {
    val addressValue = address(datasudFormationModels.lieuFormation)
    val trainingValue = training(addressValue, datasudFormationModels.offormRaisonSocialeFormateur, datasudFormationModels.formIntituleFormation, datasudFormationModels.sessDebut, datasudFormationModels.seesFin)
    val uriAddress: Option[URI] = if (addressValue.trim.isEmpty || addressValue.trim == "") None else Some(DatasudFormationUriFactory.addressURI(addressValue))
    val uriAmiTraining: Option[URI] = if (trainingValue.trim.isEmpty || trainingValue.trim == "") None else Some(DatasudFormationUriFactory.trainingURI(trainingValue))
    val uriOrganization: Option[URI] = if (datasudFormationModels.offormRaisonSocialeFormateur.isDefined) Some(DatasudFormationUriFactory.formationOrganizationURI(datasudFormationModels.offormRaisonSocialeFormateur.get)) else None
    val uriPoint: Option[URI] = if (datasudFormationModels.lieuFormation.lieuFormLatitude.isDefined && datasudFormationModels.lieuFormation.lieuFormLongitude.isDefined)  Some(DatasudFormationUriFactory.pointURI(datasudFormationModels.lieuFormation.lieuFormLatitude.get.toString, datasudFormationModels.lieuFormation.lieuFormLongitude.get.toString)) else None

    formationAnnotation(uriAmiTraining, datasudFormationModels).toSeq.flatten ++
    addressAnnotation(datasudFormationModels, uriOrganization, uriAddress, uriPoint, uriAmiTraining).toSeq ++
    taggingAnnotation(uriAmiTraining, datasudFormationModels.formationTags).toSeq.flatten
  }

  def formationAnnotation(uriAmiTraining: Option[URI], datasudFormationModels: DatasudFormationModels): Option[Seq[LiftyAnnotation]] = {
    if (uriAmiTraining.isDefined) trainingAnnotation(uriAmiTraining.get, datasudFormationModels)
    else None
  }

  def addressAnnotation(datasudFormationModels: DatasudFormationModels, uriOrganization: Option[URI], uriAddress: Option[URI],uriPoint: Option[URI],uriAmiTraining: Option[URI]): Seq[LiftyAnnotation] = {
    AddressAnnotationWriter.annotations(datasudFormationModels.formationResponsable,datasudFormationModels.lieuFormation, uriOrganization, uriAddress, uriPoint, uriAmiTraining)
  }

  def taggingAnnotation(uriAmiTraining: Option[URI], formationTags: FormationTags): Option[Seq[LiftyAnnotation]] = {
    if (uriAmiTraining.isDefined) {
      Some(TaggingAnnotationWriter.annotations(uriAmiTraining.get, formationTags))
    }
    else None
  }

  def trainingAnnotation(amiTraining: URI, datasudFormationModels: DatasudFormationModels): Option[Seq[LiftyAnnotation]] = {
    val annotationsTrainingClass = LiftyAnnotation(amiTraining, "a", createIRI(s"${LiftyConfig.ontologyURI}Training"), 1.0)
    val annotationsDurationClass = LiftyAnnotation(amiTraining, "a", createIRI(s"${DatasudConfig.ontologyMnx}Duration"), 1.0)
    Some(Seq(annotationsTrainingClass, annotationsDurationClass) ++
    trainingLiterals(amiTraining, datasudFormationModels.actNombreHeureTotale, s"${RomeConfig.amiOntoUri}", "totalHour", "http://www.w3.org/2001/XMLSchema#int") ++
    trainingLiterals(amiTraining, datasudFormationModels.actNombreHeuresEntreprise, s"${RomeConfig.amiOntoUri}", "companyHour", "http://www.w3.org/2001/XMLSchema#int") ++
    trainingLiterals(amiTraining, datasudFormationModels.actNombreHeuresCentre, s"${RomeConfig.amiOntoUri}", "classroomHour", "http://www.w3.org/2001/XMLSchema#int") ++
    trainingLiterals(amiTraining, datasudFormationModels.formObjectifFormation, s"${RomeConfig.amiOntoUri}", "objectif", "http://www.w3.org/2001/XMLSchema#string") ++
    trainingLiterals(amiTraining, datasudFormationModels.formContenuFormation, "http://purl.org/dc/terms/", "description", "http://www.w3.org/2001/XMLSchema#string") ++
    trainingLiterals(amiTraining, datasudFormationModels.formIntituleFormation, "http://purl.org/dc/terms/", "title", "http://www.w3.org/2001/XMLSchema#string") ++
    trainingLiterals(amiTraining, datasudFormationModels.urlFormation, "http://xmlns.com/foaf/0.1/", "homepage", "http://www.w3.org/2001/XMLSchema#string") ++
      datetrainingLiterals(amiTraining, datasudFormationModels.sessDebut, s"${RomeConfig.mnxOntoUri}", "startDate", "http://www.w3.org/2001/XMLSchema#dateTimeStamp") ++
      datetrainingLiterals(amiTraining, datasudFormationModels.seesFin, s"${RomeConfig.mnxOntoUri}", "endDate", "http://www.w3.org/2001/XMLSchema#dateTimeStamp") ++
    trainingLiterals(amiTraining, datasudFormationModels.actAccesHandicapes, s"${RomeConfig.amiOntoUri}", "disabledAccessibility", "http://www.w3.org/2001/XMLSchema#boolean") ++
    trainingLiterals(amiTraining, datasudFormationModels.formEligibleCpf, s"${RomeConfig.amiOntoUri}", "isCpfEligible", "http://www.w3.org/2001/XMLSchema#boolean")
    )
  }

  def trainingLiterals[A](amiTraining: URI, literal: Option[A], ontologyURI: String ,property: String, datatype: String): Option[LiftyAnnotation] = {
    if (literal.isDefined) {
      Some(LiftyAnnotation(amiTraining, s"${ontologyURI}${property}", createLiteral(literal.get.toString, createIRI(datatype)), 1.0))
    }
    else None
  }

  def datetrainingLiterals(amiTraining: URI, literal: Option[String], ontologyURI: String ,property: String, datatype: String) = {
    if (literal.isDefined) {
      val formatter = new SimpleDateFormat("dd/MM/yyyy")
      // yyyy'-'MM'-'dd HH':'mm':'ss'Z'
      //val formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
      val date = formatter.parse(literal.get).toInstant.toString
      Some(LiftyAnnotation(amiTraining, s"${ontologyURI}${property}", createLiteral(date, createIRI(datatype)), 1.0))
    }
    else None
  }

  def address(lieuFormation: LieuFormation): String = {
    s"${lieuFormation.lieuFormAdresseLigne1.getOrElse("")}${lieuFormation.lieuFormAdresseLigne2.getOrElse("")}${lieuFormation.lieuFormAdresseVille.getOrElse("")}" +
      s"${lieuFormation.lieuFormAdresseCodePostal.getOrElse(0)}${lieuFormation.lieuFormAdresseCodeInseeCommune.getOrElse(0)}"
  }

  def training(adressValue: String, offormRaisonSocialeFormateur: Option[String], formIntituleFormation: Option[String], sessDebut: Option[String], seesFin: Option[String]): String = {
    s"${adressValue}${offormRaisonSocialeFormateur.getOrElse("")}${formIntituleFormation.getOrElse("")}${sessDebut.getOrElse("")}${seesFin.getOrElse("")}"
  }

  def getDateAsString(d: Date): String = {
    val dateFormat = new SimpleDateFormat("ddMMyyyy")
    dateFormat.format(d)
  }

  def duration(sessDebut: Option[Date], sessFin: Option[Date]): String = {
     val sessD = if (sessDebut.isDefined)  getDateAsString(sessDebut.get) else ""
     val sessF = if (sessFin.isDefined) getDateAsString(sessFin.get) else ""
     s"${sessD}${sessF}"
  }
}