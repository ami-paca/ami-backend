package com.mnemotix.ami.datasud.annotator.services

import com.mnemotix.ami.datasud.annotator.services.JobRunner.filePath
import com.mnemotix.lifty.services.RDFFileUploader
import com.mnemotix.synaptix.rdf.client.models.Done
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor}
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Initiator extends LazyLogging {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  lazy val rdfFileUploader = new RDFFileUploader()

  def init: Boolean = {
    val file = new java.io.File(filePath).exists
    file
  }

  def close: Seq[Done] = {
    val fut = rdfFileUploader.bulkLoad(None)
    fut.onComplete {
      case Success(files) => for (file <- files) logger.info(file.message)
      case Failure(t) => logger.info("An error has occurred: " + t.getMessage)
    }
    Await.result(fut, Duration.Inf)
  }
}