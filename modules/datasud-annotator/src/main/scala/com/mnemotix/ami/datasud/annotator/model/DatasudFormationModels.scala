package com.mnemotix.ami.datasud.annotator.model

import java.util.Date

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

case class FormationTags(
                          formFormacode: Option[Seq[String]],
                          formNsf: Option[Seq[String]],
                          formRome: Option[Seq[String]],
                          formCodeRNCP: Option[String],
                          formCodeCertinfo: Option[String]
                        )

case class LieuFormation(lieuFormAdresseLigne1: Option[String],
                 lieuFormAdresseLigne2: Option[String],
                 lieuFormAdresseCodePostal: Option[Int],
                 lieuFormAdresseVille: Option[String],
                 lieuFormAdresseCode: Option[String],
                 lieuFormAdresseLibelleDepartement: Option[String],
                 lieuFormAdresseCodeInseeCommune: Option[String],
                 lieuFormLatitude: Option[Float],
                 lieuFormLongitude: Option[Float]
               )

case class DatasudFormationModels(formationResponsable: Option[String],
                                  formationTags: FormationTags,
                                  formIntituleFormation: Option[String],
                                  formObjectifFormation: Option[String],
                                  formContenuFormation: Option[String],
                                  formResultatAttendu: Option[String],
                                  formCertifiante: Option[Boolean],
                                  formEligibleCpf: Option[Boolean],
                                  formNiveauEntree: Option[String],
                                  formObjectifGeneralFormation: Option[String],
                                  formNiveauSortie: Option[String],
                                  actPublicVise: Option[String],
                                  lieuFormation: LieuFormation,
                                  actNiveauEntreeObligatoire: Option[Int],
                                  actNombreHeuresEntreprise: Option[Int],
                                  actTypeFormation: Option[String],
                                  offormRaisonSocialeFormateur: Option[String],
                                  urlFormation: Option[String],
                                  actAccesHandicapes: Option[Boolean],
                                  actRestauration: Option[String],
                                  actTransport: Option[String],
                                  sessDebut: Option[String],
                                  seesFin: Option[String],
                                  actModaliteRecrutement: Option[String],
                                  actNombreHeureTotale: Option[Int],
                                  actNombreHeuresCentre: Option[Int],
                                  actFinanceur: Option[String],
                                  codeDiplome: Option[String],
                                  nbCiDiff: Option[String],
                                  complementFicheRNCP: Option[String],
                                  nomNiveau: Option[String]
                                 )