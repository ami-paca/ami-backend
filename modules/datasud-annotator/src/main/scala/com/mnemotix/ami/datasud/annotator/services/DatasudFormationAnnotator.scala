package com.mnemotix.ami.datasud.annotator.services

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{GraphDSL, RunnableGraph, Sink}
import com.mnemotix.ami.datasud.annotator.helpers.{DatasudConfig, GeoPoint}
import com.mnemotix.lifty.services.{CSVExtractor, LiftyConverter, RDFFileOutput}

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DatasudFormationAnnotator {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)

  lazy val rdfExtracter = new RDFExtracter()
  lazy val liftyFileOutput = new RDFFileOutput()
  lazy val annotationRecognizer = new DatasudFormationAnnotationRecognizer()
  lazy val serializer = new DatasudFormationSerializer()

  lazy val liftyCSVExtractor = new CSVExtractor(DatasudConfig.csvfilePath, None)

  lazy val topHeadSink = Sink.ignore

  def init: Unit = {
    liftyFileOutput.init()
  }

  def annotate: Future[Done] = {
     RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
      import GraphDSL.Implicits._
      liftyCSVExtractor.extractAsync ~> serializer.serialize ~> annotationRecognizer.recognizerAsync ~> liftyFileOutput.bulkWrite ~> topHS
        ClosedShape
    }).run()
  }

  def annotateZoneEmploi = {

    lazy val rdf = new RDFExtracter()
    rdf.init
    val zoneEmploi: Seq[(String, List[GeoPoint])] = rdf.zoneGeoWkt

    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
        import GraphDSL.Implicits._
        liftyCSVExtractor.extractAsync ~> serializer.serialize ~> annotationRecognizer.zoneEmploiAsync(zoneEmploi) ~> liftyFileOutput.bulkWrite ~> topHS
        ClosedShape
    }).run()
  }
}
