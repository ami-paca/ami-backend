/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.datasud.annotator.helpers

case class GeoPoint(longitude: Double, latitude: Double)

object PolygonUtils {
  def isPointInPolygon(poly: List[GeoPoint], x: GeoPoint): Boolean = {
    (poly.last :: poly).sliding(2).foldLeft(false) { case (c, List(i, j)) =>
      val cond = {
        (
          (i.latitude <= x.latitude && x.latitude < j.latitude) ||
            (j.latitude <= x.latitude && x.latitude < i.latitude)
          ) &&
          (x.longitude < (j.longitude - i.longitude) * (x.latitude - i.latitude) / (j.latitude - i.latitude) + i.longitude)
      }

      if (cond) !c else c
    }
  }

}
