package com.mnemotix.ami.connectors.services.rest

import com.mnemotix.ami.connectors.ConnectorSpec
import com.mnemotix.ami.connectors.model.CompanyFilter
import com.mnemotix.ami.connectors.services.ApiClient
import com.mnemotix.ami.connectors.test.CriteriasLbb
import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class LbbRequestSpec extends ConnectorSpec {

  it should "request Companies" in {
    lazy val peClient = new ApiClient()
    Thread.sleep(1000)
    val token = peClient.tokenLabonneBoite()
    val js = RequestParams.bonneBoiteCriteria(CriteriasLbb.companiesSearch)
    val result = LbbRequest.recruitingCompanies(js, token)
    result.companies.isDefined shouldEqual true
    result.companies.get.size should be > 0
  }

  it should "get companies filter" in {
    lazy val peClient = new ApiClient()
    Thread.sleep(1000)
    val token = peClient.tokenLabonneBoite()
    val js = RequestParams.bonneBoiteCriteria(CriteriasLbb.companiesSearch)
    val result: CompanyFilter = LbbRequest.recruitingCompaniesFilter(js, token)
    result.filters.isDefined shouldEqual(true)
  }

  it should "count Recruiting Companies" in {
    lazy val peClient = new ApiClient()
    Thread.sleep(1000)
    val token = peClient.tokenLabonneBoite()
    val js = RequestParams.bonneBoiteCriteria(CriteriasLbb.companiesSearch)
    val result = LbbRequest.countRecruitingCompanies(js, token)
    result.companies_count.isDefined shouldBe(true)
    result.companies_count.get.toInt should be > 0
  }
}

/*
    //val bbc = BonneBoiteCriteria(Some("04019"), None, None, None, Some(50), Some(List("M1607")), None, None, None, None, None, None)
    val criterias = Map(
      "commune_id" -> "04019",
      "contract" -> "dpae",
      "distance" -> s"10",
      "rome_codes" -> "M1607",
      "headcount" ->  "all",
      "page" ->  "1",
      "page_size" -> s"20",
      "sort" -> s"score"
    )
 */