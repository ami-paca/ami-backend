package com.mnemotix.ami.connectors.helpers

import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class PEConfigSpec extends FlatSpec with Matchers {

  it should "access the config" in {
    PEConfig.accessPointUri shouldEqual "https://api.emploi-store.fr/partenaire"
    PEConfig.responseTimeout shouldEqual 10000
    PEConfig.clientId shouldEqual "PAR_cartonet_1f8d57f0f869cffe6e9f1b19bb4ac28c0ac0304e1d9c3096206897022290b0b3"
    PEConfig.clientSecret shouldEqual "f5f42cf47ec6d32d3903a0b60d147d4849963817a03c35f25f297ce2fb742310"
  }
}