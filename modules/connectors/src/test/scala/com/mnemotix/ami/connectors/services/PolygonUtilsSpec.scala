package com.mnemotix.ami.connectors.services

import com.mnemotix.ami.connectors.ConnectorSpec
import com.mnemotix.ami.connectors.helpers.ConnectorConfig
import com.mnemotix.ami.connectors.model.ZoneGeoWkt
import com.mnemotix.ami.connectors.services.utils.{GeoPoint, PolygonUtils}
import com.mnemotix.synaptix.rdf.client.RDFClient
import org.locationtech.jts.io.WKTReader

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class PolygonUtilsSpec extends ConnectorSpec {

  implicit lazy val conn = {
    RDFClient.getReadConnection(ConnectorConfig.rdfStoreRepoName)
  }

  val spqrlr = """PREFIX ope: <http://openemploi.datasud.fr/ontology/>
                 |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                 |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
                 |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                 |
                 |SELECT ?uri ?wkt WHERE {
                 |    ?uri rdf:type <http://ns.mnemotix.com/ontologies/2019/8/generic-model/Polygon> .
                 |    ?uri mnx:asWKT ?wkt
                 |}""".stripMargin

  def wkt: Future[Seq[ZoneGeoWkt]] = {

    var applicationKindTypeVocabularies = new ListBuffer[ZoneGeoWkt]()
    val futureApplicationKindIndividualsAsync = RDFClient.select(spqrlr).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        applicationKindTypeVocabularies += ZoneGeoWkt(bs.getValue("uri").stringValue(), bs.getValue("wkt").stringValue())
      }
      applicationKindTypeVocabularies.toSeq
    }

    futureApplicationKindIndividualsAsync onComplete {
      case Success(i) => println(s"The application-kind request succeed. The number of applicationKindType : ${i.size}")
      case Failure(t) => println("There is a failure here", t)
    }
    futureApplicationKindIndividualsAsync

  }

  it should "try ray casting" in {
    RDFClient.init()
    val zoneGeoWkts = Await.result(wkt, Duration.Inf)
    val wktReader = new WKTReader()
    println(zoneGeoWkts.size)

    val fre = GeoPoint(43.4333,6.7333)
    val lyon = GeoPoint(45.75, 4.85)
    val marseille = GeoPoint(43.3, 5.4)
    val mandelieu = GeoPoint(43.55, 6.93)
    val carpentras = GeoPoint(44.05, 5.05)

    zoneGeoWkts.map { zone =>
      val wktVal = wktReader.read(zone.wkt)
      val departement: List[GeoPoint] = wktVal.getCoordinates.map(c => GeoPoint(c.y, c.x)).toList
      println(departement)
      println( s"lyon is in ${zone.uri} ${PolygonUtils.isPointInPolygon(departement, lyon)}")
      println(s"marseille is in ${zone.uri} ${PolygonUtils.isPointInPolygon(departement, marseille)}")
      println(s"mandelieu is in ${zone.uri}  ${PolygonUtils.isPointInPolygon(departement, mandelieu)}")
      println(s"carpentras is in ${zone.uri}  ${PolygonUtils.isPointInPolygon(departement, carpentras)}")
      println(s"frejus is in ${zone.uri}  ${PolygonUtils.isPointInPolygon(departement, fre)}")

    }



  }

}

