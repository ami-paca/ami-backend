/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.connectors.services

import com.mnemotix.ami.connectors.ConnectorSpec
import com.mnemotix.ami.connectors.helpers.ConnectorConfig
import com.mnemotix.ami.connectors.model.ZoneGeoWkt
import com.mnemotix.synaptix.rdf.client.RDFClient

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

class RDFExtracterSpec extends ConnectorSpec {

  val repositoryName = ConnectorConfig.rdfStoreRepoName

  implicit lazy val connexion = RDFClient.getReadConnection(ConnectorConfig.rdfStoreRepoName)

  RDFClient.init()


  val spqrlr = """PREFIX ope: <http://openemploi.datasud.fr/ontology/>
                 |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                 |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
                 |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                 |
                 |SELECT ?uri ?wkt WHERE {
                 |    ?uri rdf:type <http://ns.mnemotix.com/ontologies/2019/8/generic-model/Polygon> .
                 |    ?uri mnx:asWKT ?wkt
                 |}""".stripMargin

  it should "query data with sparql select queries" in {
    var zonesGeoWkt = new ListBuffer[ZoneGeoWkt]()

      val futurezonesGeoWktAsync: Future[Seq[ZoneGeoWkt]] = RDFClient.select(spqrlr).map { res =>
        val tupleQueryResult = res.resultSet
        while (tupleQueryResult.hasNext) {
          val bs = tupleQueryResult.next()
          zonesGeoWkt += ZoneGeoWkt(bs.getValue("uri").stringValue(), bs.getValue("wkt").stringValue())
        }
        zonesGeoWkt.toSeq
      }
      futurezonesGeoWktAsync onComplete {
        case Success(i) => println(s"The application-kind request succeed. The number of applicationKindType : ${i.size}")
        case Failure(t) => println("There is a failure here", t)
      }

    val zones = Await.result(futurezonesGeoWktAsync, Duration.Inf)
    println(zones.size)
    }
}