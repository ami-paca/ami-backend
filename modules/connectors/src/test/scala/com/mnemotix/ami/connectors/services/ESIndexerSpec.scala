package com.mnemotix.ami.connectors.services

import com.mnemotix.ami.connectors.ConnectorSpec
import com.mnemotix.ami.connectors.helpers.{ConnectorConfig, DateFormatUtil, RomeConfig}
import com.mnemotix.ami.connectors.model.{Companies, IndexPEOfferV2, PEOffer}
import com.mnemotix.ami.connectors.services.index.ESMapping
import com.mnemotix.ami.connectors.test.{LbbCompanyExample, PEOfferExample}
import com.mnemotix.synaptix.core.utils.CryptoUtils
import com.mnemotix.synaptix.index.elasticsearch.models.ESMappingDefinitions
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.indexes.IndexResponse
import play.api.libs.json.{JsObject, JsValue, Json}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESIndexerSpec extends ConnectorSpec {

  it should "init an peoffer index" in {
    val indexer = new ESIndexer()
    // val indexName = "dev-openemploi-offer-pe-copy"
    val indexName = ConnectorConfig.peofferName
    indexer.init
    val mapping: ESMappingDefinitions = ESMapping.mappnPEOfferDef
    val res = indexer.createIndex(indexName, mapping)
    println(s"index name : ${res._2}")
    res._1 shouldEqual(true)
  }

  it should "insert an offer in peoffer index" ignore {
    val indexer = new ESIndexer()
    indexer.init
    val mapping: ESMappingDefinitions = ESMapping.mappnPEOfferDef
    val res = indexer.createIndex(ConnectorConfig.peofferName, mapping)
    val indexName: String = res._2
    val peOffer = Json.parse(PEOfferExample.offre1).as[PEOffer]
    val dateIndexed = DateFormatUtil.getDate(DateFormatUtil.DATE_FORMAT)
    val peOfferIndex = IndexPEOfferV2(dateIndexed, peOffer)
    val json: JsValue = Json.toJson(peOfferIndex)
    val futureResponse = indexer.insertDocument(indexName, peOffer.id.get, json.as[JsObject])
    val response: Response[IndexResponse] = Await.result(futureResponse, Duration.Inf)
    response.isSuccess shouldBe true
  }

  it should "init an test company index" ignore  {
    val indexer = new ESIndexer()
    indexer.init
    val mapping = ESMapping.mappnCompanyDef
    val res = indexer.createIndex(ConnectorConfig.companyName, mapping)
    println(s"index name : ${res._2}")
    res._1 shouldEqual true
  }

  it should "insert a company in company index" ignore  {
    val indexer = new ESIndexer()
    indexer.init
    val mapping: ESMappingDefinitions = ESMapping.mappnCompanyDef
    val res = indexer.createIndex(ConnectorConfig.companyName, mapping)
    val indexName: String = res._2
    val company = Json.parse(LbbCompanyExample.company1).as[Companies]
    val json = Json.toJson(company)
    val futureResponse = indexer.insertDocument(indexName, CryptoUtils.md5sum(company.toString), json.as[JsObject])
    val response: Response[IndexResponse] = Await.result(futureResponse, Duration.Inf)
    response.isSuccess shouldBe true
  }

  it should "init a ROME company index" ignore  {
    val indexer = new ESIndexer()
    indexer.init
    val mapping = ESMapping.mappnFicherMetierDef
    val res = indexer.createIndex(ConnectorConfig.ficheMetierName, mapping)
    println(s"index name : ${res._2}")
    res._1 shouldEqual true
  }
}