package com.mnemotix.ami.connectors.test

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object LbbCompanyExample {
  val company1 =
    """
      |{"address":"Service des ressources humaines, 52B MAIL MARCEL CACHIN, 38600 FONTAINE","alternance":true,"boosted":true,"city":"FONTAINE","contact_mode":"Envoyez votre candidature par mail","distance":32.1,"headcount_text":"50 à 99 salariés","lat":45.1948,"lon":5.68725,"matched_rome_code":"M1607","matched_rome_label":"Secrétariat","matched_rome_slug":"secretariat","naf":"6820B","naf_text":"Location de terrains et d'autres biens immobiliers","name":"ALPES ISÈRE HABITAT","siret":"77953712500170","social_network":"","stars":2.5,"url":"https://labonneboite.pole-emploi.fr/77953712500170/details?rome_code=M1607&utm_medium=web&utm_source=api__emploi_store_dev&utm_campaign=api__emploi_store_dev__cartonet","website":"http://alpeshabitat.fr/"}
      |""".stripMargin

  val company2 =
    """
      |{"address":"Service des ressources humaines, 5 RUE ALBERT SAMAIN, 38400 SAINT-MARTIN-D-HERES","alternance":true,"boosted":true,"city":"SAINT-MARTIN-D-HERES","contact_mode":"Envoyez votre candidature par mail","distance":29.3,"headcount_text":"20 à 49 salariés","lat":45.1767,"lon":5.76391,"matched_rome_code":"M1607","matched_rome_label":"Secrétariat","matched_rome_slug":"secretariat","naf":"6820A","naf_text":"Location de logements","name":"AGENCE GRESIVAUDAN GROUPE MALFANGEA","siret":"77953712500097","social_network":"","stars":2.5,"url":"https://labonneboite.pole-emploi.fr/77953712500097/details?rome_code=M1607&utm_medium=web&utm_source=api__emploi_store_dev&utm_campaign=api__emploi_store_dev__cartonet","website":"http://alpeshabitat.fr/"
      |""".stripMargin
}
