package com.mnemotix.ami.connectors.test

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object PEOfferExample {

  val offre1 =
    """
      |    {
      |      "accessibleTH": false,
      |      "alternance": false,
      |      "appellationlibelle": "Commercial / Commerciale sédentaire",
      |      "competences": [
      |        {
      |          "code": "121427",
      |          "exigence": "S",
      |          "libelle": "Développer un portefeuille clients et prospects"
      |        },
      |        {
      |          "code": "117243",
      |          "exigence": "S",
      |          "libelle": "Établir un devis"
      |        },
      |        {
      |          "code": "117180",
      |          "exigence": "S",
      |          "libelle": "Négocier un contrat"
      |        },
      |        {
      |          "code": "121280",
      |          "exigence": "S",
      |          "libelle": "Réceptionner les appels téléphoniques"
      |        },
      |        {
      |          "code": "122330",
      |          "exigence": "S",
      |          "libelle": "Renseigner un client"
      |        }
      |      ],
      |      "contact": {
      |        "coordonnees1": "Courriel : recrutement@mutuellebleue.fr",
      |        "courriel": "recrutement@mutuellebleue.fr",
      |        "nom": "MUTUELLE BLEUE - Mme SOPHIE PITHON"
      |      },
      |      "dateActualisation": "2020-07-08T00:00:00+02:00",
      |      "dateCreation": "2020-07-08T14:12:46+02:00",
      |      "description": "Rattaché(e) au Responsable Commercial Régional, vous êtes chargé(e) de développer et fidéliser le portefeuille de votre agence en proposant des produits santé et prévoyance auprès d'une clientèle de particuliers et de TPE.\n\nVos missions seront notamment les suivantes :\n- Prospecter, suivre et fidéliser un portefeuille clients, \n- Accueillir, orienter, renseigner les adhérents et les prospects à l'agence et par téléphone, \n- Comprendre le besoin, \n- Proposer une solution adaptée,\n- Argumenter et vendre, \n- Élaborer les devis, \n- Effectuer les relances clients, \n- Assurer le suivi des dossiers \n\nVous êtes doté(e) d'un fort tempérament commercial et justifiez, à minima, d'une période d'alternance ou de stages significatifs à un poste similaire. Vous justifiez d'une bonne aisance verbale et relationnelle, du sens du service et êtes réactif(ve), organisé(e), rigoureux(se) et persévérant(e). \n\nRémunération fixe + variable. \nChèques déjeuner et complémentaire santé.",
      |      "dureeTravailLibelle": "37H30 Horaires normaux",
      |      "dureeTravailLibelleConverti": "Temps plein",
      |      "entreprise": {
      |        "description": "Mutuelle Bleue propose une offre de produits Santé et Prévoyance et de services de qualité répondant aux attentes des particuliers, des professionnels et des entreprises. Nous assurons 354 000 personnes en prévoyance et 264 500 en santé.\nChez Mutuelle Bleue, nous pensons que c'est l'alliance de vos compétences et de votre personnalité qui vous permet de vous surpasser et d'être performant au quotidien.",
      |        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/913e9e66609942a78b8442431af33434.png",
      |        "nom": "MUTUELLE BLEUE"
      |      },
      |      "experienceExige": "E",
      |      "experienceLibelle": "1 an",
      |      "formations": [],
      |      "id": "103FNNV",
      |      "intitule": "Commercial / Commerciale sédentaire   (H/F)",
      |      "langues": [],
      |      "lieuTravail": {
      |        "codePostal": "78120",
      |        "commune": "78517",
      |        "latitude": 48.64361111,
      |        "libelle": "78 - RAMBOUILLET",
      |        "longitude": 1.829999999
      |      },
      |      "natureContrat": "Contrat travail",
      |      "nombrePostes": 1,
      |      "origineOffre": {
      |        "origine": "1",
      |        "partenaires": [],
      |        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/103FNNV"
      |      },
      |      "qualificationCode": "6",
      |      "qualificationLibelle": "Employé qualifié",
      |      "romeCode": "D1401",
      |      "romeLibelle": "Assistanat commercial",
      |      "salaire": {
      |        "libelle": "Annuel de 23000,00 Euros à 25000,00 Euros sur 13.5 mois"
      |      },
      |      "secteurActivite": "65",
      |      "secteurActiviteLibelle": "Autres assurances",
      |      "trancheEffectifEtab": "100 à 199 salariés",
      |      "typeContrat": "CDI",
      |      "typeContratLibelle": "Contrat à durée indéterminée"
      |    }
      |""".stripMargin

  val offre2 =
    """
      |{
      |      "accessibleTH": false,
      |      "alternance": false,
      |      "appellationlibelle": "Chef de chantier travaux publics",
      |      "competences": [
      |        {
      |          "code": "118365",
      |          "exigence": "S",
      |          "libelle": "Affecter le personnel sur des postes de travail"
      |        },
      |        {
      |          "code": "117170",
      |          "exigence": "S",
      |          "libelle": "Coordonner les prestataires, fournisseurs, intervenants"
      |        },
      |        {
      |          "code": "120648",
      |          "exigence": "S",
      |          "libelle": "Planifier l'activité du personnel"
      |        },
      |        {
      |          "code": "123640",
      |          "exigence": "S",
      |          "libelle": "Présenter le chantier à un intervenant"
      |        },
      |        {
      |          "code": "118772",
      |          "exigence": "S",
      |          "libelle": "Suivre l'état d'avancement des travaux jusqu'à réception"
      |        }
      |      ],
      |      "contact": {
      |        "coordonnees1": "Courriel : p.guerrier@ltd-international.com",
      |        "courriel": "p.guerrier@ltd-international.com",
      |        "nom": "LTD INTERNATIONAL EXPERT - M. Paul GUERRIER"
      |      },
      |      "dateActualisation": "2020-07-08T00:00:00+02:00",
      |      "dateCreation": "2020-07-08T14:12:45+02:00",
      |      "description": "Vous êtes responsable du bon déroulement du chantier au quotidien. Vous devez en assurer la préparation, le suivi technique et la qualité de réalisation, coordonner les différents corps de métier, ajuster et veiller à la disponibilité des ressources de façon à respecter les coûts et les délais de réalisation des travaux d'EP, EU, AEP.\n\nMissions\n\nVous managerez les équipes sur le terrain et vous devrez :\n-\tPrendre connaissance du dossier technique et des plans. \n-\tÉvaluer les besoins en main-d'œuvre, en matériels et matériaux pour le travail des différents corps de métier.\n-\tCalculer le volume d'heures et de main-d'œuvre nécessaires.\n-\tFaire le point sur l'avancement des travaux\n-\tTenir à jour le carnet de bord du chantier.\n",
      |      "dureeTravailLibelle": "35H Horaires normaux",
      |      "dureeTravailLibelleConverti": "Temps plein",
      |      "entreprise": {
      |        "nom": "LTD INTERNATIONAL EXPERT"
      |      },
      |      "experienceExige": "E",
      |      "experienceLibelle": "5 ans",
      |      "formations": [],
      |      "id": "103FNNT",
      |      "intitule": "Chef de chantier travaux publics",
      |      "langues": [],
      |      "lieuTravail": {
      |        "codePostal": "67000",
      |        "commune": "67482",
      |        "latitude": 48.57333333,
      |        "libelle": "67 - STRASBOURG",
      |        "longitude": 7.752222222
      |      },
      |      "natureContrat": "Contrat travail",
      |      "nombrePostes": 1,
      |      "origineOffre": {
      |        "origine": "1",
      |        "partenaires": [],
      |        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/103FNNT"
      |      },
      |      "qualificationCode": "7",
      |      "qualificationLibelle": "Technicien",
      |      "romeCode": "F1202",
      |      "romeLibelle": "Direction de chantier du BTP",
      |      "salaire": {
      |        "libelle": "Mensuel de 2700,00 Euros à 3800,00 Euros sur 13 mois"
      |      },
      |      "secteurActivite": "78",
      |      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      |      "trancheEffectifEtab": "10 à 19 salariés",
      |      "typeContrat": "CDI",
      |      "typeContratLibelle": "Contrat à durée indéterminée"
      |    }
      |""".stripMargin
}
