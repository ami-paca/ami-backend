package com.mnemotix.ami.connectors.services

import akka.Done
import com.mnemotix.ami.connectors.ConnectorSpec
import com.mnemotix.ami.connectors.helpers.{ConnectorConfig, RomeConfig}
import com.mnemotix.ami.connectors.model.FicheMetier
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.{SearchHits, SearchResponse}
import play.api.libs.json.{JsObject, JsValue, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RomeHarvesterSpec extends ConnectorSpec {

  val romeHarvester = new RomeHarvester

  it should "retrieve and insert rome fiche metier in ES" ignore {
    val romeHarvesterResp: Future[Done] = romeHarvester.romeHarvester
    val rome = Await.result(romeHarvesterResp, Duration.Inf)
    rome
  }

  // [INFO ] 2020-10-06 20:27:43 com.mnemotix.ami.connectors.services.RomeHarvester - we retrieved 532 metiers.

  it should "retrieve the fiche metier in ES" in {
    IndexClient.init()
    val futureResponseSearch: Future[Response[SearchResponse]] = IndexClient.rawQuery(RawQuery(Seq(ConnectorConfig.ficheMetierName), Json.parse(
      """
        |{
        | "from" : 0, "size" : 1000,
        | "query": {
        |   "match_all": {}
        | }
        |}
        |""".stripMargin
    ).as[JsObject]))
    val resp: Response[SearchResponse] = Await.result(futureResponseSearch, Duration.Inf)
    val searchHits: SearchResponse = resp.result

    val ficheMetier: Array[FicheMetier] = searchHits.hits.hits.map { hit =>
      val json: JsValue = Json.parse(hit.sourceAsString)
      val ficheMetier = json.as[FicheMetier]
      println(ficheMetier.code.get)
      ficheMetier
    }
    resp foreach (search => println(s"There were ${search.totalHits} total hits"))
    ficheMetier.size should be > 0
    println(ficheMetier.size)
    resp.isSuccess shouldBe true
  }
}
