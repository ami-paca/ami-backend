package com.mnemotix.ami.connectors.services

import com.mnemotix.ami.connectors.ConnectorSpec
import com.mnemotix.ami.connectors.helpers.ConnectorConfig
import com.mnemotix.ami.connectors.model.BonneBoiteCriteria
import com.mnemotix.ami.connectors.services.rest.{LbbRequest, RequestParams}
import com.mnemotix.ami.connectors.test.CriteriasLbb

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class LbbHarvesterSpec extends ConnectorSpec {

  val lbbHarvester = new LbbHarvester()

  it should "get content range" in {
    val token = LbbRequest.refreshLbbToken
    val nbr = lbbHarvester.getContentRange(RequestParams.bonneBoiteCriteria(CriteriasLbb.companiesSearch), token)
    nbr.isDefined shouldEqual(true)
    nbr.get.toInt should be > 0
  }

  it should "get a list a of communes" in {
    val communes = lbbHarvester.getListOfCommune(lbbHarvester.pacaDep)
    communes.foreach(commune => println(commune.code))

  }

  it should "bonne boite request" in {
      val bbc = BonneBoiteCriteria(Some("13005"), None, None, None, Some(50), Some(List("I1604","H1504","H2601", "H2602","H2603","H2604","H2605","H1101","H1208","H1303","I1301","I1302","I1304","I1305","I1306","I1308","I1309","I1310","I1603")), None, None, None, None, None, None)
      val futResponse = lbbHarvester.requestPE(bbc, ConnectorConfig.companyName)

      futResponse.onComplete {
        case Success(_) =>
        case Failure(err) => println("There was a problem while requesting the API.", Some(err))
      }
      val response = Await.result(futResponse, Duration.Inf)
      response.foreach(res => println(s"The indexing of P.E offer is a succes : ${res.isSuccess}"))
  }

  it should "retrieve lbbharvester" in {
    lbbHarvester.lbbHarvester
  }
}

/*
Some(List("H1504","H2601", "H2602"))
Some(List("H2603","H2604","H2605"))
Some(List("H1101","H1208","H1303"))
Some(List("I1301","I1302","I1304"))
Some(List("I1305","I1306","I1308"))
Some(List("I1309","I1310","I1603"))
Some(List("I1604"))

List("I1604","H1504","H2601", "H2602","H2603","H2604","H2605","H1101","H1208","H1303","I1301","I1302","I1304","I1305","I1306","I1308","I1309","I1310","I1603")
 */