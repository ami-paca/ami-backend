package com.mnemotix.ami.connectors.services.rest

import com.mnemotix.ami.connectors.ConnectorSpec
import com.mnemotix.ami.connectors.services.ApiClient

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import com.mnemotix.ami.connectors.test.CriteriasPE

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class PERequestSpec extends ConnectorSpec{

  it should "request an Token for RomeRequest" ignore  {
    lazy val apiClient = new ApiClient()
    val token = apiClient.tokenNomenclatureRome()
    println(token)
  }

  it should "request an Token for PEOffer" ignore  {
    lazy val apiClient = new ApiClient()
    val token: String = apiClient.refreshToken()
    println(token)
  }

  it should "request an Token for LaBonneBoite" ignore  {
    lazy val apiClient = new ApiClient()
    val token: String = apiClient.tokenLabonneBoite()
    println(token)
  }

  it should "request metiers" in {
    Thread.sleep(1000)
    lazy val apiClient = new ApiClient()
    lazy val perRequest = new PERequest(apiClient)
    val token: String = apiClient.refreshToken()
    val metiers = Await.result(perRequest.getMetiers(token), Duration.Inf)
    println(metiers.size)
    println(metiers(0))
    metiers.size should be > 0
  }

  it should "request Appellations" in {
    Thread.sleep(1000)
    lazy val apiClient = new ApiClient()
    lazy val perRequest = new PERequest(apiClient)
    val token: String = apiClient.refreshToken()
    val appellation = Await.result(perRequest.getAppellations(token), Duration.Inf)
    println(s"number of appelation is ${appellation.size}")
    appellation.size should be > 0
  }

  it should "request Commmunes" in {
    Thread.sleep(1000)
    lazy val apiClient = new ApiClient()
    lazy val perRequest = new PERequest(apiClient)
    val token: String = apiClient.refreshToken()
    val communes = Await.result(perRequest.getCommunes(token), Duration.Inf)
    println(s"number of communes is ${communes.size}")
    println(communes.last)
    communes.size should be > 0
  }

  it should "request Regions" in {
    Thread.sleep(1000)
    lazy val apiClient = new ApiClient()
    lazy val perRequest = new PERequest(apiClient)
    val token: String = apiClient.refreshToken()
    val regions = Await.result(perRequest.getRegions(token), Duration.Inf)
    regions.foreach(sr => println(s"${sr.code} : ${sr.libelle}"))
    println(s"number of regions is ${regions.size}")
    regions.size should be > 0
  }

  it should "request Contracts Types" in {
    Thread.sleep(1000)
    lazy val apiClient = new ApiClient()
    lazy val perRequest = new PERequest(apiClient)
    val token: String = apiClient.refreshToken()
    val contractTypes = Await.result(perRequest.getTypesContracts(token), Duration.Inf)
    contractTypes.map { contractType =>
      println(s"${contractType.code} - ${contractType.libelle}")
    }
    println(s"number of contract type is ${contractTypes.size}")
    contractTypes.size should be > 0
  }

  it should "request Offer" in {
    Thread.sleep(1000)
    lazy val apiClient = new ApiClient()
    lazy val perRequest = new PERequest(apiClient)
    val token = apiClient.refreshToken()
    val offres = perRequest.searchByCriteria(CriteriasPE.criterias, token)
    if (offres.resultats.isDefined) println(offres.resultats.get.size) else println("None")
    offres.resultats.get.size should be > 0
  }
}