package com.mnemotix.ami.connectors.helpers

import java.util.Date

import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DataFormatUtilSpec extends FlatSpec with Matchers {

  it should "get date as a string" in {
    val date = new Date()
    val dateString = DateFormatUtil.getDateAsString(date, DateFormatUtil.DATE_FORMAT)
    println(dateString)
    dateString.length shouldEqual 12
  }

  it should "get the string as a date" in {
    val dateString = "202007071124"
    val date = DateFormatUtil.convertStringToDate(dateString,DateFormatUtil.DATE_FORMAT)
    date.toString shouldEqual("Tue Jul 07 11:24:00 CEST 2020")
  }
}
