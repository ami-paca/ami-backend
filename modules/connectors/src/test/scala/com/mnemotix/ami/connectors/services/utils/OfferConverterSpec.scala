package com.mnemotix.ami.connectors.services.utils

import com.mnemotix.ami.connectors.ConnectorSpec

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class OfferConverterSpec extends ConnectorSpec {

  it should "retrieve the salary of an offer" in {
    val s = "Annuel de"
    val horaire = "Horaire de 10,15 Euros"
    val mensuel ="Mensuel de 2000,00 Euros à 2200,00 Euros sur 12 mois"
    val annuel = "Annuel de 48000,00 Euros sur 12 mois"
    val horaire2 = "Horaire de 13,52 Euros"
    val annuel2 = "Annuel de 22000,00 Euros à 24000,00 Euros sur 12 mois"

    val sC = OfferConverter.salaireMatcher(Some(s))
    val horaireC = OfferConverter.salaireMatcher(Some(horaire))
    val mensuelC = OfferConverter.salaireMatcher(Some(mensuel))
    val annuelC = OfferConverter.salaireMatcher(Some(annuel))
    val horaire2C = OfferConverter.salaireMatcher(Some(horaire2))
    val annuel2C = OfferConverter.salaireMatcher(Some(annuel2))

    println(sC)
    println(horaireC)
    println(mensuelC)
    println(annuelC)
    println(horaire2C)
    println(annuel2C)

  }

}
