package com.mnemotix.ami.connectors.services

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding.Get
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import com.mnemotix.ami.connectors.ConnectorSpec
import com.mnemotix.ami.connectors.helpers.PEConfig
import com.mnemotix.ami.connectors.model.{BonneBoiteCriteria, PEOfferSearchCriteria, Range}
import com.mnemotix.ami.connectors.services.rest.LbbRequest.{peClient, showStatus}
import com.mnemotix.ami.connectors.services.rest.{LbbRequest, PERequest, RequestParams}

import scala.collection.immutable
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ApiClientSpec extends ConnectorSpec {

  it should "request api LBB with criterias from map" in {


    val token = LbbRequest.refreshLbbToken
    val criterias = Map(
      "commune_id" -> "04019",
      "contract" -> "dpae",
      "distance" -> s"50",
      "rome_codes" -> "M1607",
      "headcount" ->  "all",
      "page" ->  "1",
      "page_size" -> s"20",
      "sort" -> s"score"
    )
    val resp = Await.result(apiClient.get(PEConfig.labonneboiteAccessPointUri, "/v1/company/count/", token, criterias), Duration.Inf)
    resp.entity.dataBytes.runWith(Sink.ignore)
    showStatus(resp)
    val url  = resp.headers.filter(header => header.is("location")).map(header => header.value()).mkString
    val respBonneBoite = Await.result(Http().singleRequest(Get(url)), Duration.Inf)
    showStatus(respBonneBoite)
    val futResponse: Future[immutable.Seq[String]] = respBonneBoite.entity.dataBytes.map(_.utf8String).map { res =>
      res
    }.runWith(Sink.seq)
    val response = Await.result(futResponse, Duration.Inf).mkString
    println(response)
  }


  it should "request api LBB with criterias from dao" in {

    val token = LbbRequest.refreshLbbToken
    val bbc = BonneBoiteCriteria(Some("04019"), None, None, None, Some(50), Some(List("M1607")), None, None, None, None, None, None)

    val resp = Await.result(apiClient.get(PEConfig.labonneboiteAccessPointUri, "/v1/company/count/", token, RequestParams.bonneBoiteCriteria(bbc)), Duration.Inf)
    resp.entity.dataBytes.runWith(Sink.ignore)
    showStatus(resp)
    val url  = resp.headers.filter(header => header.is("location")).map(header => header.value()).mkString
    val respBonneBoite = Await.result(Http().singleRequest(Get(url)), Duration.Inf)
    showStatus(respBonneBoite)
    val futResponse: Future[immutable.Seq[String]] = respBonneBoite.entity.dataBytes.map(_.utf8String).map { res =>
      res
    }.runWith(Sink.seq)
    val response = Await.result(futResponse, Duration.Inf).mkString
    println(response)
  }

  it should "request api PO with criterias from map" in {
    lazy val peClient = new ApiClient()
    lazy val peRequest = new PERequest(peClient)

    val token = peRequest.refreshPEToken
    val criterias = Map("range" -> "0-50", "codeROME" -> "H1504", "region" -> "93")
    val resp = Await.result(apiClient.get(PEConfig.poleEmploiAccessPoint, "/v2/offres/search", token, criterias), Duration.Inf)
    resp.status.intValue() shouldEqual(200)
  }

  it should "request api PO with criterias from dao" in {
    lazy val peClient = new ApiClient()
    lazy val peRequest = new PERequest(peClient)
    val token = peRequest.refreshPEToken
    val range = Range("0", "100")
    val criterias = new PEOfferSearchCriteria(Some(range), Some("H1504"), Some("93"))
    val resp = Await.result(apiClient.get(PEConfig.poleEmploiAccessPoint, "/v2/offres/search", token, RequestParams.peOffercriteria(criterias)), Duration.Inf)
    resp.headers.toSeq.foreach(httpHeader =>
      println(httpHeader.name() + " " + httpHeader.value())
    )
  }
}

/*
Some(List("H1504","H2601", "H2602"))
Some(List("H2603","H2604","H2605"))
Some(List("H1101","H1208","H1303"))
Some(List("I1301","I1302","I1304"))
Some(List("I1305","I1306","I1308"))
Some(List("I1309","I1310","I1603"))
Some(List("I1604"))
 */