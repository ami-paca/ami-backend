package com.mnemotix.ami.connectors.test

import java.util.Date

import com.mnemotix.ami.connectors.model.{EOffer, Entreprise, Geoloc}
import play.api.libs.json.Json

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


case class ESEoffer(esId: String, eoOffer: EOffer)

object ESEoffer {
  implicit lazy val format = Json.format[ESEoffer]
}




case class EOfferU(intitule: Option[String], description: Option[String], dateActualisation: Option[Long], dateCollecte: Option[Date], dateCreation: Option[Long], alternance: Option[Boolean],
                   experienceLibelle: Option[String], natureContrat: Option[String], typeContratLibelle: Option[String], qualificationLibelle: Option[String],
                   secteurActiviteLibelle: Option[String],trancheEffectifEtab:Option[String],  entreprise: Option[Entreprise], geoloc: Option[Geoloc],
                   competences: Option[Seq[String]], occupation: Option[String], formations: Option[Seq[String]], offreURL: Option[String], salaire: Option[String],
                   zoneEmploi: Option[String])

object EOfferU {
  implicit lazy val format = Json.format[EOfferU]
}

case class ESEofferU(esId: String, eoOffer: EOfferU)

object ESEofferU {
  implicit lazy val format = Json.format[ESEofferU]
}