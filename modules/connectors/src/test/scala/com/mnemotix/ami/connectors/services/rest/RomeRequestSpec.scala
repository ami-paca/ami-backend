package com.mnemotix.ami.connectors.services.rest

import com.mnemotix.ami.connectors.model.Metier
import org.scalatest.{FlatSpec, Matchers}
import play.api.libs.json.Json

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RomeRequestSpec extends FlatSpec with Matchers {

  it should "request ROME Metier" ignore {
    Thread.sleep(1000)
    val token: String = RomeRequest.refreshRomeToken
    val metier: Seq[Metier] = RomeRequest.metiers(token)
    println(metier(0))
    metier.size should be > 0
  }

  it should "request a ROME Metier" in {
    Thread.sleep(1000)
    val token: String = RomeRequest.refreshRomeToken
    val metier = RomeRequest.metier(token, "H2605")
    println(Json prettyPrint Json.toJson(metier))
    //metier.competencesDeBase.get.foreach(debase => println(debase.noeudCompetence.libelle))
    val nd  = metier.groupesCompetencesSpecifiques.get.map(comps => comps.competences.get.map(cps => cps.noeudCompetence.get.libelle.get))
    nd.flatten.distinct.foreach(println(_))
    metier.code.isDefined shouldEqual(true)
    metier.code.get shouldEqual("H2605")
  }
}