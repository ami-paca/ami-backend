package com.mnemotix.ami.connectors.test

import com.mnemotix.ami.connectors.model.{PEOfferSearchCriteria, Range}
import com.mnemotix.ami.connectors.services.rest.RequestParams

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object CriteriasPE {
/*
  val range = Range("0", "50")
  val domaineEmploi = DomaineEmploi(None,Some(List("H1504","H2601", "H2602")), None, None, None, None)
  val travailDetail = TravailDetail(None, None, None, None, None, None)
  val location = Location(None, None, None, None, Some("D004"), None)
  val travailCondition = TravailCondition(None, None, None, None)
  val publicationDate = PublicationDate(None, None, None)
 */

  val range = Range("0", "50")

  val criterias: Map[String, String] = RequestParams.peOffercriteria(PEOfferSearchCriteria(Some(range), None, None))
}