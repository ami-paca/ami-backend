package com.mnemotix.ami.connectors.test

import java.io.FileWriter

import com.mnemotix.ami.connectors.ConnectorSpec
import com.mnemotix.ami.connectors.model.{EOffer, Geoloc}
import com.mnemotix.ami.connectors.services.RDFExtracter
import com.mnemotix.ami.connectors.services.rest.PERequest
import com.mnemotix.ami.connectors.services.utils.{GeoPoint, PolygonUtils}
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.ESIndexable
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsObject, Json}

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.Await

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// todo try to retreive the offer to update the date

class UpdaterPEOffer extends ConnectorSpec with LazyLogging{

  lazy val idxName = "dev-openemploi-offer-pe"

  lazy val rdfExtracter = new RDFExtracter()
  lazy val perrequest = new PERequest(apiClient)

  def requester = {
    var eoffers = new ListBuffer[ESEoffer]()

    val size = Await.result(IndexClient.count(idxName), Duration.Inf)
    size.result.count

    val responseFirstRequester = firstRequester
    if (responseFirstRequester.isSuccess) {
      val scrollID = responseFirstRequester.result.scrollId
      responseFirstRequester.result.hits.hits.map {hit =>
        val json = Json.parse(hit.sourceAsString)
        val eoffer = json.as[EOffer]
        val esoffer = ESEoffer(hit.id, eoffer)
        eoffers.append(esoffer)
      }
      requesterHelper(scrollID, eoffers, size.result.count)
    }
    else {
      logger.error(s"Unable to execute the scroll query on index [${"dev-openemploi-offer-pe-copy"}]")
      throw new Exception(s"Unable to execute the scroll query on index [${"dev-openemploi-offer-pe-copy"}]")
    }
  }

  def firstRequester = {
    Await.result(IndexClient.scrollQuery(Seq(idxName), Some("3m"), 500, "dateCreation"), Duration.Inf)
  }

  def requesterHelper(scrollID: Option[String], eoffers: ListBuffer[ESEoffer], size: Long): ListBuffer[ESEoffer] = {
    if (scrollID.isDefined & eoffers.size < size) {

      val scrollResponse = Await.result(IndexClient.searchScroll(scrollID.get, None), Duration.Inf)
      if (scrollResponse.isSuccess) {
        val newScrollID: Option[String] = scrollResponse.result.scrollId
        scrollResponse.result.hits.hits.map { hit =>
          val json = Json.parse(hit.sourceAsString)
          val eoffer = json.as[EOffer]
          val esoffer = ESEoffer(hit.id, eoffer)
          eoffers.append(esoffer)
        }
        requesterHelper(newScrollID, eoffers, size)
      }
      else {
        eoffers
      }
    }
    else {
      eoffers
    }
  }

  def zoneEmplois(geoLoc: Option[Geoloc], departement: Seq[(String, List[GeoPoint])]) = {
    if (geoLoc.isDefined) {
      //println(geoLoc)
      val point = GeoPoint(geoLoc.get.lat, geoLoc.get.lon)
      val uris = departement.filter(couple => PolygonUtils.isPointInPolygon(couple._2, point)).map(couple => couple._1)
      if (uris.size > 0) Some(uris.last) else None
    }
    else None
  }

  def salary(sl: Option[String]) = {
    if (sl.isDefined) {
      Some(sl.get.trim.toInt)
    }
    else None
  }

  var i = 0

  def dateMS(dateLong : Option[Long]) = {
    if (dateLong.isDefined) {
     Some((dateLong.get.toString + "000").toLong)
    }
    else None
  }

  it should "get all offer" in {


    IndexClient.init()
    //val del = IndexClient.deleteIndex("dev-openemploi-offer-pe")
    //Await.result(del, Duration.Inf)
    rdfExtracter.init
    val zoneGeoWkts: Seq[(String, List[GeoPoint])] = rdfExtracter.zoneGeoWkt

    //zoneGeoWkts.foreach(zoneGeoWkt => println(zoneGeoWkt._1 + "  " + zoneGeoWkt._2))

    val eoffers = requester
    println(eoffers.size)
    val token = perrequest.refreshPEToken
    val writer = new FileWriter("/Users/prlherisson/Documents/mnemotix/projets/open-emploi/romeUri.txt", true)
    eoffers.map { offer =>
      //val olddateCreation = offer.eoOffer.dateCreation
      //val olddateActualisation = offer.eoOffer.dateActualisation

      //val newOffer = offer.eoOffer.copy(dateActualisation = dateMS(olddateActualisation), dateCreation = dateMS(olddateCreation))

      //i = i + 1
     // val d: Option[Geoloc] = offer.eoOffer.geoloc
      //val newze = zoneEmplois(offer.eoOffer.geoloc, zoneGeoWkts)
      //val newOffer = offer.eoOffer.copy(salaire = OfferConverter.salaireMatcher(offer.eoOffer.salaire))


       //val newOffer = offer.eoOffer.copy(zoneEmploi = newze)
      //insert
      /*
     Await.result(IndexClient.insert(ConnectorConfig.peofferName, ESIndexable(
        offer.esId, Some(Json.toJson(newOffer).as[JsObject])
      )), Duration.Inf)



*/
      //update


/*
      Await.result(IndexClient.updateById(idxName, ESIndexable(
        offer.esId, Some(Json.toJson(newOffer).as[JsObject])
      )), Duration.Inf)
*/

      //println(offer.esId)
      // val re =perrequest.offer(offer.esId, token)
      //println(re.toString())

    println(s"ID OFFER ${offer.esId}  - ${offer.eoOffer.description.get  } - DATE CREATION ${offer.eoOffer.dateCreation} - SALAIRE ${offer.eoOffer.salaire} - ZONE EMPLOI ${offer.eoOffer.zoneEmploi}")
      //writer.write(s"${offer.esId}, ${offer.eoOffer.occupation.getOrElse("")}\n")
    }
  }
}
/*
val newRange = Range(startingPoint.toString, (startingPoint + by).toString)
        val newCriterias = criterias.copy(range = Some(newRange))
 */