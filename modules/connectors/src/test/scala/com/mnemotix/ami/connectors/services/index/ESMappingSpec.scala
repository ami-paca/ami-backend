package com.mnemotix.ami.connectors.services.index

import com.mnemotix.synaptix.index.elasticsearch.models.ESMappingDefinitions
import com.sksamuel.elastic4s.requests.mappings.MappingDefinition
import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESMappingSpec extends FlatSpec with Matchers{
  it should "create an mapping for pe offer" in {
    val mapping: ESMappingDefinitions = ESMapping.mappnPEOfferDef
    val res: MappingDefinition = mapping.toMappingDefinition()
    println(res.toString)
  }

  it should "create an mapping for pe companies" ignore {
    val mapping = ESMapping.mappnCompanyDef
    val res: MappingDefinition = mapping.toMappingDefinition()
    println(res.toString)
  }

  it should "create an mapping for fiche metier" ignore {
    //val mapping = ESMapping.mappnFicherMetierDef
    val mapping = ESMapping.mappnFicherMetierDef
    val res = mapping.toMappingDefinition()
    println(res.toString)
  }

}