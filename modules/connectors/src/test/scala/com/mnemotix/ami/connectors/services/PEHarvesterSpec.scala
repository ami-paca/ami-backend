package com.mnemotix.ami.connectors.services

import com.mnemotix.ami.connectors.ConnectorSpec
import com.mnemotix.ami.connectors.model.{PEOfferSearchCriteria, Range}
import com.mnemotix.ami.connectors.services.rest.{PERequest, RequestParams}
import com.mnemotix.ami.connectors.test.CriteriasPE

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class PEHarvesterSpec extends ConnectorSpec{
  val peHarvester = new PEHarvester()

  it should "get the number of returned result" ignore  {
    lazy val perRequest = new PERequest(apiClient)
    val range = Range("0", "100")
    val criterias = PEOfferSearchCriteria(Some(range), Some("H1504"), Some("H1504") )
    val refreshPEToken = perRequest.refreshPEToken
    val number = peHarvester.getContentRange(RequestParams.peOffercriteria(criterias), refreshPEToken)
    println(number)
    number should be > 0
  }

  it should "get harvester" in {
    peHarvester.peHarvester
  }
}

/*

 */