package com.mnemotix.ami.connectors.services

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpHeader
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import com.mnemotix.ami.connectors.helpers.{ConnectorConfig, DateFormatUtil, PEConfig}
import com.mnemotix.ami.connectors.model.{OffresEmploiV2, PEOffer, PEOfferSearchCriteria, Range}
import com.mnemotix.ami.connectors.services.rest.{PERequest, RequestParams}
import com.mnemotix.ami.connectors.services.utils.OfferConverter
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.indexes.IndexResponse
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsObject, Json}

import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// todo add origineOffre: Option[OrigineOffre]

class PEHarvester extends LazyLogging {
  implicit val system = ActorSystem()
  implicit val ec = ExecutionContext.global
  implicit val materializer = ActorMaterializer()

  lazy val esIndexer = new ESIndexer
  lazy val apiClient = new ApiClient()
  lazy val peRequest = new PERequest(apiClient)
  lazy val rdfExtracter = new RDFExtracter()

  lazy val date = DateFormatUtil.getDate(DateFormatUtil.DATE_FORMAT)

  def getContentRange(criterias: Map[String, String], token: String): Int = {
    val resp = Await.result(apiClient.get(PEConfig.poleEmploiAccessPoint, "/v2/offres/search", token, criterias), Duration.Inf)
    resp.entity.dataBytes.runWith(Sink.ignore)
    val contentRange = resp.headers.filter(header => header.is("content-range"))

    if (contentRange.size > 0) {
      logger.debug(contentRange(0).value())
      val rs = contentRange.map(header => header.value()).mkString.split("/")(1).toInt
      logger.debug(rs.toString)
      rs
    }
    else 0
  }

  def requestPE(criterias: PEOfferSearchCriteria, by: Int, indexName: String): Future[Seq[Response[IndexResponse]]] = {
    @tailrec
    def requestHelper(criterias: PEOfferSearchCriteria, numberOfOffer: Int, startingPoint: Int, indexResponse: Future[Seq[Response[IndexResponse]]]): Future[Seq[Response[IndexResponse]]] = {
      logger.debug(s"Logger en français é à ç. number of offer = $numberOfOffer, starting point = $startingPoint")
      if (startingPoint < numberOfOffer && startingPoint < 999) {
        val newRange: Range = Range(startingPoint.toString, (startingPoint + by).toString)
        val newCriterias: PEOfferSearchCriteria = criterias.copy(range = Some(newRange))
        Thread.sleep(1000)
        //logger.info(s"I'm requesting - number of offer = ${numberOfOffer}, starting from = ${newCriterias.range.get.debutant}, to = ${newCriterias.range.get.dernier}")
        val refreshedPEToken = peRequest.refreshPEToken
        val offresEmploiV2: OffresEmploiV2 = peRequest.searchByCriteria(RequestParams.peOffercriteria(newCriterias), refreshedPEToken)
        val futResponses: Future[Seq[Response[IndexResponse]]] = Future.sequence(indexPE(offresEmploiV2.resultats, indexName))
        val merged: Seq[Future[Seq[Response[IndexResponse]]]] = Seq(futResponses, indexResponse)
        val futMerged: Future[Seq[Response[IndexResponse]]] = Future.sequence(merged) map {_.flatten}
        requestHelper(newCriterias, numberOfOffer, startingPoint + by, futMerged)
      }
      else {
        //logger.info(s"My last requesting - number of offer = ${numberOfOffer}, starting point = ${startingPoint}")
        indexResponse
      }
    }
    val refreshedPEToken = peRequest.refreshPEToken
    val numberOfOffer = getContentRange(RequestParams.peOffercriteria(criterias), refreshedPEToken)
    var indexResponses: mutable.Seq[Response[IndexResponse]] = new ListBuffer[Response[IndexResponse]]()
    requestHelper(criterias, numberOfOffer, 0, Future(indexResponses))
  }

  def indexPE(offers: Option[List[PEOffer]], indexName: String): Seq[Future[Response[IndexResponse]]] = {
    val zoneGeoWkts = rdfExtracter.zoneGeoWkt

    if (offers.isDefined && offers.get.size > 0) {
      offers.get.filter(_.id.isDefined).map { offer =>
        val oe = OfferConverter.peOfferToOffer(offer, Some(date), zoneGeoWkts)
        val json = Json.toJson(oe)
        println(json.toString())
        esIndexer.insertDocument(indexName, offer.id.get, json.as[JsObject])
      }
    }
    else new ListBuffer[Future[Response[IndexResponse]]]().toSeq
  }

  def criterias: Array[PEOfferSearchCriteria] = {
    val romes: Array[String] = ConnectorConfig.romesc.split(" ")
    val region = ConnectorConfig.regionsc
    romes.map(rome => PEOfferSearchCriteria(Some(Range("0", "100")), Some(rome), Some(region)))
  }

  def peHarvester = {

    esIndexer.init
    rdfExtracter.init
    criterias.map { criteria =>
      val futResponse = requestPE(criteria, 120, ConnectorConfig.peofferName)
      futResponse.onComplete {
        case Success(_) => logger.info("P.E offer are indexed / Les offres P.E ont été indexées.")
        case Failure(err) => logger.error("There was a problem while requesting the API.", Some(err))
      }
      val response = Await.result(futResponse, Duration.Inf)
    }
  }

  def shutdown: Unit = {
  }
}