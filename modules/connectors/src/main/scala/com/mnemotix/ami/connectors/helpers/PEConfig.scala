package com.mnemotix.ami.connectors.helpers

import com.typesafe.config.ConfigFactory

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object PEConfig {
  lazy val conf = Option(ConfigFactory.load().getConfig("pe")).getOrElse(ConfigFactory.empty())
  lazy val accessPointUri = conf.getString("api.access.point.uri")
  lazy val poleEmploiAccessPoint = conf.getString("api.base.poleemploi.uri")
  lazy val labonneboiteAccessPointUri = conf.getString("api.labonneboite.access.point.uri")
  lazy val oAuthUri = conf.getString("api.oauth.uri")
  lazy val clientId = conf.getString("api.client.id")
  lazy val clientSecret = conf.getString("api.client.secret")
  lazy val responseTimeout = Option(conf.getInt("api.response.timeout")).getOrElse(10000)
}