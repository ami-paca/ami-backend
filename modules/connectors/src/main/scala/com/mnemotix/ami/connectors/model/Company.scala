package com.mnemotix.ami.connectors.model

import play.api.libs.json.Json

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

case class Companies(
                      address: Option[String],
                      alternance: Option[Boolean],
                      boosted: Option[Boolean],
                      city: Option[String],
                      contact_mode: Option[String],
                      distance: Option[Double],
                      headcount_text: Option[String],
                      lat: Option[Double],
                      lon: Option[Double],
                      matched_rome_code: Option[String],
                      matched_rome_label: Option[String],
                      matched_rome_slug: Option[String],
                      naf: Option[String],
                      naf_text: Option[String],
                      name: Option[String],
                      siret: Option[String],
                      social_network: Option[String],
                      stars: Option[Double],
                      url: Option[String],
                      website: Option[String]
                    )

object Companies extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Companies]
}

case class CompanyResult(companies: Option[List[Companies]],
                         companies_count: Option[Double],
                         rome_code: Option[String],
                         rome_label: Option[String],
                         url: Option[String])

object CompanyResult extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[CompanyResult]
}


case class CompanyCount(companies_count: Option[Double],
                        rome_code: Option[String],
                        rome_label: Option[String],
                        url: Option[String])

object CompanyCount extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[CompanyCount]
}


case class Naf(
                code: Option[String],
                count: Option[Double],
                label: Option[String]
              )
object Naf extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Naf]
}

case class Headcount(
                      big: Option[Double],
                      small: Option[Double]
                    )
object Headcount extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Headcount]
}

case class Distance(
                     france: Option[Double],
                     less_100_km: Option[Double],
                     less_10_km: Option[Double],
                     less_30_km: Option[Double],
                     less_50_km: Option[Double]
                   )
object Distance extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Distance]
}

case class Contract(
                     alternance: Option[Double],
                     dpae: Option[Double]
                   )

object Contract extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Contract]
}

case class Filters(
                    contract: Option[Contract],
                    distance: Option[Distance],
                    headcount: Option[Headcount],
                    naf: Option[List[Naf]]
                  )
object Filters extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Filters]
}

case class CompanyFilter(filters: Option[Filters],
                         rome_code: Option[String],
                         rome_label: Option[String])

object CompanyFilter extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[CompanyFilter]
}