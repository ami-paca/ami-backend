package com.mnemotix.ami.connectors.helpers

import java.net.URI

import com.mnemotix.synaptix.core.utils.CryptoUtils

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
object RomeUriFactory {

  val openEmploi =  "http://ontology.datasud.fr/openemploi/data/"

  // romeCode
  def skosRomeURI(skos: String): URI = {
    new URI(s"${openEmploi}occupation/${CryptoUtils.md5sum(skos)}")
  }

  // occupation
  def occupationURI(codeRome: String): URI = {
    new URI(s"${openEmploi}occupation/${CryptoUtils.md5sum(codeRome)}")
  }

  // competences.code
  def skillURI(skillCode: String): URI = {
    new URI(s"${openEmploi}skill/${CryptoUtils.md5sum(skillCode)}")
  }

  def envWorkURI(envWorkId: String): URI = {
    new URI(s"${openEmploi}envWork/${CryptoUtils.md5sum(envWorkId)}")
  }
}