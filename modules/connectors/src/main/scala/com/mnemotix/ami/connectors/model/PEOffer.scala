package com.mnemotix.ami.connectors.model

import java.util.Date

import play.api.libs.functional.syntax._
import play.api.libs.json.{Json, _}




/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

case class Access(scope: Option[String], expires_in: Option[Int], token_type: Option[String], access_token: Option[String])

case class StandardResponse(code: Option[String], libelle: Option[String])

case class Communes(code: Option[String], libelle: Option[String], codePostal: Option[String], codeDepartement: Option[String])


case class Partenaires(nom: Option[String], url: Option[String], logo: Option[String])

object Partenaires  {
  implicit lazy val format = Json.format[Partenaires]
}

case class OrigineOffre(origine: Option[String], urlOrigine: Option[String], partenaires: Option[List[Partenaires]])

object OrigineOffre  {
  implicit lazy val format = Json.format[OrigineOffre]
}

case class QualitesProfessionnelles(
                                     description: Option[String],
                                     libelle: Option[String]
                                   )

object QualitesProfessionnelles  {
  implicit lazy val format = Json.format[QualitesProfessionnelles]
}

case class Agence(
                   telephone: Option[String],
                   courriel: Option[String]
                 )

object Agence {
  implicit lazy val format = Json.format[Agence]
}

case class Contact(
                    nom: Option[String],
                    coordonnees1: Option[String],
                    coordonnees2: Option[String],
                    coordonnees3: Option[String],
                    telephone: Option[String],
                    courriel: Option[String],
                    commentaire: Option[String],
                    urlRecruteur: Option[String],
                    urlPostulation: Option[String]
                  )

object Contact {
  implicit lazy val format = Json.format[Contact]
}

case class Salaire(
                    libelle: Option[String],
                    commentaire: Option[String],
                    complement1: Option[String],
                    complement2: Option[String]
                  )

object Salaire {
  implicit lazy val format = Json.format[Salaire]
}

case class Competences1(
                        code: Option[String],
                        libelle: Option[String],
                        exigence: Option[String]
                      )

object Competences1  {
  implicit lazy val format = Json.format[Competences1]
}

case class Permis(
                   libelle: Option[String],
                   exigence: Option[String]
                 )

object Permis {
  implicit lazy val format = Json.format[Permis]
}

case class Langues(
                    libelle: Option[String],
                    exigence: Option[String]
                  )

object Langues {
  implicit lazy val format = Json.format[Langues]
}

case class Formations(
                       codeFormation: Option[String],
                       domaineLibelle: Option[String],
                       niveauLibelle: Option[String],
                       commentaire: Option[String],
                       exigence: Option[String]
                     )

object Formations {
  implicit lazy val format = Json.format[Formations]
}


case class Entreprise(
                       nom: Option[String],
                       description: Option[String],
                       logo: Option[String],
                       url: Option[String]
                     )

object Entreprise  {
  implicit lazy val format = Json.format[Entreprise]
}

case class LieuTravail(
                        libelle: Option[String],
                        latitude: Option[Double],
                        longitude: Option[Double],
                        codePostal: Option[String],
                        commune: Option[String]
                      )

object LieuTravail {
  implicit lazy val format = Json.format[LieuTravail]
}

case class PEOffer(
                    id: Option[String],
                    intitule: Option[String],
                    description: Option[String],
                    dateCreation: Option[Date],
                    dateActualisation: Option[Date],
                    lieuTravail: Option[LieuTravail],
                    romeCode: Option[String],
                    romeLibelle: Option[String],
                    appellationLibelle: Option[String],
                    entreprise: Option[Entreprise],
                    typeContrat: Option[String],
                    typeContratLibelle: Option[String],
                    natureContrat: Option[String],
                    experienceExige: Option[String],
                    experienceLibelle: Option[String],
                    experienceCommentaire: Option[String],
                    formations: Option[List[Formations]],
                    langues: Option[List[Langues]],
                    permis: Option[List[Permis]],
                    outilsBureautiques:  Option[List[String]],
                    competences: Option[List[Competences1]],
                    salaire: Option[Salaire],
                    dureeTravailLibelle: Option[String],
                    dureeTravailLibelleConverti: Option[String],
                    complementExercice: Option[String],
                    conditionExercice: Option[String],
                    alternance: Option[Boolean],
                    contact: Option[Contact],
                    agence: Option[Agence],
                    nombrePostes: Option[Int],
                    accessibleTH: Option[Boolean],
                    deplacementCode: Option[String],
                    deplacementLibelle: Option[String],
                    qualificationCode: Option[String],
                    qualificationLibelle: Option[String],
                    secteurActivite: Option[String],
                    secteurActiviteLibelle: Option[String],
                    qualitesProfessionnelles: Option[List[QualitesProfessionnelles]],
                    trancheEffectifEtab: Option[String],
                    origineOffre: Option[OrigineOffre]
                  )

object PEOffer {
  val reads1: Reads[(Option[String], Option[String], Option[String], Option[Date], Option[Date], Option[LieuTravail], Option[String], Option[String], Option[String], Option[Entreprise],
    Option[String], Option[String],Option[String], Option[String], Option[String], Option[String],  Option[List[Formations]], Option[List[Langues]], Option[List[Permis]], Option[List[String]], Option[List[Competences1]], Option[Salaire])] = (
    (__ \ "id").readNullable[String] and
      (__ \ "intitule").readNullable[String] and
      (__ \ "description").readNullable[String] and
      (__ \ "dateCreation").readNullable[Date] and
      (__ \ "dateActualisation").readNullable[Date] and
      (__ \ "lieuTravail").readNullable[LieuTravail] and
      (__ \ "romeCode").readNullable[String] and
      (__ \ "romeLibelle").readNullable[String] and
      (__ \ "appellationlibelle").readNullable[String] and
      (__ \ "entreprise").readNullable[Entreprise] and
      (__ \ "typeContrat").readNullable[String] and
      (__ \ "typeContratLibelle").readNullable[String] and
      (__ \ "natureContrat").readNullable[String] and
      (__ \ "experienceExige").readNullable[String] and
      (__ \ "experienceLibelle").readNullable[String] and
      (__ \ "experienceCommentaire").readNullable[String] and
      (__ \ "formations").readNullable[List[Formations]] and
      (__ \ "langues").readNullable[List[Langues]] and
      (__ \ "permis").readNullable[List[Permis]] and
      (__ \ "outilsBureautiques").readNullable[List[String]] and
      (__ \ "competences").readNullable[List[Competences1]] and
      (__ \ "salaire").readNullable[Salaire]
    ).tupled

  val reads2: Reads[(Option[String], Option[String], Option[String], Option[String], Option[Boolean], Option[Contact], Option[Agence], Option[Int], Option[Boolean], Option[String],
    Option[String], Option[String], Option[String], Option[String], Option[String], Option[List[QualitesProfessionnelles]], Option[String], Option[OrigineOffre])] = (
    (__ \ "dureeTravailLibelle").readNullable[String] and
      (__ \ "dureeTravailLibelleConverti").readNullable[String] and
      (__ \ "complementExercice").readNullable[String] and
      (__ \ "conditionExercice").readNullable[String] and
      (__ \ "alternance").readNullable[Boolean] and
      (__ \ "contact").readNullable[Contact] and
      (__ \ "agence").readNullable[Agence] and
      (__ \ "nombrePostes").readNullable[Int] and
      (__ \ "accessibleTH").readNullable[Boolean] and
      (__ \ "deplacementCode").readNullable[String] and
      (__ \ "deplacementLibelle").readNullable[String] and
      (__ \ "qualificationCode").readNullable[String] and
      (__ \ "qualificationLibelle").readNullable[String] and
      (__ \ "secteurActivite").readNullable[String] and
      (__ \ "secteurActiviteLibelle").readNullable[String] and
      (__ \ "qualitesProfessionnelles").readNullable[List[QualitesProfessionnelles]] and
      (__ \ "trancheEffectifEtab").readNullable[String] and
      (__ \ "origineOffre").readNullable[OrigineOffre]
    ).tupled

  val f: (
    (Option[String], Option[String], Option[String], Option[Date], Option[Date], Option[LieuTravail], Option[String], Option[String], Option[String], Option[Entreprise],
      Option[String], Option[String],Option[String], Option[String], Option[String], Option[String], Option[List[Formations]], Option[List[Langues]], Option[List[Permis]], Option[List[String]], Option[List[Competences1]], Option[Salaire]),
      (Option[String], Option[String], Option[String], Option[String], Option[Boolean], Option[Contact], Option[Agence], Option[Int], Option[Boolean], Option[String],
        Option[String], Option[String], Option[String], Option[String], Option[String], Option[List[QualitesProfessionnelles]], Option[String],  Option[OrigineOffre])
    ) => PEOffer = {
    case (
      (
        id,
        intitule,
        description,
        dateCreation,
        dateActualisation,
        lieuTravail,
        romeCode,
        romeLibelle,
        appellationlibelle,
        entreprise,
        typeContrat,
        typeContratLibelle,
        natureContrat,
        experienceExige,
        experienceLibelle,
        experienceCommentaire,
        formations,
        langues,
        permis,
        outilsBureautiques,
        competences,
        salaire
        ),
      (
        dureeTravailLibelle,
        dureeTravailLibelleConverti,
        complementExercice,
        conditionExercice,
        alternance,
        contact,
        agence,
        nombrePostes,
        accessibleTH,
        deplacementCode,
        deplacementLibelle,
        qualificationCode,
        qualificationLibelle,
        secteurActivite,
        secteurActiviteLibelle,
        qualitesProfessionnelles,
        trancheEffectifEtab,
        origineOffre
        )
      ) => PEOffer(
      id,
      intitule,
      description,
      dateCreation,
      dateActualisation,
      lieuTravail,
      romeCode,
      romeLibelle,
      appellationlibelle,
      entreprise,
      typeContrat,
      typeContratLibelle,
      natureContrat,
      experienceExige,
      experienceLibelle,
      experienceCommentaire,
      formations,
      langues,
      permis,
      outilsBureautiques,
      competences,
      salaire,
      dureeTravailLibelle,
      dureeTravailLibelleConverti,
      complementExercice,
      conditionExercice,
      alternance,
      contact,
      agence,
      nombrePostes,
      accessibleTH,
      deplacementCode,
      deplacementLibelle,
      qualificationCode,
      qualificationLibelle,
      secteurActivite,
      secteurActiviteLibelle,
      qualitesProfessionnelles,
      trancheEffectifEtab,
      origineOffre
    )
  }

  implicit val reads: Reads[PEOffer] = (reads1 and reads2) {
    f
  }

  implicit val writes: Writes[PEOffer] = new Writes[PEOffer] {
    override def writes(o: PEOffer): JsValue = Json.obj(
      "id" -> o.id,
      "intitule" -> o.intitule,
      "description" -> o.description,
      "dateCreation" -> o.dateCreation,
      "dateActualisation" -> o.dateActualisation,
      "lieuTravail" -> o.lieuTravail,
      "romeCode" -> o.romeCode,
      "romeLibelle" -> o.romeLibelle,
      "appellationlibelle" -> o.appellationLibelle,
      "entreprise" -> o.entreprise,
      "typeContrat" -> o.typeContrat,
      "typeContratLibelle" -> o.typeContratLibelle,
      "natureContrat" -> o.natureContrat,
      "experienceExige" -> o.experienceExige,
      "experienceLibelle" -> o.experienceLibelle,
      "experienceCommentaire" -> o.experienceCommentaire,
      "formations" -> o.formations,
      "langues" -> o.langues,
      "permis" -> o.permis,
      "outilsBureautiques" -> o.outilsBureautiques,
      "competences" -> o.competences,
      "salaire" -> o.salaire,
      "dureeTravailLibelle" -> o.dureeTravailLibelle,
      "dureeTravailLibelleConverti" -> o.dureeTravailLibelleConverti,
      "complementExercice" -> o.complementExercice,
      "conditionExercice" -> o.conditionExercice,
      "alternance" -> o.alternance,
      "contact" -> o.contact,
      "agence" -> o.agence,
      "nombrePostes" -> o.nombrePostes,
      "accessibleTH" -> o.accessibleTH,
      "deplacementCode" -> o.deplacementCode,
      "deplacementLibelle" -> o.deplacementLibelle,
      "qualificationCode" -> o.qualificationCode,
      "qualificationLibelle" -> o.qualificationLibelle,
      "secteurActivite" -> o.secteurActivite,
      "secteurActiviteLibelle" -> o.secteurActiviteLibelle,
      "qualitesProfessionnelles" -> o.qualitesProfessionnelles,
      "trancheEffectifEtab" -> o.trancheEffectifEtab,
      "origineOffre" -> o.origineOffre
    )
  }
}

case class Agrega(nbResultats: Option[String], valeurPossible: Option[String])

object Agrega {
  implicit lazy val format = Json.format[Agrega]
}

case class Agregation(agrega: Option[Seq[Agrega]], filtre: Option[String])

object Agregation {
  implicit lazy val format = Json.format[Agregation]
}

case class FiltresPossibles(agregation: Option[Seq[Agregation]])

object FiltresPossibles{
  implicit lazy val format = Json.format[FiltresPossibles]
}

case class OffresEmploiV2(filtresPossibles: Option[Seq[FiltresPossibles]], resultats: Option[List[PEOffer]])

object OffresEmploiV2{
  implicit lazy val format = Json.format[OffresEmploiV2]
}

case class IndexPEOfferV2(dateCollecte: Date, peOffer: PEOffer)

object IndexPEOfferV2{
  implicit lazy val format = Json.format[IndexPEOfferV2]
}