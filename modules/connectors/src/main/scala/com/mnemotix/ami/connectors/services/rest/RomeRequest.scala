package com.mnemotix.ami.connectors.services.rest

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpResponse
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import com.mnemotix.ami.connectors.helpers.PEConfig
import com.mnemotix.ami.connectors.model.{FicheMetier, Metier}
import com.mnemotix.ami.connectors.services.ApiClient
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsValue, Json}

import scala.collection.parallel.immutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object RomeRequest extends LazyLogging {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global

  lazy val peClient = new ApiClient()

  def refreshRomeToken: String = {
    peClient.tokenNomenclatureRome()
  }

  def metiers(token: String): Seq[Metier] = {
    val resp = Await.result(peClient.get(PEConfig.accessPointUri, "/rome/v1/metier", token), Duration.Inf)
    showStatus(resp)
    val futResponse = resp.entity.dataBytes.map(_.utf8String).map { res =>
      res
    }.runWith(Sink.seq)
    val response = Await.result(futResponse, Duration.Inf).mkString
    resp.headers.foreach(httpHeader => println(httpHeader.name() + " :" + httpHeader.value()))
    val json: JsValue = Json.parse(response)
    json.as[Seq[Metier]]
  }

  def metier(token: String, code: String): FicheMetier = {
    Thread.sleep(1000)
    val resp: HttpResponse = Await.result(peClient.get(PEConfig.accessPointUri, s"/rome/v1/metier/${code}", token), Duration.Inf)
    Thread.sleep(1000)
    showStatus(resp)

    if (resp.status.intValue() == 429) {
      val retryAfter: Int = resp.headers.filter(httpHeader => httpHeader.is("retry-after")).last.value().toInt * 1000
      resp.entity.dataBytes.runWith(Sink.ignore)
      Thread.sleep(retryAfter)
      metier(token, code)
    }

    else {
      val futResponse = resp.entity.dataBytes.map(_.utf8String).map { res =>
        res
      }.runWith(Sink.seq)
      val response = Await.result(futResponse, Duration.Inf).mkString
      val json: JsValue = Json.parse(response)
      json.as[FicheMetier]
    }
  }

  def showStatus(resp: HttpResponse): Unit = {
    resp.status.intValue() match {
      case 200 =>
      case 206 => logger.info(s"${Console.BLUE}Code ${resp.status.intValue()}")
      case 302 => logger.info(s"${Console.BLUE}Code ${resp.status.intValue()}")
      case 401 => logger.error(s"${Console.RED}Code ${resp.status.intValue()}/${resp.status.reason()}.${Console.RESET}")
      case 429 => {
        val retryAfter = resp.headers.filter(httpHeader => httpHeader.is("retry-after")).last.value().toInt * 1000
        logger.warn(s"${Console.RED}Code ${resp.status.value}/${resp.status.reason()}...Waiting for $retryAfter ms before retry.${Console.RESET}")
      }
      case 400 => {
        logger.error(s"GET ${PEConfig.accessPointUri}/v2/offres/search HTTP/1.1 ${Console.RED}Code ${resp.status.value}/${resp.status.reason()} ${Console.YELLOW}${Console.RESET}")
      }
      case _ =>
        logger.error(s"${Console.YELLOW}Code ${resp.status.value}/${resp.status.reason()}.${Console.RESET}")
    }
  }
}