package com.mnemotix.ami.connectors.services

import com.mnemotix.ami.connectors.services.job.{ESIndexerJob, LbbHarversxingJob, PEHarvesxingJob}
import com.mnemotix.ami.connectors.services.scheduler.CrontabExpressionHelper
import org.quartz.{CronScheduleBuilder, JobBuilder, JobDetail, Trigger, TriggerBuilder}
import org.quartz.impl.StdSchedulerFactory

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object JobRunner extends App {

  var lbbTask = new LbbHarversxingJob
  var peTask = new PEHarvesxingJob
  var esTask = new ESIndexerJob

  esTask.run()
  //lbbTask.run()
  peTask.run()

  val quartz = StdSchedulerFactory.getDefaultScheduler

 //val lbbJob = JobBuilder.newJob(classOf[LbbHarversxingJob]).withIdentity("LbbHarversxingJob", "AMI").build()
  val peJob = JobBuilder.newJob(classOf[PEHarvesxingJob]).withIdentity("PEHarvesxingJob","AMI").build()

  val trigger: Trigger = TriggerBuilder.newTrigger.withIdentity("Trigger", "MNB").withSchedule(CronScheduleBuilder.cronSchedule(CrontabExpressionHelper.EVERY_DAY_AT_MIDNIGHT)).build

  quartz.start()
  //quartz.scheduleJob(lbbJob, trigger)
  quartz.scheduleJob(peJob, trigger)
}