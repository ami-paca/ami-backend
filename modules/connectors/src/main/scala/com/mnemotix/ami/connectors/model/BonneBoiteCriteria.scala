package com.mnemotix.ami.connectors.model

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

case class BonneBoiteCriteria(commune_id: Option[String],
                              contract: Option[String],
                              latitude: Option[Float],
                              longitude: Option[Float],
                              distance: Option[Int],
                              rome_codes: Option[List[String]],
                              rome_codes_keyword_search: Option[String],
                              naf_codes: Option[List[String]],
                              headcount: Option[String],
                              page: Option[Int],
                              page_size: Option[Int],
                              sort: Option[String])