package com.mnemotix.ami.connectors.services

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ClosedShape}
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink, Source}
import com.mnemotix.ami.connectors.helpers.{ConnectorConfig, RomeConfig}
import com.mnemotix.ami.connectors.model.{FicheMetier, Metier, PEOffer}
import com.mnemotix.ami.connectors.services.rest.RomeRequest
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.indexes.IndexResponse
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RomeHarvester extends LazyLogging {
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global

  implicit val system = ActorSystem("RomeHarvester")
  implicit val materializer = ActorMaterializer()

  lazy val esIndexer = new ESIndexer
  lazy val topHeadSink = Sink.ignore

  var i = 0

  def romeHarvester: Future[Done] = {
    val romeToken = RomeRequest.refreshRomeToken
    val metiers: Seq[Metier] = RomeRequest.metiers(romeToken)
    logger.info(s"we retrieved ${metiers.size} metiers.")

    val source = Source(metiers.to[scala.collection.immutable.Seq])

    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
      import GraphDSL.Implicits._
      source ~> getFicheMetier ~> indexFicheMetier(ConnectorConfig.ficheMetierName) ~> sinkIndexResponse ~> topHS
        ClosedShape
    }).run()
  }

  def getFicheMetier: Flow[Metier, Seq[FicheMetier], NotUsed] = {
    Flow[Metier].filter(metier => metier.code.isDefined).grouped(15).map { metiers =>
      metiers.map { metier =>
        i = i + 1

        //logger.info(s"Requesting ${metier.code.get}")
        //Thread.sleep(1000)
        if (i % 20 == 0) {
          logger.info(s"Requesting ${i} fiche métier")
        }
        val romeToken = RomeRequest.refreshRomeToken
        RomeRequest.metier(romeToken, metier.code.get)
      }
    }
  }

  def indexFicheMetier(indexName: String): Flow[Seq[FicheMetier], Seq[Future[Response[IndexResponse]]], NotUsed] = {
    Flow[Seq[FicheMetier]].map(ficheMetiers => ficheMetiers.filter(_.code.isDefined).map { ficheMetier =>
      val json = Json.toJson(ficheMetier)
      esIndexer.insertDocument(indexName, ficheMetier.code.get, json.as[JsObject])
    })
  }

  def sinkIndexResponse: Flow[Seq[Future[Response[IndexResponse]]], Unit, NotUsed] = {
      Flow[Seq[Future[Response[IndexResponse]]]].map(indexResponses =>  Future.sequence(indexResponses)).map { responses =>
      val indexesResponses: Seq[Response[IndexResponse]] = Await.result(responses, Duration.Inf)
      val logging = indexesResponses.map(indexResponse => logger.info(s"status: ${indexResponse.status}; success: ${indexResponse.isSuccess}; failure: ${indexResponse.isError}"))
    }
  }
}