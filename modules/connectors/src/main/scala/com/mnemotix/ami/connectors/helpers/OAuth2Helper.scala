package com.mnemotix.ami.connectors.helpers

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{FormData, HttpMethods, HttpRequest, HttpResponse, RequestEntity}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import com.mnemotix.ami.connectors.model.Access
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.stream.scaladsl.Sink
import spray.json.DefaultJsonProtocol._

import scala.collection.immutable
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object OAuth2Helper extends LazyLogging {

  val clientId = PEConfig.clientId
  val clientSecret = PEConfig.clientSecret

  implicit val accessFormat = jsonFormat4(Access)

  def printMessage(response: HttpResponse)(implicit system: ActorSystem, mat: Materializer) = {
    val intvalue = response.status.intValue()
    val statusReason = response.status.reason()
    val futResponse: Future[immutable.Seq[String]] = response.entity.dataBytes.map(_.utf8String).map { res =>
      res
    }.runWith(Sink.seq)
    val re = Await.result(futResponse, Duration.Inf)

    logger.warn(s"${Console.RED}Code ${intvalue}/${statusReason}...With message. ${re.mkString}")
  }

  def requestAccessToken(url: String, scope: String, grantType: String = "client_credentials")(implicit system: ActorSystem, mat: Materializer): Future[Access] = {
    val part: RequestEntity = FormData(Map("grant_type" ->  grantType, "client_id" -> clientId, "client_secret" -> clientSecret, "scope" -> scope)).toEntity

    val responsefut  = Http().singleRequest(
      HttpRequest(
        method = HttpMethods.POST,
        uri = url,
      ).withEntity(part)
    )
    responsefut onComplete {
      case Success(r) =>
      case Failure(err) => logger.error("Problem while requesting access token", Some(err))
    }
    val response: HttpResponse = Await.result(responsefut, Duration.Inf)

    response.status.intValue() match {
      case 429 => {
        val retryAfter = response.headers.filter(httpHeader => httpHeader.is("retry-after")).last.value().toInt * 1000
        printMessage(response)
        response.entity.dataBytes.runWith(Sink.ignore)
        Thread.sleep(retryAfter)
        val access = Await.result(requestAccessToken(url, scope, "client_credentials"), Duration.Inf)
        Future(access)
      }
      case 401 => {
        printMessage(response)
        response.entity.dataBytes.runWith(Sink.ignore)
        Thread.sleep(10000)
        val access = Await.result(requestAccessToken(url, scope, "client_credentials"), Duration.Inf)
        Future(access)
      }
      case 400 => {
        printMessage(response)
        response.entity.dataBytes.runWith(Sink.ignore)
        Thread.sleep(60000 * 5) // wait for 5 minutes
        val access = Await.result(requestAccessToken(url, scope, "client_credentials"), Duration.Inf)
        Future(access)
      }
      case _ => {
        try {
          logger.info(s"Code ${response.status.intValue()}/${response.status.reason()}. Token access success")
          Unmarshal(response.entity).to[Access]
        }
        catch {
          case _: Throwable => {
            printMessage(response)
            Thread.sleep(60000 * 5)
            val access = Await.result(requestAccessToken(url, scope, "client_credentials"), Duration.Inf)
            Future(access)
          }
        }
      }
    }
  }
}