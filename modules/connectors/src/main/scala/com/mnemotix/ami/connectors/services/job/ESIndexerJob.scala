package com.mnemotix.ami.connectors.services.job

import com.mnemotix.ami.connectors.helpers.ConnectorConfig
import com.mnemotix.ami.connectors.services.ESIndexer
import com.mnemotix.ami.connectors.services.index.ESMapping
import com.typesafe.scalalogging.LazyLogging
import org.quartz.{Job, JobExecutionContext}

import scala.concurrent.ExecutionContext

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESIndexerJob extends Job with LazyLogging {

  implicit val executionContext = ExecutionContext.global
  val indexer = new ESIndexer()
  indexer.init

  override def execute(jobExecutionContext: JobExecutionContext): Unit = {
    run()
  }

  def run(): Unit = {
    val resPEOfferDef = indexer.createIndex(ConnectorConfig.peofferName, ESMapping.mappnPEOfferDef)
    val resMappnCompanyDef = indexer.createIndex(ConnectorConfig.companyName, ESMapping.mappnCompanyDef)
    val resFicherMetierDef = indexer.createIndex(ConnectorConfig.ficheMetierName, ESMapping.mappnFicherMetierDef)

    logger.info(s"creation of index ${resPEOfferDef._2} is success ${resPEOfferDef._1}")
    logger.info(s"creation of index ${resMappnCompanyDef._2} is success ${resMappnCompanyDef._1}")
    logger.info(s"creation of index ${resFicherMetierDef._2} is success ${resFicherMetierDef._1}")
  }
}