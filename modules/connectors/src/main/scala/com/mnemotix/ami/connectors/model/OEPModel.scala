/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.connectors.model

import play.api.libs.json.Json

case class Skill(uri: String, coreSkill: Option[Boolean], specificSkill: Option[Boolean], softSkill: Option[Boolean], expertSkill: Option[Boolean],
                 groupUri: Option[Seq[String]])

object Skill {
  implicit lazy val format = Json.format[Skill]
}

case class SkillGroup(uri: String, skills: Seq[Skill])

object SkillGroup {
  implicit lazy val format = Json.format[SkillGroup]
}