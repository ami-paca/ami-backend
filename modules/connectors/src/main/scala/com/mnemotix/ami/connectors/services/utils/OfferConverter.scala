package com.mnemotix.ami.connectors.services.utils

import java.util.Date

import com.mnemotix.ami.connectors.helpers.{DateFormatUtil, RomeUriFactory}
import com.mnemotix.ami.connectors.model.{Competences1, EOffer, Formations, Geoloc, LieuTravail, OffreInfo, PEOffer, Recogs, Salaire}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object OfferConverter {

  def peOfferToOffer(peOffer: PEOffer,  dateCollecte: Option[Date], departement: Seq[(String,List[GeoPoint])] ): EOffer = {
    new EOffer(peOffer.intitule, peOffer.description, dateAsLong(peOffer.dateActualisation), dateCollecte, dateAsLong(peOffer.dateCreation), peOffer.alternance,
      peOffer.experienceLibelle, peOffer.natureContrat, peOffer.typeContratLibelle, peOffer.qualificationLibelle,
      peOffer.secteurActiviteLibelle, peOffer.trancheEffectifEtab, peOffer.entreprise, geoloc(peOffer.lieuTravail), competencesUri(peOffer.competences.toList.flatten),
      if(peOffer.romeCode.isDefined) Some((RomeUriFactory.occupationURI(peOffer.romeCode.get).toString)) else None,
      formationsUri(peOffer.formations.toList.flatten),
      if (peOffer.origineOffre.isDefined) peOffer.origineOffre.get.urlOrigine else None,
      salaire(peOffer.salaire), zoneEmploi(peOffer.lieuTravail,departement), offreInfo(peOffer), recogs(peOffer))
  }

  def offreInfo(peOffer: PEOffer): Option[OffreInfo] = {
    if (peOffer.origineOffre.isDefined) {
      if (peOffer.origineOffre.get.origine.isDefined && peOffer.origineOffre.get.origine.get == "1") {
        Some(OffreInfo("pôle emploi", None))
      }
      else if (peOffer.origineOffre.get.origine.isDefined && peOffer.origineOffre.get.origine.get == "2") {
        val partenaires = {
          if (peOffer.origineOffre.get.partenaires.isDefined) {
            Some(peOffer.origineOffre.get.partenaires.get.filter(_.nom.isDefined).map(_.nom.get))
          }
          else None
        }
        Some(OffreInfo("partenaire", partenaires))
      }
      else None
    }
    else None
  }

  def recogs(peOffer: PEOffer): Option[Seq[Recogs]] = {
    None
  }

  def zoneEmploi(lieuTravail: Option[LieuTravail], departement: Seq[(String,List[GeoPoint])]): Option[String] = {
    if (lieuTravail.isDefined) {
      if (lieuTravail.get.latitude.isDefined && lieuTravail.get.longitude.isDefined) {
        val point = GeoPoint(lieuTravail.get.latitude.get, lieuTravail.get.longitude.get)
        val uris = departement.filter(couple => PolygonUtils.isPointInPolygon(couple._2, point)).map(couple => couple._1)
        if (uris.size > 0) Some(uris.last) else None
      }
      else None
    }
    else None
  }

  def dateAsLong(date: Option[Date]) = {
    if (date.isDefined) {
      val dateString = date.get.toInstant.toEpochMilli
      Some(dateString)
    }
    else None
  }

  def salaire(salaire: Option[Salaire]) = {
    if (salaire.isDefined) {
      salaireMatcher(salaire.get.libelle)
    }
    else None
  }

  def salaireMatcher(salaire: Option[String]) = {
    if (salaire.isDefined) {
      val r = "(Horaire|Annuel|Mensuel)\\sde\\s(\\d+),(\\d+)".r
      val matche = r.findFirstMatchIn(salaire.get)
      if (matche.isDefined) {
        val time = matche.get.group(1)
        val salary = matche.get.group(2)
        time match {
          case "Horaire" => if (!salary.isEmpty) {
            Some((salary.toInt * 35 * 4 * 12))
          }
             else None
          case "Annuel" => if (!salary.isEmpty) Some((salary.toInt)) else None
          case "Mensuel" => if (!salary.isEmpty) Some((salary.toInt* 12))  else None
          case _ => None
        }
      }
      else None
    }
    else None
  }

  def geoloc(lieuTravail: Option[LieuTravail]) = {
    if (lieuTravail.isDefined) {
      if (lieuTravail.get.latitude.isDefined && lieuTravail.get.longitude.isDefined) {
        Some(Geoloc(lieuTravail.get.latitude.get, lieuTravail.get.longitude.get))
      }
      else None
    }
    else None
  }

  def competencesUri(competences: List[Competences1]) = {
    if (competences.size > 0) {
      Some(competences.filter(_.code.isDefined).map { competence =>
        RomeUriFactory.skillURI(competence.code.get).toString
      })
    }
    else None
  }

  def formationsUri(formations: List[Formations]) = {
    if (formations.size > 0) {
      Some(formations.filter(_.codeFormation.isDefined).map(_.codeFormation.get))
    }
    else None
  }
}