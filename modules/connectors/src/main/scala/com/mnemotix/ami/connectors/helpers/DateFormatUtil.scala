package com.mnemotix.ami.connectors.helpers

import java.text.SimpleDateFormat
import java.time.{LocalDate, ZoneId}
import java.time.format.DateTimeFormatter
import java.util.Date

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object DateFormatUtil {

  lazy val DATE_FORMAT = "yyyyMMdd"

  def getDateAsString(d: Date, format: String): String = {
    val dateFormat = new SimpleDateFormat(format)
    dateFormat.format(d)
  }

  def isoToString(d: String) = {
    val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
    LocalDate.parse(d, formatter)
  }

  def isoAsString(d: LocalDate): String = {
    val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
    d.format(formatter)
  }

  def isoToLong(d: Date): Long = {
    val formatter: DateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
    LocalDate.ofInstant(d.toInstant(), ZoneId.systemDefault()).toEpochDay
  }

  def isoAsString(d: Date): String = {
    val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
    LocalDate.ofInstant(d.toInstant(), ZoneId.systemDefault()).format(formatter)
  }

  def getDateAsLong(d: String, format: String) = {
    val dateFormat = new SimpleDateFormat(format)
    dateFormat.parse(d).getTime
  }

  def convertStringToDate(s: String, format: String): Date = {
    val dateFormat = new SimpleDateFormat(format)
    dateFormat.parse(s)
  }

  def getDate(format: String): Date = {
    val date = new Date()
    val dateString = DateFormatUtil.getDateAsString(date, format)
    convertStringToDate(dateString,format)
  }

}