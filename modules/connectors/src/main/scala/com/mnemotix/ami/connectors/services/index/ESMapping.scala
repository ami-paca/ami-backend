package com.mnemotix.ami.connectors.services.index

import com.mnemotix.synaptix.index.elasticsearch.models.ESMappingDefinitions
import play.api.libs.json.Json


/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object ESMapping {

  lazy val mappnPEOfferDef: ESMappingDefinitions = ESMappingDefinitions(Json.parse(
      """{
        |      "properties": {
        |        "alternance": {
        |          "type": "boolean"
        |        },
        |        "competences": {
        |          "type": "keyword"
        |        },
        |        "dateActualisation": {
        |          "type": "date"
        |        },
        |        "dateCollecte": {
        |          "type": "date"
        |        },
        |        "dateCreation": {
        |          "type": "date"
        |        },
        |        "description": {
        |          "type": "text",
        |          "fields": {
        |            "keyword": {
        |              "type": "keyword",
        |              "ignore_above": 256
        |            }
        |          }
        |        },
        |        "entreprise": {
        |          "properties": {
        |            "description": {
        |              "type": "text",
        |              "fields": {
        |                "keyword": {
        |                  "type": "keyword",
        |                  "ignore_above": 256
        |                }
        |              }
        |            },
        |            "logo": {
        |              "type": "keyword"
        |            },
        |            "nom": {
        |              "type": "text",
        |              "fields": {
        |                "keyword": {
        |                  "type": "keyword",
        |                  "ignore_above": 256
        |                }
        |              }
        |            },
        |            "url": {
        |              "type": "keyword"
        |            }
        |          }
        |        },
        |        "experienceLibelle": {
        |          "type": "keyword"
        |        },
        |        "formations": {
        |          "type": "keyword"
        |        },
        |        "geoloc": {
        |          "type": "geo_point"
        |        },
        |        "intitule": {
        |          "type": "text",
        |          "fields": {
        |            "keyword": {
        |              "type": "keyword",
        |              "ignore_above": 256
        |            }
        |          }
        |        },
        |        "natureContrat": {
        |          "type": "keyword"
        |        },
        |        "occupation": {
        |          "type": "keyword"
        |        },
        |        "offreURL": {
        |          "type": "keyword"
        |        },
        |        "qualificationLibelle": {
        |          "type": "keyword"
        |        },
        |        "salaire": {
        |          "type": "integer"
        |        },
        |        "secteurActiviteLibelle": {
        |          "type": "keyword"
        |        },
        |        "trancheEffectifEtab": {
        |          "type": "keyword"
        |        },
        |        "typeContratLibelle": {
        |          "type": "keyword"
        |        },
        |        "zoneEmploi": {
        |          "type": "keyword"
        |        }
        |      }}""".stripMargin
  ))

  /*
  lazy val mappnPEOfferDef: ESMappingDefinitions = ESMappingDefinitions(Json.parse(
    """
      |{
      | "properties": {
      |    "dateCollecte": {
      |        "type": "date"
      |    },
      |    "peOffer" : {
      |    "properties": {
      |      "id": {
      |        "type": "keyword"
      |      },
      |      "intitule": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "description": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "dateCreation": {
      |        "type": "date"
      |      },
      |      "dateActualisation": {
      |        "type": "date"
      |      },
      |      "lieuTravail": {
      |         "properties": {
      |           "libelle": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "latitude": {
      |             "type": "double"
      |           },
      |           "longitude": {
      |             "type": "double"
      |           },
      |           "codePostal": {
      |             "type": "keyword"
      |           },
      |           "commune": {
      |             "type": "keyword"
      |           }
      |         }
      |      },
      |      "romeCode": {
      |        "type": "keyword"
      |      },
      |      "romeLibelle": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "appellationLibelle": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "entreprise": {
      |         "properties": {
      |           "nom": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "description": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "logo": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "url": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           }
      |         }
      |      },
      |      "typeContrat": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "typeContratLibelle": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "natureContrat": {
      |        "type": "keyword"
      |      },
      |      "experienceExige": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "experienceLibelle": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "experienceCommentaire": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "formations": {
      |         "type": "nested",
      |         "properties": {
      |           "codeFormation": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "domaineLibelle": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "niveauLibelle": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "commentaire": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "exigence": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           }
      |         }
      |      },
      |      "competences": {
      |         "type": "nested",
      |         "properties": {
      |           "code": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "libelle": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "exigence": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           }
      |         }
      |      },
      |      "outilsBureautiques": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "salaire": {
      |         "properties": {
      |           "libelle": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "commentaire": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "complement1": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "complement2": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           }
      |         }
      |      },
      |      "dureeTravailLibelle": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "dureeTravailLibelleConverti": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "complementExercice": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "conditionExercice": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "alternance": {
      |        "type": "boolean"
      |      },
      |      "nombrePostes": {
      |        "type": "integer"
      |      },
      |      "accessibleTH": {
      |        "type": "boolean"
      |      },
      |      "deplacementCode": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "deplacementLibelle": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "qualificationCode": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |
      |      "qualificationLibelle": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "secteurActivite": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "qualitesProfessionnelles": {
      |         "type": "nested",
      |         "properties": {
      |           "description": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "libelle": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           }
      |         }
      |      },
      |      "secteurActiviteLibelle": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "trancheEffectifEtab": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "origineOffre": {
      |         "properties": {
      |           "origine": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "urlOrigine": {
      |             "type": "text",
      |             "analyzer": "standard"
      |           },
      |           "partenaires": {
      |             "properties": {
      |               "nom": {
      |                 "type": "text",
      |                 "analyzer": "standard"
      |               },
      |               "url": {
      |                 "type": "text",
      |                 "analyzer": "standard"
      |               },
      |               "logo": {
      |                 "type": "text",
      |                 "analyzer": "standard"
      |               }
      |             }
      |           }
      |         }
      |      }
      |    }
      |  }
      | }
      |}
      |""".stripMargin
  ))

   */

  lazy val mappnCompanyDef: ESMappingDefinitions = ESMappingDefinitions(Json.parse(
    """
      |{
      |    "properties": {
      |      "address": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "alternance": {
      |        "type": "boolean"
      |      },
      |      "boosted": {
      |        "type": "boolean"
      |      },
      |      "city": {
      |       "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "contact_mode": {
      |         "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "distance": {
      |        "type": "double"
      |      },
      |      "headcount_text": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "lat": {
      |        "type": "double"
      |      },
      |      "lon": {
      |        "type": "double"
      |      },
      |      "matched_rome_code": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "matched_rome_label": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "matched_rome_slug": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "naf": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "naf_text": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "name": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "siret": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "social_network": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "stars": {
      |        "type": "double"
      |      },
      |      "url": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      },
      |      "website": {
      |        "type": "text",
      |        "analyzer": "standard"
      |      }
      |   }
      |}
      |""".stripMargin
  ))

  lazy val mappnFicherMetierDef: ESMappingDefinitions = ESMappingDefinitions(Json.parse(
    """|{
       |   "properties": {
       |       "code": {
       |         "type": "keyword"
       |       },
       |       "libelle": {
       |         "type": "text"
       |       },
       |       "definition": {
       |         "type": "text"
       |       },
       |       "acces": {
       |         "type": "text"
       |       },
       |       "condition": {
       |         "type": "text"
       |       },
       |       "riasecMajeur": {
       |         "type": "keyword"
       |       },
       |       "riasecMineur": {
       |         "type": "keyword"
       |       },
       |       "codeIsco": {
       |         "type": "keyword"
       |       },
       |       "particulier": {
       |         "type": "boolean"
       |       },
       |       "domaineProfessionnel": {
       |         "type": "nested",
       |           "properties": {
       |             "code": {
       |               "type": "keyword"
       |             },
       |             "libelle": {
       |               "type": "text"
       |             },
       |             "grandDomaine": {
       |               "type": "nested",
       |               "properties": {
       |                 "code": {
       |                   "type": "keyword"
       |                 }
       |               }
       |             }
       |           }
       |       },
       |       "appellations": {
       |         "type": "nested",
       |             "properties": {
       |               "code": {
       |                 "type": "keyword"
       |               },
       |               "libelle": {
       |                  "type": "text"
       |               },
       |               "libelleCourt": {
       |                  "type": "text"
       |                },
       |                "particulier": {
       |                  "type": "boolean"
       |                 }
       |               }
       |            },
       |        "competencesDeBase": {
       |           "type": "nested",
       |              "properties": {
       |                "code": {
       |                  "type": "keyword"
       |                },
       |                "libelle": {
       |                  "type": "text"
       |                },
       |                "noeudCompetence": {
       |                  "type": "nested",
       |                  "properties": {
       |                    "code": {
       |                      "type": "keyword"
       |                    },
       |                    "libelle": {
       |                      "type": "text"
       |                    },
       |                    "racineCompetence": {
       |                       "type": "nested",
       |                       "properties": {
       |                         "code": {
       |                           "type": "keyword"
       |                         },
       |                         "libelle": {
       |                           "type": "text"
       |                         }
       |                       }
       |                     }
       |                   }
       |                 },
       |                 "typeCompetence": {
       |                   "type": "keyword"
       |                 },
       |                 "riasecMineur": {
       |                   "type": "keyword"
       |                 },
       |                 "riasecMajeur": {
       |                   "type": "keyword"
       |                 }
       |               }
       |          },
       |
       |
       |      "mobilitesProchesVersMetiers": {
       |                 "type": "nested",
       |                 "properties": {
       |                   "metierCible": {
       |                     "properties": {
       |                       "typeCompetence": {
       |                         "type": "keyword"
       |                       },
       |                       "libelle": {
       |                         "type": "text"
       |                       }
       |                     }
       |                   }
       |                 }
       |          },
       |         "mobilitesEvolutionsVersMetiers": {
       |             "type": "nested",
       |             "properties": {
       |                  "metierCible": {
       |                     "type": "nested",
       |                     "properties": {
       |                       "typeCompetence": {
       |                         "type": "keyword"
       |                       },
       |                       "libelle": {
       |                         "type": "text"
       |                       }
       |                     }
       |                   }
       |                 }
       |               },
       |         "themes": {
       |          "type": "nested",
       |          "properties": {
       |            "code": {
       |              "type": "keyword"
       |             },
       |            "libelle": {
       |              "type": "text"
       |             }
       |           }
       |        },
       |        "environnementsTravail": {
       |           "type": "nested",
       |           "properties": {
       |               "typeCompetence": {
       |                  "type": "keyword"
       |                 },
       |                 "libelle": {
       |                     "type": "text"
       |                  },
       |                  "typeEnvironnementTravail": {
       |                     "type": "text"
       |          }
       |        }
       |      },
       |
       |       "groupesCompetencesSpecitifiques": {
       |               "type": "nested",
       |               "properties" : {
       |                 "competences": {
       |                   "type": "nested",
       |                   "properties" : {
       |                     "code": {
       |                       "type": "keyword"
       |                     },
       |                     "libelle" : {
       |                       "type": "text"
       |                    },
       |                     "noeudCompetence": {
       |                        "type": "nested",
       |                        "properties": {
       |                           "code": {
       |                              "type": "keyword"
       |                          },
       |                          "libelle": {
       |                               "type": "text"
       |                            },
       |                            "racineCompetence": {
       |                               "type": "nested",
       |                               "properties": {
       |                               "code": {
       |                                  "type": "keyword"
       |                                },
       |                                "libelle": {
       |                                  "type": "text"
       |                                }
       |                               }
       |                              }
       |                             }
       |                           },
       |                           "typeCompetence": {
       |                              "type": "keyword"
       |                            },
       |                           "riasecMineur": {
       |                             "type": "keyword"
       |                            },
       |                           "riasecMajeur": {
       |                             "type": "keyword"
       |                           }
       |                         }
       |                       }
       |                     }
       |                   }
       |   }
       |}
       |""".stripMargin
  ))
}