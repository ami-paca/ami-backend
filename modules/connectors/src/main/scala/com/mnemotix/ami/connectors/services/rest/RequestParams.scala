package com.mnemotix.ami.connectors.services.rest

import com.mnemotix.ami.connectors.model.{BonneBoiteCriteria, PEOfferSearchCriteria, Range}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object RequestParams {

  def getRange(range: Option[Range]): String = if (range.isDefined) s"${range.get.debutant}-${range.get.dernier}" else ""
  def getValues[A](list: Option[List[A]], range: Int): String = if(list.isDefined) list.take(range).mkString(",") else ""
  def getValues[A](list: Option[List[A]]): String = if(list.isDefined) list.get.mkString(",") else ""

  def peOffercriteria(pEOfferSearchCriteria: PEOfferSearchCriteria): Map[String, String] = {
    Map(
      "range" -> getRange(pEOfferSearchCriteria.range),
      "codeROME" -> pEOfferSearchCriteria.codeROME.getOrElse(""),
      "region" -> pEOfferSearchCriteria.region.getOrElse("")
    ).filter( couple => couple._2 != "")
  }

  def bonneBoiteCriteria(criteria: BonneBoiteCriteria): Map[String, String] = {
    Map(
      "commune_id" ->  s"${criteria.commune_id.getOrElse("")}",
      "contract" -> s"${criteria.contract.getOrElse("dpae")}",
      "latitude" -> s"${criteria.latitude.getOrElse("")}",
      "longitude" -> s"${criteria.longitude.getOrElse("")}",
      "distance" -> s"${criteria.distance.getOrElse(10)}",
      "rome_codes" -> getValues(criteria.rome_codes),
      "rome_codes_keyword_search" -> s"${criteria.rome_codes_keyword_search.getOrElse("")}",
      "naf_codes" -> getValues(criteria.naf_codes),
      "headcount" ->  s"${criteria.headcount.getOrElse("all")}",
      "page" ->  s"${criteria.page.getOrElse(1)}",
      "page_size" -> s"${criteria.page_size.getOrElse(20)}",
      "sort" -> s"${criteria.sort.getOrElse("score")}"
    ).filter( couple => couple._2 != "")
  }
}