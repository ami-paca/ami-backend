package com.mnemotix.ami.connectors.services.rest

import akka.actor.ActorSystem
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import com.mnemotix.ami.connectors.model.{Communes, StandardResponse}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.Query
import com.mnemotix.ami.connectors.services.ApiClient
import play.api.libs.json.{JsValue, Json, _}
import akka.http.scaladsl.client.RequestBuilding.Get
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink

import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.concurrent.duration.Duration
import com.mnemotix.ami.connectors.model._
import spray.json._
import com.mnemotix.ami.connectors.helpers.PEConfig
import akka.http.scaladsl.model.HttpResponse
import com.typesafe.scalalogging.LazyLogging

import scala.collection.immutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object LbbRequest extends LazyLogging {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global

  lazy val peClient = new ApiClient()

  def refreshLbbToken = {
    peClient.tokenLabonneBoite()
  }

  def recruitingCompanies(params: Map[String, String], token: String): CompanyResult = {
    val resp = Await.result(peClient.get(PEConfig.labonneboiteAccessPointUri, "/v1/company/", token, params), Duration.Inf)
    resp.entity.dataBytes.runWith(Sink.ignore)
    showStatus(resp)
    val url  = resp.headers.filter(header => header.is("location")).map(header => header.value()).mkString
    val respBonneBoite = Await.result(Http().singleRequest(Get(url)), Duration.Inf)
    showStatus(respBonneBoite)
    val futResponse: Future[immutable.Seq[String]] = respBonneBoite.entity.dataBytes.map(_.utf8String).map { res =>
      res
    }.runWith(Sink.seq)
    val response = Await.result(futResponse, Duration.Inf).mkString
    val json: JsValue = Json.parse(response)
    json.as[CompanyResult]
  }

  def countRecruitingCompanies(params: Map[String, String], token: String): CompanyCount = {
    val resp: HttpResponse = Await.result(peClient.get(PEConfig.labonneboiteAccessPointUri, "/v1/company/count/", token, params), Duration.Inf)
    resp.entity.dataBytes.runWith(Sink.ignore)
    showStatus(resp)
    val url  = resp.headers.filter(header => header.is("location")).map(header => header.value()).mkString
    val respBonneBoite = Await.result(Http().singleRequest(Get(url)), Duration.Inf)
    showStatus(respBonneBoite)
    val futResponse: Future[immutable.Seq[String]] = respBonneBoite.entity.dataBytes.map(_.utf8String).map { res =>
      res
    }.runWith(Sink.seq)
    val response = Await.result(futResponse, Duration.Inf).mkString
    println(response)
    val json: JsValue = Json.parse(response)
    json.as[CompanyCount]
  }

  def recruitingCompaniesFilter(params: Map[String, String], token: String): CompanyFilter = {
    val resp = Await.result(peClient.get(PEConfig.labonneboiteAccessPointUri, "/v1/filter/", token, params), Duration.Inf)
    showStatus(resp)
    val url  = resp.headers.filter(header => header.is("location")).map(header => header.value()).mkString
    val respBonneBoite: HttpResponse = Await.result(Http().singleRequest(Get(url)), Duration.Inf)
    showStatus(respBonneBoite)
    val futResponse: Future[immutable.Seq[String]] = respBonneBoite.entity.dataBytes.map(_.utf8String).map { res =>
      res
    }.runWith(Sink.seq)
    val response = Await.result(futResponse, Duration.Inf).mkString
    val json: JsValue = Json.parse(response)
    json.as[CompanyFilter]
  }

  def showStatus(resp: HttpResponse) = {
    resp.status.intValue() match {
      case 200 =>
      case 206 => logger.info(s"${Console.BLUE}Code ${resp.status.intValue()}")
      case 302 => logger.info(s"${Console.BLUE}Code ${resp.status.intValue()}")
      case 401 => logger.error(s"${Console.RED}Code ${resp.status.intValue()}/${resp.status.reason()}.${Console.RESET}")
      case 429 => {
        val retryAfter = resp.headers.filter(httpHeader => httpHeader.is("retry-after")).last.value().toInt * 1000
        logger.warn(s"${Console.RED}Code ${resp.status.value}/${resp.status.reason()}...Waiting for $retryAfter ms before retry.${Console.RESET}")
      }
      case 400 => {
        logger.error(s"GET ${PEConfig.accessPointUri}/v2/offres/search HTTP/1.1 ${Console.RED}Code ${resp.status.value}/${resp.status.reason()} ${Console.YELLOW}${Console.RESET}")
      }
      case _ =>
        logger.error(s"${Console.YELLOW}Code ${resp.status.value}/${resp.status.reason()}.${Console.RESET}")
    }
  }

}
