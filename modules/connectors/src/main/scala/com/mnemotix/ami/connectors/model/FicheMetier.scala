package com.mnemotix.ami.connectors.model

import play.api.libs.json.{Json, _}
import play.api.libs.functional.syntax._

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

case class MobilitesEvolutionsAppellationsVersAppellations(code: Option[String])

object MobilitesEvolutionsAppellationsVersAppellations {
  implicit lazy val format = Json.format[MobilitesEvolutionsAppellationsVersAppellations]
}

case class MobilitesProchesAppellationsVersAppellations(code: Option[String])

object MobilitesProchesAppellationsVersAppellations {
  implicit lazy val format = Json.format[MobilitesProchesAppellationsVersAppellations]
}

case class MobilitesEvolutionsAppellationsVersMetiers(code: Option[String])

object MobilitesEvolutionsAppellationsVersMetiers {
  implicit lazy val format = Json.format[MobilitesEvolutionsAppellationsVersMetiers]
}

case class MobilitesProchesAppellationsVersMetiers(code: Option[String])

object MobilitesProchesAppellationsVersMetiers {
  implicit lazy val format = Json.format[MobilitesProchesAppellationsVersMetiers]
}

case class MobilitesProchesVersAppellations(code: Option[String])

object MobilitesProchesVersAppellations {
  implicit lazy val format = Json.format[MobilitesProchesVersAppellations]
}

case class Themes(code: Option[String])

object Themes {
  implicit lazy val format = Json.format[Themes]
}

case class GrandDomaine(code: Option[String],
                        libelle: Option[String]
                       )

object GrandDomaine {
  implicit lazy val format = Json.format[GrandDomaine]
}

case class DomaineProfessionnel(code: Option[String],
                                libelle: Option[String],
                                grandDomaine: Option[GrandDomaine]
                               )

object DomaineProfessionnel {
  implicit lazy val format = Json.format[DomaineProfessionnel]
}

case class Metier(code: Option[String],
                  libelle: Option[String],
                  riasecMajeur: Option[String],
                  riasecMineur: Option[String],
                  codeIsco: Option[String],
                  particulier: Option[Boolean],
                  domaineProfessionnel: Option[DomaineProfessionnel]
                 )

object Metier{
  implicit lazy val format = Json.format[Metier]
}

case class Appellations(
                         code: Option[String],
                         libelle: Option[String],
                         libelleCourt: Option[String],
                         particulier: Option[Boolean]
                       )

object Appellations  {
  implicit lazy val format = Json.format[Appellations]
}

case class NoeudCompetence(
                            code: Option[String],
                            libelle: Option[String],
                            racineCompetence: Option[GrandDomaine]
                          )

object NoeudCompetence   {
  implicit lazy val format = Json.format[NoeudCompetence]
}

case class Competences(
                        code: Option[String],
                        libelle: Option[String],
                        noeudCompetence: Option[NoeudCompetence],
                        typeCompetence: Option[String],
                        riasecMineur: Option[String],
                        riasecMajeur: Option[String]
                      )

object Competences {
  implicit lazy val format = Json.format[Competences]
}

case class GroupesCompetencesSpecifiques(
                                          competences: Option[List[Competences]]
                                        )

object GroupesCompetencesSpecifiques  {
  implicit lazy val format = Json.format[GroupesCompetencesSpecifiques]
}

case class CompetencesDeBase(
                            code: Option[String],
                            libelle: Option[String],
                            noeudCompetence: NoeudCompetence,
                            typeCompetence: Option[String],
                            riasecMineur: Option[String],
                            riasecMajeur: Option[String]
                            )

object CompetencesDeBase {
  implicit lazy val format = Json.format[CompetencesDeBase]
}

case class EnvironnementsTravail(
                                  code: Option[String],
                                  libelle: Option[String],
                                  typeEnvironnementTravail: Option[String]
                                )

object EnvironnementsTravail {
  implicit lazy val format = Json.format[EnvironnementsTravail]
}

case class MobilitesProchesVersMetiers(
                                        metierCible: Option[GrandDomaine]
                                      )

object MobilitesProchesVersMetiers {
  implicit lazy val format = Json.format[MobilitesProchesVersMetiers]
}

case class AppellationCible(
                             code: Option[String],
                             libelle: Option[String],
                             libelleCourt: Option[String],
                             metier: Option[GrandDomaine]
                           )

object AppellationCible   {
  implicit lazy val format = Json.format[AppellationCible]
}

case class MobilitesEvolutionsVersAppellations(
                                                appellationCible: Option[AppellationCible]
                                              )

object MobilitesEvolutionsVersAppellations {
  implicit lazy val format = Json.format[MobilitesEvolutionsVersAppellations]
}

case class FicheMetier(code: Option[String],
                       libelle: Option[String],
                       definition: Option[String],
                       acces: Option[String],
                       condition: Option[String],
                       riasecMajeur: Option[String],
                       riasecMineur: Option[String],
                       codeIsco: Option[String],
                       particulier: Option[Boolean],
                       domaineProfessionnel: Option[DomaineProfessionnel],
                       appellations: Option[List[Appellations]],
                       competencesDeBase: Option[List[CompetencesDeBase]],
                       groupesCompetencesSpecifiques: Option[List[GroupesCompetencesSpecifiques]],
                       environnementsTravail: Option[List[EnvironnementsTravail]],
                       themes: Option[List[Themes]],
                       mobilitesProchesVersMetiers: Option[List[MobilitesProchesVersMetiers]],
                       mobilitesEvolutionsVersMetiers: Option[List[MobilitesProchesVersMetiers]],
                       mobilitesProchesVersAppellations: Option[List[MobilitesProchesVersAppellations]],
                       mobilitesEvolutionsVersAppellations: Option[List[MobilitesEvolutionsVersAppellations]],
                       mobilitesProchesAppellationsVersMetiers: Option[List[MobilitesProchesAppellationsVersMetiers]],
                       mobilitesEvolutionsAppellationsVersMetiers: Option[List[MobilitesEvolutionsAppellationsVersMetiers]],
                       mobilitesProchesAppellationsVersAppellations: Option[List[MobilitesProchesAppellationsVersAppellations]],
                       mobilitesEvolutionsAppellationsVersAppellations: Option[List[MobilitesEvolutionsAppellationsVersAppellations]]
                      )


object FicheMetier {
  val reads1: Reads[(Option[String],Option[String],Option[String],Option[String],Option[String],Option[String],Option[String],Option[String],Option[Boolean],
                     Option[DomaineProfessionnel],Option[List[Appellations]],Option[List[CompetencesDeBase]])] = (
    (__ \ "code").readNullable[String] and
      (__ \ "libelle").readNullable[String] and
      (__ \ "definition").readNullable[String] and
      (__ \ "acces").readNullable[String] and
      (__ \ "condition").readNullable[String] and
      (__ \ "riasecMajeur").readNullable[String] and
      (__ \ "riasecMineur").readNullable[String] and
      (__ \ "codeIsco").readNullable[String] and
      (__ \ "particulier").readNullable[Boolean] and
      (__ \ "domaineProfessionnel").readNullable[DomaineProfessionnel] and
      (__ \ "appellations").readNullable[List[Appellations]] and
      (__ \ "competencesDeBase").readNullable[List[CompetencesDeBase]]
    ).tupled

  val reads2: Reads[(Option[List[GroupesCompetencesSpecifiques]], Option[List[EnvironnementsTravail]], Option[List[Themes]], Option[List[MobilitesProchesVersMetiers]],
   Option[List[MobilitesProchesVersMetiers]], Option[List[MobilitesProchesVersAppellations]], Option[List[MobilitesEvolutionsVersAppellations]], Option[List[MobilitesProchesAppellationsVersMetiers]],
   Option[List[MobilitesEvolutionsAppellationsVersMetiers]], Option[List[MobilitesProchesAppellationsVersAppellations]], Option[List[MobilitesEvolutionsAppellationsVersAppellations]])] = (
    (__ \ "groupesCompetencesSpecifiques").readNullable[List[GroupesCompetencesSpecifiques]] and
      (__ \ "environnementsTravail").readNullable[List[EnvironnementsTravail]] and
      (__ \ "themes").readNullable[List[Themes]] and
      (__ \ "mobilitesProchesVersMetiers").readNullable[List[MobilitesProchesVersMetiers]] and
      (__ \ "mobilitesEvolutionsVersMetiers").readNullable[List[MobilitesProchesVersMetiers]] and
      (__ \ "mobilitesProchesVersAppellations").readNullable[List[MobilitesProchesVersAppellations]] and
      (__ \ "mobilitesEvolutionsVersAppellations").readNullable[List[MobilitesEvolutionsVersAppellations]] and
      (__ \ "mobilitesProchesAppellationsVersMetiers").readNullable[List[MobilitesProchesAppellationsVersMetiers]] and
      (__ \ "mobilitesEvolutionsAppellationsVersMetiers").readNullable[List[MobilitesEvolutionsAppellationsVersMetiers]] and
      (__ \ "mobilitesProchesAppellationsVersAppellations").readNullable[List[MobilitesProchesAppellationsVersAppellations]] and
      (__ \ "mobilitesEvolutionsAppellationsVersAppellations").readNullable[List[MobilitesEvolutionsAppellationsVersAppellations]]
    ).tupled

  val f: (
    (Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[Boolean], Option[DomaineProfessionnel], Option[List[Appellations]], Option[List[CompetencesDeBase]]),
    (Option[List[GroupesCompetencesSpecifiques]], Option[List[EnvironnementsTravail]], Option[List[Themes]], Option[List[MobilitesProchesVersMetiers]], Option[List[MobilitesProchesVersMetiers]],
      Option[List[MobilitesProchesVersAppellations]], Option[List[MobilitesEvolutionsVersAppellations]], Option[List[MobilitesProchesAppellationsVersMetiers]],
     Option[List[MobilitesEvolutionsAppellationsVersMetiers]], Option[List[MobilitesProchesAppellationsVersAppellations]], Option[List[MobilitesEvolutionsAppellationsVersAppellations]])
    ) => FicheMetier = {
    case (
      (
        code,
        libelle,
        definition,
        acces,
        condition,
        riasecMajeur,
        riasecMineur,
        codeIsco,
        particulier,
        domaineProfessionnel,
        appellations,
        competencesDeBase
        ),
        (
          groupesCompetencesSpecifiques,
          environnementsTravail,
          themes,
          mobilitesProchesVersMetiers,
          mobilitesEvolutionsVersMetiers,
          mobilitesProchesVersAppellations,
          mobilitesEvolutionsVersAppellations,
          mobilitesProchesAppellationsVersMetiers,
          mobilitesEvolutionsAppellationsVersMetiers,
          mobilitesProchesAppellationsVersAppellations,
          mobilitesEvolutionsAppellationsVersAppellations
        )
      ) => FicheMetier(
      code,
      libelle,
      definition,
      acces,
      condition,
      riasecMajeur,
      riasecMineur,
      codeIsco,
      particulier,
      domaineProfessionnel,
      appellations,
      competencesDeBase,
      groupesCompetencesSpecifiques,
      environnementsTravail,
      themes,
      mobilitesProchesVersMetiers,
      mobilitesEvolutionsVersMetiers,
      mobilitesProchesVersAppellations,
      mobilitesEvolutionsVersAppellations,
      mobilitesProchesAppellationsVersMetiers,
      mobilitesEvolutionsAppellationsVersMetiers,
      mobilitesProchesAppellationsVersAppellations,
      mobilitesEvolutionsAppellationsVersAppellations
    )
  }

  implicit val reads: Reads[FicheMetier] = (reads1 and reads2) {
    f
  }

  implicit val writes: Writes[FicheMetier] = new Writes[FicheMetier] {
    override def writes(o: FicheMetier): JsValue = Json.obj(
      "code" -> o.code,
      "libelle" -> o.libelle,
      "definition" -> o.definition,
      "acces" -> o.acces,
      "condition" -> o.condition,
      "riasecMajeur" -> o.riasecMajeur,
      "riasecMineur" -> o.riasecMineur,
      "codeIsco" -> o.codeIsco,
      "particulier" -> o.particulier,
      "domaineProfessionnel" -> o.domaineProfessionnel,
      "appellations" -> o.appellations,
      "competencesDeBase" -> o.competencesDeBase,
      "groupesCompetencesSpecifiques" -> o.groupesCompetencesSpecifiques,
      "environnementsTravail" -> o.environnementsTravail,
      "themes" -> o.themes,
      "mobilitesProchesVersMetiers" -> o.mobilitesProchesVersMetiers,
      "mobilitesEvolutionsVersMetiers" -> o.mobilitesEvolutionsVersMetiers,
      "mobilitesProchesVersAppellations" -> o.mobilitesProchesVersAppellations,
      "mobilitesEvolutionsVersAppellations" -> o.mobilitesEvolutionsVersAppellations,
      "mobilitesProchesAppellationsVersMetiers" -> o.mobilitesProchesAppellationsVersMetiers,
      "mobilitesEvolutionsAppellationsVersMetiers" -> o.mobilitesEvolutionsAppellationsVersMetiers,
      "mobilitesProchesAppellationsVersAppellations" -> o.mobilitesProchesAppellationsVersAppellations,
      "mobilitesEvolutionsAppellationsVersAppellations" -> o.mobilitesEvolutionsAppellationsVersAppellations
    )
  }
}