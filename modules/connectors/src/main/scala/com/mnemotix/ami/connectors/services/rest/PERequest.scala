package com.mnemotix.ami.connectors.services.rest

import akka.actor.ActorSystem
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import com.mnemotix.ami.connectors.model.{Communes, StandardResponse}
import com.mnemotix.ami.connectors.services.ApiClient
import play.api.libs.json.{JsValue, Json, _}
import akka.http.scaladsl.unmarshalling.{Unmarshal, Unmarshaller}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink

import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.concurrent.duration.Duration
import com.mnemotix.ami.connectors.model._
import spray.json._
import DefaultJsonProtocol._
import com.mnemotix.ami.connectors.helpers.PEConfig
import akka.http.scaladsl.model.HttpResponse
import com.typesafe.scalalogging.LazyLogging

import scala.collection.immutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class PERequest(peClient: ApiClient)(implicit val system: ActorSystem, materializer: ActorMaterializer, ec: ExecutionContextExecutor) extends LazyLogging {

  implicit val standardResponseFormat = jsonFormat2(StandardResponse.apply)
  implicit val communesResponseFormat = jsonFormat4(Communes.apply)

  def refreshPEToken = {
    peClient.refreshToken()
  }

  def getDomainesPro(token: String) = {
    val resp = Await.result(peClient.get(PEConfig.poleEmploiAccessPoint,"/v2/referentiel/domaines", peClient.refreshToken()), Duration.Inf)
    Unmarshal(resp).to[List[StandardResponse]]
  }

  def getAppellations(token: String): Future[List[StandardResponse]] = {
    val resp = Await.result(peClient.get(PEConfig.poleEmploiAccessPoint,"/v2/referentiel/appellations", peClient.refreshToken()), Duration.Inf)
    Unmarshal(resp).to[List[StandardResponse]]
  }

  def getMetiers(token: String): Future[List[StandardResponse]] = {
    val resp = Await.result(peClient.get(PEConfig.poleEmploiAccessPoint, "/v2/referentiel/metiers", token), Duration.Inf)
    Unmarshal(resp).to[List[StandardResponse]]
  }

  def getCommunes(token: String): Future[List[Communes]] = {
    val resp = Await.result(peClient.get(PEConfig.poleEmploiAccessPoint, "/v2/referentiel/communes", token), Duration.Inf)
    Unmarshal(resp).to[List[Communes]]
  }

  def getRegions(token: String): Future[List[StandardResponse]]  = {
    val resp = Await.result(peClient.get(PEConfig.poleEmploiAccessPoint,"/v2/referentiel/regions", token), Duration.Inf)
    Unmarshal(resp).to[List[StandardResponse]]
  }

  def getTypesContracts(token: String) = {
    val resp = Await.result(peClient.get(PEConfig.poleEmploiAccessPoint, "/v2/referentiel/typesContrats", token), Duration.Inf)
    Unmarshal(resp).to[List[StandardResponse]]
  }

  def offer(id: String, token: String):JsValue = {
    val resp: HttpResponse = Await.result(peClient.get(PEConfig.poleEmploiAccessPoint, s"/v2/offres/$id", token), Duration.Inf)
    showStatus(resp)
    if (resp.status.intValue() == 429) {
      val retryAfter: Int = resp.headers.filter(httpHeader => httpHeader.is("retry-after")).last.value().toInt * 1000
      resp.entity.dataBytes.runWith(Sink.ignore)
      Thread.sleep(retryAfter)
      offer(id, token)
    }

    else if (resp.status.intValue() == 400) {
      resp.entity.dataBytes.runWith(Sink.ignore)
      val newToken = peClient.refreshToken()
      Thread.sleep(5000)
      offer(id, newToken)
    }

    else if (resp.status.intValue() == 502) {
      resp.entity.dataBytes.runWith(Sink.ignore)

      Thread.sleep(10000)
      offer(id, token)
    }

    else if (resp.status.intValue() == 204) {
      resp.entity.dataBytes.runWith(Sink.ignore)

      Thread.sleep(1000)
      offer(id, token)
    }

    else {
      val futResponse: Future[immutable.Seq[String]] = resp.entity.dataBytes.map(_.utf8String).map { res =>
        res
      }.runWith(Sink.seq)
      val response = Await.result(futResponse, Duration.Inf).mkString
      val json: JsValue = Json.parse(response)
      json
    }
  }

  def searchByCriteria(criterias: Map[String, String], token: String): OffresEmploiV2 = {
    val resp: HttpResponse = Await.result(peClient.get(PEConfig.poleEmploiAccessPoint, "/v2/offres/search", token, criterias), Duration.Inf)
    showStatus(resp)

    if (resp.status.intValue() == 429) {
      val retryAfter: Int = resp.headers.filter(httpHeader => httpHeader.is("retry-after")).last.value().toInt * 1000
      resp.entity.dataBytes.runWith(Sink.ignore)
      Thread.sleep(retryAfter)
      searchByCriteria(criterias, token)
    }

    else if (resp.status.intValue() == 400) {
      resp.entity.dataBytes.runWith(Sink.ignore)

      Thread.sleep(5000)
      searchByCriteria(criterias, token)
    }

    else if (resp.status.intValue() == 502) {
      resp.entity.dataBytes.runWith(Sink.ignore)

      Thread.sleep(10000)
      searchByCriteria(criterias, token)
    }

    else if (resp.status.intValue() == 206) {
      logger.debug(resp.status.intValue().toString)
      val futResponse: Future[immutable.Seq[String]] = resp.entity.dataBytes.map(_.utf8String).map { res =>
        res
      }.runWith(Sink.seq)
      val response = Await.result(futResponse, Duration.Inf).mkString
      val json: JsValue = Json.parse(response)
      json.as[OffresEmploiV2]
    }

    else {
      logger.debug(resp.status.intValue().toString)
      val futResponse: Future[immutable.Seq[String]] = resp.entity.dataBytes.map(_.utf8String).map { res =>
        logger.info(res)
        res
      }.runWith(Sink.seq)
      val response = Await.result(futResponse, Duration.Inf).mkString
      val json: JsValue = Json.parse(response)
      json.as[OffresEmploiV2]
    }
  }

  def showStatus(resp: HttpResponse) = {
    resp.status.intValue() match {
      case 200 =>
      case 206 => logger.info(s"${Console.BLUE}Code ${resp.status.intValue()}")
      case 302 => logger.info(s"${Console.BLUE}Code ${resp.status.intValue()}")
      case 401 => logger.error(s"${Console.RED}Code ${resp.status.intValue()}/${resp.status.reason()}.${Console.RESET} - Body = ${Unmarshaller.stringUnmarshaller(resp.entity)}")
      case 429 => {
        val retryAfter = resp.headers.filter(httpHeader => httpHeader.is("retry-after")).last.value().toInt * 1000
        logger.warn(s"${Console.RED}Code ${resp.status.value}/${resp.status.reason()}...Waiting for $retryAfter ms before retry.${Console.RESET}")
      }
      case 400 => {
        logger.error(s"GET ${PEConfig.accessPointUri}/v2/offres/search HTTP/1.1 ${Console.RED}Code ${resp.status.value}/${resp.status.reason()} - Body Message = - Body = ${Unmarshaller.stringUnmarshaller(resp.entity)} ${Console.YELLOW}${Console.RESET}")
      }
      case _ =>
        logger.error(s"${Console.YELLOW}Code ${resp.status.value}/${resp.status.reason()}.${Console.RESET}")
    }
  }
}