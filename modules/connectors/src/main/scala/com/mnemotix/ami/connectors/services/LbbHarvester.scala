package com.mnemotix.ami.connectors.services

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.mnemotix.ami.connectors.helpers.ConnectorConfig
import com.mnemotix.ami.connectors.model.{BonneBoiteCriteria, Companies}
import com.mnemotix.ami.connectors.services.rest.{LbbRequest, PERequest, RequestParams}
import com.mnemotix.synaptix.core.utils.CryptoUtils
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.indexes.IndexResponse
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsObject, Json}

import scala.annotation.tailrec
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class LbbHarvester extends LazyLogging {

  implicit val system = ActorSystem()
  implicit val ec = ExecutionContext.global
  implicit val materializer = ActorMaterializer()

  lazy val esIndexer = new ESIndexer

  lazy val pacaDep = List("04", "05", "06", "13", "83", "84")

  lazy val peClient = new ApiClient()
  lazy val peRequest = new PERequest(peClient)

  def getContentRange(criterias: Map[String, String], token: String): Option[Double] = {
    val companyCount = LbbRequest.countRecruitingCompanies(criterias, token)
    companyCount.companies_count
  }

  def getListOfCommune(departements: List[String]) = {
    val token: String = peClient.refreshToken()
    val communes = Await.result(peRequest.getCommunes(token), Duration.Inf)
    communes.filter(_.codeDepartement.isDefined).filter(commune => pacaDep.contains(commune.codeDepartement.get))
  }

  def requestPE(criterias: BonneBoiteCriteria, indexName: String): Future[Seq[Response[IndexResponse]]] = {
    @tailrec
    def requestHelper(criterias: BonneBoiteCriteria, nbrOfCompany: Int, numberRequested: Int, pageNumber: Int): Future[Seq[Response[IndexResponse]]] = {
      if (numberRequested < nbrOfCompany) {
        val newCriterias: BonneBoiteCriteria = criterias.copy(page = Some(pageNumber))
        Thread.sleep(1000)
        val companies = LbbRequest.recruitingCompanies(RequestParams.bonneBoiteCriteria(newCriterias), LbbRequest.refreshLbbToken)
        val fut: Future[Seq[Response[IndexResponse]]] = Future.sequence(indexPE(companies.companies, indexName))
        val page_size = if (!newCriterias.page_size.isDefined) 20 else newCriterias.page_size.get
        requestHelper(newCriterias, nbrOfCompany, numberRequested + page_size, pageNumber + 1)
      }
      else {
        val newCriterias = criterias.copy(page = Some(pageNumber))
        Thread.sleep(1000)
        val companies = LbbRequest.recruitingCompanies(RequestParams.bonneBoiteCriteria(newCriterias), LbbRequest.refreshLbbToken)
        Future.sequence(indexPE(companies.companies, indexName))
      }
    }

    val nbrOfCompany = getContentRange(RequestParams.bonneBoiteCriteria(criterias), LbbRequest.refreshLbbToken)
    requestHelper(criterias, nbrOfCompany.get.toInt, 0, 1)
  }

  def indexPE(companies: Option[List[Companies]], indexName: String): Seq[Future[Response[IndexResponse]]] = {
    companies.toSeq.flatten.map { company =>
      val json = Json.toJson(company)
      esIndexer.insertDocument(indexName, CryptoUtils.md5sum(company.toString), json.as[JsObject])
    }
  }

  def lbbHarvester = {
    val communes = getListOfCommune(pacaDep)
    communes.map { commune =>
      val idCommune: Option[String] = commune.code
      val bbc = BonneBoiteCriteria(idCommune, None, None, None, Some(50), Some(List("I1604","H1504","H2601", "H2602","H2603","H2604","H2605","H1101","H1208","H1303","I1301","I1302","I1304","I1305","I1306","I1308","I1309","I1310","I1603")), None, None, None, None, None, None)
      val futResponse = requestPE(bbc, ConnectorConfig.companyName)
      futResponse.onComplete {
        case Success(_) =>
        case Failure(err) => println("There was a problem while requesting the API.", Some(err))
      }
      val response = Await.result(futResponse, Duration.Inf)
      response.foreach(res => println(s"The indexing of P.E offer is a succes : ${res.isSuccess}"))
    }
  }
}