package com.mnemotix.ami.connectors.services

import java.util.Date

import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.{ESIndexable, ESMappingDefinitions}
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.indexes.IndexResponse
import com.sksamuel.elastic4s.requests.indexes.admin.{DeleteIndexResponse, IndexExistsResponse}
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESIndexer(implicit ec: ExecutionContext) extends LazyLogging {
  def init = IndexClient.init()

  def createIndex(classeName: String, mapping: ESMappingDefinitions): (Boolean, String) = {
    val index: Response[IndexExistsResponse] = isExist(classeName)
    if (index.result.exists) (true, classeName)
    else {
      (Await.result(IndexClient.createIndex(classeName, mapping.toMappingDefinition()), Duration.Inf).isSuccess, classeName)
    }
  }

  private def isExist(indexName: String): Response[IndexExistsResponse] = {
    Await.result(IndexClient.indexExists(indexName), Duration.Inf)
  }

  def insertDocument(indexName: String, id: String, document: JsObject): Future[Response[IndexResponse]] = {
    //println(Json.prettyPrint(document))
    IndexClient.insert(indexName, id, document)
  }

  def bulkInsert(indexName: String, indexAbles: Seq[ESIndexable]) = {
    IndexClient.bulkInsert(indexName, indexAbles: _*)
  }

  def deleteIndex(indexName: String): Option[Future[Response[DeleteIndexResponse]]] = {
    val index: Response[IndexExistsResponse] = isExist(indexName)
    if (index.result.exists) {
      Some(IndexClient.deleteIndex(indexName))
    }
    else None
  }

  def shutdown = IndexClient.shutdown()
}