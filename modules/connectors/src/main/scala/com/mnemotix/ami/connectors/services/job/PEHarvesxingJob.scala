package com.mnemotix.ami.connectors.services.job

import com.mnemotix.ami.connectors.model.PEOfferSearchCriteria
import com.mnemotix.ami.connectors.services.PEHarvester
import com.typesafe.scalalogging.LazyLogging
import org.quartz.{Job, JobExecutionContext}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class PEHarvesxingJob extends Job with LazyLogging {

  override def execute(jobExecutionContext: JobExecutionContext) = {
    run()
  }

  def run() = {
    logger.info(s"PEHarvesxingJob is Starting")
    val peHarversing = new PEHarvester()
    peHarversing.peHarvester
    logger.info(s"PEHarvesxingJob is Done")
  }
}