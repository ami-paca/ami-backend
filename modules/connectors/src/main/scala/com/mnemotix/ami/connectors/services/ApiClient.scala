package com.mnemotix.ami.connectors.services

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, HttpResponse, Uri}
import com.mnemotix.ami.connectors.helpers.{OAuth2Helper, PEConfig}
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.headers.RawHeader
import com.typesafe.scalalogging.LazyLogging
import akka.stream.{ActorMaterializer, Materializer}
import com.mnemotix.synaptix.core.utils.DateUtils

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ApiClient(implicit val system: ActorSystem, materializer: ActorMaterializer, ec: ExecutionContextExecutor) extends LazyLogging {

  val accessPointUri = PEConfig.accessPointUri
  val clientId = PEConfig.clientId
  val clientSecret = PEConfig.clientSecret

  var startTime: Long = DateUtils.getTime()

  def get(accessPointUri: String, accessToken: String, params: Map[String, String]): Future[HttpResponse] = {
    val request = Http().singleRequest(
      HttpRequest(
        method = HttpMethods.GET,
        uri = Uri(s"$accessPointUri").withQuery(Query(params)),
        entity = params.toString()
      ).withHeaders(
        RawHeader("Authorization", s"Bearer $accessToken"),
        RawHeader("Content-Type", "application/json;charset=UTF-8"),
        RawHeader("Cache-Control","no-store"),
        RawHeader("Pragma", "no-cache"),
        RawHeader("Accept", "application/json")
      )
    )

    request.onComplete {
      case Success(_) =>
      case Failure(err) => logger.error("There was a problem while requesting the API.", Some(err))
    }
    request
  }

  def get(accessPointUri: String, resource: String, accessToken: String): Future[HttpResponse] = {
    val request = Http().singleRequest(
      HttpRequest(
        method = HttpMethods.GET,
        uri = s"$accessPointUri$resource",
      ).withHeaders(
        RawHeader("Authorization", s"Bearer $accessToken"),
        RawHeader("Content-Type", "application/json;charset=UTF-8"),
        RawHeader("Cache-Control","no-store"),
        RawHeader("Pragma", "no-cache"),
        RawHeader("Accept", "application/json")
      )
    )
    request.onComplete {
      case Success(_) =>
      case Failure(err) => logger.error("There was a problem while requesting the API.", Some(err))
    }
    request
  }

  def get(accessPointUri: String, resource: String, accessToken: String, params: Map[String, String]): Future[HttpResponse] = {

    val request = Http().singleRequest(
      HttpRequest(
        method = HttpMethods.GET,
        uri =  Uri(s"$accessPointUri$resource").withQuery(Query(params))
      ).withHeaders(
        RawHeader("Authorization", s"Bearer $accessToken"),
        RawHeader("Content-Type", "application/json;charset=UTF-8"),
        RawHeader("Cache-Control","no-store"),
        RawHeader("Pragma", "no-cache"),
        RawHeader("Accept", "application/json")
      )
    )


    request.onComplete {
      case Success(_) =>
      case Failure(err) => logger.error("There was a problem while requesting the API.", Some(err))
    }
    request
  }

  def refreshToken(): String = {
    val scope = s"application_${PEConfig.clientId} api_offresdemploiv2 o2dsoffre"
    val oAuthUri = PEConfig.oAuthUri
    val realm = "%2Fpartenaire"
    val access = Await.result(OAuth2Helper.requestAccessToken(s"$oAuthUri?realm=$realm", scope), Duration.Inf)
    if (access.access_token.isDefined) access.access_token.get
    else throw new Exception("Unable to get access token from OAuth API")
  }

  def tokenLabonneBoite(): String = {
    val scope = s"application_${PEConfig.clientId} api_labonneboitev1"
    val oAuthUri = PEConfig.oAuthUri
    val realm = "%2Fpartenaire"
    val access = Await.result(OAuth2Helper.requestAccessToken(s"$oAuthUri?realm=$realm", scope), Duration.Inf)
    if (access.access_token.isDefined) access.access_token.get
    else throw new Exception("Unable to get access token from OAuth API")
  }

  def tokenNomenclatureRome(): String = {
    val scope = s"application_${PEConfig.clientId} api_romev1 nomenclatureRome"
    val oAuthUri = PEConfig.oAuthUri
    val realm = "%2Fpartenaire"
    val access = Await.result(OAuth2Helper.requestAccessToken(s"$oAuthUri?realm=$realm", scope), Duration.Inf)
    if (access.access_token.isDefined) access.access_token.get
    else throw new Exception("Unable to get access token from OAuth API")
  }

  def showStatus(response: HttpResponse, uri: String, params: JsObject): Unit = {
    val time: String = s"${(DateUtils.getTime() - startTime)} ms"
    response.status.intValue() match {
      case 200 =>
      case 401 => logger.error(s"${Console.RED}Code ${response.status.intValue()}/${response.status.reason()}.${Console.RESET}")
      case 429 => {
        val retryAfter = response.headers.filter(httpHeader => httpHeader.is("retry-after")).last.value().toInt * 1000
        logger.warn(s"${Console.RED}Code ${response.status.value}/${response.status.reason()}...Waiting for $retryAfter ms before retry.${Console.RESET}")
      }
      case 400 => {
        logger.error(s"POST ${uri} HTTP/1.1 ${Console.RED}Code ${response.status.value}/${response.status.reason()} ${Console.YELLOW}${Json.toJson(params)}${Console.RESET}")
      }
      case _ =>
        logger.error(s"${Console.YELLOW}Code ${response.status.value}/${response.status.reason()}.${Console.RESET}")
    }
  }
}